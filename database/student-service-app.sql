-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: student_service_app
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrators`
--

DROP TABLE IF EXISTS `administrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrators` (
  `administrator_name` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `administrator_surname` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `administrator_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `administrator_password` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `administrator_email` varchar(70) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`administrator_username`),
  UNIQUE KEY `administrator_email_UNIQUE` (`administrator_email`),
  CONSTRAINT `fk_administrators_users1` FOREIGN KEY (`administrator_username`) REFERENCES `users` (`user_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrators`
--

LOCK TABLES `administrators` WRITE;
/*!40000 ALTER TABLE `administrators` DISABLE KEYS */;
INSERT INTO `administrators` VALUES ('Milan','Vujanic','admin','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918','admin@admin.com');
/*!40000 ALTER TABLE `administrators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_type`
--

DROP TABLE IF EXISTS `question_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_type` (
  `question_type_id` int(11) NOT NULL,
  `question_type_description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`question_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_type`
--

LOCK TABLES `question_type` WRITE;
/*!40000 ALTER TABLE `question_type` DISABLE KEYS */;
INSERT INTO `question_type` VALUES (1,'radio button'),(2,'check box'),(3,'write answer');
/*!40000 ALTER TABLE `question_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires`
--

DROP TABLE IF EXISTS `questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires` (
  `questionnaire_id` int(11) NOT NULL AUTO_INCREMENT,
  `teacher_username` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`questionnaire_id`),
  KEY `fk_questionnaires_teachers1_idx` (`teacher_username`),
  KEY `fk_questionnaires_subjects1_idx` (`subject_id`),
  CONSTRAINT `fk_questionnaires_subjects1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_questionnaires_teachers1` FOREIGN KEY (`teacher_username`) REFERENCES `teachers` (`teacher_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires`
--

LOCK TABLES `questionnaires` WRITE;
/*!40000 ALTER TABLE `questionnaires` DISABLE KEYS */;
INSERT INTO `questionnaires` VALUES (8,NULL,24);
/*!40000 ALTER TABLE `questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaire_id` int(11) DEFAULT NULL,
  `question_type_id` int(11) NOT NULL,
  `question_answer_rb` varchar(500) DEFAULT NULL,
  `question_answer_chkbx` varchar(500) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  `question_text` varchar(1000) NOT NULL,
  PRIMARY KEY (`question_id`,`question_type_id`,`question_text`),
  KEY `fk_questions_questionnaires1_idx` (`questionnaire_id`),
  KEY `fk_questions_question_type1_idx` (`question_type_id`),
  KEY `fk_questions_tests1_idx` (`test_id`),
  CONSTRAINT `fk_questions_question_type1` FOREIGN KEY (`question_type_id`) REFERENCES `question_type` (`question_type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_questions_questionnaires1` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaires` (`questionnaire_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_questions_tests1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (17,8,3,NULL,NULL,NULL,' Do sparrows only fly, or they move on the ground too? How?'),(18,8,1,'They are way too big, only like to see them on TV;Sounds that they make when communicating.. wonderful;They are faster swimmers than I thought, can reach speeds of over 30 km/h;I thought their life span is longer than 90 years;They can hold up 5000 kg of water and plankton in each mouthful.. what?',NULL,NULL,' What best describes your feelings about blue whales?'),(19,8,2,NULL,'Red;Orange;Brown;Yellow;Green',NULL,' What autumn leaf color do you think is the most widespread?'),(20,8,1,'Very slow;Slow;Average speed;Fast;Lightning speed',NULL,NULL,'How fast can a snail go, in your opinion? '),(21,NULL,3,NULL,NULL,5,' Elaborate on Pythagoras theorem?'),(22,NULL,1,'22;-95;548;32*;44',NULL,5,'If x = 10 and y = 22, x + y =?'),(23,NULL,2,NULL,'0;-0.9855;-1*;25;4458*',5,' Find the smallest and the greatest number'),(24,NULL,1,'r2*PI*;r - 7;r * 33;25;1',NULL,5,'What is the formula for circle area, where r stands for radius?');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `student_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `student_email` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `student_name` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `student_surname` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `student_password` varchar(128) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`student_username`),
  UNIQUE KEY `student_email_UNIQUE` (`student_email`),
  CONSTRAINT `fk_students_users1` FOREIGN KEY (`student_username`) REFERENCES `users` (`user_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES ('darko','darko@gmail.com','Darko','Jovanovic','fe79e0ac4b7db16d59a67be682f0c2e85e24241cccbb7a6303446e7105362bcc'),('vlado','vlado@gmail.com','Vlado','Rudic','5d82f85674b8a277ad8ea4af8baa1c5374d343ddfb89a578e5dd5a16b9dc4a39');
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_questionnaires`
--

DROP TABLE IF EXISTS `students_questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_questionnaires` (
  `student_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  PRIMARY KEY (`student_username`,`questionnaire_id`),
  KEY `fk_students_questionnaires_questionnaires1_idx` (`questionnaire_id`),
  KEY `fk_students_questionnaires_students1_idx` (`student_username`),
  CONSTRAINT `fk_students_questionnaires_questionnaires1` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaires` (`questionnaire_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_students_questionnaires_students1` FOREIGN KEY (`student_username`) REFERENCES `students` (`student_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_questionnaires`
--

LOCK TABLES `students_questionnaires` WRITE;
/*!40000 ALTER TABLE `students_questionnaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `students_questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_questions`
--

DROP TABLE IF EXISTS `students_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_questions` (
  `student_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `questionnaire_id` int(11) DEFAULT NULL,
  `student_question_answer_rb` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `student_question_answer_chkbx` varchar(500) COLLATE latin1_general_ci DEFAULT NULL,
  `student_question_answer_writeanswer` varchar(1000) COLLATE latin1_general_ci DEFAULT NULL,
  `question_id` int(11) NOT NULL,
  `test_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`student_username`,`question_id`),
  KEY `fk_students_questions_students_questionnaires1_idx` (`student_username`,`questionnaire_id`),
  KEY `fk_students_questions_questions1_idx` (`question_id`),
  KEY `fk_students_questions_students_tests1_idx` (`test_id`),
  CONSTRAINT `fk_students_questions_questions1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_students_questions_students_questionnaires1` FOREIGN KEY (`student_username`, `questionnaire_id`) REFERENCES `students_questionnaires` (`student_username`, `questionnaire_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_students_questions_students_tests1` FOREIGN KEY (`test_id`) REFERENCES `students_tests` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_questions`
--

LOCK TABLES `students_questions` WRITE;
/*!40000 ALTER TABLE `students_questions` DISABLE KEYS */;
INSERT INTO `students_questions` VALUES ('vlado',NULL,NULL,NULL,'a2 + b2 = c2',21,5),('vlado',NULL,'3',NULL,NULL,22,5),('vlado',NULL,NULL,'3;5',NULL,23,5),('vlado',NULL,'1',NULL,NULL,24,5);
/*!40000 ALTER TABLE `students_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_subjects`
--

DROP TABLE IF EXISTS `students_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_subjects` (
  `student_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`student_username`,`subject_id`),
  KEY `fk_students_subjects_subjects1_idx` (`subject_id`),
  KEY `fk_students_subjects_students1_idx` (`student_username`),
  CONSTRAINT `fk_students_subjects_students1` FOREIGN KEY (`student_username`) REFERENCES `students` (`student_username`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_students_subjects_subjects1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_subjects`
--

LOCK TABLES `students_subjects` WRITE;
/*!40000 ALTER TABLE `students_subjects` DISABLE KEYS */;
INSERT INTO `students_subjects` VALUES ('vlado',24),('darko',25);
/*!40000 ALTER TABLE `students_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_teachers`
--

DROP TABLE IF EXISTS `students_teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_teachers` (
  `student_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `teacher_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`student_username`,`teacher_username`),
  KEY `fk_students_teachers_teachers1_idx` (`teacher_username`),
  KEY `fk_students_teachers_students1_idx` (`student_username`),
  CONSTRAINT `fk_students_teachers_students1` FOREIGN KEY (`student_username`) REFERENCES `students` (`student_username`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_students_teachers_teachers1` FOREIGN KEY (`teacher_username`) REFERENCES `teachers` (`teacher_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_teachers`
--

LOCK TABLES `students_teachers` WRITE;
/*!40000 ALTER TABLE `students_teachers` DISABLE KEYS */;
INSERT INTO `students_teachers` VALUES ('vlado','michael'),('darko','ravi');
/*!40000 ALTER TABLE `students_teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_tests`
--

DROP TABLE IF EXISTS `students_tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_tests` (
  `student_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `test_id` int(11) NOT NULL,
  PRIMARY KEY (`student_username`,`test_id`),
  KEY `fk_students_tests_tests1_idx` (`test_id`),
  KEY `fk_students_tests_students1_idx` (`student_username`),
  CONSTRAINT `fk_students_tests_students1` FOREIGN KEY (`student_username`) REFERENCES `students` (`student_username`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_students_tests_tests1` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_tests`
--

LOCK TABLES `students_tests` WRITE;
/*!40000 ALTER TABLE `students_tests` DISABLE KEYS */;
INSERT INTO `students_tests` VALUES ('vlado',5);
/*!40000 ALTER TABLE `students_tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_title` varchar(120) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`subject_id`),
  UNIQUE KEY `subject_title_UNIQUE` (`subject_title`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
INSERT INTO `subjects` VALUES (24,'Biology'),(25,'Mathematics');
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_category`
--

DROP TABLE IF EXISTS `teacher_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_category` (
  `teacher_category_id` int(11) NOT NULL,
  `teacher_category_description` varchar(45) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`teacher_category_id`),
  UNIQUE KEY `teacher_category_description_UNIQUE` (`teacher_category_description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_category`
--

LOCK TABLES `teacher_category` WRITE;
/*!40000 ALTER TABLE `teacher_category` DISABLE KEYS */;
INSERT INTO `teacher_category` VALUES (2,'assistant'),(3,'demonstrator'),(1,'professor');
/*!40000 ALTER TABLE `teacher_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers` (
  `teacher_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `teacher_email` varchar(70) COLLATE latin1_general_ci NOT NULL,
  `teacher_name` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `teacher_surname` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `teacher_password` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `teacher_category_id` int(11) NOT NULL,
  PRIMARY KEY (`teacher_username`,`teacher_category_id`),
  UNIQUE KEY `teacher_email_UNIQUE` (`teacher_email`),
  KEY `fk_teachers_teacher_category1_idx` (`teacher_category_id`),
  CONSTRAINT `fk_teachers_teacher_category1` FOREIGN KEY (`teacher_category_id`) REFERENCES `teacher_category` (`teacher_category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_teachers_users1` FOREIGN KEY (`teacher_username`) REFERENCES `users` (`user_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
INSERT INTO `teachers` VALUES ('michael','michael@gmail.com','Michael','Ramsden','34550715062af006ac4fab288de67ecb44793c3a05c475227241535f6ef7a81b',2),('ravi','ravi@gmail.com','Ravi','Zacharias','372fb41298889bc4df223a601cf4475407cdeae0938692ae018737ec1335190b',1);
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers_subjects`
--

DROP TABLE IF EXISTS `teachers_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers_subjects` (
  `teacher_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`teacher_username`,`subject_id`),
  KEY `fk_teachers_subjects_subjects1_idx` (`subject_id`),
  KEY `fk_teachers_subjects_teachers1_idx` (`teacher_username`),
  CONSTRAINT `fk_teachers_subjects_subjects1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_teachers_subjects_teachers1` FOREIGN KEY (`teacher_username`) REFERENCES `teachers` (`teacher_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers_subjects`
--

LOCK TABLES `teachers_subjects` WRITE;
/*!40000 ALTER TABLE `teachers_subjects` DISABLE KEYS */;
INSERT INTO `teachers_subjects` VALUES ('michael',24),('ravi',25);
/*!40000 ALTER TABLE `teachers_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tests` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) NOT NULL,
  PRIMARY KEY (`test_id`,`subject_id`),
  KEY `fk_tests_subjects1_idx` (`subject_id`),
  CONSTRAINT `fk_tests_subjects1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tests`
--

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
INSERT INTO `tests` VALUES (5,25);
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_privileges`
--

DROP TABLE IF EXISTS `user_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_privileges` (
  `user_privilege_id` int(11) NOT NULL,
  `user_privilege_description` varchar(45) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`user_privilege_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_privileges`
--

LOCK TABLES `user_privileges` WRITE;
/*!40000 ALTER TABLE `user_privileges` DISABLE KEYS */;
INSERT INTO `user_privileges` VALUES (1,'administrator'),(2,'teacher'),(3,'student');
/*!40000 ALTER TABLE `user_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_username` varchar(45) COLLATE latin1_general_ci NOT NULL,
  `user_email` varchar(90) COLLATE latin1_general_ci NOT NULL,
  `user_password` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `user_privilege_id` int(11) NOT NULL,
  PRIMARY KEY (`user_username`,`user_privilege_id`),
  UNIQUE KEY `user_email_UNIQUE` (`user_email`),
  KEY `fk_users_user_privileges_idx` (`user_privilege_id`),
  CONSTRAINT `fk_users_user_privileges` FOREIGN KEY (`user_privilege_id`) REFERENCES `user_privileges` (`user_privilege_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('admin','admin@admin.com','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',1),('darko','darko@gmail.com','fe79e0ac4b7db16d59a67be682f0c2e85e24241cccbb7a6303446e7105362bcc',3),('michael','michael@gmail.com','34550715062af006ac4fab288de67ecb44793c3a05c475227241535f6ef7a81b',2),('ravi','ravi@gmail.com','372fb41298889bc4df223a601cf4475407cdeae0938692ae018737ec1335190b',2),('vlado','vlado@gmail.com','5d82f85674b8a277ad8ea4af8baa1c5374d343ddfb89a578e5dd5a16b9dc4a39',3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-31 23:40:54
