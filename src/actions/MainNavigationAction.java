package actions;

import controller.Controller;
import frames.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.text.TextAlignment;

import java.awt.*;

public class MainNavigationAction implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent event) {


        // find out what button was pressed on main navigation from welcome frame, and set up desired frame
        if (event.getSource() == Controller.getInstance().getWelcomeFrame().getB_subjects()) {

            Controller.getInstance().getTopPageNavigationFrame().getB_subjects().setDisable(true);

            SubjectsFrame subjectsFrame = new SubjectsFrame();
            Controller.getInstance().setSubjectsFrame(subjectsFrame);

            Scene scene = new Scene(subjectsFrame, 900, 815);
            Controller.getInstance().setSceneSubjects(scene);

            Controller.getInstance().getPrimaryStage().setScene(Controller.getInstance().getSceneSubjects());
            Controller.getInstance().getPrimaryStage().setTitle("Subjects");

        }

        if (event.getSource() == Controller.getInstance().getWelcomeFrame().getB_teachers()) {

            Controller.getInstance().getTopPageNavigationFrame().getB_teachers().setDisable(true);

            TeachersFrame teachersFrame = new TeachersFrame();
            Controller.getInstance().setTeachersFrame(teachersFrame);

            Scene scene = new Scene(teachersFrame, 900, 815);
            Controller.getInstance().setSceneTeachers(scene);

            Controller.getInstance().getPrimaryStage().setScene(Controller.getInstance().getSceneTeachers());
            Controller.getInstance().getPrimaryStage().setTitle("Teachers");

        }

        if (event.getSource() == Controller.getInstance().getWelcomeFrame().getB_students()) {

            Controller.getInstance().getTopPageNavigationFrame().getB_students().setDisable(true);

            StudentsFrame studentsFrame = new StudentsFrame();
            Controller.getInstance().setStudentsFrame(studentsFrame);

            Scene scene = new Scene(studentsFrame, 900, 815);
            Controller.getInstance().setSceneStudents(scene);

            Controller.getInstance().getPrimaryStage().setScene(Controller.getInstance().getSceneStudents());
            Controller.getInstance().getPrimaryStage().setTitle("Students");

        }

        if (event.getSource() == Controller.getInstance().getWelcomeFrame().getB_questionnaires()) {

            Controller.getInstance().getTopPageNavigationFrame().getB_questionnaires().setDisable(true);

            QuestionnairesFrame questionnairesFrame = new QuestionnairesFrame();
            Controller.getInstance().setQuestionnairesFrame(questionnairesFrame);

            Scene scene = new Scene(questionnairesFrame, 900, 815);
            Controller.getInstance().setSceneQuestionnaires(scene);

            Controller.getInstance().getPrimaryStage().setScene(Controller.getInstance().getSceneQuestionnaires());
            Controller.getInstance().getPrimaryStage().setTitle("Questionnaires");

        }

        if (event.getSource() == Controller.getInstance().getWelcomeFrame().getB_questionnaireResults()) {

            Controller.getInstance().getTopPageNavigationFrame().getB_questionnaireResults().setDisable(true);

            QuestionnaireResultsFrame questionnaireResultsFrame = new QuestionnaireResultsFrame();
            Controller.getInstance().setQuestionnaireResultsFrame(questionnaireResultsFrame);

            Scene scene = new Scene(questionnaireResultsFrame, 900, 815);
            Controller.getInstance().setSceneQuestionnaireResults(scene);

            Controller.getInstance().getPrimaryStage().setScene(Controller.getInstance().getSceneQuestionnaireResults());
            Controller.getInstance().getPrimaryStage().setTitle("Questionnaire results");

        }

        if (event.getSource() == Controller.getInstance().getWelcomeFrame().getB_questions()) {

            Controller.getInstance().getTopPageNavigationFrame().getB_questions().setDisable(true);

            QuestionsFrame questionsFrame = new QuestionsFrame();
            Controller.getInstance().setQuestionsFrame(questionsFrame);

            Scene scene = new Scene(questionsFrame, 900, 815);
            Controller.getInstance().setSceneQuestions(scene);

            Controller.getInstance().getPrimaryStage().setScene(Controller.getInstance().getSceneQuestions());
            Controller.getInstance().getPrimaryStage().setTitle("Questions");

        }

        if (event.getSource() == Controller.getInstance().getWelcomeFrame().getB_tests()) {

            Controller.getInstance().getTopPageNavigationFrame().getB_tests().setDisable(true);

            TestsFrame testsFrame = new TestsFrame();
            Controller.getInstance().setTestsFrame(testsFrame);

            Scene scene = new Scene(testsFrame, 900, 815);
            Controller.getInstance().setSceneTests(scene);

            Controller.getInstance().getPrimaryStage().setScene(Controller.getInstance().getSceneTests());
            Controller.getInstance().getPrimaryStage().setTitle("Tests");

        }

        if (event.getSource() == Controller.getInstance().getWelcomeFrame().getB_testResults()) {

            Controller.getInstance().getTopPageNavigationFrame().getB_testResults().setDisable(true);

            TestResultsFrame testResultsFrame = new TestResultsFrame();
            Controller.getInstance().setTestResultsFrame(testResultsFrame);

            Scene scene = new Scene(testResultsFrame, 900, 815);
            Controller.getInstance().setSceneTestResults(scene);

            Controller.getInstance().getPrimaryStage().setScene(Controller.getInstance().getSceneTestResults());
            Controller.getInstance().getPrimaryStage().setTitle("Test results");

        }

    }

}
