package actions;

import controller.Controller;
import frames.MyProfileFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class MyProfileFrameAction implements EventHandler<ActionEvent> {

    public void handle(ActionEvent event) {

        MyProfileFrame myProfileFrame = new MyProfileFrame();
        Controller.getInstance().setMyProfileFrame(myProfileFrame);

        Scene scene = new Scene(myProfileFrame, 900, 815);
        Controller.getInstance().getPrimaryStage().setScene(scene);
        Controller.getInstance().getPrimaryStage().setTitle("My profile");
    }
}
