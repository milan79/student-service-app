package actions;

import controller.Controller;
import frames.LoginFrame;
import frames.WelcomeFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;

import javax.swing.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;


public class LoginAction implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent event) {

        String usernameOrEmail = Controller.getInstance().getLoginFrame().getTf_usernameOrEmail().getText().trim();
        String password = Controller.getInstance().getLoginFrame().getPf_password().getText().trim();

        if ((usernameOrEmail.equals("") || usernameOrEmail == null) || (password.equals("") || password == null)) {

            if ((usernameOrEmail.equals("") || usernameOrEmail == null) && (password.equals("") || password == null)) {

                LoginFrame loginFrame = new LoginFrame();
                Controller.getInstance().setLoginFrame(loginFrame);

                Label l_temp = Controller.getInstance().getLoginFrame().getL_usernameOrEmailFalse();
                Controller.getInstance().getLoginFrame().getVb_usernameOrEmailFalse().getChildren().add(l_temp);

                Label l_temp2 = Controller.getInstance().getLoginFrame().getL_passwordFalse();
                Controller.getInstance().getLoginFrame().getVb_passwordFalse().getChildren().add(l_temp2);

                Scene scene = new Scene(Controller.getInstance().getLoginFrame(), 900, 815);
                Controller.getInstance().getPrimaryStage().setScene(scene);
                Controller.getInstance().getPrimaryStage().setTitle("Login");
            }

            if ((usernameOrEmail.equals("") || usernameOrEmail == null) && (!(password.equals("") || password == null))) {

                LoginFrame loginFrame = new LoginFrame();
                Controller.getInstance().setLoginFrame(loginFrame);

                Label l_temp = Controller.getInstance().getLoginFrame().getL_usernameOrEmailFalse();
                Controller.getInstance().getLoginFrame().getVb_usernameOrEmailFalse().getChildren().add(l_temp);

                Controller.getInstance().getLoginFrame().getPf_password().setText(password);

                Scene scene = new Scene(Controller.getInstance().getLoginFrame(), 900, 815);
                Controller.getInstance().getPrimaryStage().setScene(scene);
                Controller.getInstance().getPrimaryStage().setTitle("Login");
            }

            if ((!(usernameOrEmail.equals("") || usernameOrEmail == null)) && (password.equals("") || password == null)) {

                LoginFrame loginFrame = new LoginFrame();
                Controller.getInstance().setLoginFrame(loginFrame);

                Label l_temp2 = Controller.getInstance().getLoginFrame().getL_passwordFalse();
                Controller.getInstance().getLoginFrame().getVb_passwordFalse().getChildren().add(l_temp2);

                Controller.getInstance().getLoginFrame().getTf_usernameOrEmail().setText(usernameOrEmail);

                Scene scene = new Scene(Controller.getInstance().getLoginFrame(), 900, 815);
                Controller.getInstance().getPrimaryStage().setScene(scene);
                Controller.getInstance().getPrimaryStage().setTitle("Login");
            }

        } else {

            LoginFrame loginFrame = new LoginFrame();
            Controller.getInstance().setLoginFrame(loginFrame);

            Controller.getInstance().getLoginFrame().getTf_usernameOrEmail().setText(usernameOrEmail);
            Controller.getInstance().getLoginFrame().getPf_password().setText(password);

            Scene scene = new Scene(Controller.getInstance().getLoginFrame(), 900, 815);
            Controller.getInstance().getPrimaryStage().setScene(scene);
            Controller.getInstance().getPrimaryStage().setTitle("Login");

            // Check whether user with username/email and password is registered and stored in database
            if (Controller.getInstance().verifyLogin(usernameOrEmail, password)) {

                // Create new welcomeFrame and set it through Controller
                WelcomeFrame welcomeFrame = new WelcomeFrame();
                Controller.getInstance().setWelcomeFrame(welcomeFrame);

                // Create new scene, put welcomeFrame, and load scene into stage
                Scene scene2 = new Scene(welcomeFrame, 900, 815);
                Controller.getInstance().getPrimaryStage().setScene(scene2);
                Controller.getInstance().getPrimaryStage().setTitle("Welcome");
            } else {
                JOptionPane.showMessageDialog(null, "Incorrect username/email or password");
            }
        }
    }

}
