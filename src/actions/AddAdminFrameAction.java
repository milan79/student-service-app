package actions;

import controller.Controller;
import frames.AddAdminFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class AddAdminFrameAction implements EventHandler<ActionEvent> {

    public void handle(ActionEvent event) {

        AddAdminFrame addAdminFrame = new AddAdminFrame();
        Controller.getInstance().setAddAdminFrame(addAdminFrame);

        // Create new scene, put welcomeFrame, and load scene into stage
        Scene scene = new Scene(addAdminFrame, 900, 815);
        Controller.getInstance().getPrimaryStage().setScene(scene);
        Controller.getInstance().getPrimaryStage().setTitle("Add admin");

    }
}
