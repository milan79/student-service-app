package actions;

import controller.Controller;
import frames.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;

public class TopPageNavigationAction implements EventHandler<ActionEvent> {

    @Override
    public void handle(ActionEvent event) {

        Controller k = Controller.getInstance();

         if (event.getSource() == k.getTopPageNavigationFrame().getB_subjects()) {

             TopPageNavigationFrame topPageNavigationFrame = new TopPageNavigationFrame();
             k.setTopPageNavigationFrame(topPageNavigationFrame);

             k.getTopPageNavigationFrame().getB_subjects().setDisable(true);
            k.getTopPageNavigationFrame().getB_teachers().setDisable(false);
            k.getTopPageNavigationFrame().getB_students().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaires().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaireResults().setDisable(false);
            k.getTopPageNavigationFrame().getB_questions().setDisable(false);
            k.getTopPageNavigationFrame().getB_tests().setDisable(false);
            k.getTopPageNavigationFrame().getB_testResults().setDisable(false);

            SubjectsFrame subjectsFrame = new SubjectsFrame();
            k.setSubjectsFrame(subjectsFrame);

            Scene scene = new Scene(subjectsFrame, 900, 815);
            k.setSceneSubjects(scene);

            k.getPrimaryStage().setScene(k.getSceneSubjects());
            k.getPrimaryStage().setTitle("Subjects");

        }

        if (event.getSource() == k.getTopPageNavigationFrame().getB_teachers()) {

            TopPageNavigationFrame topPageNavigationFrame = new TopPageNavigationFrame();
            k.setTopPageNavigationFrame(topPageNavigationFrame);

            k.getTopPageNavigationFrame().getB_subjects().setDisable(false);
            k.getTopPageNavigationFrame().getB_teachers().setDisable(true);
            k.getTopPageNavigationFrame().getB_students().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaires().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaireResults().setDisable(false);
            k.getTopPageNavigationFrame().getB_questions().setDisable(false);
            k.getTopPageNavigationFrame().getB_tests().setDisable(false);
            k.getTopPageNavigationFrame().getB_testResults().setDisable(false);

            TeachersFrame teachersFrame = new TeachersFrame();
            k.setTeachersFrame(teachersFrame);

            Scene scene = new Scene(teachersFrame, 900, 815);
            k.setSceneTeachers(scene);

            k.getPrimaryStage().setScene(k.getSceneTeachers());
            k.getPrimaryStage().setTitle("Teachers");

        }

        if (event.getSource() == k.getTopPageNavigationFrame().getB_students()) {

            TopPageNavigationFrame topPageNavigationFrame = new TopPageNavigationFrame();
            k.setTopPageNavigationFrame(topPageNavigationFrame);

            k.getTopPageNavigationFrame().getB_subjects().setDisable(false);
            k.getTopPageNavigationFrame().getB_teachers().setDisable(false);
            k.getTopPageNavigationFrame().getB_students().setDisable(true);
            k.getTopPageNavigationFrame().getB_questionnaires().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaireResults().setDisable(false);
            k.getTopPageNavigationFrame().getB_questions().setDisable(false);
            k.getTopPageNavigationFrame().getB_tests().setDisable(false);
            k.getTopPageNavigationFrame().getB_testResults().setDisable(false);

            StudentsFrame studentsFrame = new StudentsFrame();
            k.setStudentsFrame(studentsFrame);

            Scene scene = new Scene(studentsFrame, 900, 815);
            k.setSceneStudents(scene);

            k.getPrimaryStage().setScene(k.getSceneStudents());
            k.getPrimaryStage().setTitle("Students");

        }

        if (event.getSource() == k.getTopPageNavigationFrame().getB_questionnaires()) {

            TopPageNavigationFrame topPageNavigationFrame = new TopPageNavigationFrame();
            k.setTopPageNavigationFrame(topPageNavigationFrame);

            k.getTopPageNavigationFrame().getB_subjects().setDisable(false);
            k.getTopPageNavigationFrame().getB_teachers().setDisable(false);
            k.getTopPageNavigationFrame().getB_students().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaires().setDisable(true);
            k.getTopPageNavigationFrame().getB_questionnaireResults().setDisable(false);
            k.getTopPageNavigationFrame().getB_questions().setDisable(false);
            k.getTopPageNavigationFrame().getB_tests().setDisable(false);
            k.getTopPageNavigationFrame().getB_testResults().setDisable(false);

            QuestionnairesFrame questionnairesFrame = new QuestionnairesFrame();
            k.setQuestionnairesFrame(questionnairesFrame);

            Scene scene = new Scene(questionnairesFrame, 900, 815);
            k.setSceneQuestionnaires(scene);

            k.getPrimaryStage().setScene(k.getSceneQuestionnaires());
            k.getPrimaryStage().setTitle("Questionnaires");

        }

        if (event.getSource() == k.getTopPageNavigationFrame().getB_questionnaireResults()) {

            TopPageNavigationFrame topPageNavigationFrame = new TopPageNavigationFrame();
            k.setTopPageNavigationFrame(topPageNavigationFrame);

            k.getTopPageNavigationFrame().getB_subjects().setDisable(false);
            k.getTopPageNavigationFrame().getB_teachers().setDisable(false);
            k.getTopPageNavigationFrame().getB_students().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaires().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaireResults().setDisable(true);
            k.getTopPageNavigationFrame().getB_questions().setDisable(false);
            k.getTopPageNavigationFrame().getB_tests().setDisable(false);
            k.getTopPageNavigationFrame().getB_testResults().setDisable(false);

            QuestionnaireResultsFrame questionnaireResultsFrame = new QuestionnaireResultsFrame();
            k.setQuestionnaireResultsFrame(questionnaireResultsFrame);

            Scene scene = new Scene(questionnaireResultsFrame, 900, 815);
            k.setSceneQuestionnaireResults(scene);

            k.getPrimaryStage().setScene(k.getSceneQuestionnaireResults());
            k.getPrimaryStage().setTitle("Questionnaire results");

        }

        if (event.getSource() == k.getTopPageNavigationFrame().getB_questions()) {

            TopPageNavigationFrame topPageNavigationFrame = new TopPageNavigationFrame();
            k.setTopPageNavigationFrame(topPageNavigationFrame);

            k.getTopPageNavigationFrame().getB_subjects().setDisable(false);
            k.getTopPageNavigationFrame().getB_teachers().setDisable(false);
            k.getTopPageNavigationFrame().getB_students().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaires().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaireResults().setDisable(false);
            k.getTopPageNavigationFrame().getB_questions().setDisable(true);
            k.getTopPageNavigationFrame().getB_tests().setDisable(false);
            k.getTopPageNavigationFrame().getB_testResults().setDisable(false);

            QuestionsFrame questionsFrame = new QuestionsFrame();
            k.setQuestionsFrame(questionsFrame);

            Scene scene = new Scene(questionsFrame, 900, 815);
            k.setSceneQuestions(scene);

            k.getPrimaryStage().setScene(k.getSceneQuestions());
            k.getPrimaryStage().setTitle("Questions");

        }

        if (event.getSource() == k.getTopPageNavigationFrame().getB_tests()) {

            TopPageNavigationFrame topPageNavigationFrame = new TopPageNavigationFrame();
            k.setTopPageNavigationFrame(topPageNavigationFrame);

            k.getTopPageNavigationFrame().getB_subjects().setDisable(false);
            k.getTopPageNavigationFrame().getB_teachers().setDisable(false);
            k.getTopPageNavigationFrame().getB_students().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaires().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaireResults().setDisable(false);
            k.getTopPageNavigationFrame().getB_questions().setDisable(false);
            k.getTopPageNavigationFrame().getB_tests().setDisable(true);
            k.getTopPageNavigationFrame().getB_testResults().setDisable(false);

            TestsFrame testsFrame = new TestsFrame();
            k.setTestsFrame(testsFrame);

            Scene scene = new Scene(testsFrame, 900, 815);
            k.setSceneTests(scene);

            k.getPrimaryStage().setScene(k.getSceneTests());
            k.getPrimaryStage().setTitle("Tests");

        }

        if (event.getSource() == k.getTopPageNavigationFrame().getB_testResults()) {

            TopPageNavigationFrame topPageNavigationFrame = new TopPageNavigationFrame();
            k.setTopPageNavigationFrame(topPageNavigationFrame);

            k.getTopPageNavigationFrame().getB_subjects().setDisable(false);
            k.getTopPageNavigationFrame().getB_teachers().setDisable(false);
            k.getTopPageNavigationFrame().getB_students().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaires().setDisable(false);
            k.getTopPageNavigationFrame().getB_questionnaireResults().setDisable(false);
            k.getTopPageNavigationFrame().getB_questions().setDisable(false);
            k.getTopPageNavigationFrame().getB_tests().setDisable(false);
            k.getTopPageNavigationFrame().getB_testResults().setDisable(true);

            TestResultsFrame testResultsFrame = new TestResultsFrame();
            k.setTestResultsFrame(testResultsFrame);

            Scene scene = new Scene(testResultsFrame, 900, 815);
            k.setSceneTestResults(scene);

            k.getPrimaryStage().setScene(k.getSceneTestResults());
            k.getPrimaryStage().setTitle("Test results");

        }
    }
}
