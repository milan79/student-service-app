package controller;

import data.Question;
import data.Student;
import data.Subject;
import data.Teacher;
import frames.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.swing.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.*;
import java.util.ArrayList;

public class Controller {

    private static Controller instance = null;

    private Stage primaryStage;
    private Scene sceneLogin;
    private Scene sceneWelcome;
    private Scene sceneStudents;
    private Scene sceneTeachers;
    private Scene sceneSubjects;
    private Scene sceneQuestionnaires;
    private Scene sceneQuestionnaireResults;
    private Scene sceneQuestions;
    private Scene sceneTests;
    private Scene sceneTestResults;
    private Scene sceneSpecificQuestionnaire;

    private LoginFrame loginFrame;
    private WelcomeFrame welcomeFrame;
    private SubjectsFrame subjectsFrame;
    private TeachersFrame teachersFrame;
    private StudentsFrame studentsFrame;
    private QuestionnairesFrame questionnairesFrame;
    private QuestionnaireResultsFrame questionnaireResultsFrame;
    private QuestionsFrame questionsFrame;
    private TestsFrame testsFrame;
    private TestResultsFrame testResultsFrame;
    private TopPageNavigationFrame topPageNavigationFrame;
    private SpecificQuestionnaireFrame specificQuestionnaireFrame;
    private AddAdminFrame addAdminFrame;
    private MyProfileFrame myProfileFrame;

    private ObservableList<Subject> subjectList;
    private ObservableList<Teacher> teachersList;
    private ObservableList<Subject> teacherSubjectsList;
    private ObservableList<Student> subjectStudentsList;
    private ObservableList<Subject> studentSubjectsList;
    private ObservableList<Teacher> studentTeachersList;
    private ObservableList<Student> studentList;
    private ObservableList<Question> questionList;

    private Connection connect;
    private Statement st;
    private PreparedStatement pst;
    private ResultSet rs;

    // make constructor private, to implement singleton pattern
    private Controller() {
        mysqlConnection();
    }

    // singleton pattern implemented - only one instance of Controller allowed
    public static Controller getInstance() {
        if (instance == null) {
            synchronized(Controller.class) {
                if (instance == null) {
                    instance = new Controller();
                }
            }
        }
        return instance;
    }

    // set up connection with mysql database
    private void mysqlConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_service_app", "root", "test");
            JOptionPane.showMessageDialog(null, "Successful connection to database");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Connection unsuccessful");
        }
    }

    // method for verifying login data, checks whether username and password are stored in database
    public boolean verifyLogin(String usernameOrEmail, String password) {

        String hashPassword = hashPassword(password);

        String query = "SELECT * FROM administrators WHERE (administrator_username = ? OR administrator_email = ?) AND administrator_password = ?";
        boolean answer = false;

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, usernameOrEmail);
            pst.setString(2, usernameOrEmail);
            pst.setString(3, hashPassword);
            rs = pst.executeQuery();

            if (rs.next()) {
                answer = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return answer;
    }

    public String hashPassword(String pass) {
        String hashPassword = "";
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(
                    pass.getBytes(StandardCharsets.UTF_8));
            hashPassword = bytesToHex(encodedhash);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashPassword;
    }

    private String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public boolean addAdministrator(TextField tf_adminName, TextField tf_adminSurname, TextField tf_adminUsername, TextField tf_adminPassword, TextField tf_adminEmail) {

        boolean answer = false;

        String name = tf_adminName.getText().trim();
        String surname = tf_adminSurname.getText().trim();
        String username = tf_adminUsername.getText().trim();
        String password = tf_adminPassword.getText().trim();
        String hashPassword = hashPassword(password);
        String email = tf_adminEmail.getText().trim();

        if ((name == null || name.equals("")) || (surname == null || surname.equals("")) || (username == null || username.equals("")) ||
                (password == null || password.equals("")) || (email == null || email.equals(""))) {
            JOptionPane.showMessageDialog(null, "All field must be populated");
            return false;
        }

        if (((getAdministratorName(tf_adminUsername) != null) || (getAdministratorName(tf_adminEmail) != null))) {

            if (getAdministratorName(tf_adminUsername) != null) {
                JOptionPane.showMessageDialog(null, "Administrator with that username already exists");
                return false;
            }
            if (getAdministratorName(tf_adminEmail) != null) {
                JOptionPane.showMessageDialog(null, "Administrator with that email already exists");
                return false;
            }

        } else {
            String query = "INSERT INTO administrators (administrator_name, administrator_surname, " +
                    "administrator_username, administrator_password, administrator_email) VALUES (?,?,?,?,?)";

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, name);
                pst.setString(2, surname);
                pst.setString(3, username);
                pst.setString(4, hashPassword);
                pst.setString(5, email);

                pst.executeUpdate();

                JOptionPane.showMessageDialog(null, "successfully added new admin");
                return true;
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "adding new admin failed");
            }
        }
        return false;
    }

    public ArrayList<String> getAdministratorData(String usernameOrEmail) {

        ArrayList<String> data = new ArrayList<String>();
        String name = null;
        String surname = null;
        String username = null;
        String email = null;

        String query = "SELECT administrator_username, administrator_name, administrator_surname, administrator_email FROM administrators WHERE administrator_username=? OR " +
                "administrator_email=?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, usernameOrEmail);
            pst.setString(2, usernameOrEmail);

            rs = pst.executeQuery();

            if (rs.next()) {
                name = rs.getString("administrator_name");
                surname = rs.getString("administrator_surname");
                email = rs.getString("administrator_email");
                username = rs.getString("administrator_username");

                data.add(name);
                data.add(surname);
                data.add(username);
                data.add(email);

            } else {
                query = "SELECT administrator_username, administrator_name, administrator_surname, administrator_username FROM administrators WHERE administrator_email=?";

                try {
                    pst = connect.prepareStatement(query);
                    pst.setString(1, usernameOrEmail);

                    rs = pst.executeQuery();

                    if (rs.next()) {
                        name = rs.getString("administrator_name");
                        surname = rs.getString("administrator_surname");
                        username = rs.getString("administrator_username");
                        email = rs.getString("administrator_email");

                        data.add(name);
                        data.add(surname);
                        data.add(username);
                        data.add(email);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public void updateAdministrator (TextField tf_adminName, TextField tf_adminSurname, TextField tf_adminUsername, TextField tf_adminPassword, TextField tf_adminEmail) {

        String name = tf_adminName.getText().trim();
        String surname = tf_adminSurname.getText().trim();
        String username = tf_adminUsername.getText().trim();
        String password = tf_adminPassword.getText().trim();
        String hashPassword = null;
        if (password != null || (!password.equals(""))) {
            hashPassword = hashPassword(password);
        }
        String email = tf_adminEmail.getText().trim();

        String query = null;

        if (password != null && (!password.equals(""))) {
            query = "UPDATE administrators SET administrator_name = ?, administrator_surname = ?, administrator_password = ?, administrator_email = ? " +
                    "WHERE administrator_username = ?";

            try{
                pst = connect.prepareStatement(query);

                pst.setString(1, name);
                pst.setString(2, surname);
                pst.setString(3, hashPassword);
                pst.setString(4, email);
                pst.setString(5, username);

                pst.executeUpdate();
                JOptionPane.showMessageDialog(null, "Administrator data successfully updated");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action failed");
            }
        } else {
            query = "UPDATE administrators SET administrator_name = ?, administrator_surname = ?, administrator_email = ? " +
                    "WHERE administrator_username = ?";

            try{
                pst = connect.prepareStatement(query);

                pst.setString(1, name);
                pst.setString(2, surname);
                pst.setString(3, email);
                pst.setString(4, username);

                pst.executeUpdate();

                JOptionPane.showMessageDialog(null, "Administrator data successfully updated");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action failed");
            }
        }
    }

    public String getAdministratorName(TextField tf_usernameOrEmail) {

        String usernameOrEmail = tf_usernameOrEmail.getText().trim();

        String name = null;

        String query = "SELECT administrator_name FROM administrators WHERE administrator_username=? OR administrator_email=?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, usernameOrEmail);
            pst.setString(2, usernameOrEmail);

            rs = pst.executeQuery();

            if (rs.next()) {
                name = rs.getString(1);
            }

            return name;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setWelcomeFrame() {
        welcomeFrame = new WelcomeFrame();
        setWelcomeFrame(welcomeFrame);

        Scene scene = new Scene(welcomeFrame, 900, 815);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Welcome");
    }

    public ObservableList<Subject> getSubjects() {
        subjectList = FXCollections.observableArrayList();

        String query = "SELECT * FROM subjects";

        try {
            pst = connect.prepareStatement(query);

            rs = pst.executeQuery();
            while (rs.next()) {
                String subjectTitle = rs.getString("subject_title");

                Subject subject = new Subject(subjectTitle);

                subjectList.add(subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subjectList;
    }

    public int getSubjectId(String subjectTitle) {

        int subjectId = 0;

        String query = "SELECT subject_id from subjects WHERE subject_title = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1,subjectTitle);
            rs = pst.executeQuery();

            if (rs.next()) {
                subjectId = rs.getInt("subject_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subjectId;
    }

    public ObservableList<Subject> getSubjects(String text) {

        subjectList = FXCollections.observableArrayList();

        String query = "SELECT * FROM subjects WHERE subject_title LIKE '" + text + "%' ORDER BY subject_title";

        try {
            pst = connect.prepareStatement(query);

            rs = pst.executeQuery();

            while (rs.next()) {
                String subjectTitle = rs.getString("subject_title");

                Subject subject = new Subject(subjectTitle);

                subjectList.add(subject);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subjectList;
    }

    public void addSubject(String subjectTitle) {

        if (subjectTitle.equals("") || subjectTitle == null) {
            JOptionPane.showMessageDialog(null, "You must enter subject title");
            return;
        }

        String query = "INSERT INTO subjects (subject_title) VALUES (?)";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, subjectTitle);

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Subject successfully added");

        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Action not successful");
        }
    }

    public boolean updateSubject(String oldSubjectTitle, String newSubjectTitle) {

        if (oldSubjectTitle.equals("") || oldSubjectTitle == null) {
            JOptionPane.showMessageDialog(null, "You must enter old subject title");
            return false;
        }

        if (newSubjectTitle.equals("") || newSubjectTitle == null) {
            JOptionPane.showMessageDialog(null, "You must enter new subject title");
            return false;
        }

        if (newSubjectTitle.equals(oldSubjectTitle)) {
            JOptionPane.showMessageDialog(null, "You must change subject title");
            return false;
        }

        String query = "UPDATE subjects SET subject_title = ? WHERE subject_title = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, newSubjectTitle);
            pst.setString(2, oldSubjectTitle);

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Subject name successfully updated");
            return true;

        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Action not successful");
        }
        return false;
    }

    public boolean deleteSubject(String subjectTitle) {

        if (subjectTitle.equals("") || subjectTitle == null) {
            JOptionPane.showMessageDialog(null, "You must specify subject to delete");
            return false;
        }

        String query = "DELETE FROM subjects WHERE subject_title = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, subjectTitle);

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Subject successfully deleted");
            return true;

        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Action not successful");
        }
        return false;
    }

    public ObservableList<Teacher> getSubjectTeachers(String subjectTitle) {

        teachersList = FXCollections.observableArrayList();

        int subjectId = getSubjectId(subjectTitle);

        String query = "SELECT teachers.teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teacher_category_id FROM " +
                       "subjects JOIN teachers_subjects ON subjects.subject_id = teachers_subjects.subject_id JOIN " +
                       "teachers ON teachers_subjects.teacher_username = teachers.teacher_username " +
                       "WHERE subjects.subject_id = ? ORDER BY teacher_surname";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, subjectId);
            rs = pst.executeQuery();

            while (rs.next()) {
                String teacherUsername = rs.getString(1);
                String teacherEmail = rs.getString(2);
                String teacherName = rs.getString(3);
                String teacherSurname = rs.getString(4);
                String teacherPassword = rs.getString(5);
                int teacherCategoryId = rs.getInt(6);

                Teacher teacher = new Teacher(teacherUsername, teacherEmail, teacherName, teacherSurname, teacherPassword, teacherCategoryId);
                teachersList.add(teacher);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teachersList;
    }

    public ObservableList<Subject> getTeacherSubjects(String teacherEmail) {

        teacherSubjectsList = FXCollections.observableArrayList();

        String teacherUsername = getTeacherUsername(teacherEmail);

        String query = "SELECT subject_title FROM " +
                "teachers_subjects JOIN subjects ON teachers_subjects.subject_id = subjects.subject_id " +
                "WHERE teacher_username = ? ORDER BY subject_title";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, teacherUsername);
            rs = pst.executeQuery();

            while (rs.next()) {
                String subject_title = rs.getString(1);

                Subject subject = new Subject(subject_title);
                teacherSubjectsList.add(subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teacherSubjectsList;
    }

    public boolean isTeacherOnSubject(String teacherEmail, String subjectTitle) {
        ObservableList<Subject> subjects = FXCollections.observableArrayList();
        subjects = getTeacherSubjects(teacherEmail);

        for (Subject s : subjects) {
            if (s.getSubjectTitle().equalsIgnoreCase(subjectTitle)) {
                return true;
            }
        }
        return false;
    }

    public ObservableList<Teacher> getTeachers() {

        teachersList = FXCollections.observableArrayList();

        String query = "SELECT teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teacher_category_id FROM " +
                "teachers ORDER BY teacher_surname";

        try {
            pst = connect.prepareStatement(query);

            rs = pst.executeQuery();

            while (rs.next()) {
                String teacherUsername = rs.getString(1);
                String teacherEmail = rs.getString(2);
                String teacherName = rs.getString(3);
                String teacherSurname = rs.getString(4);
                String teacherPassword = rs.getString(5);
                int teacherCategoryId = rs.getInt(6);

                Teacher teacher = new Teacher(teacherUsername, teacherEmail, teacherName, teacherSurname, teacherPassword, teacherCategoryId);
                teachersList.add(teacher);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teachersList;
    }

    public ObservableList<Teacher> getTeacherByUsername(String teacherUsername) {

        teachersList = FXCollections.observableArrayList();

        String query = "SELECT teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teacher_category_id FROM " +
                "teachers WHERE teacher_username = ? ORDER BY teacher_surname";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, teacherUsername);
            rs = pst.executeQuery();

            if (rs.next()) {
                String teacherUsername1 = rs.getString(1);
                String teacherEmail = rs.getString(2);
                String teacherName = rs.getString(3);
                String teacherSurname = rs.getString(4);
                String teacherPassword = rs.getString(5);
                int teacherCategoryId = rs.getInt(6);

                Teacher teacher = new Teacher(teacherUsername1, teacherEmail, teacherName, teacherSurname, teacherPassword, teacherCategoryId);
                teachersList.add(teacher);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teachersList;
    }

    public ObservableList<Teacher> getTeachersByTitle(String title) {

        teachersList = FXCollections.observableArrayList();

        String query = "SELECT teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teachers.teacher_category_id FROM " +
                "teachers JOIN teacher_category ON teachers.teacher_category_id = teacher_category.teacher_category_id " +
                "WHERE teacher_category.teacher_category_description = ? ORDER BY teacher_surname";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, title.toLowerCase());

            rs = pst.executeQuery();

            while (rs.next()) {
                String teacherUsername = rs.getString(1);
                String teacherEmail = rs.getString(2);
                String teacherName = rs.getString(3);
                String teacherSurname = rs.getString(4);
                String teacherPassword = rs.getString(5);
                int teacherCategoryId = rs.getInt(6);

                Teacher teacher = new Teacher(teacherUsername, teacherEmail, teacherName, teacherSurname, teacherPassword, teacherCategoryId);
                teachersList.add(teacher);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teachersList;
    }

    public ObservableList<Teacher> getTeachersByCriteria(String searchCriteria, String criteriaData, String teacherTitle) {

        int teacherCategory = 0;

        if (teacherTitle != null) {
            if (teacherTitle.equals("Professor")) {
                teacherCategory = 1;
            } else if (teacherTitle.equals("Assistant")) {
                teacherCategory = 2;
            } else if(teacherTitle.equals("Demonstrator")) {
                teacherCategory = 3;
            }
        }

        teachersList = FXCollections.observableArrayList();

        String query = "";

        if (teacherCategory != 0) {
            if (searchCriteria.equals("Teacher name")) {
                query = "SELECT teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teachers.teacher_category_id FROM " +
                        "teachers WHERE teacher_name LIKE '" + criteriaData + "%' AND teacher_category_id = " + teacherCategory +
                        " ORDER BY teacher_surname";
            } else if (searchCriteria.equals("Teacher surname")) {
                query = "SELECT teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teachers.teacher_category_id FROM " +
                        "teachers WHERE teacher_surname LIKE '" + criteriaData + "%' AND teacher_category_id = " + teacherCategory +
                        " ORDER BY teacher_surname";
            } else if (searchCriteria.equals("Teacher e-mail")) {
                query = "SELECT teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teachers.teacher_category_id FROM " +
                        "teachers WHERE teacher_email LIKE '" + criteriaData + "%' AND teacher_category_id = " + teacherCategory +
                        " ORDER BY teacher_surname";
            }
        } else if (teacherCategory == 0) {
            if (searchCriteria.equals("Teacher name")) {
                query = "SELECT teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teachers.teacher_category_id FROM " +
                        "teachers WHERE teacher_name LIKE '" + criteriaData + "%' ORDER BY teacher_surname";
            } else if (searchCriteria.equals("Teacher surname")) {
                query = "SELECT teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teachers.teacher_category_id FROM " +
                        "teachers WHERE teacher_surname LIKE '" + criteriaData + "%' ORDER BY teacher_surname";
            } else if (searchCriteria.equals("Teacher e-mail")) {
                query = "SELECT teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teachers.teacher_category_id FROM " +
                        "teachers WHERE teacher_email LIKE '" + criteriaData + "%' ORDER BY teacher_surname";
            }
        }

        try {
            pst = connect.prepareStatement(query);

            rs = pst.executeQuery();

            while (rs.next()) {
                String teacherUsername = rs.getString(1);
                String teacherEmail = rs.getString(2);
                String teacherName = rs.getString(3);
                String teacherSurname = rs.getString(4);
                String teacherPassword = rs.getString(5);
                int teacherCategoryId = rs.getInt(6);

                Teacher teacher = new Teacher(teacherUsername, teacherEmail, teacherName, teacherSurname, teacherPassword, teacherCategoryId);
                teachersList.add(teacher);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teachersList;
    }

    public ObservableList<Teacher> getTeachersQuestionnairesByCriteria(String searchCriteria, String criteriaData, String teacherTitle) {

        teachersList = Controller.getInstance().getTeacherQuestionnaire(teacherTitle);

        ObservableList<Teacher> removeList = FXCollections.observableArrayList();

        if (searchCriteria.equals("Teacher name")) {
            for (Teacher t : teachersList) {
                if (!(t.getTeacherName().toLowerCase().startsWith(criteriaData.toLowerCase()))) {
                    removeList.add(t);
                }
            }
        } else if (searchCriteria.equals("Teacher surname")) {
            for (Teacher t : teachersList) {
                if (!(t.getTeacherSurname().toLowerCase().startsWith(criteriaData.toLowerCase()))) {
                    removeList.add(t);
                }
            }
        } else if (searchCriteria.equals("Teacher e-mail")) {
            for (Teacher t : teachersList) {
                if (!(t.getTeacherEmail().toLowerCase().startsWith(criteriaData.toLowerCase()))) {
                    removeList.add(t);
                }
            }
        }

        removeList.forEach(teachersList::remove);

        return teachersList;
    }

    public ObservableList<Student> getStudentsByCriteria(String searchCriteria, String criteriaData) {

        studentList = FXCollections.observableArrayList();

        String query = null;

        if (searchCriteria.equals("Student name")) {
            query = "SELECT student_name, student_surname, student_email, student_username, student_password FROM students " +
                    "WHERE student_name LIKE '" + criteriaData + "%' ORDER BY student_surname";
        } else if (searchCriteria.equals("Student surname")) {
            query = "SELECT student_name, student_surname, student_email, student_username, student_password FROM students " +
                    "WHERE student_surname LIKE '" + criteriaData + "%' ORDER BY student_surname";
        } else if (searchCriteria.equals("Student e-mail")) {
            query = "SELECT student_name, student_surname, student_email, student_username, student_password FROM students " +
                    "WHERE student_email LIKE '" + criteriaData + "%' ORDER BY student_surname";
        }

        try {
            pst = connect.prepareStatement(query);

            rs = pst.executeQuery();

            while (rs.next()) {
                String studentName = rs.getString(1);
                String studentSurname = rs.getString(2);
                String studentEmail = rs.getString(3);
                String studentUsername = rs.getString(4);
                String studentPassword = rs.getString(5);

                Student student = new Student(studentUsername, studentEmail, studentName, studentSurname, studentPassword);
                studentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentList;
    }

    public ObservableList<Teacher> getStudentTeachers(String studentEmail) {

        studentTeachersList = FXCollections.observableArrayList();

        String studentUsername = getStudentUsername(studentEmail);

        String query = "SELECT teachers.teacher_username, teacher_email, teacher_name, teacher_surname, teacher_password, teacher_category_id " +
                "FROM students_teachers JOIN teachers ON students_teachers.teacher_username = teachers.teacher_username" +
                " WHERE student_username = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, studentUsername);

            rs = pst.executeQuery();
            while (rs.next()) {
                String teacherUsername = rs.getString(1);
                String teacherEmail = rs.getString(2);
                String teacherName = rs.getString(3);
                String teacherSurname = rs.getString(4);
                String teacherPassword = rs.getString(5);
                int teacherCategoryId = rs.getInt(6);

                Teacher teacher = new Teacher(teacherUsername, teacherEmail, teacherName, teacherSurname, teacherPassword, teacherCategoryId);
                studentTeachersList.add(teacher);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentTeachersList;
    }

    public String getTeacherUsername(String teacherEmail) {

        String teacherUsername = null;

        String query = "SELECT teacher_username FROM teachers WHERE teacher_email = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, teacherEmail);

            rs = pst.executeQuery();

            if (rs.next()) {
                teacherUsername = rs.getString("teacher_username");
            }
            return teacherUsername;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void assignTeacherToSubject(String subjectTitle, String teacherEmail) {

        int subjectId = getSubjectId(subjectTitle);
        String teacherUsername = getTeacherUsername(teacherEmail);

        if (teacherUsername != null && subjectId != 0) {
            String query = "INSERT INTO teachers_subjects (teacher_username, subject_id) VALUES (?,?)";

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, teacherUsername);
                pst.setInt(2, subjectId);

                int i = pst.executeUpdate();
                if (i != 0) {
                    JOptionPane.showMessageDialog(null, "Teacher successfully assigned to subject");
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful / teacher might be already assiged to subject");
            }
        } else {
            if (teacherUsername == null) {
                JOptionPane.showMessageDialog(null, "Incorrect teacher e-mail");
            }

            if (subjectId == 0) {
                JOptionPane.showMessageDialog(null, "Incorrect subject title");
            }
        }
    }

    public void removeTeacherFromSubject(String subjectTitle, String teacherEmail) {

        int subjectId = getSubjectId(subjectTitle);
        String teacherUsername = getTeacherUsername(teacherEmail);

        if (teacherUsername != null && subjectId != 0) {
            String query = "DELETE FROM teachers_subjects WHERE teacher_username = ? AND subject_id = ?";

            try {
                PreparedStatement ps = connect.prepareStatement(query);
                ps.setString(1, teacherUsername);
                ps.setInt(2, subjectId);

                boolean isOnSubject = isTeacherOnSubject(teacherEmail, subjectTitle);
                if (isOnSubject) {
                    ps.executeUpdate();
                }

                if (!isOnSubject) {
                    JOptionPane.showMessageDialog(null, "Teacher is not assigned to this subject");
                    return;
                }

                JOptionPane.showMessageDialog(null, "Teacher successfully removed from subject");
            } catch (Exception e) {
                //JOptionPane.showMessageDialog(null, "Action not successful");
                e.printStackTrace();
            }
        } else {
            if (teacherUsername == null) {
                JOptionPane.showMessageDialog(null, "Incorrect teacher e-mail");
            }

            if (subjectId == 0) {
                JOptionPane.showMessageDialog(null, "Incorrect subject title");
            }
        }
    }

    public void assignStudentToSubject(String subjectTitle, String studentEmail) {

        int subjectId = getSubjectId(subjectTitle);
        String studentUsername = getStudentUsername(studentEmail);

        if (studentUsername != null && subjectId != 0) {
            String query = "INSERT INTO students_subjects (student_username, subject_id) VALUES (?,?)";

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, studentUsername);
                pst.setInt(2, subjectId);

                int i = pst.executeUpdate();
                if (i != 0) {
                    JOptionPane.showMessageDialog(null, "Student successfully assigned to subject");
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action failed / student might be already assigned to subject");
            }
        } else {
            if (studentUsername == null) {
                JOptionPane.showMessageDialog(null, "Incorrect student e-mail");
            }

            if (subjectId == 0) {
                JOptionPane.showMessageDialog(null, "Incorrect subject title");
            }
        }
    }

    public void removeStudentFromSubject(String subjectTitle, String studentEmail) {

        int subjectId = getSubjectId(subjectTitle);
        String studentUsername = getStudentUsername(studentEmail);

        if (studentUsername != null && subjectId != 0) {
            String query = "DELETE FROM students_subjects WHERE student_username = ? AND subject_id = ?";

            try {
                PreparedStatement ps = connect.prepareStatement(query);
                ps.setString(1, studentUsername);
                ps.setInt(2, subjectId);

                ObservableList<Subject> studentSubjects = getStudentSubjects(studentEmail);
                boolean isOnSubject = false;
                for (int n = 0; n < studentSubjects.size(); n++) {
                    if (studentSubjects.get(n).getSubjectTitle().equalsIgnoreCase(subjectTitle)) {
                        ps.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Student successfully removed from subject");
                        isOnSubject = true;
                        break;
                    }
                }

                if (!isOnSubject) {
                    JOptionPane.showMessageDialog(null, "Student is not assigned to this subject");
                    return;
                }

                ObservableList<Teacher> studentTeachers = getStudentTeachers(studentEmail);
                studentSubjects = getStudentSubjects(studentEmail);
                ObservableList<Subject> teacherSubjects;

                int count = 0;

                for (int i = 0; i < studentTeachers.size(); i++) {
                    teacherSubjects = getTeacherSubjects(studentTeachers.get(i).getTeacherEmail());
                    for (int j = 0; j < studentSubjects.size(); j++) {
                        for (int k = 0; k < teacherSubjects.size(); k++) {
                            if (studentSubjects.get(j).getSubjectTitle().equalsIgnoreCase(teacherSubjects.get(k).getSubjectTitle())) {
                                count++;
                            }
                        }
                    }
                    if (count == 0) {
                        removeStudentFromTeacher(studentTeachers.get(i).getTeacherEmail(), studentEmail);
                    }
                    count = 0;
                }

            } catch (Exception e) {
                //JOptionPane.showMessageDialog(null, "Action failed");
                e.printStackTrace();
            }
        } else {
            if (studentUsername == null) {
                JOptionPane.showMessageDialog(null, "Incorrect student e-mail");
            }

            if (subjectId == 0) {
                JOptionPane.showMessageDialog(null, "Incorrect subject title");
            }
        }
    }

   public void assignStudentToTeacher(String teacherEmail, String studentEmail) {

        String studentUsername = getStudentUsername(studentEmail);
        String teacherUsername = getTeacherUsername(teacherEmail);

        ArrayList<Integer> teachersSubjects = getSubjectsAssignedToTeacher(teacherEmail);
        ArrayList<Integer> studentsSubjects = getSubjectsAssignedToStudent(studentEmail);

        for (Integer i : teachersSubjects) {
            for (Integer j : studentsSubjects) {
                if (i == j) {
                    String query = "INSERT INTO students_teachers (student_username, teacher_username) VALUES (?,?)";

                    try {
                        pst = connect.prepareStatement(query);
                        pst.setString(1, studentUsername);
                        pst.setString(2, teacherUsername);

                        int z = pst.executeUpdate();

                        if (z != 0) {
                            JOptionPane.showMessageDialog(null, "Student successfully assigned to teacher");
                        }
                        return;
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Action failed / Student might be already assigned to teacher");
                        return;
                    }
                }
            }
        }
        JOptionPane.showMessageDialog(null, "Teacher must be assigned to student's subject");
    }

    public void removeStudentFromTeacher(String teacherEmail, String studentEmail) {

        String studentUsername = getStudentUsername(studentEmail);
        String teacherUsername = getTeacherUsername(teacherEmail);

        if (studentUsername != null && teacherUsername != null) {
            String query = "DELETE FROM students_teachers WHERE student_username = ? AND teacher_username = ?";

            try {
                PreparedStatement ps = connect.prepareStatement(query);
                ps.setString(1, studentUsername);
                ps.setString(2, teacherUsername);

                ObservableList<Teacher> studentTeachers = getStudentTeachers(studentEmail);
                boolean isOnSubject = false;
                for (int n = 0; n < studentTeachers.size(); n++) {
                    if (studentTeachers.get(n).getTeacherEmail().equalsIgnoreCase(teacherEmail)) {
                        ps.executeUpdate();
                        JOptionPane.showMessageDialog(null, "Student successfully removed from teacher");
                        isOnSubject = true;
                        break;
                    }
                }

                if (!isOnSubject) {
                    JOptionPane.showMessageDialog(null, "Student is not assigned to this teacher");
                    return;
                }

            } catch (Exception e) {
                //JOptionPane.showMessageDialog(null, "Action failed");
                e.printStackTrace();
            }
        } else {
            if (studentUsername == null) {
                JOptionPane.showMessageDialog(null, "Incorrect student e-mail");
            }

            if (teacherUsername == null) {
                JOptionPane.showMessageDialog(null, "Incorrect teacher e-mail");
            }
        }
    }

    public ArrayList<String> getTeachersOnSubject(String subjectTitle) {

        ArrayList<String> teachersOnSubject = new ArrayList<>();

        int subjectId = getSubjectId(subjectTitle);

        String query = "SELECT teacher_username FROM teachers_subjects WHERE subject_id = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, subjectId);

            rs = pst.executeQuery();

            while (rs.next()) {
                String teacherUsername = rs.getString("teacher_username");

                teachersOnSubject.add(teacherUsername);
            }
            return teachersOnSubject;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Integer> getSubjectsAssignedToTeacher(String teacherEmail) {

        ArrayList<Integer> subjectTitles = new ArrayList<>();

        String teacherUsername = getTeacherUsername(teacherEmail);

        String query = "SELECT subject_id FROM teachers_subjects WHERE teacher_username = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, teacherUsername);

            rs = pst.executeQuery();

            while (rs.next()) {
                int subjectId = rs.getInt(1);

                subjectTitles.add(subjectId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subjectTitles;
    }

    public ArrayList<Integer> getSubjectsAssignedToStudent(String studentEmail) {

        ArrayList<Integer> subjectTitles = new ArrayList<>();

        String studentUsername = getStudentUsername(studentEmail);

        String query = "SELECT subject_id FROM students_subjects WHERE student_username = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, studentUsername);

            rs = pst.executeQuery();

            while (rs.next()) {
                int subjectId = rs.getInt(1);

                subjectTitles.add(subjectId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subjectTitles;
    }

    public boolean verifySubjectStudent(String subjectTitle, String studentEmail) {

        String studentUsername = getStudentUsername(studentEmail);

        ArrayList<String> teachersOnSubject = getTeachersOnSubject(subjectTitle);

        boolean test = false;

        for (String s : teachersOnSubject) {
            String query = "SELECT * FROM students_teachers WHERE student_username = ? AND teacher_username = ?";

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, studentUsername);
                pst.setString(2, s);

                rs = pst.executeQuery();

                if (rs.next()) {
                    test = true;
                    return test;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return test;
    }


    public String getStudentUsername(String studentEmail) {

        String studentUsername = null;

        String query = "SELECT student_username FROM students WHERE student_email = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, studentEmail);

            rs = pst.executeQuery();

            if (rs.next()) {
                studentUsername = rs.getString("student_username");
            }
            return studentUsername;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean addNewTeacher(TextField tf_teacherUsername, TextField tf_teacherEmail, TextField tf_teacherPassword, TextField tf_teacherName, TextField tf_teacherSurname, TextField tf_teacherTitle) {

        String teacherUsername = tf_teacherUsername.getText().trim();
        String teacherEmail = tf_teacherEmail.getText().trim();
        String teacherPassword = tf_teacherPassword.getText().trim();
        String hashTeacherPassword = hashPassword(teacherPassword);
        String teacherName = tf_teacherName.getText().trim();
        String teacherSurname = tf_teacherSurname.getText().trim();
        String teacherTitle = tf_teacherTitle.getText().trim();
        int teacherTitleId = 0;
        if (teacherTitle.equalsIgnoreCase("Professor")) {
            teacherTitleId = 1;
        } else if (teacherTitle.equalsIgnoreCase("Assistant")) {
            teacherTitleId = 2;
        } else if (teacherTitle.equalsIgnoreCase("Demonstrator")) {
            teacherTitleId = 3;
        }

        if ((teacherUsername == null || teacherUsername.equals("")) || (teacherEmail == null || teacherEmail.equals("")) || (teacherPassword == null || teacherPassword.equals("")) ||
                (teacherName == null || teacherName.equals("")) || (teacherSurname == null || teacherSurname.equals("")) || (teacherTitle == null || teacherTitle.equals(""))) {
            JOptionPane.showMessageDialog(null, "All fields must be populated");
            return false;
        }

        if (teacherTitleId == 0) {
            JOptionPane.showMessageDialog(null, "Incorrect teacher title");
            return false;
        }

        if ((userExist(teacherEmail)) || userExist(teacherUsername)) {

            if (userExist(teacherEmail)) {
                JOptionPane.showMessageDialog(null, "User with that e-mail already exists");
                return false;
            }
            if (userExist(teacherUsername)) {
                JOptionPane.showMessageDialog(null, "User with that username already exists");
                return false;
            }
        } else {

            String query = "INSERT INTO teachers (teacher_username,teacher_email,teacher_password,teacher_name,teacher_surname,teacher_category_id) VALUES (?,?,?,?,?,?);";

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, teacherUsername);
                pst.setString(2, teacherEmail);
                pst.setString(3, hashTeacherPassword);
                pst.setString(4, teacherName);
                pst.setString(5, teacherSurname);
                pst.setInt(6, teacherTitleId);

                int i = pst.executeUpdate();

                if (i != 0) {
                    JOptionPane.showMessageDialog(null, "Teacher successfully added");
                    return true;
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful");
                return false;
            }

        }
        return false;
    }

    public boolean deleteTeacher(String teacherEmail) {

        if (teacherEmail == null || teacherEmail.equals((""))) {
            JOptionPane.showMessageDialog(null, "Please enter teacher e-mail");
            return false;
        }

        String query = "DELETE FROM teachers WHERE teacher_email = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, teacherEmail);

            int i = pst.executeUpdate();
            if (i != 0) {
                JOptionPane.showMessageDialog(null, "Teacher successfully deleted");
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Teacher with that e-mail does not exist");
                return false;
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Action failed");
        }
        return false;
    }

    public boolean updateTeacher(TextField tf_teacherUsername, TextField tf_teacherEmail, TextField tf_teacherName, TextField tf_teacherSurname,
                              TextField tf_teacherPassword, TextField tf_teacherTitle) {

        ObservableList<Teacher> teacher = getTeacherByUsername(tf_teacherUsername.getText());
        Teacher teacher1;

        try {
            teacher1 = teacher.get(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No teacher exists with that username");
            return false;
        }

        String teacherUsername = tf_teacherUsername.getText().trim();
        String teacherEmail = tf_teacherEmail.getText().trim();
        String teacherPassword = tf_teacherPassword.getText().trim();
        String hashTeacherPassword = "";
        if (teacherPassword != null && (!teacherPassword.equals(""))) {
            hashTeacherPassword = hashPassword(teacherPassword);
        }
        String teacherName = tf_teacherName.getText().trim();
        String teacherSurname = tf_teacherSurname.getText().trim();
        String teacherTitle = tf_teacherTitle.getText().trim();
        int teacherTitleId = 0;
        if (teacherTitle.equalsIgnoreCase("Professor")) {
            teacherTitleId = 1;
        } else if (teacherTitle.equalsIgnoreCase("Assistant")) {
            teacherTitleId = 2;
        } else if (teacherTitle.equalsIgnoreCase("Demonstrator")) {
            teacherTitleId = 3;
        }

        if (teacherUsername == null || teacherUsername.equals("")) {
            JOptionPane.showMessageDialog(null, "Username field must be populated");
            return false;
        }

        if ((teacherEmail == null || teacherEmail.equals("")) && (teacherPassword == null || teacherPassword.equals("")) &&
                (teacherName == null || teacherName.equals("")) && (teacherSurname == null || teacherSurname.equals("")) && (teacherTitle == null || teacherTitle.equals(""))) {
            JOptionPane.showMessageDialog(null, "All teacher data stayed the same");
            return false;
        }

        if ((teacherEmail.equals(teacher1.getTeacherEmail())) && ((hashTeacherPassword.equals(teacher1.getTeacherPassword())) || (teacherPassword.equals(""))) &&
                (teacherName.equals(teacher1.getTeacherName())) && (teacherSurname.equals(teacher1.getTeacherSurname())) && (teacherTitle.equals(teacher1.getTeacherTitleDescription()))) {
            JOptionPane.showMessageDialog(null, "All teacher data stayed the same");
            return false;
        }

        if (teacherTitleId == 0) {
            JOptionPane.showMessageDialog(null, "Incorrect teacher title");
            return false;
        } else if (teacherPassword != null && (!teacherPassword.equals(""))) {

            String query = "UPDATE teachers SET teacher_email = ?, teacher_name = ?, teacher_surname = ?, teacher_password = ?, " +
                    "teacher_category_id = ? WHERE teacher_username = ?;";

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, teacherEmail);
                pst.setString(2, teacherName);
                pst.setString(3, teacherSurname);
                pst.setString(4, hashTeacherPassword);
                pst.setInt(5, teacherTitleId);
                pst.setString(6, teacherUsername);

                int i = pst.executeUpdate();

                if (i != 0) {
                    JOptionPane.showMessageDialog(null, "Teacher data successfully updated");
                    return true;
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful");
            }

        } else if (teacherPassword == null || (teacherPassword.equals(""))) {

            String query = "UPDATE teachers SET teacher_email = ?, teacher_name = ?, teacher_surname = ?, " +
                    "teacher_category_id = ? WHERE teacher_username = ?;";

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, teacherEmail);
                pst.setString(2, teacherName);
                pst.setString(3, teacherSurname);
                pst.setInt(4, teacherTitleId);
                pst.setString(5, teacherUsername);

                int i = pst.executeUpdate();

                if (i != 0) {
                    JOptionPane.showMessageDialog(null, "Teacher data successfully updated");
                    return true;
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful");
            }
        }
        return true;
    }

    public boolean userExist(String userEmailOrPassword) {

        String query = "SELECT * FROM users WHERE user_email = ? OR user_username = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, userEmailOrPassword);
            pst.setString(2, userEmailOrPassword);

            rs = pst.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public ObservableList<Student> getStudents() {

        studentList = FXCollections.observableArrayList();

        String query = "SELECT * FROM students ORDER BY student_surname";

        try {
            pst = connect.prepareStatement(query);

            rs = pst.executeQuery();

            while (rs.next()) {
                String studentName = rs.getString("student_name");
                String studentSurname = rs.getString("student_surname");
                String studentEmail = rs.getString("student_email");
                String studentPassword = rs.getString("student_password");
                String studentUsername = rs.getString("student_username");

                Student student = new Student(studentUsername, studentEmail, studentName, studentSurname, studentPassword);
                studentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentList;
    }

    public ObservableList<Student> getStudentByUsername(String studentUsername) {

        studentList = FXCollections.observableArrayList();

        String query = "SELECT student_username, student_email, student_name, student_surname, student_password FROM " +
                "students WHERE student_username = ? ORDER BY student_surname";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, studentUsername);
            rs = pst.executeQuery();

            if (rs.next()) {
                String studentUsername1 = rs.getString(1);
                String studentEmail = rs.getString(2);
                String studentName = rs.getString(3);
                String studentSurname = rs.getString(4);
                String studentPassword = rs.getString(5);

                Student student = new Student(studentUsername1, studentEmail, studentName, studentSurname, studentPassword);
                studentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentList;
    }

    public ObservableList<Student> getStudentsAssignedToTeacher(String teacherEmail) {

        studentList = FXCollections.observableArrayList();

        String teacherUsername = getTeacherUsername(teacherEmail);

        String query = "SELECT student_name, student_surname, student_email FROM students_teachers JOIN students ON " +
                "students_teachers.student_username = students.student_username WHERE students_teachers.teacher_username = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, teacherUsername);

            rs = pst.executeQuery();

            while (rs.next()) {
                String studentUsername = "";
                String student_email = rs.getString("student_email");
                String student_name = rs.getString("student_name");
                String student_surname = rs.getString("student_surname");
                String studentPassword = "";

                Student student = new Student(studentUsername, student_email, student_name, student_surname, studentPassword);
                studentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentList;
    }

    public boolean addNewStudent(TextField tf_studentUsername, TextField tf_studentEmail, TextField tf_studentPassword, TextField tf_studentName, TextField tf_studentSurname) {

        String studentUsername = tf_studentUsername.getText().trim();
        String studentEmail = tf_studentEmail.getText().trim();
        String studentPassword = tf_studentPassword.getText().trim();
        String hashStudentPassword = "";
        if (studentPassword != null || (!studentPassword.equals(""))) {
            hashStudentPassword = hashPassword(studentPassword);
        }
        String studentName = tf_studentName.getText().trim();
        String studentSurname = tf_studentSurname.getText().trim();

        if ((studentUsername == null || studentUsername.equals("")) || (studentEmail == null || studentEmail.equals("")) || (studentPassword == null || studentPassword.equals("")) ||
                (studentName == null || studentName.equals("")) || (studentSurname == null || studentSurname.equals(""))) {
            JOptionPane.showMessageDialog(null, "All field must be populated");
            return false;
        }

        if ((userExist(studentEmail)) || userExist(studentUsername)) {

            if (userExist(studentEmail)) {
                JOptionPane.showMessageDialog(null, "User with that e-mail already exists");
                return false;
            }
            if (userExist(studentUsername)) {
                JOptionPane.showMessageDialog(null, "User with that username already exists");
                return false;
            }
        } else {

            String query = "INSERT INTO students (student_username, student_email,student_password,student_name,student_surname) VALUES (?,?,?,?,?);";

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, studentUsername);
                pst.setString(2, studentEmail);
                pst.setString(3, hashStudentPassword);
                pst.setString(4, studentName);
                pst.setString(5, studentSurname);

                int i = pst.executeUpdate();

                if (i != 0) {
                    JOptionPane.showMessageDialog(null, "Student successfully added");
                    return true;
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful");
                return false;
            }

        }
        return false;
    }

    public boolean deleteStudent(String studentEmail) {

        if (studentEmail == null || studentEmail.equals("")) {
            JOptionPane.showMessageDialog(null, "Please enter student e-mail");
            return false;
        }

        String query = "DELETE FROM students WHERE student_email = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, studentEmail);

            int i = pst.executeUpdate();
            if (i != 0) {
                JOptionPane.showMessageDialog(null, "Student successfully deleted");
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Student with that e-mail does not exist");
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Action failed");
        }
        return false;
    }

    public boolean updateStudent(TextField tf_studentUsername, TextField tf_studentEmail, TextField tf_studentName, TextField tf_studentSurname,
                              TextField tf_studentPassword) {

        ObservableList<Student> student = getStudentByUsername(tf_studentUsername.getText());
        Student student1;

        try {
            student1 = student.get(0);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No student exists with that username");
            return false;
        }

        String studentUsername = tf_studentUsername.getText().trim();
        String studentEmail = tf_studentEmail.getText().trim();
        String studentPassword = tf_studentPassword.getText().trim();
        String hashStudentPassword = "";
        if (studentPassword != null || (!studentPassword.equals(""))) {
            hashStudentPassword = hashPassword(studentPassword);
        }
        String studentName = tf_studentName.getText().trim();
        String studentSurname = tf_studentSurname.getText().trim();

        if (studentUsername == null || studentUsername.equals("")) {
            JOptionPane.showMessageDialog(null, "Username field must be populated");
            return false;
        }

        if ((studentEmail == null || studentEmail.equals("")) && (studentPassword == null || studentPassword.equals("")) &&
                (studentName == null || studentName.equals("")) && (studentSurname == null || studentSurname.equals(""))) {
            JOptionPane.showMessageDialog(null, "All student data stayed the same");
            return false;
        }

        if ((studentEmail.equals(student1.getStudentEmail())) && ((hashStudentPassword.equals(student1.getStudentPassword())) || (studentPassword.equals(""))) &&
                (studentName.equals(student1.getStudentName())) && (studentSurname.equals(student1.getStudentSurname()))) {
            JOptionPane.showMessageDialog(null, "All student data stayed the same");
            return false;
        }

        if (studentPassword != null && (!studentPassword.equals(""))) {

            String query = "UPDATE students SET student_email = ?, student_name = ?, student_surname = ?, student_password = ? " +
                    "WHERE student_username = ?;";

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, studentEmail);
                pst.setString(2, studentName);
                pst.setString(3, studentSurname);
                pst.setString(4, hashStudentPassword);
                pst.setString(5, studentUsername);

                int i = pst.executeUpdate();

                if (i != 0) {
                    JOptionPane.showMessageDialog(null, "Student data successfully updated");
                    return true;
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful");
            }

        } else if (studentPassword == null || (studentPassword.equals(""))) {

            String query = "UPDATE students SET student_email = ?, student_name = ?, student_surname = ? " +
                    "WHERE student_username = ?;";

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, studentEmail);
                pst.setString(2, studentName);
                pst.setString(3, studentSurname);
                pst.setString(4, studentUsername);

                int i = pst.executeUpdate();

                if (i != 0) {
                    JOptionPane.showMessageDialog(null, "Student data successfully updated");
                    return true;
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful");
            }
        }
        return false;
    }

    public ObservableList<Student> getSubjectStudents(String subjectTitle) {

        subjectStudentsList = FXCollections.observableArrayList();

        int subjectId = getSubjectId(subjectTitle);

        String query = "SELECT student_name, student_surname, student_email FROM " +
                "subjects JOIN students_subjects ON subjects.subject_id = students_subjects.subject_id JOIN " +
                "students ON students_subjects.student_username = students.student_username WHERE subjects.subject_id = ? " +
                "ORDER BY student_surname";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, subjectId);
            rs = pst.executeQuery();

            while (rs.next()) {
                String studentName = rs.getString(1);
                String studentSurname = rs.getString(2);
                String studentEmail = rs.getString(3);
                String studentPassword = null;
                String studentUsername = null;

                Student student = new Student(studentUsername, studentEmail, studentName, studentSurname, studentPassword);
                subjectStudentsList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return subjectStudentsList;
    }

    public ObservableList<Subject> getStudentSubjects(String studentEmail) {

        studentSubjectsList = FXCollections.observableArrayList();

        String studentUsername = getStudentUsername(studentEmail);

        String query = "SELECT subject_title FROM " +
                "students_subjects JOIN subjects ON students_subjects.subject_id = subjects.subject_id " +
                "WHERE students_subjects.student_username = ? ORDER BY subject_title";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, studentUsername);
            rs = pst.executeQuery();

            while (rs.next()) {
                String subjectTitle = rs.getString(1);

                Subject subject = new Subject(subjectTitle);
                studentSubjectsList.add(subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentSubjectsList;
    }



    public void addNewQuestionnaire(String data) {

        if (getSubjectId(data) != 0){

             String query = "INSERT INTO questionnaires (subject_id) VALUES (?)";

            int subjectId = getSubjectId(data);

            try {
                pst = connect.prepareStatement(query);
                pst.setInt(1, subjectId);

                pst.executeUpdate();
                JOptionPane.showMessageDialog(null, "Questionnaire for subject successfully added");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful");
            }
        }

        if (getTeacherUsername(data) != null) {

            String query = "INSERT INTO questionnaires (teacher_username) VALUES (?)";

            String teacherUsername = getTeacherUsername(data);

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, teacherUsername);

                pst.executeUpdate();
                JOptionPane.showMessageDialog(null, "Questionnaire for teacher successfully added");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful");
            }
        }
    }

    public void deleteQuestionnaire(String data) {
        if (getSubjectId(data) != 0) {

            String query = "DELETE FROM questionnaires WHERE subject_id = ?";

            int subjectId = getSubjectId(data);

            try {
                pst = connect.prepareStatement(query);
                pst.setInt(1, subjectId);

                pst.executeUpdate();
                JOptionPane.showMessageDialog(null, "Questionnaire for subject successfully removed");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful");
            }
        }

        if (getTeacherUsername(data) != null) {

            String query = "DELETE FROM questionnaires WHERE teacher_username = ?";

            String teacherUsername = getTeacherUsername(data);

            try {
                pst = connect.prepareStatement(query);
                pst.setString(1, teacherUsername);

                pst.executeUpdate();
                JOptionPane.showMessageDialog(null, "Questionnaire for teacher successfully removed");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Action not successful");
            }
        }
    }

    public void addNewTest(String subjectTitle) {

        int subjectId = getSubjectId(subjectTitle);

        String query = "INSERT INTO tests (subject_id) VALUES (?)";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, subjectId);

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Test for subject successfully added");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Action not successful");
        }
    }

    public void deleteTest(String subjectTitle) {

        int subjectId = getSubjectId(subjectTitle);

        String query = "DELETE FROM tests WHERE subject_id = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, subjectId);

            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "Test successfully deleted");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Action not successful");
        }
    }

    public ObservableList<Subject> getSubjectQuestionnaire(String subjectTitle) {

        if (subjectTitle != null) {
            subjectList = getSubjects(subjectTitle);
        } else if (subjectTitle == null) {
            subjectList = getSubjects();
        }

        ObservableList<Subject> removeList = FXCollections.observableArrayList();

        for (Subject s : subjectList) {
            if (!verifySubjectQuestionnaire(s.getSubjectTitle())) {
                removeList.add(s);
            }
        }

        removeList.forEach(subjectList::remove);
        return subjectList;
    }

    public ObservableList<Subject> getSubjectTest(String subjectTitle) {

        if (subjectTitle != null) {
            subjectList = getSubjects(subjectTitle);
        } else if (subjectTitle == null) {
            subjectList = getSubjects();
        }

        ObservableList<Subject> removeList = FXCollections.observableArrayList();

        for (Subject s : subjectList) {
            if (!verifySubjectTest(s.getSubjectTitle())) {
                removeList.add(s);
            }
        }

        removeList.forEach(subjectList::remove);
        return subjectList;
    }

    public ObservableList<Student> getSubjectQuestionnaireStudents(String subjectTitle) {
        studentList = FXCollections.observableArrayList();

        int questionnaireId = getQuestionnaireId(subjectTitle);

        String query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_questionnaires JOIN " +
                "students ON students_questionnaires.student_username = students.student_username WHERE questionnaire_id = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, questionnaireId);
            rs = pst.executeQuery();

            while (rs.next()) {
                String studentUsername = rs.getString(1);
                String studentEmail = rs.getString(2);
                String studentName = rs.getString(3);
                String studentSurname = rs.getString(4);
                String studentPassword = rs.getString(5);

                Student student = new Student(studentUsername, studentEmail, studentName, studentSurname, studentPassword);

                studentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return studentList;
    }

    public ObservableList<Student> getSubjectQuestionnaireStudents(String subjectTitle, String searchCriteria, String criteriaData) {
        studentList = FXCollections.observableArrayList();

        int questionnaireId = getQuestionnaireId(subjectTitle);

        String query = "";

        if (searchCriteria.equalsIgnoreCase("Student name")) {
            query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_questionnaires JOIN " +
                    "students ON students_questionnaires.student_username = students.student_username WHERE questionnaire_id = ? AND students.student_name LIKE '" + criteriaData + "%' ORDER BY students.student_surname";
        } else if (searchCriteria.equalsIgnoreCase("Student surname")) {
            query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_questionnaires JOIN " +
                    "students ON students_questionnaires.student_username = students.student_username WHERE questionnaire_id = ? AND students.student_surname LIKE '" + criteriaData + "%' ORDER BY students.student_surname";
        } else if (searchCriteria.equalsIgnoreCase("Student e-mail")) {
            query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_questionnaires JOIN " +
                    "students ON students_questionnaires.student_username = students.student_username WHERE questionnaire_id = ? AND students.student_email LIKE '" + criteriaData + "%' ORDER BY students.student_surname";
        }

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, questionnaireId);
            rs = pst.executeQuery();

            while (rs.next()) {
                String studentUsername = rs.getString(1);
                String studentEmail = rs.getString(2);
                String studentName = rs.getString(3);
                String studentSurname = rs.getString(4);
                String studentPassword = rs.getString(5);

                Student student = new Student(studentUsername, studentEmail, studentName, studentSurname, studentPassword);

                studentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return studentList;
    }

    public ObservableList<Student> getTeacherQuestionnaireStudents(String teacherEmail) {
        studentList = FXCollections.observableArrayList();

        int questionnaireId = getQuestionnaireId(teacherEmail);

        String query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_questionnaires JOIN " +
                "students ON students_questionnaires.student_username = students.student_username WHERE questionnaire_id = ? ORDER BY student_surname";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, questionnaireId);
            rs = pst.executeQuery();

            while (rs.next()) {
                String studentUsername = rs.getString(1);
                String studentEmail = rs.getString(2);
                String studentName = rs.getString(3);
                String studentSurname = rs.getString(4);
                String studentPassword = rs.getString(5);

                Student student = new Student(studentUsername, studentEmail, studentName, studentSurname, studentPassword);

                studentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return studentList;
    }

    public ObservableList<Student> getTeacherQuestionnaireStudents(String teacherEmail, String searchCriteria, String criteriaData) {
        studentList = FXCollections.observableArrayList();

        int questionnaireId = getQuestionnaireId(teacherEmail);

        String query = "";

        if (searchCriteria.equalsIgnoreCase("Student name")) {
            query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_questionnaires JOIN " +
                    "students ON students_questionnaires.student_username = students.student_username WHERE questionnaire_id = ? AND students.student_name LIKE '" + criteriaData + "%' ORDER BY students.student_surname";
        } else if (searchCriteria.equalsIgnoreCase("Student surname")) {
            query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_questionnaires JOIN " +
                    "students ON students_questionnaires.student_username = students.student_username WHERE questionnaire_id = ? AND students.student_surname LIKE '" + criteriaData + "%' ORDER BY students.student_surname";
        } else if (searchCriteria.equalsIgnoreCase("Student e-mail")) {
            query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_questionnaires JOIN " +
                    "students ON students_questionnaires.student_username = students.student_username WHERE questionnaire_id = ? AND students.student_email LIKE '" + criteriaData + "%' ORDER BY students.student_surname";
        }

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, questionnaireId);
            rs = pst.executeQuery();

            while (rs.next()) {
                String studentUsername = rs.getString(1);
                String studentEmail = rs.getString(2);
                String studentName = rs.getString(3);
                String studentSurname = rs.getString(4);
                String studentPassword = rs.getString(5);

                Student student = new Student(studentUsername, studentEmail, studentName, studentSurname, studentPassword);

                studentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return studentList;
    }

    public ObservableList<Student> getSubjectTestStudents(String subjectTitle) {
        studentList = FXCollections.observableArrayList();

        int testId = getTestId(subjectTitle);

        String query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_tests JOIN " +
                    "students ON students_tests.student_username = students.student_username WHERE test_id = ? ORDER BY student_surname";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, testId);
            rs = pst.executeQuery();

            while (rs.next()) {
                String studentUsername = rs.getString(1);
                String studentEmail = rs.getString(2);
                String studentName = rs.getString(3);
                String studentSurname = rs.getString(4);
                String studentPassword = rs.getString(5);

                Student student = new Student(studentUsername, studentEmail, studentName, studentSurname, studentPassword);

                studentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return studentList;
    }

    public ObservableList<Student> getSubjectTestStudents(String subjectTitle, String searchCriteria, String criteriaData) {
        studentList = FXCollections.observableArrayList();

        int testId = getTestId(subjectTitle);

        String query = "";

        if (searchCriteria.equalsIgnoreCase("Student name")) {
            query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_tests JOIN " +
                    "students ON students_tests.student_username = students.student_username WHERE test_id = ? AND students.student_name LIKE '" + criteriaData + "%' ORDER BY students.student_surname";
        } else if (searchCriteria.equalsIgnoreCase("Student surname")) {
            query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_tests JOIN " +
                    "students ON students_tests.student_username = students.student_username WHERE test_id = ? AND students.student_surname LIKE '" + criteriaData + "%' ORDER BY students.student_surname";
        } else if (searchCriteria.equalsIgnoreCase("Student e-mail")) {
            query = "SELECT students.student_username, student_email, student_name, student_surname, student_password FROM students_tests JOIN " +
                    "students ON students_tests.student_username = students.student_username WHERE test_id = ? AND students.student_email LIKE '" + criteriaData + "%' ORDER BY students.student_surname";
        }

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, testId);
            rs = pst.executeQuery();

            while (rs.next()) {
                String studentUsername = rs.getString(1);
                String studentEmail = rs.getString(2);
                String studentName = rs.getString(3);
                String studentSurname = rs.getString(4);
                String studentPassword = rs.getString(5);

                Student student = new Student(studentUsername, studentEmail, studentName, studentSurname, studentPassword);

                studentList.add(student);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return studentList;
    }

    public ObservableList<Teacher> getTeacherQuestionnaire(String teacherTitle) {

        if (teacherTitle != null) {
            if (teacherTitle.equals("Professor")) {
                teachersList = getTeachersByTitle("Professor");
            } else if (teacherTitle.equals("Assistant")) {
                teachersList = getTeachersByTitle("Assistant");
            } else if(teacherTitle.equals("Demonstrator")) {
                teachersList = getTeachersByTitle("Demonstrator");
            }
        } else if (teacherTitle == null) {
            teachersList = getTeachers();
        }

        ObservableList<Teacher> removeList = FXCollections.observableArrayList();

        for (Teacher t : teachersList) {
            if (!verifyTeacherQuestionnaire(t.getTeacherEmail())) {
                removeList.add(t);
            }
        }

        removeList.forEach(teachersList::remove);
        return teachersList;
    }

    public boolean verifySubjectQuestionnaire(String subjectTitle) {

        int subjectId = getSubjectId(subjectTitle);

        String query = "SELECT questionnaire_id FROM questionnaires WHERE subject_id = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, subjectId);

            rs = pst.executeQuery();

            if (rs.next()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean verifySubjectTest(String subjectTitle) {

        int subjectId = getSubjectId(subjectTitle);

        String query = "SELECT test_id FROM tests WHERE subject_id = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, subjectId);

            rs = pst.executeQuery();

            if (rs.next()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean verifyTeacherQuestionnaire(String teacherEmail) {

        String teacherUsername = getTeacherUsername(teacherEmail);

        String query = "SELECT questionnaire_id FROM questionnaires WHERE teacher_username = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setString(1, teacherUsername);

            rs = pst.executeQuery();

            if (rs.next()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public ObservableList<Question> getQuestionnaireQuestions(String data) {

        questionList = FXCollections.observableArrayList();

        int questionnaireId = getQuestionnaireId(data);

        String query = "SELECT question_type_id, question_answer_rb, question_answer_chkbx, question_text FROM questions WHERE questionnaire_id = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, questionnaireId);

            rs = pst.executeQuery();

            while (rs.next()) {
                int questionTypeId = rs.getInt(1);
                String questionAnswerRb = rs.getString(2);
                String questionAnswerChkbx = rs.getString(3);
                String questionText = rs.getString(4);

                Question question = new Question(questionnaireId, questionTypeId, questionAnswerRb, questionAnswerChkbx, 0, questionText);

                questionList.add(question);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionList;
    }

    public ObservableList<Question> getQuestionnaireQuestions(String data, String questionType) {

        questionList = getQuestionnaireQuestions(data);

        ObservableList<Question> removeList = FXCollections.observableArrayList();

        for (Question q : questionList) {
            if (!q.getQuestionTypeDescription().equalsIgnoreCase(questionType)) {
                removeList.add(q);
            }
        }

        removeList.forEach(questionList::remove);

        return questionList;
    }

    public ObservableList<Question> getQuestionnaireQuestions(String data, String questionType, String text) {

        questionList = getQuestionnaireQuestions(data, questionType);

        ObservableList<Question> removeList = FXCollections.observableArrayList();

        for (Question q : questionList) {
            if (!q.getQuestionText().toLowerCase().contains(text)) {
                removeList.add(q);
            }
        }

        removeList.forEach(questionList::remove);

        return questionList;
    }

    public ObservableList<Question> getTestQuestions(String subjectTitle) {

        questionList = FXCollections.observableArrayList();

        int testId = getTestId(subjectTitle);

        String query = "SELECT question_type_id, question_answer_rb, question_answer_chkbx, question_text FROM questions WHERE test_id = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, testId);

            rs = pst.executeQuery();

            while (rs.next()) {
                int questionTypeId = rs.getInt(1);
                String questionAnswerRb = rs.getString(2);
                String questionAnswerChkbx = rs.getString(3);
                String questionText = rs.getString(4);

                Question question = new Question(0, questionTypeId, questionAnswerRb, questionAnswerChkbx, testId, questionText);

                questionList.add(question);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionList;
    }

    public ObservableList<Question> getTestQuestions(String subjectTitle, String questionType) {

        questionList = getTestQuestions(subjectTitle);

        ObservableList<Question> removeList = FXCollections.observableArrayList();

        for (Question q : questionList) {
            if (!q.getQuestionTypeDescription().equalsIgnoreCase(questionType)) {
                removeList.add(q);
            }
        }

        removeList.forEach(questionList::remove);

        return questionList;
    }

    public ObservableList<Question> getTestQuestions(String subjectTitle, String questionType, String text) {

        questionList = getTestQuestions(subjectTitle, questionType);

        ObservableList<Question> removeList = FXCollections.observableArrayList();

        for (Question q : questionList) {
            if (!q.getQuestionText().toLowerCase().contains(text)) {
                removeList.add(q);
            }
        }

        removeList.forEach(questionList::remove);

        return questionList;
    }

    public boolean addQuestionnaireQuestion(String data, String questionType,
                                TextField tf_answer1, TextField tf_answer2, TextField tf_answer3, TextField tf_answer4, TextField tf_answer5,
                                ArrayList<Integer> correctAnswers, String questionText) {

        if ((data == null || (data.equals(""))) || (questionType == null || (questionType.equals(""))) || (questionText == null || (questionText.equals("")))) {
            JOptionPane.showMessageDialog(null, "All required fields must be populated");
            return false;
        }

        if (questionType.toLowerCase().equals("radio button") || questionType.toLowerCase().equals("check box")) {
            if ((tf_answer1.getText() == null || (tf_answer1.getText().equals(""))) || (tf_answer2.getText() == null || (tf_answer2.getText().equals(""))) ||
                    (tf_answer3.getText() == null || (tf_answer3.getText().equals(""))) || (tf_answer4.getText() == null || (tf_answer4.getText().equals(""))) ||
                    (tf_answer5.getText() == null || (tf_answer5.getText().equals(""))) || correctAnswers == null) {
                JOptionPane.showMessageDialog(null, "All required fields must be populated");
                return false;
            }
        }

        int questionnaireId = getQuestionnaireId(data);

        int questionTypeId = 0;
        String answers = "";

        String query = "";
        if (questionType.toLowerCase().equals("radio button")) {
            questionTypeId = 1;
            answers = constructTestAnswers(tf_answer1, tf_answer2, tf_answer3, tf_answer4, tf_answer5, correctAnswers);
            query = "INSERT INTO questions (question_type_id, questionnaire_id, question_answer_rb, question_text) VALUES (?,?,?,?)";
        } else if (questionType.toLowerCase().equals("check box")) {
            questionTypeId = 2;
            answers = constructTestAnswers(tf_answer1, tf_answer2, tf_answer3, tf_answer4, tf_answer5, correctAnswers);
            query = "INSERT INTO questions (question_type_id, questionnaire_id, question_answer_chkbx, question_text) VALUES (?,?,?,?)";
        } else if (questionType.toLowerCase().equals("write answer")) {
            questionTypeId = 3;
            query = "INSERT INTO questions (question_type_id, questionnaire_id, question_text) VALUES (?,?,?)";
        }

        try {
            pst = connect.prepareStatement(query);

            pst.setInt(1, questionTypeId);
            pst.setInt(2, questionnaireId);
            if (questionTypeId == 1 || questionTypeId == 2) {
                pst.setString(3, answers);
                pst.setString(4, questionText);
            } else if (questionTypeId == 3) {
                pst.setString(3, questionText);
            }

            int i = pst.executeUpdate();

            if (i != 0) {
                JOptionPane.showMessageDialog(null, "Questionnaire question successfully added");
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Action not successful");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void deleteQuestionnaireQuestion(String data, String questionText) {

        int questionnaireId = getQuestionnaireId(data);

        String query = "DELETE FROM questions WHERE questionnaire_id = ? AND question_text = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, questionnaireId);
            pst.setString(2, questionText);

            int i = pst.executeUpdate();

            if (i != 0) {
                JOptionPane.showMessageDialog(null, "Questionnaire question successfully deleted");
            } else {
                JOptionPane.showMessageDialog(null, "Action failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean updateQuestionnaireQuestion(String data, String questionType,
                                         TextField tf_answer1, TextField tf_answer2, TextField tf_answer3, TextField tf_answer4, TextField tf_answer5,
                                         ArrayList<Integer> correctAnswers, String oldQuestionText, String newQuestionText) {

        int questionnaireId = getQuestionnaireId(data);

        int questionTypeId = 0;
        String answers = "";

        String query = "";
        if (questionType.toLowerCase().equals("radio button")) {
            questionTypeId = 1;
            answers = constructTestAnswers(tf_answer1, tf_answer2, tf_answer3, tf_answer4, tf_answer5, correctAnswers);
            query = "UPDATE questions SET question_answer_rb = ?, question_text = ?" +
                    "WHERE questionnaire_id = ? AND question_type_id = ? AND question_text = ?";
        } else if (questionType.toLowerCase().equals("check box")) {
            questionTypeId = 2;
            answers = constructTestAnswers(tf_answer1, tf_answer2, tf_answer3, tf_answer4, tf_answer5, correctAnswers);
            query = "UPDATE questions SET question_answer_chkbx = ?, question_text = ?" +
                    "WHERE questionnaire_id = ? AND question_type_id = ? AND question_text = ?";
        } else if (questionType.toLowerCase().equals("write answer")) {
            questionTypeId = 3;
            query = "UPDATE questions SET question_text = ?" +
                    "WHERE questionnaire_id = ? AND question_type_id = ? AND question_text = ?";
        }

        if ((data == null || (data.equals(""))) || (questionType == null || (questionType.equals(""))) || (newQuestionText == null || (newQuestionText.equals("")))) {
            JOptionPane.showMessageDialog(null, "All required fields must be populated");
            return false;
        }

        if (questionType.toLowerCase().equals("radio button") || questionType.toLowerCase().equals("check box")) {
            if ((tf_answer1.getText() == null || (tf_answer1.getText().equals(""))) || (tf_answer2.getText() == null || (tf_answer2.getText().equals(""))) ||
                    (tf_answer3.getText() == null || (tf_answer3.getText().equals(""))) || (tf_answer4.getText() == null || (tf_answer4.getText().equals(""))) ||
                    (tf_answer5.getText() == null || (tf_answer5.getText().equals(""))) || correctAnswers == null) {
                JOptionPane.showMessageDialog(null, "All required fields must be populated");
                return false;
            }
        }

        try {
            pst = connect.prepareStatement(query);
            if (questionTypeId == 1 || questionTypeId == 2) {
                pst.setString(1, answers);
                pst.setString(2, newQuestionText);
                pst.setInt(3, questionnaireId);
                pst.setInt(4, questionTypeId);
                pst.setString(5, oldQuestionText);
            } else if (questionTypeId == 3) {
                pst.setString(1, newQuestionText);
                pst.setInt(2, questionnaireId);
                pst.setInt(3, questionTypeId);
                pst.setString(4, oldQuestionText);
            }

            int i = pst.executeUpdate();

            if (i != 0) {
                JOptionPane.showMessageDialog(null, "Questionnaire question successfully updated");
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Action not successful");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean addTestQuestion(String subjectTitle, String questionType,
                                         TextField tf_answer1, TextField tf_answer2, TextField tf_answer3, TextField tf_answer4, TextField tf_answer5,
                                         ArrayList<Integer> correctAnswers, String questionText) {

        if ((subjectTitle == null || (subjectTitle.equals(""))) || (questionType == null || (questionType.equals(""))) || (questionText == null || (questionText.equals("")))) {
            JOptionPane.showMessageDialog(null, "All required fields must be populated");
            return false;
        }

        if (questionType.toLowerCase().equals("radio button") || questionType.toLowerCase().equals("check box")) {
            if ((tf_answer1.getText() == null || (tf_answer1.getText().equals(""))) || (tf_answer2.getText() == null || (tf_answer2.getText().equals(""))) ||
                    (tf_answer3.getText() == null || (tf_answer3.getText().equals(""))) || (tf_answer4.getText() == null || (tf_answer4.getText().equals(""))) ||
                    (tf_answer5.getText() == null || (tf_answer5.getText().equals(""))) || correctAnswers == null) {
                JOptionPane.showMessageDialog(null, "All required fields must be populated");
                return false;
            }
            if (questionType.toLowerCase().equals("radio button")) {
                if (correctAnswers.size() != 1) {
                    JOptionPane.showMessageDialog(null, "You have to specify exactly one correct answer");
                    return false;
                }
            }
        }

        int testId = getTestId(subjectTitle);

        int questionTypeId = 0;
        String answers = "";

        String query = "";
        if (questionType.toLowerCase().equals("radio button")) {
            questionTypeId = 1;
            answers = constructTestAnswers(tf_answer1, tf_answer2, tf_answer3, tf_answer4, tf_answer5, correctAnswers);
            query = "INSERT INTO questions (question_type_id, test_id, question_answer_rb, question_text) VALUES (?,?,?,?)";
        } else if (questionType.toLowerCase().equals("check box")) {
            questionTypeId = 2;
            answers = constructTestAnswers(tf_answer1, tf_answer2, tf_answer3, tf_answer4, tf_answer5, correctAnswers);
            query = "INSERT INTO questions (question_type_id, test_id, question_answer_chkbx, question_text) VALUES (?,?,?,?)";
        } else if (questionType.toLowerCase().equals("write answer")) {
            questionTypeId = 3;
            query = "INSERT INTO questions (question_type_id, test_id, question_text) VALUES (?,?,?)";
        }

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, questionTypeId);
            pst.setInt(2, testId);
            if (questionTypeId == 1 || questionTypeId == 2) {
                pst.setString(3, answers);
                pst.setString(4, questionText);
            } else if (questionTypeId == 3) {
                pst.setString(3, questionText);
            }

            int i = pst.executeUpdate();

            if (i != 0) {
                JOptionPane.showMessageDialog(null, "Test question successfully added");
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Action not successful");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void deleteTestQuestion(String subjectTitle, String questionText) {

        int testId = getTestId(subjectTitle);

        String query = "DELETE FROM questions WHERE test_id = ? AND question_text = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, testId);
            pst.setString(2, questionText);

            int i = pst.executeUpdate();

            if (i != 0) {
                JOptionPane.showMessageDialog(null, "Test question successfully deleted");
            } else {
                JOptionPane.showMessageDialog(null, "Action failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean updateTestQuestion(String subjectTitle, String questionType,
                                            TextField tf_answer1, TextField tf_answer2, TextField tf_answer3, TextField tf_answer4, TextField tf_answer5,
                                            ArrayList<Integer> correctAnswers, String oldQuestionText, String newQuestionText) {

        int testId = getTestId(subjectTitle);

        int questionTypeId = 0;
        String answers = "";

        String query = "";
        if (questionType.toLowerCase().equals("radio button")) {
            questionTypeId = 1;
            answers = constructTestAnswers(tf_answer1, tf_answer2, tf_answer3, tf_answer4, tf_answer5, correctAnswers);
            query = "UPDATE questions SET question_answer_rb = ?, question_text = ? " +
                    "WHERE (test_id = ? AND question_type_id = ? AND question_text = ?)";
        } else if (questionType.toLowerCase().equals("check box")) {
            questionTypeId = 2;
            answers = constructTestAnswers(tf_answer1, tf_answer2, tf_answer3, tf_answer4, tf_answer5, correctAnswers);
            query = "UPDATE questions SET question_answer_chkbx = ?, question_text = ? " +
                    "WHERE (test_id = ? AND question_type_id = ? AND question_text = ?)";
        } else if (questionType.toLowerCase().equals("write answer")) {
            questionTypeId = 3;
            query = "UPDATE questions SET question_text = ? " +
                    "WHERE (test_id = ? AND question_type_id = ? AND question_text = ?)";
        }

        if ((subjectTitle == null || (subjectTitle.equals(""))) || (questionType == null || (questionType.equals(""))) || (newQuestionText == null || (newQuestionText.equals("")))) {
            JOptionPane.showMessageDialog(null, "All required fields must be populated");
            return false;
        }

        if (questionType.toLowerCase().equals("radio button") || questionType.toLowerCase().equals("check box")) {
            if ((tf_answer1.getText() == null || (tf_answer1.getText().equals(""))) || (tf_answer2.getText() == null || (tf_answer2.getText().equals(""))) ||
                    (tf_answer3.getText() == null || (tf_answer3.getText().equals(""))) || (tf_answer4.getText() == null || (tf_answer4.getText().equals(""))) ||
                    (tf_answer5.getText() == null || (tf_answer5.getText().equals(""))) || correctAnswers == null) {
                JOptionPane.showMessageDialog(null, "All required fields must be populated");
                return false;
            }
            if (questionType.toLowerCase().equals("radio button")) {
                if (correctAnswers.size() != 1) {
                    JOptionPane.showMessageDialog(null, "You have to specify exactly one correct answer");
                    return false;
                }
            }
        }

        try {
            pst = connect.prepareStatement(query);
            if (questionTypeId == 1 || questionTypeId == 2) {
                pst.setString(1, answers);
                pst.setString(2, newQuestionText);
                pst.setInt(3, testId);
                pst.setInt(4, questionTypeId);
                pst.setString(5, oldQuestionText);
            } else if (questionTypeId == 3) {
                pst.setString(1, newQuestionText);
                pst.setInt(2, testId);
                pst.setInt(3, questionTypeId);
                pst.setString(4, oldQuestionText);
            }

            int i = pst.executeUpdate();

            if (i != 0) {
                JOptionPane.showMessageDialog(null, "Test question successfully updated");
                return true;
            } else {
                JOptionPane.showMessageDialog(null, "Action not successful");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getQuestionnaireId(String data) {

        int questionnaireId = 0;

        String query = "SELECT questionnaire_id FROM questionnaires WHERE teacher_username = ?";

        String teacherUsername = getTeacherUsername(data);
        int subject_id = 0;

        if (teacherUsername == null) {
            query = "SELECT questionnaire_id FROM questionnaires WHERE subject_id = ?";
            subject_id = getSubjectId(data);
        }

        try {
            pst = connect.prepareStatement(query);
            if (teacherUsername != null) {
                pst.setString(1, teacherUsername);
                rs = pst.executeQuery();

                if (rs.next()) {
                    questionnaireId = rs.getInt(1);
                }
            } else {
                pst.setInt(1, subject_id);
                rs = pst.executeQuery();

                if (rs.next()) {
                    questionnaireId = rs.getInt(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionnaireId;
    }

    public int getTestId(String subjectTitle) {

        int testId = 0;
        int subjectId = getSubjectId(subjectTitle);

        String query = "SELECT test_id FROM tests WHERE subject_id = ?";

        try {
            pst = connect.prepareStatement(query);
            if (subjectId != 0) {
                pst.setInt(1, subjectId);
                rs = pst.executeQuery();

                if (rs.next()) {
                    testId = rs.getInt(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return testId;
    }

    public String constructTestAnswers(TextField tf_answer1, TextField tf_answer2, TextField tf_answer3, TextField tf_answer4, TextField tf_answer5, ArrayList<Integer> correctAnswers) {
        String answer1 = tf_answer1.getText().trim();
        String answer2 = tf_answer2.getText().trim();
        String answer3 = tf_answer3.getText().trim();
        String answer4 = tf_answer4.getText().trim();
        String answer5 = tf_answer5.getText().trim();

        for (int i = 0; i < correctAnswers.size(); i++) {
            if (correctAnswers.get(i) == 1) {
                answer1 = answer1 + "*";
            }
            if (correctAnswers.get(i) == 2) {
                answer2 = answer2 + "*";
            }
            if (correctAnswers.get(i) == 3) {
                answer3 = answer3 + "*";
            }
            if (correctAnswers.get(i) == 4) {
                answer4 = answer4 + "*";
            }
            if (correctAnswers.get(i) == 5) {
                answer5 = answer5 + "*";
            }
        }

        String answers = answer1 + ";" + answer2 + ";" + answer3 + ";" + answer4 + ";" + answer5;

        return answers;
    }

    public int getQuestionnaireQuestionId(String data, String questionText) {
        int questionnaireId = getQuestionnaireId(data);

        int questionId = 0;

        String query = "SELECT question_id FROM questions WHERE questionnaire_id = ? AND question_text = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, questionnaireId);
            pst.setString(2, questionText);

            rs = pst.executeQuery();

            if (rs.next()) {
                questionId = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionId;
    }

    public int getTestQuestionId(String subjectTitle, String questionText) {
        int testId = getTestId(subjectTitle);

        int questionId = 0;

        String query = "SELECT question_id FROM questions WHERE test_id = ? AND question_text = ?";

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, testId);
            pst.setString(2, questionText);

            rs = pst.executeQuery();

            if (rs.next()) {
                questionId = rs.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionId;
    }

    public String getQuestionnaireStudentAnswer(String data, String questionText, String studentEmail, String questionType) {

        String answer = "";

        int questionId = getQuestionnaireQuestionId(data, questionText);

        int questionnaireId = getQuestionnaireId(data);

        String studentUsername = getStudentUsername(studentEmail);

        String query = "";

        if (questionType.equalsIgnoreCase("radio button")) {
            query = "SELECT student_question_answer_rb FROM students_questions WHERE question_id = ? AND questionnaire_id = ? AND student_username = ?";
        } else if (questionType.equalsIgnoreCase("check box")) {
            query = "SELECT student_question_answer_chkbx FROM students_questions WHERE question_id = ? AND questionnaire_id = ? AND student_username = ?";
        } else if (questionType.equalsIgnoreCase("write answer")) {
            query = "SELECT student_question_answer_writeanswer FROM students_questions WHERE question_id = ? AND questionnaire_id = ? AND student_username = ?";
        }

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, questionId);
            pst.setInt(2, questionnaireId);
            pst.setString(3, studentUsername);

            rs = pst.executeQuery();
            if (rs.next()) {
                answer = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return answer;
    }

    public String getTestStudentAnswer(String subjectTitle, String questionText, String studentEmail, String questionType) {

        String answer = "";

        int questionId = getTestQuestionId(subjectTitle, questionText);

        int testId = getTestId(subjectTitle);

        String studentUsername = getStudentUsername(studentEmail);

        String query = "";

        if (questionType.equalsIgnoreCase("radio button")) {
            query = "SELECT student_question_answer_rb FROM students_questions WHERE question_id = ? AND test_id = ? AND student_username = ?";
        } else if (questionType.equalsIgnoreCase("check box")) {
            query = "SELECT student_question_answer_chkbx FROM students_questions WHERE question_id = ? AND test_id = ? AND student_username = ?";
        } else if (questionType.equalsIgnoreCase("write answer")) {
            query = "SELECT student_question_answer_writeanswer FROM students_questions WHERE question_id = ? AND test_id = ? AND student_username = ?";
        }

        try {
            pst = connect.prepareStatement(query);
            pst.setInt(1, questionId);
            pst.setInt(2, testId);
            pst.setString(3, studentUsername);

            rs = pst.executeQuery();
            if (rs.next()) {
                answer = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return answer;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public Scene getSceneLogin() {
        return sceneLogin;
    }

    public void setSceneLogin(Scene sceneLogin) {
        this.sceneLogin = sceneLogin;
    }

    public Scene getSceneWelcome() {
        return sceneWelcome;
    }

    public void setSceneWelcome(Scene scene) {
        this.sceneWelcome = scene;
    }

    public Scene getSceneStudents() {
        return sceneStudents;
    }

    public void setSceneStudents(Scene sceneStudents) {
        this.sceneStudents = sceneStudents;
    }

    public Scene getSceneTeachers() {
        return sceneTeachers;
    }

    public void setSceneTeachers(Scene sceneTeachers) {
        this.sceneTeachers = sceneTeachers;
    }

    public Scene getSceneSubjects() {
        return sceneSubjects;
    }

    public void setSceneSubjects(Scene sceneSubjects) {
        this.sceneSubjects = sceneSubjects;
    }

    public Scene getSceneQuestionnaires() {
        return sceneQuestionnaires;
    }

    public void setSceneQuestionnaires(Scene sceneQuestionnaires) {
        this.sceneQuestionnaires = sceneQuestionnaires;
    }

    public Scene getSceneQuestionnaireResults() {
        return sceneQuestionnaireResults;
    }

    public void setSceneQuestionnaireResults(Scene sceneQuestionnaireResults) {
        this.sceneQuestionnaireResults = sceneQuestionnaireResults;
    }

    public Scene getSceneQuestions() {
        return sceneQuestions;
    }

    public void setSceneQuestions(Scene sceneQuestions) {
        this.sceneQuestions = sceneQuestions;
    }

    public Scene getSceneTests() {
        return sceneTests;
    }

    public void setSceneTests(Scene sceneTests) {
        this.sceneTests = sceneTests;
    }

    public Scene getSceneTestResults() {
        return sceneTestResults;
    }

    public void setSceneTestResults(Scene sceneTestResults) {
        this.sceneTestResults = sceneTestResults;
    }

    public Scene getSceneSpecificQuestionnaire() {
        return sceneSpecificQuestionnaire;
    }

    public void setSceneSpecificQuestionnaire(Scene sceneSpecificQuestionnaire) {
        this.sceneSpecificQuestionnaire = sceneSpecificQuestionnaire;
    }

    public void setLoginFrame(LoginFrame loginFrame) {
        this.loginFrame = loginFrame;
    }

    public LoginFrame getLoginFrame() {
        return loginFrame;
    }

    public void setWelcomeFrame(WelcomeFrame welcomeFrame) {
        this.welcomeFrame = welcomeFrame;
    }

    public WelcomeFrame getWelcomeFrame() {
        return welcomeFrame;
    }

    public SubjectsFrame getSubjectsFrame() {
        return subjectsFrame;
    }

    public void setSubjectsFrame(SubjectsFrame subjectsFrame) {
        this.subjectsFrame = subjectsFrame;
    }

    public TeachersFrame getTeachersFrame() {
        return teachersFrame;
    }

    public void setTeachersFrame(TeachersFrame teachersFrame) {
        this.teachersFrame = teachersFrame;
    }

    public StudentsFrame getStudentsFrame() {
        return studentsFrame;
    }

    public void setStudentsFrame(StudentsFrame studentsFrame) {
        this.studentsFrame = studentsFrame;
    }

    public QuestionnairesFrame getQuestionnairesFrame() {
        return questionnairesFrame;
    }

    public void setQuestionnairesFrame(QuestionnairesFrame questionnairesFrame) {
        this.questionnairesFrame = questionnairesFrame;
    }

    public QuestionnaireResultsFrame getQuestionnaireResultsFrame() {
        return questionnaireResultsFrame;
    }

    public void setQuestionnaireResultsFrame(QuestionnaireResultsFrame questionnaireResultsFrame) {
        this.questionnaireResultsFrame = questionnaireResultsFrame;
    }

    public QuestionsFrame getQuestionsFrame() {
        return questionsFrame;
    }

    public void setQuestionsFrame(QuestionsFrame questionsFrame) {
        this.questionsFrame = questionsFrame;
    }

    public TestsFrame getTestsFrame() {
        return testsFrame;
    }

    public void setTestsFrame(TestsFrame testsFrame) {
        this.testsFrame = testsFrame;
    }

    public TestResultsFrame getTestResultsFrame() {
        return testResultsFrame;
    }

    public void setTestResultsFrame(TestResultsFrame testResultsFrame) {
        this.testResultsFrame = testResultsFrame;
    }

    public TopPageNavigationFrame getTopPageNavigationFrame() {
        return topPageNavigationFrame;
    }

    public void setTopPageNavigationFrame(TopPageNavigationFrame topPageNavigationFrame) {
        this.topPageNavigationFrame = topPageNavigationFrame;
    }

    public SpecificQuestionnaireFrame getSpecificQuestionnaireFrame() {
        return specificQuestionnaireFrame;
    }

    public void setSpecificQuestionnaireFrame(SpecificQuestionnaireFrame specificQuestionnaireFrame) {
        this.specificQuestionnaireFrame = specificQuestionnaireFrame;
    }

    public AddAdminFrame getAddAdminFrame() {
        return addAdminFrame;
    }

    public void setAddAdminFrame(AddAdminFrame addAdminFrame) {
        this.addAdminFrame = addAdminFrame;
    }

    public MyProfileFrame getMyProfileFrame() {
        return myProfileFrame;
    }

    public void setMyProfileFrame(MyProfileFrame myProfileFrame) {
        this.myProfileFrame = myProfileFrame;
    }

    public ObservableList<Subject> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(ObservableList<Subject> subjectList) {
        this.subjectList = subjectList;
    }

    public ObservableList<Teacher> getTeachersList() {
        return teachersList;
    }

    public void setTeachersList(ObservableList<Teacher> teacherList) {
        this.teachersList = teacherList;
    }

    public ObservableList<Student> getSubjectStudentsList() {
        return subjectStudentsList;
    }

    public void setSubjectStudentsList(ObservableList<Student> subjectStudentsList) {
        this.subjectStudentsList = subjectStudentsList;
    }

    public ObservableList<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(ObservableList<Student> studentList) {
        this.studentList = studentList;
    }

    public ObservableList<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(ObservableList<Question> questionList) {
        this.questionList = questionList;
    }
}
