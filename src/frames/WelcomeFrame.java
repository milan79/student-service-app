package frames;

import actions.AddAdminFrameAction;
import actions.MainNavigationAction;
import actions.MyProfileFrameAction;
import controller.Controller;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

public class WelcomeFrame extends VBox {

    private Button b_addAdmin;
    private Button b_myAccount;

    private HBox hb_admin;

    private Label l_welcome;
    private HBox hb_welcome;

    private Label l_choose;

    private Button b_subjects;
    private Button b_teachers;
    private Button b_students;
    private Button b_questionnaires;
    private Button b_questionnaireResults;
    private Button b_questions;
    private Button b_tests;
    private Button b_testResults;

    private GridPane gp_navigation;

    private VBox vb_welcomeNavigation;

    public WelcomeFrame() {
        super();
        setPadding(new Insets(20));
        setStyle("-fx-background-color: #cce6ff;");

        setComponents();
        getChildren().addAll(hb_admin, vb_welcomeNavigation);

        TopPageNavigationFrame topPageNavigationFrame = new TopPageNavigationFrame();
        Controller.getInstance().setTopPageNavigationFrame(topPageNavigationFrame);
    }

    private void setComponents() {
        b_addAdmin = new Button();
        b_addAdmin.setOnAction(new AddAdminFrameAction());
        setButtonProperties(b_addAdmin, "Add new admin", 150, 30, 12);

        b_myAccount = new Button();
        b_myAccount.setOnAction(new MyProfileFrameAction());
        setButtonProperties(b_myAccount, "My profile", 150, 30, 12);

        hb_admin = new HBox(20);
        hb_admin.setAlignment(Pos.TOP_RIGHT);
        hb_admin.getChildren().addAll(b_addAdmin, b_myAccount);

        l_welcome = new Label();
        TextField tf_usernameOrEmail = Controller.getInstance().getLoginFrame().getTf_usernameOrEmail();
        l_welcome.setText("Welcome " + Controller.getInstance().getAdministratorName(tf_usernameOrEmail));
        l_welcome.setStyle("-fx-font-size: 24px;");

        hb_welcome = new HBox();
        hb_welcome.setAlignment(Pos.CENTER);
        hb_welcome.getChildren().add(l_welcome);

        gp_navigation = new GridPane();
        gp_navigation.setAlignment(Pos.CENTER);
        gp_navigation.setVgap(12);
        gp_navigation.setHgap(12);

        ColumnConstraints col_constraints = new ColumnConstraints();
        col_constraints.setPrefWidth(170.0);

        gp_navigation.getColumnConstraints().add(col_constraints);

        l_choose = new Label();
        l_choose.setText("Choose\ncategory");
        l_choose.setStyle("-fx-font-size: 14pt;");
        l_choose.setWrapText(true);
        l_choose.setTextAlignment(TextAlignment.CENTER);
        l_choose.setAlignment(Pos.CENTER);
        GridPane.setConstraints(l_choose, 0, 0);
        GridPane.setHalignment(l_choose, HPos.CENTER);
        GridPane.setValignment(l_choose, VPos.CENTER);

        b_subjects = new Button();
        setButtonProperties(b_subjects, "Subjects", 170, 80, 14);
        b_subjects.setOnAction(new MainNavigationAction());
        GridPane.setConstraints(b_subjects, 1, 0);

        b_teachers = new Button();
        setButtonProperties(b_teachers, "Teachers", 170, 80, 14);
        b_teachers.setOnAction(new MainNavigationAction());
        GridPane.setConstraints(b_teachers, 2, 0);

        b_students = new Button();
        setButtonProperties(b_students, "Students", 170, 80, 14);
        b_students.setOnAction(new MainNavigationAction());
        GridPane.setConstraints(b_students, 0, 1);

        b_questionnaires = new Button();
        setButtonProperties(b_questionnaires, "Questionnaires", 170, 80, 14);
        b_questionnaires.setOnAction(new MainNavigationAction());
        GridPane.setConstraints(b_questionnaires, 1, 1);

        b_questionnaireResults = new Button();
        setButtonProperties(b_questionnaireResults, "Questionnaire results", 170, 80, 14);
        b_questionnaireResults.setOnAction(new MainNavigationAction());
        b_questionnaireResults.setWrapText(true);
        b_questionnaireResults.setTextAlignment(TextAlignment.CENTER);
        GridPane.setConstraints(b_questionnaireResults, 2, 1);

        b_questions = new Button();
        setButtonProperties(b_questions, "Questions", 170, 80, 14);
        b_questions.setOnAction(new MainNavigationAction());
        GridPane.setConstraints(b_questions, 0, 2);

        b_tests = new Button();
        setButtonProperties(b_tests, "Tests", 170, 80, 14);
        b_tests.setOnAction(new MainNavigationAction());
        GridPane.setConstraints(b_tests, 1, 2);

        b_testResults = new Button();
        setButtonProperties(b_testResults, "Test results", 170, 80, 14);
        b_testResults.setOnAction(new MainNavigationAction());
        GridPane.setConstraints(b_testResults, 2, 2);

        gp_navigation.getChildren().addAll(l_choose, b_subjects, b_teachers, b_students, b_questionnaires, b_questionnaireResults, b_questions,
                                           b_tests, b_testResults);

        vb_welcomeNavigation = new VBox(40);
        vb_welcomeNavigation.setAlignment(Pos.CENTER);
        vb_welcomeNavigation.getChildren().addAll(hb_welcome, gp_navigation);
        vb_welcomeNavigation.setPadding(new Insets(200, 0, 0, 0));
    }

    private void setButtonProperties(Button button, String text, double width, double height, double fontSize) {
        button.setText(text);
        button.setPrefWidth(width);
        button.setPrefHeight(height);
        button.setStyle("-fx-font-size: " + fontSize + "pt;");
    }

    public Button getB_subjects() {
        return b_subjects;
    }

    public Button getB_teachers() {
        return b_teachers;
    }

    public Button getB_students() {
        return b_students;
    }

    public Button getB_questionnaires() {
        return b_questionnaires;
    }

    public Button getB_questionnaireResults() {
        return b_questionnaireResults;
    }

    public Button getB_questions() {
        return b_questions;
    }

    public Button getB_tests() {
        return b_tests;
    }

    public Button getB_testResults() {
        return b_testResults;
    }
}
