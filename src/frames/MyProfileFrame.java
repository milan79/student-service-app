package frames;

import controller.Controller;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.ArrayList;

public class MyProfileFrame extends VBox {

    private Label l_adminName;
    private TextField tf_adminName;
    private HBox hb_adminName;

    private Label l_adminSurname;
    private TextField tf_adminSurname;
    private HBox hb_adminSurname;

    private Label l_adminEmail;
    private TextField tf_adminEmail;
    private HBox hb_adminEmail;

    private Label l_adminUsernme;
    private TextField tf_adminUsername;
    private Label l_notEditable;
    private VBox vb_adminUsername;
    private HBox hb_adminUsername;

    private Label l_adminPassword;
    private TextField tf_adminPassword;
    private Label l_newPassword;
    private VBox vb_adminPassword;
    private HBox hb_adminPassword;

    private Button b_update;
    private Button b_cancel;
    private HBox hb_buttons;

    private VBox vb_data;

    private Background vBox_background;

    public MyProfileFrame() {
        super(10);
        setAlignment(Pos.CENTER);

        setComponents();

        String usernameOrEmail = Controller.getInstance().getLoginFrame().getTf_usernameOrEmail().getText().trim();

        ArrayList<String> data = Controller.getInstance().getAdministratorData(usernameOrEmail);
        getTf_adminName().setText(data.get(0));
        getTf_adminSurname().setText(data.get(1));
        getTf_adminUsername().setText(data.get(2));
        getTf_adminEmail().setText(data.get(3));

        getChildren().add(vb_data);

        setBackground(vBox_background);

    }

    private void setComponents() {

        l_adminName = new Label();
        l_adminName.setText("Name:");
        l_adminName.setStyle("-fx-font-size:12pt;");
        l_adminName.setMinWidth(120);

        tf_adminName = new TextField();
        tf_adminName.setMinWidth(230);

        hb_adminName = new HBox();
        hb_adminName.getChildren().addAll(l_adminName, tf_adminName);
        hb_adminName.setAlignment(Pos.TOP_CENTER);

        l_adminSurname = new Label();
        l_adminSurname.setText("Surname:");
        l_adminSurname.setStyle("-fx-font-size:12pt;");
        l_adminSurname.setMinWidth(120);

        tf_adminSurname = new TextField();
        tf_adminSurname.setMinWidth(230);

        hb_adminSurname = new HBox();
        hb_adminSurname.getChildren().addAll(l_adminSurname, tf_adminSurname);
        hb_adminSurname.setAlignment(Pos.TOP_CENTER);

        l_adminUsernme = new Label();
        l_adminUsernme.setText("Username*:");
        l_adminUsernme.setStyle("-fx-font-size:12pt;");
        l_adminUsernme.setMinWidth(120);

        tf_adminUsername = new TextField();
        tf_adminUsername.setMinWidth(230);
        tf_adminUsername.setEditable(false);

        l_notEditable = new Label();
        l_notEditable.setText("*not editable");
        l_notEditable.setStyle("-fx-font-size:8pt; -fx-font-weight:bold;");
        l_notEditable.setPadding(new Insets(0, 0, 0, 5));

        vb_adminUsername = new VBox();
        vb_adminUsername.getChildren().addAll(tf_adminUsername, l_notEditable);

        hb_adminUsername = new HBox();
        hb_adminUsername.getChildren().addAll(l_adminUsernme, vb_adminUsername);
        hb_adminUsername.setAlignment(Pos.TOP_CENTER);

        l_adminPassword = new Label();
        l_adminPassword.setText("Password:");
        l_adminPassword.setStyle("-fx-font-size:12pt;");
        l_adminPassword.setMinWidth(120);

        tf_adminPassword = new TextField();
        tf_adminPassword.setMinWidth(230);

        l_newPassword = new Label();
        l_newPassword.setText("*only new password");
        l_newPassword.setStyle("-fx-font-size:8pt; -fx-font-weight:bold;");
        l_newPassword.setPadding(new Insets(0, 0, 0, 5));

        vb_adminPassword = new VBox();
        vb_adminPassword.getChildren().addAll(tf_adminPassword, l_newPassword);

        hb_adminPassword = new HBox();
        hb_adminPassword.getChildren().addAll(l_adminPassword, vb_adminPassword);
        hb_adminPassword.setAlignment(Pos.TOP_CENTER);

        l_adminEmail = new Label();
        l_adminEmail.setText("Email:");
        l_adminEmail.setStyle("-fx-font-size:12pt;");
        l_adminEmail.setMinWidth(120);

        tf_adminEmail = new TextField();
        tf_adminEmail.setMinWidth(230);

        hb_adminEmail = new HBox();
        hb_adminEmail.getChildren().addAll(l_adminEmail, tf_adminEmail);
        hb_adminEmail.setAlignment(Pos.TOP_CENTER);

        b_update = new Button();
        b_update.setText("Update");
        b_update.setPrefWidth(90);
        b_update.setPrefHeight(35);
        b_update.setStyle("-fx-font-size:13pt;");
        b_update.setOnAction(e -> {
            Controller.getInstance().updateAdministrator(tf_adminName, tf_adminSurname, tf_adminUsername, tf_adminPassword, tf_adminEmail);

            Controller.getInstance().setWelcomeFrame();
        });

        b_cancel = new Button();
        b_cancel.setText("Cancel");
        b_cancel.setPrefWidth(90);
        b_cancel.setPrefHeight(35);
        b_cancel.setStyle("-fx-font-size:13pt;");
        b_cancel.setOnAction(e -> {
            Controller.getInstance().setWelcomeFrame();
        });

        hb_buttons = new HBox(25);
        hb_buttons.getChildren().addAll(b_update, b_cancel);
        hb_buttons.setAlignment(Pos.TOP_CENTER);
        hb_buttons.setPadding(new Insets(20, 0, 0, 0));

        vb_data = new VBox(15);
        vb_data.getChildren().addAll(hb_adminName, hb_adminSurname, hb_adminUsername, hb_adminPassword, hb_adminEmail, hb_buttons);
        vb_data.setAlignment(Pos.CENTER);
        vb_data.setMaxWidth(520);

        vb_data.setStyle("-fx-background-color: #cce6ff;" +
                "-fx-background-radius: 20px;" +
                "-fx-padding: 30px 45px;" +
                "-fx-border-style: solid outside;" +
                "-fx-border-width: 3px;" +
                "-fx-border-color: #339cff;" +
                "-fx-border-radius: 20px;");

        vBox_background = new Background(new BackgroundFill(Color.web("#75a3a3"), CornerRadii.EMPTY, Insets.EMPTY));
    }

    public TextField getTf_adminName() {
        return tf_adminName;
    }

    public TextField getTf_adminSurname() {
        return tf_adminSurname;
    }

    public TextField getTf_adminEmail() {
        return tf_adminEmail;
    }

    public TextField getTf_adminUsername() {
        return tf_adminUsername;
    }
}
