package frames;

import actions.LoginAction;
import actions.MainNavigationAction;
import actions.TopPageNavigationAction;
import controller.Controller;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.text.TextAlignment;

public class TopPageNavigationFrame extends HBox {

    private Button b_subjects;
    private Button b_teachers;
    private Button b_students;
    private Button b_questionnaires;

    private Button b_questionnaireResults;
    private Button b_questions;
    private Button b_tests;
    private Button b_testResults;

    private GridPane gp_topNavigation;

    public TopPageNavigationFrame() {
        super();
        setAlignment(Pos.CENTER);
        setPadding(new Insets(10, 0, 0, 0));

        setComponents();
        getChildren().add(gp_topNavigation);
    }

    private void setComponents() {

        gp_topNavigation = new GridPane();
        gp_topNavigation.setAlignment(Pos.CENTER);
        gp_topNavigation.setVgap(8);
        gp_topNavigation.setHgap(8);
        gp_topNavigation.setPadding(new Insets(0, 0, 10, 0));

        ColumnConstraints columnConstraints = new ColumnConstraints();
        columnConstraints.setPrefWidth(120);

        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setPrefHeight(30);

        gp_topNavigation.getColumnConstraints().add(columnConstraints);
        gp_topNavigation.getRowConstraints().add(rowConstraints);

        b_subjects = new Button();
        setButtonProperties(b_subjects, "Subjects",120, 30, 10);
        b_subjects.setOnAction(new TopPageNavigationAction());
        GridPane.setConstraints(b_subjects, 0, 0);

        b_teachers = new Button();
        setButtonProperties(b_teachers, "Teachers", 120, 30, 10);
        b_teachers.setOnAction(new TopPageNavigationAction());
        GridPane.setConstraints(b_teachers, 1, 0);

        b_students = new Button();
        setButtonProperties(b_students, "Students", 120, 30, 10);
        b_students.setOnAction(new TopPageNavigationAction());
        GridPane.setConstraints(b_students, 2, 0);

        b_questionnaires = new Button();
        setButtonProperties(b_questionnaires, "Questionnaires", 120, 30, 10);
        b_questionnaires.setOnAction(new TopPageNavigationAction());
        GridPane.setConstraints(b_questionnaires, 0, 1);

        b_questionnaireResults = new Button();
        b_questionnaireResults.setWrapText(true);
        b_questionnaireResults.setTextAlignment(TextAlignment.CENTER);
        setButtonProperties(b_questionnaireResults, "Questionnaire results", 120, 68, 10);
        b_questionnaireResults.setOnAction(new TopPageNavigationAction());
        GridPane.setConstraints(b_questionnaireResults, 3, 0, 1, 2);

        b_questions = new Button();;
        setButtonProperties(b_questions, "Questions", 120, 30, 10);
        b_questions.setOnAction(new TopPageNavigationAction());
        GridPane.setConstraints(b_questions, 1, 1);

        b_tests = new Button();;
        setButtonProperties(b_tests, "Tests", 120, 30, 10);
        b_tests.setOnAction(new TopPageNavigationAction());
        GridPane.setConstraints(b_tests, 2, 1);

        b_testResults = new Button();
        b_testResults.setWrapText(true);
        b_testResults.setTextAlignment(TextAlignment.CENTER);
        setButtonProperties(b_testResults, "Test\nresults", 120, 68, 10);
        b_testResults.setOnAction(new TopPageNavigationAction());
        GridPane.setConstraints(b_testResults, 4, 0, 1, 2);

        gp_topNavigation.getChildren().addAll(b_subjects, b_teachers, b_students, b_questionnaires, b_questionnaireResults, b_questions,
                                              b_tests, b_testResults);
    }

    private void setButtonProperties(Button button, String text, double width, double height, int fontSize) {
        button.setText(text);
        button.setPrefWidth(width);
        button.setPrefHeight(height);
        button.setStyle("-fx-font-size: " + fontSize + "pt;");
    }

    public Button getB_subjects() {
        return b_subjects;
    }

    public void setB_subjects(Button b_subjects) {
        this.b_subjects = b_subjects;
    }

    public Button getB_teachers() {
        return b_teachers;
    }

    public void setB_teachers(Button b_teachers) {
        this.b_teachers = b_teachers;
    }

    public Button getB_students() {
        return b_students;
    }

    public void setB_students(Button b_students) {
        this.b_students = b_students;
    }

    public Button getB_questionnaires() {
        return b_questionnaires;
    }

    public void setB_questionnaires(Button b_questionnaires) {
        this.b_questionnaires = b_questionnaires;
    }

    public Button getB_questionnaireResults() {
        return b_questionnaireResults;
    }

    public void setB_questionnaireResults(Button b_questionnaireResults) {
        this.b_questionnaireResults = b_questionnaireResults;
    }

    public Button getB_questions() {
        return b_questions;
    }

    public void setB_questions(Button b_questions) {
        this.b_questions = b_questions;
    }

    public Button getB_tests() {
        return b_tests;
    }

    public void setB_tests(Button b_tests) {
        this.b_tests = b_tests;
    }

    public Button getB_testResults() {
        return b_testResults;
    }

    public void setB_testResults(Button b_testResults) {
        this.b_testResults = b_testResults;
    }

}
