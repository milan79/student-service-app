package frames;

import controller.Controller;
import data.Subject;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import javax.swing.*;

public class TestResultsFrame extends VBox {

    private Text txt_test;

    private Label l_subject;
    private TextField tf_subject;

    private Label l_studentName;
    private TextField tf_studentName;

    private Label l_studentSurname;
    private TextField tf_studentSurname;

    private Label l_studentEmail;
    private TextField tf_studentEmail;

    private Button b_findStudent;

    private Button b_view;

    private HBox hb_button;

    private Separator sp_test;

    private Label l_subjectName;
    private TextField tf_subjectName;
    private Label l_placeholder;

    private HBox hb_searchCriteria;

    private TableView<Subject> tw_subjects;

    private VBox vb_twSubjects;

    private GridPane gp_dataDisplay;

    public TestResultsFrame() {
        super(10);
        setStyle("-fx-background-color: #cce6ff;");

        setComponents();
        getChildren().addAll(Controller.getInstance().getTopPageNavigationFrame(), gp_dataDisplay);
    }

    private void setComponents() {

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.CENTER);
        gp_dataDisplay.setMinWidth(860);
        GridPane.setMargin(gp_dataDisplay, new Insets(0, 0, 15, 0));

        txt_test = new Text("SPECIFIC TEST DATA");
        txt_test.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_test, 0, 0, 2, 1);
        GridPane.setMargin(txt_test, new Insets(0, 0, 0, 25));

        l_studentName = new Label();
        l_studentName.setText("Student\nname:");
        l_studentName.setWrapText(true);
        l_studentName.setTextAlignment(TextAlignment.LEFT);
        GridPane.setConstraints(l_studentName, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentName, new Insets(15, 5, 0, 25));

        tf_studentName = new TextField();
        tf_studentName.setMaxWidth(140);
        GridPane.setConstraints(tf_studentName, 1, 1);
        GridPane.setMargin(tf_studentName, new Insets(15, 20, 0, 0));

        l_studentSurname = new Label();
        l_studentSurname.setText("Student\nsurname:");
        l_studentSurname.setWrapText(true);
        l_studentSurname.setTextAlignment(TextAlignment.LEFT);
        GridPane.setConstraints(l_studentSurname, 2, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentSurname, new Insets(15, 5, 0, 0));

        tf_studentSurname = new TextField();
        tf_studentSurname.setMaxWidth(140);
        GridPane.setConstraints(tf_studentSurname, 3, 1);
        GridPane.setMargin(tf_studentSurname, new Insets(15, 20, 0, 0));

        l_studentEmail = new Label();
        l_studentEmail.setText("Student\ne-mail:");
        l_studentEmail.setWrapText(true);
        l_studentEmail.setTextAlignment(TextAlignment.LEFT);
        GridPane.setConstraints(l_studentEmail, 4, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentEmail, new Insets(15, 5, 0, 0));

        tf_studentEmail = new TextField();
        tf_studentEmail.setMaxWidth(140);
        GridPane.setConstraints(tf_studentEmail, 5, 1);
        GridPane.setMargin(tf_studentEmail, new Insets(15, 40, 0, 0));

        b_findStudent = new Button();
        b_findStudent.setText("Find student");
        b_findStudent.setStyle("-fx-font-size: 12pt;");
        b_findStudent.setPrefWidth(130);
        b_findStudent.setPrefHeight(20);
        b_findStudent.setDisable(true);
        b_findStudent.setOnAction(e -> SelectStudentFrame.display("TestResults"));
        GridPane.setConstraints(b_findStudent, 6, 1, 1, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(b_findStudent, new Insets(15, 0, 0, 0));

        l_subject = new Label();
        l_subject.setText("Subject:");
        l_subject.setWrapText(true);
        l_subject.setTextAlignment(TextAlignment.LEFT);
        GridPane.setConstraints(l_subject, 0, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_subject, new Insets(15, 5, 0, 0));

        tf_subject = new TextField();
        GridPane.setConstraints(tf_subject, 1, 2, 2, 1);
        GridPane.setMargin(tf_subject, new Insets(15, 0, 0, 0));

        b_view = new Button();
        b_view.setText("View");
        b_view.setStyle("-fx-font-size: 14pt;");
        b_view.setPrefWidth(130);
        b_view.setMinHeight(60);
        b_view.setDisable(true);
        b_view.setOnAction(e -> SpecificTestFrame.display());

        hb_button = new HBox();
        hb_button.getChildren().add(b_view);
        hb_button.setAlignment(Pos.CENTER);
        GridPane.setConstraints(hb_button, 0, 3, 7, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(hb_button, new Insets(20, 0, 12, 0));

        sp_test = new Separator();
        GridPane.setConstraints(sp_test, 0, 4, 7, 1, HPos.CENTER, VPos.TOP);
        GridPane.setMargin(sp_test, new Insets(12, 0, 0, 0));

        l_subjectName = new Label();
        l_subjectName.setText("Subject:");
        l_subjectName.setPadding(new Insets(4, 0, 0, 0));

        tf_subjectName = new TextField();
        tf_subjectName.setPrefWidth(240);
        tf_subjectName.setOnKeyReleased(event -> {
            String subjectTitleText = tf_subjectName.getText().trim();
            tw_subjects.setItems(Controller.getInstance().getSubjectTest(subjectTitleText));
        });

        l_placeholder = new Label();
        l_placeholder.setPrefWidth(390);

        hb_searchCriteria = new HBox(5);
        hb_searchCriteria.getChildren().addAll(l_subjectName, tf_subjectName, l_placeholder);
        GridPane.setConstraints(hb_searchCriteria, 0, 5, 7, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_searchCriteria, new Insets(30, 0, 0, 25));

        TableColumn<Subject, String> col_subjectName = new TableColumn<>("Subject");
        col_subjectName.setMinWidth(220);
        col_subjectName.setStyle("-fx-font-size: 10pt;");
        col_subjectName.setCellValueFactory(new PropertyValueFactory<>("subjectTitle"));
        col_subjectName.setCellFactory(TooltippedTableCell.forTableColumn());

        tw_subjects = new TableView<>();
        tw_subjects.getColumns().add(col_subjectName);
        tw_subjects.setMaxWidth(220);
        tw_subjects.setStyle("-fx-font-size: 10pt;");
        tw_subjects.setRowFactory( tv -> {
            TableRow<Subject> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You have to select populated row");
                    return;
                }

                String subjectTitle = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Subject rowData = row.getItem();

                    subjectTitle = rowData.getSubjectTitle();
                }

                tf_subject.setText(subjectTitle);

                tf_studentName.setText("");
                tf_studentSurname.setText("");
                tf_studentEmail.setText("");

                b_findStudent.setDisable(false);
                b_view.setDisable(true);

            });
            return row;
        });

        tw_subjects.setItems(Controller.getInstance().getSubjectTest(null));

        vb_twSubjects = new VBox();
        vb_twSubjects.getChildren().add(tw_subjects);
        vb_twSubjects.setMaxWidth(220);
        GridPane.setConstraints(vb_twSubjects, 0,6, 2, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(vb_twSubjects, new Insets(15, 0, 15,0));

        gp_dataDisplay.getChildren().addAll(txt_test, l_studentName, tf_studentName,
                l_studentSurname, tf_studentSurname, l_studentEmail, b_findStudent, l_subject, tf_subject, tf_studentEmail, hb_button, sp_test,
                hb_searchCriteria, vb_twSubjects);
    }

    public TextField getTf_subject() {
        return tf_subject;
    }

    public TextField getTf_studentName() {
        return tf_studentName;
    }

    public TextField getTf_studentSurname() {
        return tf_studentSurname;
    }

    public TextField getTf_studentEmail() {
        return tf_studentEmail;
    }

    public Button getB_view() {
        return b_view;
    }
}
