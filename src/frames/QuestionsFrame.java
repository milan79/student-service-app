package frames;

import controller.Controller;
import data.Question;
import data.Subject;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import javax.swing.*;
import java.util.ArrayList;

public class QuestionsFrame extends VBox {

    private Text txt_question;

    private Label l_questionText;
    private TextArea ta_questionText;
    private String oldQuestionText;

    private Label l_questionRefersTo;
    private TextField tf_questionRefersTo;

    private HBox hb_questionRefersTo;

    private Label l_questionType;
    private ComboBox<String> cb_type;
    private String questionType;

    private HBox hb_questionType;

    private VBox vb_questionType;

    private Label l_firstResponse;
    private TextField tf_firstResponse;

    private Label l_secondResponse;
    private TextField tf_secondResponse;

    private Label l_thirdResponse;
    private TextField tf_thirdResponse;

    private Label l_fourthResponse;
    private TextField tf_fourthResponse;

    private Label l_fifthResponse;
    private TextField tf_fifthResponse;

    private Label l_correctAnswer;

    private CheckBox cb_correctAnswer_1;
    private CheckBox cb_correctAnswer_2;
    private CheckBox cb_correctAnswer_3;
    private CheckBox cb_correctAnswer_4;
    private CheckBox cb_correctAnswer_5;

    private HBox hb_correctAnswer;

    private Button b_add;
    private Button b_delete;
    private Button b_update;

    private Button b_clear;

    private HBox hb_clear;

    private HBox hb_buttons;

    private Separator sp_question;

    private Button b_findQuestionnaire;
    private Button b_findTest;

    private ComboBox cb_questionType;
    private String type;

    private HBox hb_cbSearch;

    private Label l_questionWords;
    private TextField tf_questionWords;

    private HBox hb_questionWords;

    private HBox hb_search;

    private TableView<Question> tw_question;

    private HBox hb_twQuestion;

    private Label l_questionnaire;
    private Label l_test;

    private Label l_subject;
    private Label l_subjectText;

    private HBox hb_subject;
    private VBox vb_subject;

    private Label l_teacherName;
    private Label l_teacherNameText;

    private HBox hb_teacherName;

    private Label l_teacherSurname;
    private Label l_teacherSurnameText;

    private HBox hb_teacherSurname;

    private Label l_teacherEmail;
    private Label l_teacherEmailText;

    private HBox hb_teacherEmail;

    private Label l_teacherTitle;
    private Label l_teacherTitleText;

    private HBox hb_teacherTitle;

    private VBox vb_teacher;

    private GridPane gp_dataDisplay;

    public QuestionsFrame() {
        super(10);
        setStyle("-fx-background-color: #cce6ff;");

        setComponents();
        getChildren().addAll(Controller.getInstance().getTopPageNavigationFrame(), gp_dataDisplay);
    }

    private void setComponents() {

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.CENTER);
        gp_dataDisplay.setMinWidth(870);
        GridPane.setMargin(gp_dataDisplay, new Insets(0, 0, 15, 0));

        txt_question = new Text("QUESTION DATA");
        txt_question.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_question, 0, 0, 2, 1);
        GridPane.setMargin(txt_question, new Insets(0,0,0,0));

        l_questionRefersTo = new Label();
        l_questionRefersTo.setText("Refers to:");
        l_questionRefersTo.setPadding(new Insets(4,0,0,0));

        tf_questionRefersTo = new TextField();
        tf_questionRefersTo.setPrefWidth(140);
        tf_questionRefersTo.setEditable(false);
        tf_questionRefersTo.setDisable(true);

        hb_questionRefersTo = new HBox(5);
        hb_questionRefersTo.getChildren().addAll(l_questionRefersTo, tf_questionRefersTo);
        hb_questionRefersTo.setAlignment(Pos.TOP_RIGHT);

        l_questionType = new Label();
        l_questionType.setText("Type:");
        l_questionType.setPadding(new Insets(4,0,0,0));

        cb_type = new ComboBox();
        cb_type.setPromptText("Choose type");
        cb_type.getItems().addAll("Write answer", "Radio button", "Check box");
        cb_type.setPrefWidth(140);
        cb_type.setDisable(true);
        cb_type.setOnAction(e -> {
             if (cb_type.getValue() != null) {
                 questionType = cb_type.getValue().trim();

                 if (cb_type.getValue().equalsIgnoreCase("write answer") && (b_add.isDisabled() && b_delete.isDisabled() && b_update.isDisabled() && b_clear.isDisabled())) {

                     tf_firstResponse.setDisable(true);
                     tf_secondResponse.setDisable(true);
                     tf_thirdResponse.setDisable(true);
                     tf_fourthResponse.setDisable(true);
                     tf_fifthResponse.setDisable(true);

                     cb_correctAnswer_1.setDisable(true);
                     cb_correctAnswer_2.setDisable(true);
                     cb_correctAnswer_3.setDisable(true);
                     cb_correctAnswer_4.setDisable(true);
                     cb_correctAnswer_5.setDisable(true);
                 } else if (cb_type.getValue().equalsIgnoreCase("radio button") || cb_type.getValue().equalsIgnoreCase("check box")) {

                     tf_firstResponse.setDisable(false);
                     tf_secondResponse.setDisable(false);
                     tf_thirdResponse.setDisable(false);
                     tf_fourthResponse.setDisable(false);
                     tf_fifthResponse.setDisable(false);

                     if (l_questionnaire.getText() != null && (!l_questionnaire.getText().equals(""))) {
                         cb_correctAnswer_1.setDisable(true);
                         cb_correctAnswer_2.setDisable(true);
                         cb_correctAnswer_3.setDisable(true);
                         cb_correctAnswer_4.setDisable(true);
                         cb_correctAnswer_5.setDisable(true);
                     } else if (l_test.getText() != null && (!l_test.getText().equals(""))) {
                         cb_correctAnswer_1.setDisable(false);
                         cb_correctAnswer_2.setDisable(false);
                         cb_correctAnswer_3.setDisable(false);
                         cb_correctAnswer_4.setDisable(false);
                         cb_correctAnswer_5.setDisable(false);
                     }
                 }
             }

        });

        hb_questionType = new HBox(5);
        hb_questionType.getChildren().addAll(l_questionType, cb_type);
        hb_questionType.setAlignment(Pos.TOP_RIGHT);

        vb_questionType = new VBox(5);
        vb_questionType.getChildren().addAll(hb_questionRefersTo, hb_questionType);
        GridPane.setConstraints(vb_questionType, 0, 1, 2, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(vb_questionType, new Insets(16, 5, 0, 20));

        l_questionText = new Label();
        l_questionText.setText("Question:");
        GridPane.setConstraints(l_questionText, 2, 1, 1, 1, HPos.RIGHT, VPos.TOP);
        GridPane.setMargin(l_questionText, new Insets(16, 5, 0, 20));

        ta_questionText = new TextArea();
        ta_questionText.setPrefHeight(60);
        ta_questionText.setPrefWidth(360);
        ta_questionText.setDisable(true);
        GridPane.setConstraints(ta_questionText, 3, 1);
        GridPane.setMargin(ta_questionText, new Insets(12, 0, 5, 0));
        ta_questionText.setOnKeyReleased(event -> {
            setClearAndAddButton();
        });

        l_firstResponse = new Label();
        l_firstResponse.setText("Answer_1:");
        GridPane.setConstraints(l_firstResponse, 0, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_firstResponse, new Insets(5, 5, 0, 0));

        tf_firstResponse = new TextField();
        tf_firstResponse.setMaxWidth(400);
        tf_firstResponse.setDisable(true);
        tf_firstResponse.setOnKeyReleased(event -> {
            setClearAndAddButton();
        });
        GridPane.setConstraints(tf_firstResponse, 1, 2, 3, 1);
        GridPane.setMargin(tf_firstResponse, new Insets(5, 0, 0, 0));

        l_secondResponse = new Label();
        l_secondResponse.setText("Answer_2:");
        GridPane.setConstraints(l_secondResponse, 0, 3, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_secondResponse, new Insets(5, 5, 0, 0));

        tf_secondResponse = new TextField();
        tf_secondResponse.setMaxWidth(400);
        tf_secondResponse.setDisable(true);
        tf_secondResponse.setOnKeyReleased(event -> {
            setClearAndAddButton();
        });
        GridPane.setConstraints(tf_secondResponse, 1, 3, 3, 1);
        GridPane.setMargin(tf_secondResponse, new Insets(5, 0, 0, 0));

        l_thirdResponse = new Label();
        l_thirdResponse.setText("Answer_3:");
        GridPane.setConstraints(l_thirdResponse, 0, 4, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_thirdResponse, new Insets(5, 5, 0, 0));

        tf_thirdResponse = new TextField();
        tf_thirdResponse.setMaxWidth(400);
        tf_thirdResponse.setDisable(true);
        tf_thirdResponse.setOnKeyReleased(event -> {
            setClearAndAddButton();
        });
        GridPane.setConstraints(tf_thirdResponse, 1, 4, 3, 1);
        GridPane.setMargin(tf_thirdResponse, new Insets(5, 0, 0, 0));

        l_fourthResponse = new Label();
        l_fourthResponse.setText("Answer_4:");
        GridPane.setConstraints(l_fourthResponse, 0, 5, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_fourthResponse, new Insets(5, 5, 0, 0));

        tf_fourthResponse = new TextField();
        tf_fourthResponse.setMaxWidth(400);
        tf_fourthResponse.setDisable(true);
        tf_fourthResponse.setOnKeyReleased(event -> {
            setClearAndAddButton();
        });
        GridPane.setConstraints(tf_fourthResponse, 1, 5, 3, 1);
        GridPane.setMargin(tf_fourthResponse, new Insets(5, 0, 0, 0));

        l_fifthResponse = new Label();
        l_fifthResponse.setText("Answer_5:");
        GridPane.setConstraints(l_fifthResponse, 0, 6, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_fifthResponse, new Insets(5, 5, 0, 0));

        tf_fifthResponse = new TextField();
        tf_fifthResponse.setMaxWidth(400);
        tf_fifthResponse.setDisable(true);
        tf_fifthResponse.setOnKeyReleased(event -> {
            setClearAndAddButton();
        });
        GridPane.setConstraints(tf_fifthResponse, 1, 6, 3, 1);
        GridPane.setMargin(tf_fifthResponse, new Insets(5, 0, 0, 0));

        l_correctAnswer = new Label();
        l_correctAnswer.setText("Correct answer(s):");
        GridPane.setConstraints(l_correctAnswer, 0, 7, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_correctAnswer, new Insets(10, 5, 10, 0));

        cb_correctAnswer_1 = new CheckBox("1.");
        cb_correctAnswer_1.setDisable(true);
        cb_correctAnswer_1.setOnAction(e -> {
            setClearAndAddButton();
        });

        cb_correctAnswer_2 = new CheckBox("2.");
        cb_correctAnswer_2.setDisable(true);
        cb_correctAnswer_2.setOnAction(e -> {
            setClearAndAddButton();
        });

        cb_correctAnswer_3 = new CheckBox("3.");
        cb_correctAnswer_3.setDisable(true);
        cb_correctAnswer_3.setOnAction(e -> {
            setClearAndAddButton();
        });

        cb_correctAnswer_4 = new CheckBox("4.");
        cb_correctAnswer_4.setDisable(true);
        cb_correctAnswer_4.setOnAction(e -> {
            setClearAndAddButton();
        });

        cb_correctAnswer_5 = new CheckBox("5.");
        cb_correctAnswer_5.setDisable(true);
        cb_correctAnswer_5.setOnAction(e -> {
            setClearAndAddButton();
        });

        hb_correctAnswer = new HBox(15);
        hb_correctAnswer.getChildren().addAll(cb_correctAnswer_1, cb_correctAnswer_2, cb_correctAnswer_3, cb_correctAnswer_4, cb_correctAnswer_5);
        GridPane.setConstraints(hb_correctAnswer, 1, 7, 3, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_correctAnswer, new Insets(10, 0, 0, 0));

        b_add = new Button();
        b_add.setText("Add");
        b_add.setPrefWidth(70);
        b_add.setPrefHeight(35);
        b_add.setDisable(true);
        b_add.setOnAction(e -> {
            ArrayList<Integer> correctAnswers = new ArrayList<>();
            if (cb_correctAnswer_1.isSelected()) {
                correctAnswers.add(1);
            }
            if (cb_correctAnswer_2.isSelected()) {
                correctAnswers.add(2);
            }
            if (cb_correctAnswer_3.isSelected()) {
                correctAnswers.add(3);
            }
            if (cb_correctAnswer_4.isSelected()) {
                correctAnswers.add(4);
            }
            if (cb_correctAnswer_5.isSelected()) {
                correctAnswers.add(5);
            }

            ArrayList<Integer> tempList = new ArrayList<>();

            String tempQS = "";
            String tempQT = "";
            String tempTS = "";

            boolean result = false;

            if (l_questionnaire.getText() != null && (!l_questionnaire.getText().equals(""))) {
                if (l_subjectText.getText() != null && (!l_subjectText.getText().equals(""))) {
                    result = Controller.getInstance().addQuestionnaireQuestion(l_subjectText.getText(), questionType, tf_firstResponse, tf_secondResponse,
                            tf_thirdResponse, tf_fourthResponse, tf_fifthResponse, tempList, ta_questionText.getText());
                    tempQS = "yes";
                }
                if (l_teacherEmailText.getText() != null && (!l_teacherEmailText.getText().equals(""))) {
                    result = Controller.getInstance().addQuestionnaireQuestion(l_teacherEmailText.getText(), questionType, tf_firstResponse, tf_secondResponse,
                            tf_thirdResponse, tf_fourthResponse, tf_fifthResponse, tempList, ta_questionText.getText());
                    tempQT = "yes";
                }
            }
            if (l_test.getText() != null && (!l_test.getText().equals(""))) {
                result = Controller.getInstance().addTestQuestion(l_subjectText.getText(), questionType, tf_firstResponse, tf_secondResponse,
                        tf_thirdResponse, tf_fourthResponse, tf_fifthResponse, correctAnswers, ta_questionText.getText());
                tempTS = "yes";
            }

            if (result) {
                if (tempQS.equals("yes")) {
                    tw_question.setItems(Controller.getInstance().getQuestionnaireQuestions(l_subjectText.getText()));
                }

                if (tempQT.equals("yes")) {
                    tw_question.setItems(Controller.getInstance().getQuestionnaireQuestions(l_teacherEmailText.getText()));
                }

                if (tempTS.equals("yes")) {
                    tw_question.setItems(Controller.getInstance().getTestQuestions(l_subjectText.getText()));
                }

                cb_type.getSelectionModel().clearSelection();

                ta_questionText.setText("");
                tf_firstResponse.setText("");
                tf_secondResponse.setText("");
                tf_thirdResponse.setText("");
                tf_fourthResponse.setText("");
                tf_fifthResponse.setText("");

                cb_correctAnswer_1.setSelected(false);
                cb_correctAnswer_2.setSelected(false);
                cb_correctAnswer_3.setSelected(false);
                cb_correctAnswer_4.setSelected(false);
                cb_correctAnswer_5.setSelected(false);

                tf_questionRefersTo.setDisable(true);

                b_add.setDisable(true);
                b_clear.setDisable(true);
            }

        });

        b_delete = new Button();
        b_delete.setText("Delete");
        b_delete.setPrefWidth(70);
        b_delete.setPrefHeight(35);
        b_delete.setDisable(true);
        b_delete.setOnAction(e -> {

            String tempQS = "";
            String tempQT = "";
            String tempTS = "";

            if (l_questionnaire.getText() != null && (!l_questionnaire.getText().equals(""))) {
                if (l_subjectText.getText() != null && (!l_subjectText.getText().equals(""))) {
                    Controller.getInstance().deleteQuestionnaireQuestion(l_subjectText.getText(), ta_questionText.getText());
                    tempQS = "yes";
                }
                if (l_teacherEmailText.getText() != null && (!l_teacherEmailText.getText().equals(""))) {
                    Controller.getInstance().deleteQuestionnaireQuestion(l_teacherEmailText.getText(), ta_questionText.getText());
                    tempQT = "yes";
                }
            }
            if (l_test.getText() != null && (!l_test.getText().equals(""))) {
                Controller.getInstance().deleteTestQuestion(l_subjectText.getText(), ta_questionText.getText());
                tempTS = "yes";
            }

            if (tempQS.equals("yes")) {
                tw_question.setItems(Controller.getInstance().getQuestionnaireQuestions(l_subjectText.getText()));
            }

            if (tempQT.equals("yes")) {
                tw_question.setItems(Controller.getInstance().getQuestionnaireQuestions(l_teacherEmailText.getText()));
            }

            if (tempTS.equals("yes")) {
                tw_question.setItems(Controller.getInstance().getTestQuestions(l_subjectText.getText()));
            }

            cb_type.getSelectionModel().clearSelection();

            ta_questionText.setText("");
            tf_firstResponse.setText("");
            tf_secondResponse.setText("");
            tf_thirdResponse.setText("");
            tf_fourthResponse.setText("");
            tf_fifthResponse.setText("");

            cb_correctAnswer_1.setSelected(false);
            cb_correctAnswer_2.setSelected(false);
            cb_correctAnswer_3.setSelected(false);
            cb_correctAnswer_4.setSelected(false);
            cb_correctAnswer_5.setSelected(false);

            tf_questionRefersTo.setDisable(true);

            b_delete.setDisable(true);
            b_update.setDisable(true);
            b_clear.setDisable(true);
        });

        b_update = new Button();
        b_update.setText("Update");
        b_update.setPrefWidth(70);
        b_update.setPrefHeight(35);
        b_update.setDisable(true);
        b_update.setOnAction(e -> {
            ArrayList<Integer> correctAnswers = new ArrayList<>();
            if (cb_correctAnswer_1.isSelected()) {
                correctAnswers.add(1);
            }
            if (cb_correctAnswer_2.isSelected()) {
                correctAnswers.add(2);
            }
            if (cb_correctAnswer_3.isSelected()) {
                correctAnswers.add(3);
            }
            if (cb_correctAnswer_4.isSelected()) {
                correctAnswers.add(4);
            }
            if (cb_correctAnswer_5.isSelected()) {
                correctAnswers.add(5);
            }

            ArrayList<Integer> tempList = new ArrayList<>();

            String tempQS = "";
            String tempQT = "";
            String tempTS = "";

            boolean result = false;

            if (l_questionnaire.getText() != null && (!l_questionnaire.getText().equals(""))) {
                if (l_subjectText.getText() != null && (!l_subjectText.getText().equals(""))) {
                    result = Controller.getInstance().updateQuestionnaireQuestion(l_subjectText.getText(), questionType, tf_firstResponse, tf_secondResponse,
                            tf_thirdResponse, tf_fourthResponse, tf_fifthResponse, tempList, oldQuestionText, ta_questionText.getText());
                    tempQS = "yes";
                }
                if (l_teacherEmailText.getText() != null && (!l_teacherEmailText.getText().equals(""))) {
                    result = Controller.getInstance().updateQuestionnaireQuestion(l_teacherEmailText.getText(), questionType, tf_firstResponse, tf_secondResponse,
                            tf_thirdResponse, tf_fourthResponse, tf_fifthResponse, tempList, oldQuestionText, ta_questionText.getText());
                    tempQT = "yes";
                }
            }
            if (l_test.getText() != null && (!l_test.getText().equals(""))) {
                result = Controller.getInstance().updateTestQuestion(l_subjectText.getText(), questionType, tf_firstResponse, tf_secondResponse,
                        tf_thirdResponse, tf_fourthResponse, tf_fifthResponse, correctAnswers, oldQuestionText, ta_questionText.getText());
                tempTS = "yes";
            }

            if (result) {
                if (tempQS.equals("yes")) {
                    tw_question.setItems(Controller.getInstance().getQuestionnaireQuestions(l_subjectText.getText()));
                }

                if (tempQT.equals("yes")) {
                    tw_question.setItems(Controller.getInstance().getQuestionnaireQuestions(l_teacherEmailText.getText()));
                }

                if (tempTS.equals("yes")) {
                    tw_question.setItems(Controller.getInstance().getTestQuestions(l_subjectText.getText()));
                }

                cb_type.getSelectionModel().clearSelection();

                ta_questionText.setText("");
                tf_firstResponse.setText("");
                tf_secondResponse.setText("");
                tf_thirdResponse.setText("");
                tf_fourthResponse.setText("");
                tf_fifthResponse.setText("");

                cb_correctAnswer_1.setSelected(false);
                cb_correctAnswer_2.setSelected(false);
                cb_correctAnswer_3.setSelected(false);
                cb_correctAnswer_4.setSelected(false);
                cb_correctAnswer_5.setSelected(false);

                tf_questionRefersTo.setDisable(true);

                b_delete.setDisable(true);
                b_update.setDisable(true);
                b_clear.setDisable(true);
            }

        });

        b_clear = new Button();
        b_clear.setText("Clear");
        b_clear.setPrefWidth(70);
        b_clear.setPrefHeight(35);
        b_clear.setDisable(true);
        b_clear.setOnAction(e -> {

            cb_type.getSelectionModel().clearSelection();
            ta_questionText.setText("");
            tf_firstResponse.setText("");
            tf_secondResponse.setText("");
            tf_thirdResponse.setText("");
            tf_fourthResponse.setText("");
            tf_fifthResponse.setText("");

            cb_correctAnswer_1.setSelected(false);
            cb_correctAnswer_2.setSelected(false);
            cb_correctAnswer_3.setSelected(false);
            cb_correctAnswer_4.setSelected(false);
            cb_correctAnswer_5.setSelected(false);

            b_clear.setDisable(true);
            b_add.setDisable(true);
            b_delete.setDisable(true);
            b_update.setDisable(true);
        });

        hb_clear = new HBox();
        hb_clear.getChildren().add(b_clear);
        hb_clear.setMinHeight(35);
        hb_clear.setPadding(new Insets(0, 0, 0, 40));

        hb_buttons = new HBox(15);
        hb_buttons.getChildren().addAll(b_add, b_delete, b_update, hb_clear);
        GridPane.setConstraints(hb_buttons, 1, 8, 3, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_buttons, new Insets(10, 0, 12, 0));

        sp_question = new Separator();
        GridPane.setConstraints(sp_question, 0, 9, 4, 1, HPos.CENTER, VPos.TOP);

        cb_questionType = new ComboBox();
        cb_questionType.setPromptText("Choose question type");
        cb_questionType.getItems().addAll("Write answer", "Radio button", "Check box");
        cb_questionType.setMinWidth(190);
        cb_questionType.setPadding(new Insets(0, 0, 0, 10));
        cb_questionType.setOnAction(e -> {
            type = cb_questionType.getValue().toString();
            if (vb_subject.getChildren().contains(l_questionnaire) || vb_teacher.getChildren().contains(l_questionnaire)) {
                if (l_subjectText.getText() != null && (!l_subjectText.getText().equals(""))) {
                    tw_question.setItems(Controller.getInstance().getQuestionnaireQuestions(l_subjectText.getText(), type));
                }
                if (l_teacherEmailText.getText() != null && (!l_teacherEmailText.getText().equals(""))) {
                    tw_question.setItems(Controller.getInstance().getQuestionnaireQuestions(l_teacherEmailText.getText(), type));
                }
            }
            if (vb_subject.getChildren().contains(l_test)) {
                tw_question.setItems(Controller.getInstance().getTestQuestions(l_subjectText.getText(), type));
            }
        });

        b_findQuestionnaire = new Button();
        b_findQuestionnaire.setText("Find questionnaire");
        b_findQuestionnaire.setStyle("-fx-font-size: 12pt;");
        b_findQuestionnaire.setPrefWidth(180);
        b_findQuestionnaire.setPrefHeight(20);
        b_findQuestionnaire.setOnAction(e -> {
            ObservableList<Node> tempList = vb_subject.getChildren();

            int numOfElements = tempList.size();

            if (numOfElements > 0) {
                for (int i = numOfElements - 1; i >= 0; i--) {
                    vb_subject.getChildren().remove(i);
                }
            }

            ObservableList<Node> tempList2 = vb_teacher.getChildren();

            int numOfElements2 = tempList2.size();

            if (numOfElements2 > 0) {
                for (int i = numOfElements2 - 1; i >= 0; i--) {
                    vb_teacher.getChildren().remove(i);
                }
            }

            SelectQuestionnaireFrame.display("questionnaire");
        });

        b_findTest = new Button();
        b_findTest.setText("Find test");
        b_findTest.setStyle("-fx-font-size: 12pt;");
        b_findTest.setPrefWidth(120);
        b_findTest.setPrefHeight(20);
        b_findTest.setOnAction(e -> {
            ObservableList<Node> tempList = vb_subject.getChildren();

            int numOfElements = tempList.size();

            if (numOfElements > 0) {
                for (int i = numOfElements - 1; i >= 0; i--) {
                    vb_subject.getChildren().remove(i);
                }
            }

            SelectTestFrame.display("test");
        });

        hb_cbSearch = new HBox(10);
        hb_cbSearch.getChildren().addAll(b_findQuestionnaire, b_findTest, cb_questionType);
        hb_cbSearch.setAlignment(Pos.CENTER_LEFT);

        l_questionWords = new Label();
        l_questionWords.setText("Search for text:");

        tf_questionWords = new TextField();
        tf_questionWords.setPrefWidth(180);
        tf_questionWords.setOnKeyReleased(event -> {
            String text = tf_questionWords.getText().trim();
            if (vb_subject.getChildren().contains(l_questionnaire) || vb_teacher.getChildren().contains(l_questionnaire)) {
                if (l_subjectText.getText() != null && (!l_subjectText.getText().equals(""))) {
                    tw_question.setItems(Controller.getInstance().getQuestionnaireQuestions(l_subjectText.getText(), type, text));
                }
                if (l_teacherEmailText.getText() != null && (!l_teacherEmailText.getText().equals(""))) {
                    tw_question.setItems(Controller.getInstance().getQuestionnaireQuestions(l_teacherEmailText.getText(), type, text));
                }
            }
            if (vb_subject.getChildren().contains(l_test)) {
                tw_question.setItems(Controller.getInstance().getTestQuestions(l_subjectText.getText(), type, text));
            }
        });

        hb_questionWords = new HBox(5);
        hb_questionWords.getChildren().addAll(l_questionWords, tf_questionWords);
        hb_questionWords.setAlignment(Pos.CENTER_LEFT);

        hb_search = new HBox(30);
        hb_search.getChildren().addAll(hb_cbSearch, hb_questionWords);
        hb_search.setMinWidth(850);
        GridPane.setConstraints(hb_search, 0, 10, 4, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_search, new Insets(30, 0, 0, -80));

        TableColumn<Question, String> col_questionText = new TableColumn<>("Question text");
        col_questionText.setMinWidth(360);
        col_questionText.setStyle("-fx-font-size: 9pt;");
        col_questionText.setCellValueFactory(new PropertyValueFactory<>("questionText"));
        col_questionText.setCellFactory(tc -> {
            TableCell<Question, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            text.wrappingWidthProperty().bind(col_questionText.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            return cell ;
        });

        TableColumn<Question, String> col_questionType = new TableColumn<>("Question type");
        col_questionType.setMinWidth(140);
        col_questionType.setStyle("-fx-font-size: 9pt;");
        col_questionType.setCellValueFactory(new PropertyValueFactory<>("questionTypeDescription"));
        col_questionType.setCellFactory(TooltippedTableCell.forTableColumn());

        tw_question = new TableView<>();
        tw_question.getColumns().addAll(col_questionText, col_questionType);
        tw_question.setMinWidth(500);
        tw_question.setMaxWidth(500);
        tw_question.setStyle("-fx-font-size: 10pt;");
        tw_question.setRowFactory( tv -> {
            TableRow<Question> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You must select populated row");
                    return;
                }
                String questionText = "";
                String questionTypeDescription = "";
                String questionAnswers = "";

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Question rowData = row.getItem();

                    questionText = rowData.getQuestionText();
                    oldQuestionText = questionText;
                    questionTypeDescription = rowData.getQuestionTypeDescription();

                    if (questionTypeDescription.equalsIgnoreCase("radio button")) {
                        questionAnswers = rowData.getQuestionAnswerRb();
                        cb_type.getSelectionModel().select("Radio button");
                    }
                    if (questionTypeDescription.equalsIgnoreCase("check box")) {
                        questionAnswers = rowData.getQuestionAnswerChkbx();
                        cb_type.getSelectionModel().select("Check box");
                    } else if (questionTypeDescription.equalsIgnoreCase("write answer")) {
                        cb_type.getSelectionModel().select("Write answer");
                    }
                    cb_type.setEditable(false);

                    b_update.setDisable(false);
                    b_delete.setDisable(false);
                    b_add.setDisable(true);
                    b_clear.setDisable(false);
                }

                if (l_subjectText.getText() != null && (!l_subjectText.getText().equals(""))) {
                    tf_questionRefersTo.setText(l_subjectText.getText());
                } else if (l_teacherEmailText.getText() != null && (!l_teacherEmailText.getText().equals(""))) {
                    tf_questionRefersTo.setText(l_teacherEmailText.getText());
                }

                ta_questionText.setText(questionText);

                String[] individualAnswers = questionAnswers.split(";");

                for (int i = 0; i < individualAnswers.length; i++) {
                    if (individualAnswers[i].endsWith("*")) {
                        switch (i) {
                            case 0:
                                cb_correctAnswer_1.setSelected(true);
                                individualAnswers[i] = individualAnswers[i].substring(0, individualAnswers[i].length() - 1);
                                break;
                            case 1:
                                cb_correctAnswer_2.setSelected(true);
                                individualAnswers[i] = individualAnswers[i].substring(0, individualAnswers[i].length() - 1);
                                break;
                            case 2:
                                cb_correctAnswer_3.setSelected(true);
                                individualAnswers[i] = individualAnswers[i].substring(0, individualAnswers[i].length() - 1);
                                break;
                            case 3:
                                cb_correctAnswer_4.setSelected(true);
                                individualAnswers[i] = individualAnswers[i].substring(0, individualAnswers[i].length() - 1);
                                break;
                            case 4:
                                cb_correctAnswer_5.setSelected(true);
                                individualAnswers[i] = individualAnswers[i].substring(0, individualAnswers[i].length() - 1);
                                break;
                        }
                    }
                }

                if (!questionTypeDescription.equalsIgnoreCase("write answer")) {
                    tf_firstResponse.setText(individualAnswers[0]);
                    tf_secondResponse.setText(individualAnswers[1]);
                    tf_thirdResponse.setText(individualAnswers[2]);
                    tf_fourthResponse.setText(individualAnswers[3]);
                    tf_fifthResponse.setText(individualAnswers[4]);
                }

                tf_firstResponse.setDisable(false);
                tf_secondResponse.setDisable(false);
                tf_thirdResponse.setDisable(false);
                tf_fourthResponse.setDisable(false);
                tf_fifthResponse.setDisable(false);

                if (l_questionnaire.getText() != null && (!l_questionnaire.getText().equals(""))) {

                    cb_correctAnswer_1.setDisable(true);
                    cb_correctAnswer_2.setDisable(true);
                    cb_correctAnswer_3.setDisable(true);
                    cb_correctAnswer_4.setDisable(true);
                    cb_correctAnswer_5.setDisable(true);
                } else if (l_test.getText() != null && (!l_test.getText().equals(""))) {
                    cb_correctAnswer_1.setDisable(false);
                    cb_correctAnswer_2.setDisable(false);
                    cb_correctAnswer_3.setDisable(false);
                    cb_correctAnswer_4.setDisable(false);
                    cb_correctAnswer_5.setDisable(false);
                }

            });
            return row;
        });

        l_questionnaire = new Label();
        l_questionnaire.setStyle("-fx-font-size: 10pt;");
        l_questionnaire.setPadding(new Insets(2,0,15,0));

        l_test = new Label();
        l_test.setStyle("-fx-font-size: 10pt;");
        l_test.setPadding(new Insets(2,0,15,0));

        l_subject = new Label();
        l_subject.setText("SUBJECT:");
        l_subject.setStyle("-fx-font-size: 10pt;");
        l_subject.setMaxWidth(80);
        l_subject.setPadding(new Insets(2,0,0,0));

        l_subjectText = new Label();
        l_subjectText.setStyle("-fx-font-size: 11pt;");
        l_subjectText.setMaxWidth(250);
        l_subjectText.setMinWidth(250);
        l_subjectText.setWrapText(true);

        hb_subject = new HBox(5);
        hb_subject.getChildren().addAll(l_subject, l_subjectText);

        vb_subject = new VBox(15);
        vb_subject.getChildren().addAll(l_questionnaire, hb_subject);

        l_teacherName = new Label();
        l_teacherName.setText("NAME:");
        l_teacherName.setStyle("-fx-font-size: 10pt;");
        l_teacherName.setMaxWidth(80);
        l_teacherName.setPadding(new Insets(2,0,0,0));

        l_teacherNameText = new Label();
        l_teacherNameText.setStyle("-fx-font-size: 11pt;");
        l_teacherNameText.setMaxWidth(250);
        l_teacherNameText.setMinWidth(250);
        l_teacherNameText.setWrapText(true);

        hb_teacherName = new HBox(5);
        hb_teacherName.getChildren().addAll(l_teacherName, l_teacherNameText);

        l_teacherSurname = new Label();
        l_teacherSurname.setText("SURNAME:");
        l_teacherSurname.setStyle("-fx-font-size: 10pt;");
        l_teacherSurname.setMaxWidth(80);
        l_teacherSurname.setPadding(new Insets(2,0,0,0));

        l_teacherSurnameText = new Label();
        l_teacherSurnameText.setStyle("-fx-font-size: 11pt;");
        l_teacherSurnameText.setMaxWidth(250);
        l_teacherSurnameText.setMinWidth(250);
        l_teacherSurnameText.setWrapText(true);

        hb_teacherSurname = new HBox(5);
        hb_teacherSurname.getChildren().addAll(l_teacherSurname, l_teacherSurnameText);

        l_teacherEmail = new Label();
        l_teacherEmail.setText("E-MAIL:");
        l_teacherEmail.setStyle("-fx-font-size: 10pt;");
        l_teacherEmail.setMaxWidth(80);
        l_teacherEmail.setPadding(new Insets(2,0,0,0));

        l_teacherEmailText = new Label();
        l_teacherEmailText.setStyle("-fx-font-size: 11pt;");
        l_teacherEmailText.setMaxWidth(250);
        l_teacherEmailText.setMinWidth(250);
        l_teacherEmailText.setWrapText(true);

        hb_teacherEmail = new HBox(5);
        hb_teacherEmail.getChildren().addAll(l_teacherEmail, l_teacherEmailText);

        l_teacherTitle = new Label();
        l_teacherTitle.setText("TITLE:");
        l_teacherTitle.setStyle("-fx-font-size: 10pt;");
        l_teacherTitle.setMaxWidth(80);
        l_teacherTitle.setPadding(new Insets(2,0,0,0));

        l_teacherTitleText = new Label();
        l_teacherTitleText.setStyle("-fx-font-size: 11pt;");
        l_teacherTitleText.setMaxWidth(250);
        l_teacherTitleText.setMinWidth(250);
        l_teacherTitleText.setWrapText(true);

        hb_teacherTitle = new HBox(5);
        hb_teacherTitle.getChildren().addAll(l_teacherTitle, l_teacherTitleText);

        vb_teacher = new VBox(10);
        vb_teacher.getChildren().addAll(l_questionnaire, hb_teacherName, hb_teacherSurname, hb_teacherEmail, hb_teacherTitle);

        hb_twQuestion = new HBox(30);
        hb_twQuestion.getChildren().add(tw_question);
        hb_twQuestion.setMinWidth(870);
        GridPane.setConstraints(hb_twQuestion, 0,11, 4, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(hb_twQuestion, new Insets(15, 0, 15,0));

        gp_dataDisplay.getChildren().addAll(txt_question, vb_questionType, l_questionText, ta_questionText,
                                            l_firstResponse, tf_firstResponse, l_secondResponse, tf_secondResponse,
                                            l_thirdResponse, tf_thirdResponse, l_fourthResponse, tf_fourthResponse,
                                            l_fifthResponse, tf_fifthResponse, l_correctAnswer, hb_correctAnswer, hb_buttons, sp_question,
                                            hb_search, hb_twQuestion);
    }

    private boolean clear() {
        if ((ta_questionText.getText() != null || (!ta_questionText.getText().equals(""))) || (tf_firstResponse.getText() != null || (!tf_firstResponse.getText().equals("")))
                || (tf_secondResponse.getText() != null || (!tf_secondResponse.getText().equals(""))) || (tf_thirdResponse.getText() != null || (!tf_thirdResponse.getText().equals("")))
                || (tf_fourthResponse.getText() != null || (!tf_fourthResponse.getText().equals(""))) || (tf_fifthResponse.getText() != null || (!tf_fifthResponse.getText().equals("")))
                || (cb_correctAnswer_1.isSelected()) || (cb_correctAnswer_2.isSelected()) || (cb_correctAnswer_3.isSelected()) || (cb_correctAnswer_4.isSelected()) ||
                (cb_correctAnswer_5.isSelected())) {
            return true;
        }
        return false;
    }

    private boolean hasText() {
        if ((ta_questionText.getText() == null || (ta_questionText.getText().equals(""))) && (tf_firstResponse.getText() == null || (tf_firstResponse.getText().equals("")))
                && (tf_secondResponse.getText() == null || (tf_secondResponse.getText().equals(""))) && (tf_thirdResponse.getText() == null || (tf_thirdResponse.getText().equals("")))
                && (tf_fourthResponse.getText() == null || (tf_fourthResponse.getText().equals(""))) && (tf_fifthResponse.getText() == null || (tf_fifthResponse.getText().equals("")))
                && (!cb_correctAnswer_1.isSelected()) && (!cb_correctAnswer_2.isSelected()) && (!cb_correctAnswer_3.isSelected()) && (!cb_correctAnswer_4.isSelected()) &&
                (!cb_correctAnswer_5.isSelected())) {
            return false;
        }
        return true;
    }

    private void setClearAndAddButton() {
        boolean answer = clear();
        if (answer) {
            b_clear.setDisable(false);
            if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                b_add.setDisable(true);
            } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                b_add.setDisable(false);
            }
        }

        if (!hasText()) {
            b_clear.setDisable(true);
            b_add.setDisable(true);
        }
    }

    public Label getL_subjectText() {
        return l_subjectText;
    }

    public Label getL_teacherNameText() {
        return l_teacherNameText;
    }

    public Label getL_teacherSurnameText() {
        return l_teacherSurnameText;
    }

    public Label getL_teacherEmailText() {
        return l_teacherEmailText;
    }

    public Label getL_teacherTitleText() {
        return l_teacherTitleText;
    }

    public HBox getHb_twQuestion() {
        return hb_twQuestion;
    }

    public HBox getHb_subject() {
        return hb_subject;
    }

    public VBox getVb_subject() {
        return vb_subject;
    }

    public VBox getVb_teacher() {
        return vb_teacher;
    }

    public HBox getHb_teacherName() {
        return hb_teacherName;
    }

    public HBox getHb_teacherSurname() {
        return hb_teacherSurname;
    }

    public HBox getHb_teacherEmail() {
        return hb_teacherEmail;
    }

    public HBox getHb_teacherTitle() {
        return hb_teacherTitle;
    }

    public Button getB_delete() {
        return b_delete;
    }

    public Button getB_update() {
        return b_update;
    }

    public Label getL_questionnaire() {
        return l_questionnaire;
    }

    public Label getL_test() {
        return l_test;
    }

    public TableView<Question> getTw_question() {
        return tw_question;
    }

    public TextField getTf_questionRefersTo() {
        return tf_questionRefersTo;
    }

    public TextArea getTa_questionText() {
        return ta_questionText;
    }

    public ComboBox<String> getCb_type() {
        return cb_type;
    }

    public TextField getTf_firstResponse() {
        return tf_firstResponse;
    }

    public TextField getTf_secondResponse() {
        return tf_secondResponse;
    }

    public TextField getTf_thirdResponse() {
        return tf_thirdResponse;
    }

    public TextField getTf_fourthResponse() {
        return tf_fourthResponse;
    }

    public TextField getTf_fifthResponse() {
        return tf_fifthResponse;
    }

    public CheckBox getCb_correctAnswer_1() {
        return cb_correctAnswer_1;
    }

    public CheckBox getCb_correctAnswer_2() {
        return cb_correctAnswer_2;
    }

    public CheckBox getCb_correctAnswer_3() {
        return cb_correctAnswer_3;
    }

    public CheckBox getCb_correctAnswer_4() {
        return cb_correctAnswer_4;
    }

    public CheckBox getCb_correctAnswer_5() {
        return cb_correctAnswer_5;
    }

    public Button getB_clear() {
        return b_clear;
    }
}
