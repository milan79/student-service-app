package frames;

import controller.Controller;
import data.Subject;
import data.Teacher;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import javax.swing.*;

public class QuestionnairesFrame extends VBox {

    private Text txt_questionnaire;

    private Label l_subject;
    private TextField tf_subject;

    private Button b_clear;
    private Button b_findSubject;

    private Label l_teacherName;
    private TextField tf_teacherName;

    private Label l_teacherSurname;
    private TextField tf_teacherSurname;

    private Label l_teacherEmail;
    private TextField tf_teacherEmail;

    private TextField tf_placeholderTeacherTitle;

    private Button b_findTeacher;

    private Button b_add;
    private Button b_delete;
    private Button b_view;

    private HBox hb_buttons;

    private Separator sp_questionnaire;

    private Text txt_existingQuestionnaire;

    private ComboBox cb_questionnaireType;
    private TextField tf_subjectTitle;

    private HBox hb_subjectTitle;

    private ComboBox cb_teacherTitle;
    private String teacherTitle;

    private ComboBox cb_searchCriteria;
    private TextField tf_searchCriteria;
    private String searchCriteria;

    private HBox hb_searchCriteria;

    private HBox hb_search;

    private TableView<Subject> tw_subjects;
    private TableView<Teacher> tw_teachers;

    private VBox vb_Questionnaire;

    private GridPane gp_dataDisplay;

    public QuestionnairesFrame() {
        super(10);
        setStyle("-fx-background-color: #cce6ff;");

        setComponents();
        getChildren().addAll(Controller.getInstance().getTopPageNavigationFrame(), gp_dataDisplay);
    }

    private void setComponents() {

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.CENTER);

        txt_questionnaire = new Text("QUESTIONNAIRE DATA");
        txt_questionnaire.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_questionnaire, 0, 0, 2, 1);
        GridPane.setMargin(txt_questionnaire, new Insets(0, 0, 0, 0));

        l_subject = new Label();
        l_subject.setText("Subject:");
        l_subject.setTextAlignment(TextAlignment.LEFT);
        GridPane.setConstraints(l_subject, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_subject, new Insets(12, 5, 0, 0));

        tf_subject = new TextField();
        tf_subject.setEditable(false);
        GridPane.setConstraints(tf_subject, 1, 1, 2, 1);
        GridPane.setMargin(tf_subject, new Insets(12, 0, 0, 0));

        b_clear = new Button();
        b_clear.setText("Clear");
        b_clear.setPrefWidth(70);
        b_clear.setPrefHeight(20);
        b_clear.setDisable(true);
        GridPane.setConstraints(b_clear, 3, 1, 1, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(b_clear, new Insets(12, 0, 0, 20));
        b_clear.setOnAction(e -> {

            tf_subject.setText("");
            tf_teacherName.setText("");
            tf_teacherSurname.setText("");
            tf_teacherEmail.setText("");

            b_clear.setDisable(true);
            b_add.setDisable(true);
            b_delete.setDisable(true);
            b_view.setDisable(true);
        });

        b_findSubject = new Button();
        b_findSubject.setText("Find subject");
        b_findSubject.setStyle("-fx-font-size: 12pt;");
        b_findSubject.setPrefWidth(160);
        b_findSubject.setPrefHeight(20);
        b_findSubject.setOnAction(e -> SelectSubjectFrame.display("questionnaires"));
        GridPane.setConstraints(b_findSubject, 4, 1, 2, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(b_findSubject, new Insets(9, 0, 0, -30));

        l_teacherName = new Label();
        l_teacherName.setText("Teacher\nname:");
        l_teacherName.setWrapText(true);
        l_teacherName.setTextAlignment(TextAlignment.LEFT);
        GridPane.setConstraints(l_teacherName, 0, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherName, new Insets(12, 5, 0, 0));

        tf_teacherName = new TextField();
        tf_teacherName.setMaxWidth(140);
        tf_teacherName.setEditable(false);
        GridPane.setConstraints(tf_teacherName, 1, 2);
        GridPane.setMargin(tf_teacherName, new Insets(12, 20, 0, 0));

        l_teacherSurname = new Label();
        l_teacherSurname.setText("Teacher\nsurname:");
        l_teacherSurname.setWrapText(true);
        l_teacherSurname.setTextAlignment(TextAlignment.LEFT);
        GridPane.setConstraints(l_teacherSurname, 2, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherSurname, new Insets(12, 5, 0, 0));

        tf_teacherSurname = new TextField();
        tf_teacherSurname.setMaxWidth(140);
        tf_teacherSurname.setEditable(false);
        GridPane.setConstraints(tf_teacherSurname, 3, 2);
        GridPane.setMargin(tf_teacherSurname, new Insets(12, 20, 0, 0));

        l_teacherEmail = new Label();
        l_teacherEmail.setText("Teacher\ne-mail:");
        l_teacherEmail.setWrapText(true);
        l_teacherEmail.setTextAlignment(TextAlignment.LEFT);
        GridPane.setConstraints(l_teacherEmail, 4, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherEmail, new Insets(12, 5, 0, 0));

        tf_teacherEmail = new TextField();
        tf_teacherEmail.setMaxWidth(140);
        tf_teacherEmail.setEditable(false);
        GridPane.setConstraints(tf_teacherEmail, 5, 2);
        GridPane.setMargin(tf_teacherEmail, new Insets(12, 0, 0, 0));

        tf_placeholderTeacherTitle = new TextField();

        b_findTeacher = new Button();
        b_findTeacher.setText("Find teacher");
        b_findTeacher.setStyle("-fx-font-size: 12pt;");
        b_findTeacher.setPrefWidth(160);
        b_findTeacher.setPrefHeight(20);
        b_findTeacher.setOnAction(e -> SelectTeacherFrame.display("Questionnaires"));
        GridPane.setConstraints(b_findTeacher, 6, 2, 1, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(b_findTeacher, new Insets(12, 10, 0, 40));

        b_add = new Button();
        b_add.setText("Add");
        b_add.setPrefWidth(70);
        b_add.setPrefHeight(35);
        b_add.setDisable(true);
        b_add.setOnAction(e -> {
            if ((!tf_subject.equals("")) || tf_subject != null) {
                Controller.getInstance().addNewQuestionnaire(tf_subject.getText());
                tw_subjects.setItems(Controller.getInstance().getSubjectQuestionnaire(null));
            }

            if ((!tf_teacherEmail.equals("")) || tf_teacherEmail != null) {
                Controller.getInstance().addNewQuestionnaire(tf_teacherEmail.getText());
                tw_teachers.setItems(Controller.getInstance().getTeacherQuestionnaire(null));
            }
        });

        b_delete = new Button();
        b_delete.setText("Delete");
        b_delete.setPrefWidth(70);
        b_delete.setPrefHeight(35);
        b_delete.setDisable(true);
        b_delete.setOnAction(e -> {
            if ((!tf_subject.equals("")) || tf_subject != null) {
                Controller.getInstance().deleteQuestionnaire(tf_subject.getText());
                tw_subjects.setItems(Controller.getInstance().getSubjectQuestionnaire(null));
            }

            if ((!tf_teacherEmail.equals("")) || tf_teacherEmail != null) {
                Controller.getInstance().deleteQuestionnaire(tf_teacherEmail.getText());
                tw_teachers.setItems(Controller.getInstance().getTeacherQuestionnaire(null));
            }
        });

        b_view = new Button();
        b_view.setText("View");
        b_view.setPrefWidth(70);
        b_view.setPrefHeight(35);
        b_view.setDisable(true);
        b_view.setOnAction(e -> QuestionnairesFrameView.display());

        hb_buttons = new HBox(15);
        hb_buttons.getChildren().addAll(b_add, b_delete, b_view);
        GridPane.setConstraints(hb_buttons, 1, 3, 6, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_buttons, new Insets(12, 0, 12, 0));

        sp_questionnaire = new Separator();
        GridPane.setConstraints(sp_questionnaire, 0, 4, 7, 1, HPos.CENTER, VPos.TOP);

        txt_existingQuestionnaire = new Text("EXISTING QUESTIONNAIRES");
        txt_existingQuestionnaire.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_existingQuestionnaire, 0, 5, 2, 1);
        GridPane.setMargin(txt_existingQuestionnaire, new Insets(10, 0, 0, 0));

        cb_questionnaireType = new ComboBox<>();
        cb_questionnaireType.setPromptText("Refers to");
        cb_questionnaireType.getItems().addAll("Subject", "Teacher");
        cb_questionnaireType.setPrefWidth(140);
        cb_questionnaireType.setOnAction(e -> {
            String type = cb_questionnaireType.getValue().toString().trim();
            if (type.equals("Subject")) {
                if (hb_search.getChildren().contains(hb_searchCriteria)) {
                    hb_search.getChildren().remove(hb_searchCriteria);
                }
                if (!hb_search.getChildren().contains(hb_subjectTitle)) {
                    hb_search.getChildren().add(hb_subjectTitle);
                }
                if (vb_Questionnaire.getChildren().contains(tw_teachers)) {
                    vb_Questionnaire.getChildren().remove(tw_teachers);
                }
                if (!vb_Questionnaire.getChildren().contains(tw_subjects)) {
                    vb_Questionnaire.getChildren().add(tw_subjects);
                }
                tw_subjects.setItems(Controller.getInstance().getSubjectQuestionnaire(null));
            }

            if (type.equals("Teacher")) {
                if (hb_search.getChildren().contains(hb_subjectTitle)) {
                    hb_search.getChildren().remove(hb_subjectTitle);
                }
                if (!hb_search.getChildren().contains(hb_searchCriteria)) {
                    hb_search.getChildren().add(hb_searchCriteria);
                }
                if (vb_Questionnaire.getChildren().contains(tw_subjects)) {
                    vb_Questionnaire.getChildren().remove(tw_subjects);
                }
                if (!vb_Questionnaire.getChildren().contains(tw_teachers)) {
                    vb_Questionnaire.getChildren().add(tw_teachers);
                }
                tw_teachers.setItems(Controller.getInstance().getTeacherQuestionnaire(null));
            }
        });

        tf_subjectTitle = new TextField();
        tf_subjectTitle.setPrefWidth(170);
        tf_subjectTitle.setOnKeyReleased(event -> {
            String subjectTitleText = tf_subjectTitle.getText().trim();
            tw_subjects.setItems(Controller.getInstance().getSubjectQuestionnaire(subjectTitleText));
        });

        hb_subjectTitle = new HBox();
        hb_subjectTitle.getChildren().add(tf_subjectTitle);
        hb_subjectTitle.setMinWidth(300);
        hb_subjectTitle.setPadding(new Insets(0, 0, 0, 30));

        cb_teacherTitle = new ComboBox();
        cb_teacherTitle.setPromptText("Choose title");
        cb_teacherTitle.getItems().addAll("Professor", "Assistant", "Demonstrator");
        cb_teacherTitle.setPrefWidth(160);
        cb_teacherTitle.setPadding(new Insets(0, 5, 0, 10));
        cb_teacherTitle.setOnAction(e -> {
            teacherTitle = cb_teacherTitle.getValue().toString().trim();
            tw_teachers.setItems(Controller.getInstance().getTeacherQuestionnaire(teacherTitle));
        });

        cb_searchCriteria = new ComboBox();
        cb_searchCriteria.setPromptText("Choose search criteria");
        cb_searchCriteria.getItems().addAll("Teacher name", "Teacher surname", "Teacher e-mail");
        cb_searchCriteria.setPrefWidth(220);
        cb_searchCriteria.setOnAction(e -> {
            searchCriteria = cb_searchCriteria.getValue().toString().trim();
        });

        tf_searchCriteria = new TextField();
        tf_searchCriteria.setPrefWidth(150);
        tf_searchCriteria.setOnKeyReleased(event -> {
            String criteriaData = tf_searchCriteria.getText().trim();
            tw_teachers.setItems(Controller.getInstance().getTeachersQuestionnairesByCriteria(searchCriteria, criteriaData, teacherTitle));
        });

        hb_searchCriteria = new HBox(5);
        hb_searchCriteria.getChildren().addAll(cb_teacherTitle, cb_searchCriteria, tf_searchCriteria);
        hb_searchCriteria.setPadding(new Insets(0, 0, 0, 30));

        hb_search = new HBox();
        hb_search.getChildren().addAll(cb_questionnaireType, hb_subjectTitle);
        GridPane.setConstraints(hb_search, 0, 6, 7, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_search, new Insets(30, 0, 0, 0));

        TableColumn<Subject, String> col_subjectName = new TableColumn<>("Subject");
        col_subjectName.setMinWidth(220);
        col_subjectName.setStyle("-fx-font-size: 10pt;");
        col_subjectName.setCellValueFactory(new PropertyValueFactory<>("subjectTitle"));
        col_subjectName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherName = new TableColumn<>("Name");
        col_teacherName.setMinWidth(120);
        col_teacherName.setStyle("-fx-font-size: 9pt;");
        col_teacherName.setCellValueFactory(new PropertyValueFactory<>("teacherName"));
        col_teacherName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherSurname = new TableColumn<>("Surname");
        col_teacherSurname.setMinWidth(160);
        col_teacherSurname.setStyle("-fx-font-size: 9pt;");
        col_teacherSurname.setCellValueFactory(new PropertyValueFactory<>("teacherSurname"));
        col_teacherSurname.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherEmail = new TableColumn<>("E-mail");
        col_teacherEmail.setMinWidth(200);
        col_teacherEmail.setStyle("-fx-font-size: 9pt;");
        col_teacherEmail.setCellValueFactory(new PropertyValueFactory<>("teacherEmail"));
        col_teacherEmail.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherCategoryDescription = new TableColumn<>("Title");
        col_teacherCategoryDescription.setMinWidth(100);
        col_teacherCategoryDescription.setStyle("-fx-font-size: 9pt;");
        col_teacherCategoryDescription.setCellValueFactory(new PropertyValueFactory<>("teacherTitleDescription"));
        col_teacherCategoryDescription.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacher = new TableColumn<>("Teacher");
        col_teacher.setMinWidth(580);
        col_teacher.setStyle("-fx-font-size: 10pt;");
        col_teacher.getColumns().addAll(col_teacherName, col_teacherSurname, col_teacherEmail, col_teacherCategoryDescription);

        tw_subjects = new TableView<>();
        tw_subjects.getColumns().add(col_subjectName);
        tw_subjects.setMaxWidth(220);
        tw_subjects.setStyle("-fx-font-size: 9pt;");
        tw_subjects.setRowFactory( tv -> {
            TableRow<Subject> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You have to select populated row");
                    return;
                }

                String subjectTitle = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Subject rowData = row.getItem();

                    subjectTitle = rowData.getSubjectTitle();
                }

                tf_subject.setText(subjectTitle);

                tf_teacherSurname.setText("");
                tf_teacherName.setText("");
                tf_teacherEmail.setText("");
                tf_placeholderTeacherTitle.setText("");

                b_add.setDisable(true);
                b_delete.setDisable(false);
                b_view.setDisable(false);
                b_clear.setDisable(false);
            });
            return row;
        });

        tw_teachers = new TableView<>();
        tw_teachers.getColumns().add(col_teacher);
        tw_teachers.setMaxWidth(580);
        tw_teachers.setStyle("-fx-font-size: 9pt;");
        tw_teachers.setRowFactory( tv -> {
            TableRow<Teacher> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You have to select populated row");
                    return;
                }

                String teacherName = null;
                String teacherSurname = null;
                String teacherEmail = null;
                String teacherTitle = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Teacher rowData = row.getItem();

                    teacherName = rowData.getTeacherName();
                    teacherSurname = rowData.getTeacherSurname();
                    teacherEmail = rowData.getTeacherEmail();
                    teacherTitle = rowData.getTeacherTitleDescription();
                }

                tf_teacherName.setText(teacherName);
                tf_teacherSurname.setText(teacherSurname);
                tf_teacherEmail.setText(teacherEmail);
                tf_placeholderTeacherTitle.setText(teacherTitle);

                tf_subject.setText("");

                b_add.setDisable(true);
                b_delete.setDisable(false);
                b_view.setDisable(false);

            });
            return row;
        });

        vb_Questionnaire = new VBox();
        vb_Questionnaire.getChildren().add(tw_subjects);
        GridPane.setConstraints(vb_Questionnaire, 0,7, 7, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(vb_Questionnaire, new Insets(15, 0, 15,0));

        gp_dataDisplay.getChildren().addAll(txt_questionnaire, l_subject, tf_subject, b_clear, b_findSubject, l_teacherName, tf_teacherName,
                                            l_teacherSurname, tf_teacherSurname, l_teacherEmail, tf_teacherEmail, b_findTeacher, hb_buttons, sp_questionnaire,
                                            txt_existingQuestionnaire, hb_search, vb_Questionnaire);
    }

    public TextField getTf_subject() {
        return tf_subject;
    }

    public TextField getTf_teacherName() {
        return tf_teacherName;
    }

    public TextField getTf_teacherSurname() {
        return tf_teacherSurname;
    }

    public TextField getTf_teacherEmail() {
        return tf_teacherEmail;
    }

    public TextField getTf_placeholderTeacherTitle() {
        return tf_placeholderTeacherTitle;
    }

    public Button getB_add() {
        return b_add;
    }

    public Button getB_delete() {
        return b_delete;
    }

    public Button getB_view() {
        return b_view;
    }

    public Button getB_clear() {
        return b_clear;
    }
}
