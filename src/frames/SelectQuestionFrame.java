package frames;

import controller.Controller;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SelectQuestionFrame {

    private static Stage window;
    private static Scene scene;

    private static Text txt_question;

    private static Label l_questionText;
    private static TextArea ta_questionText;

    private static Label l_questionRefersTo;
    private static TextField tf_questionRefersTo;

    private static HBox hb_questionRefersTo;

    private static Label l_questionType;
    private static TextField tf_questionType;

    private static HBox hb_questionType;

    private static VBox vb_questionType;

    private static Label l_firstResponse;
    private static TextField tf_firstResponse;

    private static Label l_secondResponse;
    private static TextField tf_secondResponse;

    private static Label l_thirdResponse;
    private static TextField tf_thirdResponse;

    private static Label l_fourthResponse;
    private static TextField tf_fourthResponse;

    private static Label l_fifthResponse;
    private static TextField tf_fifthResponse;

    private static Label l_correctAnswer;

    private static CheckBox cb_correctAnswer_1;
    private static CheckBox cb_correctAnswer_2;
    private static CheckBox cb_correctAnswer_3;
    private static CheckBox cb_correctAnswer_4;
    private static CheckBox cb_correctAnswer_5;

    private static HBox hb_correctAnswer;

    private static Button b_add;

    private static HBox hb_bAdd;

    private static Separator sp_question;

    private static ComboBox cb_referesTo;

    private static ComboBox cb_questionType;

    private static HBox hb_cbSearch;

    private static Label l_questionWords;
    private static TextField tf_questionWords;

    private static HBox hb_questionWords;

    private static HBox hb_search;

    private static GridPane gp_display;

    public static void display() {

        window = new Stage();
        window.setTitle("Select question");
        window.initModality(Modality.APPLICATION_MODAL);

        gp_display = new GridPane();
        gp_display.setAlignment(Pos.TOP_CENTER);
        gp_display.setPadding(new Insets(30, 0, 15, 0));

        txt_question = new Text("QUESTION DATA");
        txt_question.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_question, 0, 0, 2, 1);
        GridPane.setMargin(txt_question, new Insets(0,0,10,0));

        l_questionRefersTo = new Label();
        l_questionRefersTo.setText("Refers to:");
        l_questionRefersTo.setPadding(new Insets(4,0,0,0));

        tf_questionRefersTo = new TextField();
        tf_questionRefersTo.setPrefWidth(140);

        hb_questionRefersTo = new HBox(5);
        hb_questionRefersTo.getChildren().addAll(l_questionRefersTo, tf_questionRefersTo);
        hb_questionRefersTo.setAlignment(Pos.TOP_RIGHT);

        l_questionType = new Label();
        l_questionType.setText("Type:");
        l_questionType.setPadding(new Insets(4,0,0,0));

        tf_questionType = new TextField();
        tf_questionType.setPrefWidth(140);

        hb_questionType = new HBox(5);
        hb_questionType.getChildren().addAll(l_questionType, tf_questionType);
        hb_questionType.setAlignment(Pos.TOP_RIGHT);

        vb_questionType = new VBox(5);
        vb_questionType.getChildren().addAll(hb_questionRefersTo, hb_questionType);
        GridPane.setConstraints(vb_questionType, 0, 1, 2, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(vb_questionType, new Insets(16, 5, 0, 20));

        l_questionText = new Label();
        l_questionText.setText("Question:");
        GridPane.setConstraints(l_questionText, 2, 1, 1, 1, HPos.RIGHT, VPos.TOP);
        GridPane.setMargin(l_questionText, new Insets(16, 5, 0, 20));

        ta_questionText = new TextArea();
        ta_questionText.setPrefHeight(60);
        ta_questionText.setPrefWidth(360);
        GridPane.setConstraints(ta_questionText, 3, 1);
        GridPane.setMargin(ta_questionText, new Insets(12, 0, 5, 0));

        l_firstResponse = new Label();
        l_firstResponse.setText("Answer_1:");
        GridPane.setConstraints(l_firstResponse, 0, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_firstResponse, new Insets(5, 5, 0, 0));

        tf_firstResponse = new TextField();
        tf_firstResponse.setMaxWidth(400);
        GridPane.setConstraints(tf_firstResponse, 1, 2, 3, 1);
        GridPane.setMargin(tf_firstResponse, new Insets(5, 0, 0, 0));

        l_secondResponse = new Label();
        l_secondResponse.setText("Answer_2:");
        GridPane.setConstraints(l_secondResponse, 0, 3, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_secondResponse, new Insets(5, 5, 0, 0));

        tf_secondResponse = new TextField();
        tf_secondResponse.setMaxWidth(400);
        GridPane.setConstraints(tf_secondResponse, 1, 3, 3, 1);
        GridPane.setMargin(tf_secondResponse, new Insets(5, 0, 0, 0));

        l_thirdResponse = new Label();
        l_thirdResponse.setText("Answer_3:");
        GridPane.setConstraints(l_thirdResponse, 0, 4, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_thirdResponse, new Insets(5, 5, 0, 0));

        tf_thirdResponse = new TextField();
        tf_thirdResponse.setMaxWidth(400);
        GridPane.setConstraints(tf_thirdResponse, 1, 4, 3, 1);
        GridPane.setMargin(tf_thirdResponse, new Insets(5, 0, 0, 0));

        l_fourthResponse = new Label();
        l_fourthResponse.setText("Answer_4:");
        GridPane.setConstraints(l_fourthResponse, 0, 5, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_fourthResponse, new Insets(5, 5, 0, 0));

        tf_fourthResponse = new TextField();
        tf_fourthResponse.setMaxWidth(400);
        GridPane.setConstraints(tf_fourthResponse, 1, 5, 3, 1);
        GridPane.setMargin(tf_fourthResponse, new Insets(5, 0, 0, 0));

        l_fifthResponse = new Label();
        l_fifthResponse.setText("Answer_5:");
        GridPane.setConstraints(l_fifthResponse, 0, 6, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_fifthResponse, new Insets(5, 5, 0, 0));

        tf_fifthResponse = new TextField();
        tf_fifthResponse.setMaxWidth(400);
        GridPane.setConstraints(tf_fifthResponse, 1, 6, 3, 1);
        GridPane.setMargin(tf_fifthResponse, new Insets(5, 0, 0, 0));

        l_correctAnswer = new Label();
        l_correctAnswer.setText("Correct answer(s):");
        GridPane.setConstraints(l_correctAnswer, 0, 7, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_correctAnswer, new Insets(10, 5, 10, 0));

        cb_correctAnswer_1 = new CheckBox("1.");

        cb_correctAnswer_2 = new CheckBox("2.");

        cb_correctAnswer_3 = new CheckBox("3.");

        cb_correctAnswer_4 = new CheckBox("4.");

        cb_correctAnswer_5 = new CheckBox("5.");

        hb_correctAnswer = new HBox(15);
        hb_correctAnswer.getChildren().addAll(cb_correctAnswer_1, cb_correctAnswer_2, cb_correctAnswer_3, cb_correctAnswer_4, cb_correctAnswer_5);
        GridPane.setConstraints(hb_correctAnswer, 1, 7, 3, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_correctAnswer, new Insets(10, 0, 0, 0));

        b_add = new Button();
        b_add.setText("Add question");
        b_add.setPrefWidth(150);
        b_add.setPrefHeight(40);
        b_add.setStyle("-fx-font-size: 12pt;");

        hb_bAdd = new HBox(15);
        hb_bAdd.getChildren().addAll(b_add);
        GridPane.setConstraints(hb_bAdd, 1, 8, 3, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_bAdd, new Insets(10, 0, 12, 0));

        sp_question = new Separator();
        GridPane.setConstraints(sp_question, 0, 9, 4, 1, HPos.CENTER, VPos.TOP);

        cb_questionType = new ComboBox();
        cb_questionType.setPromptText("Choose type");
        cb_questionType.getItems().addAll("Write answer", "Radio button", "Check box");
        cb_questionType.setPrefWidth(150);

        cb_referesTo = new ComboBox();
        cb_referesTo.setPromptText("Refers to");
        cb_referesTo.getItems().addAll("Subject", "Teacher");
        cb_referesTo.setPrefWidth(150);

        hb_cbSearch = new HBox(10);
        hb_cbSearch.getChildren().addAll(cb_referesTo, cb_questionType);

        l_questionWords = new Label();
        l_questionWords.setText("Search for word(s):");
        l_questionWords.setPadding(new Insets(4,0,0,0));

        tf_questionWords = new TextField();
        tf_questionWords.setPrefWidth(200);

        hb_questionWords = new HBox(5);
        hb_questionWords.getChildren().addAll(l_questionWords, tf_questionWords);

        hb_search = new HBox(30);
        hb_search.getChildren().addAll(hb_cbSearch, hb_questionWords);
        GridPane.setConstraints(hb_search, 0, 10, 4, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_search, new Insets(30, 0, 0, 0));

        gp_display.getChildren().addAll(txt_question, vb_questionType, l_questionText, ta_questionText,
                l_firstResponse, tf_firstResponse, l_secondResponse, tf_secondResponse,
                l_thirdResponse, tf_thirdResponse, l_fourthResponse, tf_fourthResponse,
                l_fifthResponse, tf_fifthResponse, l_correctAnswer, hb_correctAnswer, hb_bAdd, sp_question,
                hb_search);

        scene = new Scene(gp_display, 800, 800);
        window.setScene(scene);
        window.showAndWait();
    }

}
