package frames;

import controller.Controller;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

public class AddAdminFrame extends VBox {

    private Label l_adminName;
    private TextField tf_adminName;
    private HBox hb_adminName;

    private Label l_adminSurname;
    private TextField tf_adminSurname;
    private HBox hb_adminSurname;

    private Label l_adminEmail;
    private TextField tf_adminEmail;
    private HBox hb_adminEmail;

    private Label l_adminUsernme;
    private TextField tf_adminUsername;
    private HBox hb_adminUsername;

    private Label l_adminPassword;
    private TextField tf_adminPassword;
    private HBox hb_adminPassword;

    private Button b_add;
    private Button b_cancel;
    private HBox hb_buttons;

    private VBox vb_data;

    private Background vBox_background;

    public AddAdminFrame() {
        super(10);
        setAlignment(Pos.CENTER);

        setComponents();
        getChildren().add(vb_data);

        setBackground(vBox_background);
    }

    private void setComponents() {

        l_adminName = new Label();
        l_adminName.setText("Name:");
        l_adminName.setStyle("-fx-font-size:12pt;");
        l_adminName.setMinWidth(120);

        tf_adminName = new TextField();
        tf_adminName.setMinWidth(230);

        hb_adminName = new HBox();
        hb_adminName.getChildren().addAll(l_adminName, tf_adminName);
        hb_adminName.setAlignment(Pos.TOP_CENTER);

        l_adminSurname = new Label();
        l_adminSurname.setText("Surname:");
        l_adminSurname.setStyle("-fx-font-size:12pt;");
        l_adminSurname.setMinWidth(120);

        tf_adminSurname = new TextField();
        tf_adminSurname.setMinWidth(230);

        hb_adminSurname = new HBox();
        hb_adminSurname.getChildren().addAll(l_adminSurname, tf_adminSurname);
        hb_adminSurname.setAlignment(Pos.TOP_CENTER);

        l_adminUsernme = new Label();
        l_adminUsernme.setText("Username:");
        l_adminUsernme.setStyle("-fx-font-size:12pt;");
        l_adminUsernme.setMinWidth(120);

        tf_adminUsername = new TextField();
        tf_adminUsername.setMinWidth(230);

        hb_adminUsername = new HBox();
        hb_adminUsername.getChildren().addAll(l_adminUsernme, tf_adminUsername);
        hb_adminUsername.setAlignment(Pos.TOP_CENTER);

        l_adminPassword = new Label();
        l_adminPassword.setText("Password:");
        l_adminPassword.setStyle("-fx-font-size:12pt;");
        l_adminPassword.setMinWidth(120);

        tf_adminPassword = new TextField();
        tf_adminPassword.setMinWidth(230);

        hb_adminPassword = new HBox();
        hb_adminPassword.getChildren().addAll(l_adminPassword, tf_adminPassword);
        hb_adminPassword.setAlignment(Pos.TOP_CENTER);

        l_adminEmail = new Label();
        l_adminEmail.setText("Email:");
        l_adminEmail.setStyle("-fx-font-size:12pt;");
        l_adminEmail.setMinWidth(120);

        tf_adminEmail = new TextField();
        tf_adminEmail.setMinWidth(230);

        hb_adminEmail = new HBox();
        hb_adminEmail.getChildren().addAll(l_adminEmail, tf_adminEmail);
        hb_adminEmail.setAlignment(Pos.TOP_CENTER);

        b_add = new Button();
        b_add.setText("Add");
        b_add.setPrefWidth(90);
        b_add.setPrefHeight(35);
        b_add.setStyle("-fx-font-size:13pt;");
        b_add.setOnAction(e -> {
            boolean answer = Controller.getInstance().addAdministrator(tf_adminName, tf_adminSurname, tf_adminUsername, tf_adminPassword, tf_adminEmail);

            if (answer) {
                Controller.getInstance().setWelcomeFrame();
            }
        });

        b_cancel = new Button();
        b_cancel.setText("Cancel");
        b_cancel.setPrefWidth(90);
        b_cancel.setPrefHeight(35);
        b_cancel.setStyle("-fx-font-size:13pt;");
        b_cancel.setOnAction(e -> {
            Controller.getInstance().setWelcomeFrame();
        });

        hb_buttons = new HBox(25);
        hb_buttons.getChildren().addAll(b_add, b_cancel);
        hb_buttons.setAlignment(Pos.TOP_CENTER);
        hb_buttons.setPadding(new Insets(20, 0, 0, 0));

        vb_data = new VBox(15);
        vb_data.getChildren().addAll(hb_adminName, hb_adminSurname, hb_adminUsername, hb_adminPassword, hb_adminEmail, hb_buttons);
        vb_data.setAlignment(Pos.CENTER);
        vb_data.setMaxWidth(400);

        vb_data.setStyle("-fx-background-color: #cce6ff;" +
                "-fx-background-radius: 20px;" +
                "-fx-padding: 30px 45px;" +
                "-fx-border-style: solid outside;" +
                "-fx-border-width: 3px;" +
                "-fx-border-color: #339cff;" +
                "-fx-border-radius: 20px;");

        vBox_background = new Background(new BackgroundFill(Color.web("#75a3a3"), CornerRadii.EMPTY, Insets.EMPTY));
    }
}
