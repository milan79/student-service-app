package frames;

import controller.Controller;
import data.Question;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;

public class SpecificTestFrame {

    private static Stage window;
    private static Scene scene;

    private static VBox vb_root;

    private static Text txt_subject;

    private static Label l_subjectName;
    private static Label l_subjectNameValue;

    private static HBox hb_subject;

    private static VBox vb_subject;

    private static Text txt_student;

    private static Label l_studentName;
    private static Label l_studentNameValue;

    private static HBox hb_studentName;

    private static Label l_studentSurname;
    private static Label l_studentSurnameValue;

    private static HBox hb_studentSurname;

    private static HBox hb_studentData1;

    private static Label l_studentEmail;
    private static Label l_studentEmailValue;

    private static HBox hb_studentData2;

    private static VBox vb_student;

    private static VBox vb_studentAll;

    private static VBox vb_header;

    private static Button b_close;

    private static HBox hb_bClose;

    private static HBox hb_topRow;

    private static Label l_questionNumber;

    private static Label l_question;

    private static TextArea ta_questionText;

    private static RadioButton rb_1;
    private static RadioButton rb_2;
    private static RadioButton rb_3;
    private static RadioButton rb_4;
    private static RadioButton rb_5;
    private static ToggleGroup tg_radioButtons;

    private static CheckBox cb_1;
    private static CheckBox cb_2;
    private static CheckBox cb_3;
    private static CheckBox cb_4;
    private static CheckBox cb_5;

    private static VBox vb_offeredAnswers;

    private static HBox hb_question;

    private static Label l_answer;
    private static TextArea ta_answerText;

    private static HBox hb_answer;

    private static VBox vb_question;

    private static ObservableList<Question> questions;

    private static HBox hb_questionAnswer;

    private static VBox vb_questionAnswer;

    private static GridPane gp_test;

    private static Separator horSeparator1;
    private static Separator horSeparator2;

    private static GridPane gp_display;

    public static void display() {

        window = new Stage();
        window.setTitle("Test result");
        window.initModality(Modality.APPLICATION_MODAL);

        gp_display = new GridPane();
        gp_display.setAlignment(Pos.TOP_LEFT);
        gp_display.setPadding(new Insets(30, 0, 15, 30));

        txt_subject = new Text("SUBJECT DATA");
        txt_subject.setStyle("-fx-font-size: 12pt;");

        l_subjectName = new Label();
        l_subjectName.setText("TITLE:");
        l_subjectName.setStyle("-fx-font-size: 10pt;");
        l_subjectName.setMaxWidth(80);
        l_subjectName.setPadding(new Insets(2,0,0,0));

        l_subjectNameValue = new Label();
        l_subjectNameValue.setText(Controller.getInstance().getTestResultsFrame().getTf_subject().getText());
        l_subjectNameValue.setStyle("-fx-font-size: 11pt;");
        l_subjectNameValue.setMaxWidth(435);
        l_subjectNameValue.setMinWidth(435);
        l_subjectNameValue.setWrapText(true);

        hb_subject = new HBox(5);
        hb_subject.getChildren().addAll(l_subjectName, l_subjectNameValue);
        hb_subject.setPadding(new Insets(0, 0, 0, 10));
        hb_subject.setAlignment(Pos.TOP_LEFT);

        b_close = new Button();
        b_close.setText("Close");
        b_close.setPrefWidth(80);
        b_close.setPrefHeight(40);
        b_close.setStyle("-fx-font-size: 12pt;");
        b_close.setOnAction(e -> window.close());

        hb_bClose = new HBox();
        hb_bClose.getChildren().add(b_close);
        hb_bClose.setAlignment(Pos.CENTER_RIGHT);
        hb_bClose.setMaxWidth(600);
        hb_bClose.setMinWidth(600);
        hb_bClose.setPadding(new Insets(0,0,0,500));

        hb_topRow = new HBox();
        hb_topRow.getChildren().addAll(txt_subject, hb_bClose);
        hb_topRow.setAlignment(Pos.TOP_LEFT);

        vb_subject = new VBox();
        vb_subject.getChildren().addAll(hb_topRow, hb_subject);

        txt_student = new Text("STUDENT DATA");
        txt_student.setStyle("-fx-font-size: 12pt;");

        l_studentName = new Label();
        l_studentName.setText("NAME:");
        l_studentName.setStyle("-fx-font-size: 10pt;");
        l_studentName.setPrefWidth(50);
        l_studentName.setPadding(new Insets(2,0,0,0));

        l_studentNameValue = new Label();
        l_studentNameValue.setText(Controller.getInstance().getTestResultsFrame().getTf_studentName().getText());
        l_studentNameValue.setStyle("-fx-font-size: 11pt;");
        l_studentNameValue.setMaxWidth(180);
        l_studentNameValue.setMinWidth(180);
        l_studentNameValue.setWrapText(true);

        hb_studentName = new HBox(5);
        hb_studentName.getChildren().addAll(l_studentName, l_studentNameValue);
        hb_studentName.setAlignment(Pos.TOP_LEFT);

        l_studentSurname = new Label();
        l_studentSurname.setText("SURNAME:");
        l_studentSurname.setStyle("-fx-font-size: 10pt;");
        l_studentSurname.setPrefWidth(72);
        l_studentSurname.setPadding(new Insets(2,0,0,0));

        l_studentSurnameValue = new Label();
        l_studentSurnameValue.setText(Controller.getInstance().getTestResultsFrame().getTf_studentSurname().getText());
        l_studentSurnameValue.setStyle("-fx-font-size: 11pt;");
        l_studentSurnameValue.setMaxWidth(180);
        l_studentSurnameValue.setMinWidth(180);
        l_studentSurnameValue.setWrapText(true);

        hb_studentSurname = new HBox(5);
        hb_studentSurname.getChildren().addAll(l_studentSurname, l_studentSurnameValue);
        hb_studentSurname.setAlignment(Pos.TOP_LEFT);

        hb_studentData1 = new HBox(30);
        hb_studentData1.getChildren().addAll(hb_studentName, hb_studentSurname);

        l_studentEmail = new Label();
        l_studentEmail.setText("E-MAIL:");
        l_studentEmail.setStyle("-fx-font-size: 10pt;");
        l_studentEmail.setPrefWidth(50);
        l_studentEmail.setPadding(new Insets(2,0,0,0));

        l_studentEmailValue = new Label();
        l_studentEmailValue.setText(Controller.getInstance().getTestResultsFrame().getTf_studentEmail().getText());
        l_studentEmailValue.setStyle("-fx-font-size: 11pt;");
        l_studentEmailValue.setMaxWidth(180);
        l_studentEmailValue.setMinWidth(180);
        l_studentEmailValue.setWrapText(true);

        hb_studentData2 = new HBox(5);
        hb_studentData2.getChildren().addAll(l_studentEmail, l_studentEmailValue);
        hb_studentData2.setAlignment(Pos.TOP_LEFT);

        vb_student = new VBox(10);
        vb_student.getChildren().addAll(hb_studentData1, hb_studentData2);
        vb_student.setPadding(new Insets(0, 0, 0, 10));
        vb_student.setAlignment(Pos.CENTER_LEFT);

        vb_studentAll = new VBox(10);
        vb_studentAll.getChildren().addAll(txt_student, vb_student);

        vb_header = new VBox(35);
        GridPane.setConstraints(vb_header, 0, 0);
        vb_header.getChildren().addAll(vb_subject, vb_studentAll);

        ColumnConstraints colConstraint_1 = new ColumnConstraints();
        colConstraint_1.setPrefWidth(700);

        gp_test = new GridPane();
        gp_test.getColumnConstraints().addAll(colConstraint_1);
        GridPane.setConstraints(gp_test, 0, 1);
        GridPane.setMargin(gp_test, new Insets(40, 0, 0, 0));

        questions = Controller.getInstance().getTestQuestions(l_subjectNameValue.getText());

        for (int i = 0; i < questions.size(); i++) {

            l_questionNumber = new Label((i + 1) + ".");
            l_questionNumber.setStyle("-fx-font-size: 13pt;");
            l_questionNumber.setPrefWidth(50);
            l_questionNumber.setAlignment(Pos.CENTER);

            l_question = new Label("Q:");
            l_question.setStyle("-fx-font-size: 11pt;");
            l_question.setPrefWidth(20);
            l_question.setAlignment(Pos.TOP_RIGHT);
            l_question.setPadding(new Insets(4.3,0,0,0));

            ta_questionText = new TextArea((i + 1) + " question");
            ta_questionText.setStyle("-fx-font-size: 11pt; -fx-overflow-wrap: break-word;");
            ta_questionText.setPrefWidth(575);
            ta_questionText.setPrefHeight(40);
            ta_questionText.setWrapText(true);
            ta_questionText.setText(questions.get(i).getQuestionText());

            hb_question = new HBox(15);
            hb_question.getChildren().addAll(l_question, ta_questionText);
            hb_question.setPadding(new Insets(0, 10, 0, 10));

            l_answer = new Label("A:");
            l_answer.setStyle("-fx-font-size: 11pt;");
            l_answer.setPrefWidth(20);
            l_answer.setAlignment(Pos.TOP_RIGHT);

            if (questions.get(i).getQuestionTypeDescription().equalsIgnoreCase("write answer")) {
                ta_answerText = new TextArea();
                ta_answerText.setStyle("-fx-font-size: 11pt; -fx-overflow-wrap: break-word;");
                ta_answerText.setPrefWidth(575);
                ta_answerText.setPrefHeight(40);
                ta_answerText.setWrapText(true);

                String answer = Controller.getInstance().getTestStudentAnswer(l_subjectNameValue.getText(), questions.get(i).getQuestionText(),
                            l_studentEmailValue.getText(), "write answer");

                ta_answerText.setText(answer);

                hb_answer = new HBox(15);
                hb_answer.getChildren().addAll(l_answer, ta_answerText);
                hb_answer.setPadding(new Insets(0, 10, 0, 10));

                l_answer.setPadding(new Insets(4.3,0,0,0));
            } else if (questions.get(i).getQuestionTypeDescription().equalsIgnoreCase("radio button")) {
                tg_radioButtons = new ToggleGroup();

                String answers = questions.get(i).getQuestionAnswerRb();
                String[] individualAnswers = answers.split(";");

                int correctAnswer = 0;

                for (int j = 0; j < individualAnswers.length; j++) {
                    if (individualAnswers[j].endsWith("*")) {
                        correctAnswer = j + 1;
                        individualAnswers[j] = individualAnswers[j].substring(0, individualAnswers[j].length() - 1);
                        break;
                    }
                }

                String answer = Controller.getInstance().getTestStudentAnswer(l_subjectNameValue.getText(), questions.get(i).getQuestionText(),
                        l_studentEmailValue.getText(), "radio button");

                int studentAnswer = 0;

                if (!answer.equals("")) {
                    studentAnswer = Integer.parseInt(answer);
                }

                rb_1 = new RadioButton();
                rb_1.setText(individualAnswers[0]);
                rb_1.setToggleGroup(tg_radioButtons);
                rb_2 = new RadioButton();
                rb_2.setText(individualAnswers[1]);
                rb_2.setToggleGroup(tg_radioButtons);
                rb_3 = new RadioButton();
                rb_3.setText(individualAnswers[2]);
                rb_3.setToggleGroup(tg_radioButtons);
                rb_4 = new RadioButton();
                rb_4.setText(individualAnswers[3]);
                rb_4.setToggleGroup(tg_radioButtons);
                rb_5 = new RadioButton();
                rb_5.setText(individualAnswers[4]);
                rb_5.setToggleGroup(tg_radioButtons);

                if (correctAnswer == studentAnswer) {
                    switch (correctAnswer) {
                        case 1:
                            isRbTrue(rb_1, true);
                            break;
                        case 2:
                            isRbTrue(rb_2, true);
                            break;
                        case 3:
                            isRbTrue(rb_3, true);
                            break;
                        case 4:
                            isRbTrue(rb_4, true);
                            break;
                        case 5:
                            isRbTrue(rb_5, true);
                            break;
                    }
                } else if (correctAnswer != studentAnswer) {
                    switch (correctAnswer) {
                        case 1:
                            isRbMissed(rb_1, true);
                            break;
                        case 2:
                            isRbMissed(rb_2, true);
                            break;
                        case 3:
                            isRbMissed(rb_3, true);
                            break;
                        case 4:
                            isRbMissed(rb_4, true);
                            break;
                        case 5:
                            isRbMissed(rb_5, true);
                            break;
                    }

                    switch (studentAnswer) {
                        case 1:
                            isRbTrue(rb_1, false);
                            break;
                        case 2:
                            isRbTrue(rb_2, false);
                            break;
                        case 3:
                            isRbTrue(rb_3, false);
                            break;
                        case 4:
                            isRbTrue(rb_4, false);
                            break;
                        case 5:
                            isRbTrue(rb_5, false);
                            break;
                    }
                }

                vb_offeredAnswers = new VBox(5);
                vb_offeredAnswers.getChildren().addAll(rb_1, rb_2, rb_3, rb_4, rb_5);

                hb_answer = new HBox(15);
                hb_answer.getChildren().addAll(l_answer, vb_offeredAnswers);
                hb_answer.setPadding(new Insets(0, 10, 0, 10));
            } else if (questions.get(i).getQuestionTypeDescription().equalsIgnoreCase("check box")) {
                String answers = questions.get(i).getQuestionAnswerChkbx();
                String[] individualAnswers = answers.split(";");

                ArrayList<Integer> correctAnswers = new ArrayList<>();

                for (int j = 0; j < individualAnswers.length; j++) {
                    if (individualAnswers[j].endsWith("*")) {
                        correctAnswers.add(j + 1);
                        individualAnswers[j] = individualAnswers[j].substring(0, individualAnswers[j].length() - 1);
                    }
                }

                String answer = Controller.getInstance().getTestStudentAnswer(l_subjectNameValue.getText(), questions.get(i).getQuestionText(),
                        l_studentEmailValue.getText(), "check box");

                String[] answerArray = answer.split(";");

                ArrayList<Integer> studentAnswers = new ArrayList<>();

                for (int j = 0; j < answerArray.length; j++) {
                    if (!answerArray[j].equals("")) {
                        studentAnswers.add(Integer.parseInt(answerArray[j]));
                    }
                }

                ArrayList<Integer> trueAnswers = new ArrayList<>();
                ArrayList<Integer> falseAnswers = new ArrayList<>();
                ArrayList<Integer> missedAnswers = new ArrayList<>();

                for (int x : correctAnswers) {
                    if (studentAnswers.contains(x)) {
                        trueAnswers.add(x);
                    } else {
                        missedAnswers.add(x);
                    }
                }

                for (int x : studentAnswers) {
                    if (!trueAnswers.contains(x)) {
                        falseAnswers.add(x);
                    }
                }

                cb_1 = new CheckBox();
                cb_1.setText(individualAnswers[0]);
                cb_2 = new CheckBox();
                cb_2.setText(individualAnswers[1]);
                cb_3 = new CheckBox();
                cb_3.setText(individualAnswers[2]);
                cb_4 = new CheckBox();
                cb_4.setText(individualAnswers[3]);
                cb_5 = new CheckBox();
                cb_5.setText(individualAnswers[4]);

                for (int j = 0; j < trueAnswers.size(); j++) {
                    switch (trueAnswers.get(j)) {
                        case 1:
                            isCbTrue(cb_1, true);
                            break;
                        case 2:
                            isCbTrue(cb_2, true);
                            break;
                        case 3:
                            isCbTrue(cb_3, true);
                            break;
                        case 4:
                            isCbTrue(cb_4, true);
                            break;
                        case 5:
                            isCbTrue(cb_5, true);
                            break;
                    }
                }

                for (int j = 0; j < falseAnswers.size(); j++) {
                    switch (falseAnswers.get(j)) {
                        case 1:
                            isCbTrue(cb_1, false);
                            break;
                        case 2:
                            isCbTrue(cb_2, false);
                            break;
                        case 3:
                            isCbTrue(cb_3, false);
                            break;
                        case 4:
                            isCbTrue(cb_4, false);
                            break;
                        case 5:
                            isCbTrue(cb_5, false);
                            break;
                    }
                }

                for (int j = 0; j < missedAnswers.size(); j++) {
                    switch (missedAnswers.get(j)) {
                        case 1:
                            isCbMissed(cb_1, true);
                            break;
                        case 2:
                            isCbMissed(cb_2, true);
                            break;
                        case 3:
                            isCbMissed(cb_3, true);
                            break;
                        case 4:
                            isCbMissed(cb_4, true);
                            break;
                        case 5:
                            isCbMissed(cb_5, true);
                            break;
                    }
                }

                vb_offeredAnswers = new VBox(5);
                vb_offeredAnswers.getChildren().addAll(cb_1, cb_2, cb_3, cb_4, cb_5);

                hb_answer = new HBox(15);
                hb_answer.getChildren().addAll(l_answer, vb_offeredAnswers);
                hb_answer.setPadding(new Insets(0, 10, 0, 10));
            }

            vb_question = new VBox(10);
            vb_question.getChildren().addAll(hb_question, hb_answer);

            hb_questionAnswer = new HBox();
            hb_questionAnswer.getChildren().addAll(l_questionNumber, vb_question);
            hb_questionAnswer.setAlignment(Pos.CENTER_LEFT);

            horSeparator1 = new Separator();
            horSeparator1.setPrefWidth(700);

            horSeparator2 = new Separator();
            horSeparator2.setPrefWidth(700);
            horSeparator2.setPadding(new Insets(0,0,15,0));

            vb_questionAnswer = new VBox();
            vb_questionAnswer.getChildren().addAll(horSeparator1, horSeparator2, hb_questionAnswer);

            GridPane.setConstraints(vb_questionAnswer, 0, i, 1, 1, HPos.LEFT, VPos.CENTER);
            GridPane.setMargin(vb_questionAnswer, new Insets(10,10,10, 0));

            gp_test.getChildren().add(vb_questionAnswer);
        }

        gp_display.getChildren().addAll(vb_header, gp_test);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(gp_display);

        vb_root = new VBox();
        vb_root.getChildren().addAll(scrollPane);
        vb_root.setAlignment(Pos.CENTER);

        scene = new Scene(vb_root, 800, 700);
        window.setScene(scene);
        window.showAndWait();
    }

    private static void isRbTrue(RadioButton rb, Boolean isTrue) {
        if (isTrue) {
            rb.setStyle("-fx-color: #40ff00");
        } else {
            rb.setStyle("-fx-color: #ff3333");
        }
    }

    private static void isRbMissed(RadioButton rb, Boolean isMissed) {
        if (isMissed) {
            rb.setStyle("-fx-color: #ff6600");
        }
    }

    private static void isCbTrue(CheckBox cb, Boolean isTrue) {
        if (isTrue) {
            cb.setStyle("-fx-color: #40ff00");
        } else {
            cb.setStyle("-fx-color: #ff3333");
        }
    }

    private static void isCbMissed(CheckBox cb, Boolean isMissed) {
        if (isMissed) {
            cb.setStyle("-fx-color: #ff6600");
        }
    }
}
