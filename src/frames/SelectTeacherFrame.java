package frames;

import controller.Controller;
import data.Teacher;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SelectTeacherFrame {

    private static Stage window;
    private static Scene scene;

    private static Text txt_teacher;

    private static Label l_teacherName;
    private static TextField tf_teacherName;

    private static Label l_teacherSurname;
    private static TextField tf_teacherSurname;

    private static Label l_teacherTitle;
    private static TextField tf_teacherTitle;

    private static Label l_teacherEmail;
    private static TextField tf_teacherEmail;

    private static TextField tf_placeholderUsername;

    private static Button b_select;

    private static HBox hb_bSelect;

    private static Separator sp_teacher;

    private static ComboBox cb_title;
    private static String teacherTitle;

    private static ComboBox cb_searchCriteria;
    private static TextField tf_searchCriteria;
    private static String searchCriteria = "";

    private static HBox hb_searchCriteria;

    private static HBox hb_search;

    private static TableView<Teacher> tw_teacher;

    private static VBox vb_twTeacher;

    private static GridPane gp_dataDisplay;

    public static void display(String callingFrame) {

        window = new Stage();
        window.setTitle("Select teacher");
        window.initModality(Modality.APPLICATION_MODAL);

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.TOP_CENTER);
        gp_dataDisplay.setPadding(new Insets(30, 0, 15, 0));

        txt_teacher = new Text("TEACHER DATA");
        txt_teacher.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_teacher, 0, 0, 2, 1);
        GridPane.setMargin(txt_teacher, new Insets(0, 0, 10, 0));

        l_teacherName = new Label();
        l_teacherName.setText("Name:");
        GridPane.setConstraints(l_teacherName, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherName, new Insets(12, 5, 0, 0));

        tf_teacherName = new TextField();
        tf_teacherName.setMaxWidth(140);
        tf_teacherName.setEditable(false);
        GridPane.setConstraints(tf_teacherName, 1, 1);
        GridPane.setMargin(tf_teacherName, new Insets(12, 20, 0, 0));

        l_teacherSurname = new Label();
        l_teacherSurname.setText("Surname:");
        GridPane.setConstraints(l_teacherSurname, 2, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherSurname, new Insets(12, 5, 0, 0));

        tf_teacherSurname = new TextField();
        tf_teacherSurname.setMaxWidth(140);
        tf_teacherSurname.setEditable(false);
        GridPane.setConstraints(tf_teacherSurname, 3, 1);
        GridPane.setMargin(tf_teacherSurname, new Insets(12, 20, 0, 0));

        l_teacherTitle = new Label();
        l_teacherTitle.setText("Title:");
        GridPane.setConstraints(l_teacherTitle, 0, 2,1,1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherTitle, new Insets(12, 5, 12, 0));

        tf_teacherTitle = new TextField();
        tf_teacherTitle.setMaxWidth(140);
        tf_teacherTitle.setEditable(false);
        GridPane.setConstraints(tf_teacherTitle, 1, 2);
        GridPane.setMargin(tf_teacherTitle, new Insets(12, 0, 12, 0));

        l_teacherEmail = new Label();
        l_teacherEmail.setText("E-mail:");
        GridPane.setConstraints(l_teacherEmail, 2, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherEmail, new Insets(12, 5, 12, 0));

        tf_teacherEmail = new TextField();
        tf_teacherEmail.setMaxWidth(140);
        tf_teacherEmail.setEditable(false);
        GridPane.setConstraints(tf_teacherEmail, 3, 2);
        GridPane.setMargin(tf_teacherEmail, new Insets(12, 0, 12, 0));

        b_select = new Button();
        b_select.setText("Confirm");
        b_select.setPrefWidth(130);
        b_select.setPrefHeight(40);
        b_select.setStyle("-fx-font-size: 12pt;");
        b_select.setOnAction(e -> {
            if (callingFrame.equals("Subjects")) {
                Controller.getInstance().getSubjectsFrame().getTf_teacherName().setText(tf_teacherName.getText());
                Controller.getInstance().getSubjectsFrame().getTf_teacherSurname().setText(tf_teacherSurname.getText());
                Controller.getInstance().getSubjectsFrame().getTf_teacherEmail().setText(tf_teacherEmail.getText());
                Controller.getInstance().getSubjectsFrame().getTf_teacherTitle().setText(tf_teacherTitle.getText());

                Controller.getInstance().getSubjectsFrame().getB_assignTeacherToSubject().setDisable(false);
                Controller.getInstance().getSubjectsFrame().getB_removeTeacherFromSubject().setDisable(false);
            } else if (callingFrame.equals("Teachers")) {
                Controller.getInstance().getTeachersFrame().getTf_teacherName().setText(tf_teacherName.getText());
                Controller.getInstance().getTeachersFrame().getTf_teacherSurname().setText(tf_teacherSurname.getText());
                Controller.getInstance().getTeachersFrame().getTf_teacherEmail().setText(tf_teacherEmail.getText());
                Controller.getInstance().getTeachersFrame().getTf_teacherTitle().setText(tf_teacherTitle.getText());
                Controller.getInstance().getTeachersFrame().getTf_teacherUsername().setText(tf_placeholderUsername.getText());
            } else if (callingFrame.equals("Questionnaires")) {
                Controller.getInstance().getQuestionnairesFrame().getTf_teacherName().setText(tf_teacherName.getText());
                Controller.getInstance().getQuestionnairesFrame().getTf_teacherSurname().setText(tf_teacherSurname.getText());
                Controller.getInstance().getQuestionnairesFrame().getTf_teacherEmail().setText(tf_teacherEmail.getText());
                Controller.getInstance().getQuestionnairesFrame().getTf_placeholderTeacherTitle().setText(tf_teacherTitle.getText());

                Controller.getInstance().getQuestionnairesFrame().getB_clear().setDisable(false);
                Controller.getInstance().getQuestionnairesFrame().getTf_subject().setText("");

                if (Controller.getInstance().verifyTeacherQuestionnaire(tf_teacherEmail.getText())) {
                    Controller.getInstance().getQuestionnairesFrame().getB_add().setDisable(true);
                    Controller.getInstance().getQuestionnairesFrame().getB_delete().setDisable(false);
                    Controller.getInstance().getQuestionnairesFrame().getB_view().setDisable(false);
                } else {
                    Controller.getInstance().getQuestionnairesFrame().getB_add().setDisable(false);
                    Controller.getInstance().getQuestionnairesFrame().getB_delete().setDisable(true);
                    Controller.getInstance().getQuestionnairesFrame().getB_view().setDisable(true);
                }
            } else if (callingFrame.equals("QuestionnaireResults")) {
                Controller.getInstance().getQuestionnaireResultsFrame().getTf_teacherName().setText(tf_teacherName.getText());
                Controller.getInstance().getQuestionnaireResultsFrame().getTf_teacherSurname().setText(tf_teacherSurname.getText());
                Controller.getInstance().getQuestionnaireResultsFrame().getTf_teacherEmail().setText(tf_teacherEmail.getText());
            }

            window.close();
        });

        hb_bSelect = new HBox(15);
        hb_bSelect.getChildren().addAll(b_select);
        GridPane.setConstraints(hb_bSelect, 1, 3, 2, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_bSelect, new Insets(10, 0, 12, 0));

        sp_teacher = new Separator();
        GridPane.setConstraints(sp_teacher, 0, 4, 4, 1, HPos.CENTER, VPos.TOP);

        cb_title = new ComboBox<>();
        cb_title.setPromptText("Choose title");
        cb_title.getItems().addAll("Professor", "Assistant", "Demonstrator");
        cb_title.setPrefWidth(140);
        cb_title.setOnAction(e -> {
            teacherTitle = cb_title.getValue().toString().trim();
            tw_teacher.setItems(Controller.getInstance().getTeachersByTitle(teacherTitle));
        });

        cb_searchCriteria = new ComboBox();
        cb_searchCriteria.setPromptText("Choose search criteria");
        cb_searchCriteria.getItems().addAll("Teacher name", "Teacher surname", "Teacher e-mail");
        cb_searchCriteria.setPrefWidth(200);
        cb_searchCriteria.setOnAction(e -> {
            searchCriteria = cb_searchCriteria.getValue().toString();
        });

        tf_searchCriteria = new TextField();
        tf_searchCriteria.setPrefWidth(170);
        tf_searchCriteria.setOnKeyReleased(event -> {
            String criteriaData = tf_searchCriteria.getText().trim();
            tw_teacher.setItems(Controller.getInstance().getTeachersByCriteria(searchCriteria, criteriaData, teacherTitle));
        });

        hb_searchCriteria = new HBox(5);
        hb_searchCriteria.getChildren().addAll(cb_searchCriteria, tf_searchCriteria);
        hb_searchCriteria.setPadding(new Insets(0, 0, 0, 30));

        hb_search = new HBox();
        hb_search.getChildren().addAll(cb_title, hb_searchCriteria);
        GridPane.setConstraints(hb_search, 0, 5, 4, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_search, new Insets(30, 0, 0, 0));

        TableColumn<Teacher, String> col_teacherName = new TableColumn<>("Teacher name");
        col_teacherName.setMinWidth(140);
        col_teacherName.setStyle("-fx-font-size: 9pt;");
        col_teacherName.setCellValueFactory(new PropertyValueFactory<>("teacherName"));
        col_teacherName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherSurname = new TableColumn<>("Teacher surname");
        col_teacherSurname.setMinWidth(180);
        col_teacherSurname.setStyle("-fx-font-size: 9pt;");
        col_teacherSurname.setCellValueFactory(new PropertyValueFactory<>("teacherSurname"));
        col_teacherSurname.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherEmail = new TableColumn<>("Teacher e-mail");
        col_teacherEmail.setMinWidth(240);
        col_teacherEmail.setStyle("-fx-font-size: 9pt;");
        col_teacherEmail.setCellValueFactory(new PropertyValueFactory<>("teacherEmail"));
        col_teacherEmail.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherTitle = new TableColumn<>("Title");
        col_teacherTitle.setMinWidth(140);
        col_teacherTitle.setStyle("-fx-font-size: 9pt;");
        col_teacherTitle.setCellValueFactory(new PropertyValueFactory<>("teacherTitleDescription"));
        col_teacherTitle.setCellFactory(TooltippedTableCell.forTableColumn());

        tw_teacher = new TableView<>();
        tw_teacher.getColumns().addAll(col_teacherName, col_teacherSurname, col_teacherEmail, col_teacherTitle);
        tw_teacher.setMinWidth(680);
        tw_teacher.setStyle("-fx-font-size: 9pt;");

        tf_placeholderUsername = new TextField();

        tw_teacher.setRowFactory( tv -> {
            TableRow<Teacher> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                String teacherUsername = null;
                String teacherName = null;
                String teacherSurname = null;
                String teacherEmail = null;
                String teacherTitleDescription = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Teacher rowData = row.getItem();

                    teacherUsername = rowData.getTeacherUsername();
                    teacherName = rowData.getTeacherName();
                    teacherSurname = rowData.getTeacherSurname();
                    teacherEmail = rowData.getTeacherEmail();
                    teacherTitleDescription = rowData.getTeacherTitleDescription();
                }

                tf_placeholderUsername.setText(teacherUsername);
                tf_teacherName.setText(teacherName);
                tf_teacherSurname.setText(teacherSurname);
                tf_teacherEmail.setText(teacherEmail);
                tf_teacherTitle.setText(teacherTitleDescription);

            });
            return row;
        });

        tw_teacher.setItems(Controller.getInstance().getTeachers());

        vb_twTeacher = new VBox();
        vb_twTeacher.getChildren().add(tw_teacher);
        vb_twTeacher.setMinWidth(680);
        GridPane.setConstraints(vb_twTeacher, 0,6, 4, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(vb_twTeacher, new Insets(15, 0, 15,0));

        gp_dataDisplay.getChildren().addAll(txt_teacher, l_teacherName, tf_teacherName, l_teacherSurname, tf_teacherSurname,
                l_teacherTitle, tf_teacherTitle, l_teacherEmail, tf_teacherEmail, hb_bSelect, sp_teacher, hb_search, vb_twTeacher);

        scene = new Scene(gp_dataDisplay, 800, 800);
        window.setScene(scene);
        window.showAndWait();
    }
}
