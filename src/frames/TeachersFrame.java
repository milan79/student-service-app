package frames;

import controller.Controller;
import data.Student;
import data.Subject;
import data.Teacher;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import javax.swing.*;

public class TeachersFrame extends VBox {

    private Text txt_teacher;

    private Label l_teacherName;
    private TextField tf_teacherName;

    private Label l_teacherSurname;
    private TextField tf_teacherSurname;

    private Label l_teacherTitle;
    private TextField tf_teacherTitle;
    private Button b_clear;

    private HBox hb_clear;

    private Label l_teacherEmail;
    private TextField tf_teacherEmail;

    private Label l_teacherUsernme;
    private TextField tf_teacherUsername;

    private Label l_teacherPassword;
    private TextField tf_teacherPassword;

    private Button b_add;
    private Button b_delete;
    private Button b_update;

    private HBox hb_buttons;

    private Separator sp_teacher;

    private Text txt_student;

    private Label l_studentName;
    private TextField tf_studentName;

    private Label l_studentSurname;
    private TextField tf_studentSurname;

    private Label l_studentEmail;
    private TextField tf_studentEmail;

    private Button b_removeStudentFromTeacher;

    private Button b_assignStudentToTeacher;

    private Button b_findStudent;

    private Separator sp_student;

    private ComboBox cb_title;
    private String teacherTitle;

    private ComboBox cb_searchCriteria;
    private String searchCriteria;
    private TextField tf_searchCriteria;

    private HBox hb_searchCriteria;

    private HBox hb_search;

    private TableView<Teacher> tw_teacher;
    private TableView<Student> tw_student;

    private HBox hb_twTeacherStudent;

    private GridPane gp_dataDisplay;

    public TeachersFrame() {
        super(10);
        setStyle("-fx-background-color: #cce6ff;");

        setComponents();
        getChildren().addAll(Controller.getInstance().getTopPageNavigationFrame(), gp_dataDisplay);
    }

    private void setComponents() {

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.CENTER);
        gp_dataDisplay.setMinWidth(870);
        GridPane.setMargin(gp_dataDisplay, new Insets(0, 0, 15, 0));

        txt_teacher = new Text("TEACHER DATA");
        txt_teacher.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_teacher, 0, 0, 2, 1);
        GridPane.setMargin(txt_teacher, new Insets(0, 0, 0, 0));

        l_teacherName = new Label();
        l_teacherName.setText("Name:");
        GridPane.setConstraints(l_teacherName, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherName, new Insets(12, 5, 0, 0));

        tf_teacherName = new TextField();
        tf_teacherName.setMaxWidth(140);
        tf_teacherName.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });
        GridPane.setConstraints(tf_teacherName, 1, 1);
        GridPane.setMargin(tf_teacherName, new Insets(12, 20, 0, 0));

        l_teacherSurname = new Label();
        l_teacherSurname.setText("Surname:");
        GridPane.setConstraints(l_teacherSurname, 2, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherSurname, new Insets(12, 5, 0, 0));

        tf_teacherSurname = new TextField();
        tf_teacherSurname.setMaxWidth(140);
        tf_teacherSurname.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });
        GridPane.setConstraints(tf_teacherSurname, 3, 1);
        GridPane.setMargin(tf_teacherSurname, new Insets(12, 20, 0, 0));

        l_teacherTitle = new Label();
        l_teacherTitle.setText("Title:");
        GridPane.setConstraints(l_teacherTitle, 4, 1,1,1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherTitle, new Insets(12, 5, 0, 0));

        tf_teacherTitle = new TextField();
        tf_teacherTitle.setMaxWidth(140);
        tf_teacherTitle.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });

        b_clear = new Button();
        b_clear.setText("Clear");
        b_clear.setPrefWidth(70);
        b_clear.setPrefHeight(35);
        b_clear.setDisable(true);
        b_clear.setOnAction(e -> {
            tf_teacherUsername.setText("");
            tf_teacherName.setText("");
            tf_teacherSurname.setText("");
            tf_teacherEmail.setText("");
            tf_teacherTitle.setText("");
            tf_teacherPassword.setText("");

            b_add.setDisable(true);
            b_delete.setDisable(true);
            b_update.setDisable(true);
            b_clear.setDisable(true);

        });

        hb_clear = new HBox(20);
        hb_clear.getChildren().addAll(tf_teacherTitle, b_clear);
        GridPane.setConstraints(hb_clear, 5, 1);
        GridPane.setMargin(hb_clear, new Insets(12, 0, 0, 0));

        l_teacherEmail = new Label();
        l_teacherEmail.setText("E-mail:");
        GridPane.setConstraints(l_teacherEmail, 0, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherEmail, new Insets(5, 5, 0, 0));

        tf_teacherEmail = new TextField();
        tf_teacherEmail.setMaxWidth(140);
        tf_teacherEmail.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });
        GridPane.setConstraints(tf_teacherEmail, 1, 2);
        GridPane.setMargin(tf_teacherEmail, new Insets(5, 0, 0, 0));

        l_teacherUsernme = new Label();
        l_teacherUsernme.setText("Username:");
        GridPane.setConstraints(l_teacherUsernme, 2, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherUsernme, new Insets(5, 5, 0, 0));

        tf_teacherUsername = new TextField();
        tf_teacherUsername.setMaxWidth(140);
        tf_teacherUsername.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });
        GridPane.setConstraints(tf_teacherUsername, 3, 2);
        GridPane.setMargin(tf_teacherUsername, new Insets(5, 20, 0, 0));

        l_teacherPassword = new Label();
        l_teacherPassword.setText("Password:");
        GridPane.setConstraints(l_teacherPassword, 4, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherPassword, new Insets(5, 5, 0, 0));

        tf_teacherPassword = new TextField();
        tf_teacherPassword.setMaxWidth(140);
        tf_teacherPassword.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });
        GridPane.setConstraints(tf_teacherPassword, 5, 2);
        GridPane.setMargin(tf_teacherPassword, new Insets(5, 0, 0, 0));

//        b_findTeacher = new Button();
//        b_findTeacher.setText("Find teacher");
//        b_findTeacher.setStyle("-fx-font-size: 12pt;");
//        b_findTeacher.setPrefWidth(160);
//        b_findTeacher.setPrefHeight(20);
//        b_findTeacher.setOnAction(e -> SelectTeacherFrame.display("Teachers"));
//        GridPane.setConstraints(b_findTeacher, 5, 1,2,2, HPos.LEFT, VPos.CENTER);
//        GridPane.setMargin(b_findTeacher, new Insets(12, 0, 0, 180));

        b_add = new Button();
        b_add.setText("Add");
        b_add.setPrefWidth(70);
        b_add.setPrefHeight(35);
        b_add.setDisable(true);
        b_add.setOnAction(e -> {
            boolean result = Controller.getInstance().addNewTeacher(tf_teacherUsername, tf_teacherEmail,  tf_teacherPassword, tf_teacherName, tf_teacherSurname, tf_teacherTitle);
            tw_teacher.setItems(Controller.getInstance().getTeachers());

            if (result) {
                tf_teacherUsername.setText("");
                tf_teacherName.setText("");
                tf_teacherSurname.setText("");
                tf_teacherEmail.setText("");
                tf_teacherTitle.setText("");
                tf_teacherPassword.setText("");

                b_add.setDisable(true);
                b_clear.setDisable(true);
                b_delete.setDisable(true);
                b_update.setDisable(true);
            }
        });

        b_delete = new Button();
        b_delete.setText("Delete");
        b_delete.setPrefWidth(70);
        b_delete.setPrefHeight(35);
        b_delete.setDisable(true);
        b_delete.setOnAction(e -> {
            boolean test = Controller.getInstance().deleteTeacher(tf_teacherEmail.getText());

            if (test) {
                tw_teacher.setItems(Controller.getInstance().getTeachers());

                tf_teacherUsername.setText("");
                tf_teacherName.setText("");
                tf_teacherSurname.setText("");
                tf_teacherEmail.setText("");
                tf_teacherTitle.setText("");
                tf_teacherPassword.setText("");

                b_add.setDisable(true);
                b_delete.setDisable(true);
                b_update.setDisable(true);
                b_clear.setDisable(true);
            }

        });

        b_update = new Button();
        b_update.setText("Update");
        b_update.setPrefWidth(70);
        b_update.setPrefHeight(35);
        b_update.setDisable(true);
        b_update.setOnAction(e -> {
            boolean test = Controller.getInstance().updateTeacher(tf_teacherUsername, tf_teacherEmail, tf_teacherName, tf_teacherSurname, tf_teacherPassword, tf_teacherTitle);

            if (test) {
                tw_teacher.setItems(Controller.getInstance().getTeachers());

                tf_teacherUsername.setText("");
                tf_teacherName.setText("");
                tf_teacherSurname.setText("");
                tf_teacherEmail.setText("");
                tf_teacherTitle.setText("");
                tf_teacherPassword.setText("");

                b_add.setDisable(true);
                b_delete.setDisable(true);
                b_update.setDisable(true);
                b_clear.setDisable(true);
            }

        });

        hb_buttons = new HBox(15);
        hb_buttons.getChildren().addAll(b_add, b_delete, b_update);
        GridPane.setConstraints(hb_buttons, 1, 3, 5, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_buttons, new Insets(10, 0, 12, 0));

        sp_teacher = new Separator();
        GridPane.setConstraints(sp_teacher, 0, 4, 6, 1, HPos.CENTER, VPos.TOP);

        txt_student = new Text("STUDENT DATA");
        txt_student.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_student, 0, 5, 2, 1);
        GridPane.setMargin(txt_student, new Insets(10, 0, 0, 0));

        b_findStudent = new Button();
        b_findStudent.setText("Find student");
        b_findStudent.setStyle("-fx-font-size: 12pt;");
        b_findStudent.setPrefWidth(160);
        b_findStudent.setPrefHeight(20);
        b_findStudent.setOnAction(e -> SelectStudentFrame.display("Teachers"));
        GridPane.setConstraints(b_findStudent, 5, 6, 1, 2, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(b_findStudent, new Insets(0, 50, 0, 130));

        l_studentName = new Label();
        l_studentName.setText("Name:");
        GridPane.setConstraints(l_studentName, 0, 6, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentName, new Insets(12, 5, 0, 0));

        tf_studentName = new TextField();
        tf_studentName.setMaxWidth(140);
        GridPane.setConstraints(tf_studentName, 1, 6);
        GridPane.setMargin(tf_studentName, new Insets(12, 20, 0, 0));

        l_studentSurname = new Label();
        l_studentSurname.setText("Surname:");
        GridPane.setConstraints(l_studentSurname, 2, 6);
        GridPane.setMargin(l_studentSurname, new Insets(12, 5, 0, 0));

        tf_studentSurname = new TextField();
        tf_studentSurname.setMaxWidth(140);
        GridPane.setConstraints(tf_studentSurname, 3, 6);
        GridPane.setMargin(tf_studentSurname, new Insets(12, 20, 0, 0));

        b_assignStudentToTeacher = new Button();
        b_assignStudentToTeacher.setText("Assign to teacher");
        b_assignStudentToTeacher.setPrefWidth(160);
        b_assignStudentToTeacher.setDisable(true);
        b_assignStudentToTeacher.setOnAction(e -> {
            Controller.getInstance().assignStudentToTeacher(tf_teacherEmail.getText(), tf_studentEmail.getText());
            tw_student.setItems(Controller.getInstance().getStudentsAssignedToTeacher(tf_teacherEmail.getText()));

            tf_studentName.setText("");
            tf_studentSurname.setText("");
            tf_studentEmail.setText("");

            b_assignStudentToTeacher.setDisable(true);
            b_removeStudentFromTeacher.setDisable(true);
        });
        GridPane.setConstraints(b_assignStudentToTeacher, 4, 6, 2, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(b_assignStudentToTeacher, new Insets(12, 0, 0, 0));

        l_studentEmail = new Label();
        l_studentEmail.setText("E-mail:");
        GridPane.setConstraints(l_studentEmail, 0, 7, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentEmail, new Insets(5, 5, 12, 0));

        tf_studentEmail = new TextField();
        tf_studentEmail.setMaxWidth(140);
        GridPane.setConstraints(tf_studentEmail, 1, 7);
        GridPane.setMargin(tf_studentEmail, new Insets(5, 20, 12, 0));

        b_removeStudentFromTeacher = new Button();
        b_removeStudentFromTeacher.setText("Remove from teacher");
        b_removeStudentFromTeacher.setPrefWidth(160);
        b_removeStudentFromTeacher.setDisable(true);
        b_removeStudentFromTeacher.setOnAction(e -> {
            Controller.getInstance().removeStudentFromTeacher(tf_teacherEmail.getText(), tf_studentEmail.getText());
            tw_student.setItems(Controller.getInstance().getStudentsAssignedToTeacher(tf_teacherEmail.getText()));

            tf_studentName.setText("");
            tf_studentSurname.setText("");
            tf_studentEmail.setText("");

            b_assignStudentToTeacher.setDisable(true);
            b_removeStudentFromTeacher.setDisable(true);
        });
        GridPane.setConstraints(b_removeStudentFromTeacher, 4, 7, 2, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(b_removeStudentFromTeacher, new Insets(5, 0, 12, 0));

        sp_student = new Separator();
        GridPane.setConstraints(sp_student, 0, 8, 6, 1, HPos.CENTER, VPos.TOP);

        cb_title = new ComboBox<>();
        cb_title.setPromptText("Choose title");
        cb_title.getItems().addAll("Professor", "Assistant", "Demonstrator");
        cb_title.setPrefWidth(140);
        cb_title.setOnAction(e -> {
            teacherTitle = cb_title.getValue().toString().trim();
            tw_teacher.setItems(Controller.getInstance().getTeachersByTitle(teacherTitle));
        });

        cb_searchCriteria = new ComboBox();
        cb_searchCriteria.setPromptText("Choose search criteria");
        cb_searchCriteria.getItems().addAll("Teacher name", "Teacher surname", "Teacher e-mail");
        cb_searchCriteria.setPrefWidth(200);
        cb_searchCriteria.setOnAction(e -> {
            searchCriteria = cb_searchCriteria.getValue().toString().trim();
        });

        tf_searchCriteria = new TextField();
        tf_searchCriteria.setPrefWidth(170);
        tf_searchCriteria.setOnKeyReleased(event -> {
            String criteriaData = tf_searchCriteria.getText().trim();
            tw_teacher.setItems(Controller.getInstance().getTeachersByCriteria(searchCriteria, criteriaData, teacherTitle));
        });

        hb_searchCriteria = new HBox(5);
        hb_searchCriteria.getChildren().addAll(cb_searchCriteria, tf_searchCriteria);
        hb_searchCriteria.setPadding(new Insets(0, 0, 0, 30));

        hb_search = new HBox();
        hb_search.getChildren().addAll(cb_title, hb_searchCriteria);
        GridPane.setConstraints(hb_search, 0, 9, 7, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_search, new Insets(30, 0, 0, 0));

        TableColumn<Teacher, String> col_teacherName = new TableColumn<>("Name");
        col_teacherName.setMinWidth(90);
        col_teacherName.setStyle("-fx-font-size: 9pt;");
        col_teacherName.setCellValueFactory(new PropertyValueFactory<>("teacherName"));
        col_teacherName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherSurname = new TableColumn<>("Surname");
        col_teacherSurname.setMinWidth(115);
        col_teacherSurname.setStyle("-fx-font-size: 9pt;");
        col_teacherSurname.setCellValueFactory(new PropertyValueFactory<>("teacherSurname"));
        col_teacherSurname.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherEmail = new TableColumn<>("E-mail");
        col_teacherEmail.setMinWidth(170);
        col_teacherEmail.setStyle("-fx-font-size: 9pt;");
        col_teacherEmail.setCellValueFactory(new PropertyValueFactory<>("teacherEmail"));
        col_teacherEmail.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherTitle = new TableColumn<>("Title");
        col_teacherTitle.setMinWidth(100);
        col_teacherTitle.setStyle("-fx-font-size: 9pt;");
        col_teacherTitle.setCellValueFactory(new PropertyValueFactory<>("teacherTitleDescription"));
        col_teacherTitle.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacher = new TableColumn<>("Teacher");
        col_teacher.setMinWidth(475);
        col_teacher.setStyle("-fx-font-size: 10pt;");
        col_teacher.getColumns().addAll(col_teacherName, col_teacherSurname, col_teacherEmail, col_teacherTitle);

        TableColumn<Student, String> col_studentName = new TableColumn<>("Name");
        col_studentName.setMinWidth(90);
        col_studentName.setStyle("-fx-font-size: 9pt;");
        col_studentName.setCellValueFactory(new PropertyValueFactory<>("studentName"));
        col_studentName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_studentSurname = new TableColumn<>("Surname");
        col_studentSurname.setMinWidth(115);
        col_studentSurname.setStyle("-fx-font-size: 9pt;");
        col_studentSurname.setCellValueFactory(new PropertyValueFactory<>("studentSurname"));
        col_studentSurname.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_studentEmail = new TableColumn<>("E-mail");
        col_studentEmail.setMinWidth(170);
        col_studentEmail.setStyle("-fx-font-size: 9pt;");
        col_studentEmail.setCellValueFactory(new PropertyValueFactory<>("studentEmail"));
        col_studentEmail.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_student = new TableColumn<>("Student");
        col_student.setMinWidth(375);
        col_student.setStyle("-fx-font-size: 10pt;");
        col_student.getColumns().addAll(col_studentName, col_studentSurname, col_studentEmail);
        col_student.setCellFactory(TooltippedTableCell.forTableColumn());

        tw_teacher = new TableView<>();
        tw_teacher.getColumns().add(col_teacher);
        tw_teacher.setMinWidth(475);
        tw_teacher.setStyle("-fx-font-size: 9pt;");
        tw_teacher.setRowFactory( tv -> {
            TableRow<Teacher> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You have to select populated row");
                    return;
                }
                String teacherUsername = null;
                String teacherName = null;
                String teacherSurname = null;
                String teacherEmail = null;
                String teacherTitleDescription = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Teacher rowData = row.getItem();

                    teacherUsername = rowData.getTeacherUsername();
                    teacherName = rowData.getTeacherName();
                    teacherSurname = rowData.getTeacherSurname();
                    teacherEmail = rowData.getTeacherEmail();
                    teacherTitleDescription = rowData.getTeacherTitleDescription();
                }

                tf_teacherUsername.setText(teacherUsername);
                tf_teacherName.setText(teacherName);
                tf_teacherSurname.setText(teacherSurname);
                tf_teacherEmail.setText(teacherEmail);
                tf_teacherTitle.setText(teacherTitleDescription);
                tf_teacherPassword.setText("");

                b_add.setDisable(true);
                b_delete.setDisable(false);
                b_update.setDisable(false);
                b_clear.setDisable(false);

                tw_student.setItems(Controller.getInstance().getStudentsAssignedToTeacher(teacherEmail));
            });
            return row;
        });

        tw_teacher.setItems(Controller.getInstance().getTeachers());

        tw_student = new TableView<>();
        tw_student.getColumns().add(col_student);
        tw_student.setMinWidth(375);
        tw_student.setStyle("-fx-font-size: 9pt;");
        tw_student.setRowFactory( tv -> {
            TableRow<Student> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You have to select populated row");
                    return;
                }

                String studentName = null;
                String studentSurname = null;
                String studentEmail = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Student rowData = row.getItem();

                    studentName = rowData.getStudentName();
                    studentSurname = rowData.getStudentSurname();
                    studentEmail = rowData.getStudentEmail();
                }

                tf_studentName.setText(studentName);
                tf_studentSurname.setText(studentSurname);
                tf_studentEmail.setText(studentEmail);

                b_assignStudentToTeacher.setDisable(false);
                b_removeStudentFromTeacher.setDisable(false);

            });
            return row;
        });

        hb_twTeacherStudent = new HBox(10);
        hb_twTeacherStudent.getChildren().addAll(tw_teacher, tw_student);
        hb_twTeacherStudent.setMinWidth(860);
        GridPane.setConstraints(hb_twTeacherStudent, 0,10, 6, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(hb_twTeacherStudent, new Insets(15, 0, 15,0));


        gp_dataDisplay.getChildren().addAll(txt_teacher, b_findStudent, l_teacherName, tf_teacherName, l_teacherSurname, tf_teacherSurname,
                l_teacherTitle, hb_clear/*tf_teacherTitle*/, l_teacherEmail, tf_teacherEmail, l_teacherUsernme, tf_teacherUsername,
                l_teacherPassword, tf_teacherPassword, /*b_findTeacher,*/ hb_buttons, sp_teacher, txt_student, l_studentName, tf_studentName, l_studentSurname,
                tf_studentSurname, l_studentEmail, tf_studentEmail, b_assignStudentToTeacher, b_removeStudentFromTeacher,
                sp_student, hb_search, hb_twTeacherStudent);
    }

    public TextField getTf_teacherName() {
        return tf_teacherName;
    }

    public TextField getTf_teacherSurname() {
        return tf_teacherSurname;
    }

    public TextField getTf_teacherTitle() {
        return tf_teacherTitle;
    }

    public TextField getTf_teacherEmail() {
        return tf_teacherEmail;
    }

    public TextField getTf_teacherUsername() {
        return tf_teacherUsername;
    }

    public TextField getTf_teacherPassword() {
        return tf_teacherPassword;
    }

    public TextField getTf_studentName() {
        return tf_studentName;
    }

    public TextField getTf_studentSurname() {
        return tf_studentSurname;
    }

    public TextField getTf_studentEmail() {
        return tf_studentEmail;
    }

    public Button getB_removeStudentFromTeacher() {
        return b_removeStudentFromTeacher;
    }

    public Button getB_assignStudentToTeacher() {
        return b_assignStudentToTeacher;
    }

    private boolean clear() {
        if ((tf_teacherUsername.getText() != null || (!tf_teacherUsername.getText().equals(""))) || (tf_teacherEmail.getText() != null || (!tf_teacherEmail.getText().equals("")))
                || (tf_teacherName.getText() != null || (!tf_teacherName.getText().equals(""))) || (tf_teacherSurname.getText() != null || (!tf_teacherSurname.getText().equals("")))
                || (tf_teacherPassword.getText() != null || (!tf_teacherPassword.getText().equals(""))) || (tf_teacherTitle.getText() != null || (!tf_teacherTitle.getText().equals("")))) {
            return true;
        }
        return false;
    }

    private boolean hasText() {
        if ((tf_teacherUsername.getText() == null || tf_teacherUsername.getText().equals("")) && (tf_teacherEmail.getText() == null || tf_teacherEmail.getText().equals(""))
                && (tf_teacherName.getText() == null || tf_teacherName.getText().equals("")) && (tf_teacherSurname.getText() == null || tf_teacherSurname.getText().equals(""))
                && (tf_teacherPassword.getText() == null || tf_teacherPassword.getText().equals("")) && (tf_teacherTitle.getText() == null || tf_teacherTitle.getText().equals(""))) {
            return false;
        }
        return true;
    }
}
