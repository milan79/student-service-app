package frames;

import controller.Controller;
import data.Subject;
import data.Teacher;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SelectTestFrame {

    private static Stage window;
    private static Scene scene;

    private static Text txt_questionnaire;

    private static Label l_subject;
    private static TextField tf_subject;

    private static Button b_select;

    private static HBox hb_bSelect;

    private static Separator sp_teacher;

    private static Label l_subjectText;
    private static TextField tf_subjectTitle;

    private static HBox hb_subjectTitle;

    private static HBox hb_search;

    private static TableView<Subject> tw_subjects;

    private static VBox vb_Questionnaire;

    private static GridPane gp_dataDisplay;

    public static void display(String questionnaireOrTest) {

        window = new Stage();
        window.setTitle("Select questionnaire");
        window.initModality(Modality.APPLICATION_MODAL);

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.TOP_CENTER);
        gp_dataDisplay.setPadding(new Insets(30, 0, 15, 0));

        txt_questionnaire = new Text("TEST DATA");
        txt_questionnaire.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_questionnaire, 0, 0, 2, 1);
        GridPane.setMargin(txt_questionnaire, new Insets(0, 0, 10, 0));

        l_subject = new Label();
        l_subject.setText("Title:");
        GridPane.setConstraints(l_subject, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_subject, new Insets(12, 5, 0, 0));

        tf_subject = new TextField();
        tf_subject.setMaxWidth(140);
        tf_subject.setEditable(false);
        GridPane.setConstraints(tf_subject, 1, 1);
        GridPane.setMargin(tf_subject, new Insets(12, 20, 0, 0));

        b_select = new Button();
        b_select.setText("Confirm");
        b_select.setPrefWidth(130);
        b_select.setPrefHeight(40);
        b_select.setStyle("-fx-font-size: 12pt;");
        b_select.setOnAction(e -> {
            VBox vb_tempSubject = Controller.getInstance().getQuestionsFrame().getVb_subject();

            HBox hb_tempSubject = Controller.getInstance().getQuestionsFrame().getHb_subject();
            Label l_tempTest = Controller.getInstance().getQuestionsFrame().getL_test();
            Label l_tempQuestionnaire = Controller.getInstance().getQuestionsFrame().getL_questionnaire();

            ObservableList<Node> tempList = Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren();

            l_tempQuestionnaire.setText("");

            if (questionnaireOrTest.equalsIgnoreCase("test")) {

                l_tempTest.setText("TEST");

                if (tempList.size() == 1) {
                    Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren().add(1, vb_tempSubject);
                } else if (tempList.size() == 2) {
                    Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren().remove(1);
                    Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren().add(1, vb_tempSubject);
                }

                Controller.getInstance().getQuestionsFrame().getVb_subject().getChildren().addAll(l_tempTest, hb_tempSubject);

                Controller.getInstance().getQuestionsFrame().getTf_questionRefersTo().setText(tf_subject.getText());

                Controller.getInstance().getQuestionsFrame().getL_subjectText().setText(tf_subject.getText());

                Controller.getInstance().getQuestionsFrame().getTw_question().setItems(Controller.getInstance().getTestQuestions(tf_subject.getText()));
            }

            Controller.getInstance().getQuestionsFrame().getTf_questionRefersTo().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTa_questionText().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTa_questionText().setText("");
            Controller.getInstance().getQuestionsFrame().getCb_type().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getCb_type().getSelectionModel().clearSelection();
            Controller.getInstance().getQuestionsFrame().getTf_firstResponse().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTf_firstResponse().setText("");
            Controller.getInstance().getQuestionsFrame().getTf_secondResponse().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTf_secondResponse().setText("");
            Controller.getInstance().getQuestionsFrame().getTf_thirdResponse().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTf_thirdResponse().setText("");
            Controller.getInstance().getQuestionsFrame().getTf_fourthResponse().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTf_fourthResponse().setText("");
            Controller.getInstance().getQuestionsFrame().getTf_fifthResponse().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTf_fifthResponse().setText("");

            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_1().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_1().setSelected(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_2().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_2().setSelected(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_3().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_3().setSelected(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_4().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_4().setSelected(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_5().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_5().setSelected(false);

            Controller.getInstance().getQuestionsFrame().getB_delete().setDisable(true);
            Controller.getInstance().getQuestionsFrame().getB_update().setDisable(true);
            Controller.getInstance().getQuestionsFrame().getB_clear().setDisable(true);

            window.close();
        });

        hb_bSelect = new HBox(15);
        hb_bSelect.getChildren().addAll(b_select);
        GridPane.setConstraints(hb_bSelect, 1, 3, 2, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_bSelect, new Insets(10, 0, 12, 0));

        sp_teacher = new Separator();
        GridPane.setConstraints(sp_teacher, 0, 4, 4, 1, HPos.CENTER, VPos.TOP);

        l_subjectText = new Label();
        l_subjectText.setText("Subject:");
        l_subjectText.setPadding(new Insets(4, 0, 0, 0));

        tf_subjectTitle = new TextField();
        tf_subjectTitle.setPrefWidth(170);
        tf_subjectTitle.setOnKeyReleased(event -> {
            String subjectTitleText = tf_subjectTitle.getText().trim();
            tw_subjects.setItems(Controller.getInstance().getSubjectTest(subjectTitleText));
        });

        hb_subjectTitle = new HBox(5);
        hb_subjectTitle.getChildren().addAll(l_subjectText, tf_subjectTitle);
        hb_subjectTitle.setMinWidth(300);
        hb_subjectTitle.setPadding(new Insets(0));

        hb_search = new HBox();
        hb_search.getChildren().add(hb_subjectTitle);
        GridPane.setConstraints(hb_search, 0, 6, 7, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_search, new Insets(30, 0, 0, 0));

        TableColumn<Subject, String> col_subjectName = new TableColumn<>("Subject");
        col_subjectName.setMinWidth(220);
        col_subjectName.setStyle("-fx-font-size: 10pt;");
        col_subjectName.setCellValueFactory(new PropertyValueFactory<>("subjectTitle"));
        col_subjectName.setCellFactory(TooltippedTableCell.forTableColumn());

        tw_subjects = new TableView<>();
        tw_subjects.getColumns().add(col_subjectName);
        tw_subjects.setMaxWidth(220);
        tw_subjects.setStyle("-fx-font-size: 9pt;");
        tw_subjects.setRowFactory( tv -> {
            TableRow<Subject> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                String subjectTitle = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Subject rowData = row.getItem();

                    subjectTitle = rowData.getSubjectTitle();
                }

                tf_subject.setText(subjectTitle);

            });
            return row;
        });

        tw_subjects.setItems(Controller.getInstance().getSubjectTest(null));

        vb_Questionnaire = new VBox();
        vb_Questionnaire.getChildren().add(tw_subjects);
        GridPane.setConstraints(vb_Questionnaire, 0,7, 7, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(vb_Questionnaire, new Insets(15, 0, 15,0));

        gp_dataDisplay.getChildren().addAll(txt_questionnaire, l_subject, tf_subject, hb_bSelect, sp_teacher, hb_search, vb_Questionnaire);

        scene = new Scene(gp_dataDisplay, 800, 800);
        window.setScene(scene);
        window.showAndWait();
    }

    public static TextField getTf_subject() {
        return tf_subject;
    }

}

