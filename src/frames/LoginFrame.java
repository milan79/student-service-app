package frames;

import actions.LoginAction;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;


public class LoginFrame extends VBox {

    private TextField tf_usernameOrEmail;

    private Label l_usernameOrEmailFalse;

    private VBox vb_usernameOrEmailFalse;

    private PasswordField pf_password;

    private Label l_passwordFalse;

    private VBox vb_passwordFalse;

    private Button b_loginButton;

    private GridPane gp_loginForm;

    private Background vBox_background;

    public LoginFrame() {
        super();
        setPadding(new Insets(20));
        setAlignment(Pos.CENTER);

        setComponents();
        getChildren().add(gp_loginForm);

        setBackground(vBox_background);
    }

    private void setComponents() {

        vBox_background = new Background(new BackgroundFill(Color.web("#75a3a3"), CornerRadii.EMPTY, Insets.EMPTY));

        tf_usernameOrEmail = new TextField();
        tf_usernameOrEmail.setPromptText("Username/email");
        tf_usernameOrEmail.setStyle("-fx-font-size: 14pt; -fx-prompt-text-fill: #d1d1e0");
        tf_usernameOrEmail.setMinWidth(250.0);
        tf_usernameOrEmail.setOnAction(new LoginAction());
        tf_usernameOrEmail.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                new LoginAction();
            }
        });

        l_usernameOrEmailFalse = new Label();
        l_usernameOrEmailFalse.setText("You must enter username/email");
        l_usernameOrEmailFalse.setStyle("-fx-text-fill: #ff6600;" +
                                        "-fx-font-size: 11pt");

        vb_usernameOrEmailFalse = new VBox(10);
        vb_usernameOrEmailFalse.getChildren().add(tf_usernameOrEmail);

        GridPane.setConstraints(vb_usernameOrEmailFalse, 0, 0);

        pf_password = new PasswordField();
        pf_password.setPromptText("Password");
        pf_password.setStyle("-fx-font-size: 14pt; -fx-prompt-text-fill: #d1d1e0");
        pf_password.setMinWidth(250.0);
        pf_password.setOnAction(new LoginAction());
        pf_password.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                new LoginAction();
            }
        });

        l_passwordFalse = new Label();
        l_passwordFalse.setText("You must enter password");
        l_passwordFalse.setStyle("-fx-text-fill: #ff6600;" +
                "-fx-font-size: 11pt");

        vb_passwordFalse = new VBox(10);
        vb_passwordFalse.getChildren().add(pf_password);

        GridPane.setConstraints(vb_passwordFalse, 0, 1);

        b_loginButton = new Button();
        b_loginButton.setText("Login");
        b_loginButton.setStyle("-fx-font-size: 14pt");
        b_loginButton.setMinWidth(250.0);
        b_loginButton.setOnAction(new LoginAction());

        GridPane.setConstraints(b_loginButton, 0, 2);

        gp_loginForm = new GridPane();
        gp_loginForm.setMaxWidth(346.0);
        gp_loginForm.setVgap(20);
        gp_loginForm.setAlignment(Pos.CENTER);
        gp_loginForm.getChildren().addAll(vb_usernameOrEmailFalse, vb_passwordFalse, b_loginButton);

        gp_loginForm.setStyle("-fx-background-color: #cce6ff;" +
                              "-fx-background-radius: 20px;" +
                              "-fx-padding: 30px 45px;" +
                              "-fx-border-style: solid outside;" +
                              "-fx-border-width: 3px;" +
                              "-fx-border-color: #339cff;" +
                              "-fx-border-radius: 20px;");

        ColumnConstraints col_constraint = new ColumnConstraints();
        col_constraint.setPrefWidth(250.0);
        gp_loginForm.getColumnConstraints().add(col_constraint);
    }

    public TextField getTf_usernameOrEmail() {
        return tf_usernameOrEmail;
    }

    public PasswordField getPf_password() {
        return pf_password;
    }

    public Label getL_usernameOrEmailFalse() {
        return l_usernameOrEmailFalse;
    }

    public VBox getVb_usernameOrEmailFalse() {
        return vb_usernameOrEmailFalse;
    }

    public Label getL_passwordFalse() {
        return l_passwordFalse;
    }

    public VBox getVb_passwordFalse() {
        return vb_passwordFalse;
    }
}
