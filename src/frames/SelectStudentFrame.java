package frames;

import controller.Controller;
import data.Student;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SelectStudentFrame {

    private static Stage window;
    private static Scene scene;

    private static Text txt_student;

    private static Label l_studentName;
    private static TextField tf_studentName;

    private static Label l_studentSurname;
    private static TextField tf_studentSurname;

    private static Label l_studentEmail;
    private static TextField tf_studentEmail;

    private static Button b_select;

    private static HBox hb_bSelect;

    private static Separator sp_student;

    private static ComboBox cb_search;
    private static TextField tf_search;
    private static String searchCriteria;

    private static HBox hb_search;

    private static TableView<Student> tw_student;

    private static VBox vb_twStudent;

    private static GridPane gp_dataDisplay;

    public static void display(String callingFrame) {

        window = new Stage();
        window.setTitle("Select student");
        window.initModality(Modality.APPLICATION_MODAL);

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.TOP_CENTER);
        gp_dataDisplay.setPadding(new Insets(30, 0, 15, 0));

        txt_student = new Text("STUDENT DATA");
        txt_student.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_student, 0, 0, 2, 1);
        GridPane.setMargin(txt_student, new Insets(0, 0, 10, 0));

        l_studentName = new Label();
        l_studentName.setText("Name:");
        GridPane.setConstraints(l_studentName, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentName, new Insets(12, 5, 12, 0));

        tf_studentName = new TextField();
        tf_studentName.setMaxWidth(140);
        GridPane.setConstraints(tf_studentName, 1, 1);
        GridPane.setMargin(tf_studentName, new Insets(12, 20, 12, 0));

        l_studentSurname = new Label();
        l_studentSurname.setText("Surname:");
        GridPane.setConstraints(l_studentSurname, 2, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentSurname, new Insets(12, 5, 12, 0));

        tf_studentSurname = new TextField();
        tf_studentSurname.setMaxWidth(180);
        GridPane.setConstraints(tf_studentSurname, 3, 1);
        GridPane.setMargin(tf_studentSurname, new Insets(12, 20, 12, 0));

        l_studentEmail = new Label();
        l_studentEmail.setText("E-mail:");
        GridPane.setConstraints(l_studentEmail, 4, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentEmail, new Insets(12, 5, 12, 0));

        tf_studentEmail = new TextField();
        tf_studentEmail.setMaxWidth(200);
        GridPane.setConstraints(tf_studentEmail, 5, 1);
        GridPane.setMargin(tf_studentEmail, new Insets(12, 0, 12, 0));

        b_select = new Button();
        b_select.setText("Confirm");
        b_select.setPrefWidth(130);
        b_select.setPrefHeight(40);
        b_select.setStyle("-fx-font-size: 12pt;");
        b_select.setOnAction(e -> {
            if (callingFrame.equals("Subjects")) {
                Controller.getInstance().getSubjectsFrame().getTf_studentName().setText(tf_studentName.getText());
                Controller.getInstance().getSubjectsFrame().getTf_studentSurname().setText(tf_studentSurname.getText());
                Controller.getInstance().getSubjectsFrame().getTf_studentEmail().setText(tf_studentEmail.getText());

                Controller.getInstance().getSubjectsFrame().getB_assignStudentToSubject().setDisable(false);
                Controller.getInstance().getSubjectsFrame().getB_removeStudentFromSubject().setDisable(false);
            } else if (callingFrame.equals("Teachers")) {
                Controller.getInstance().getTeachersFrame().getTf_studentName().setText(tf_studentName.getText());
                Controller.getInstance().getTeachersFrame().getTf_studentSurname().setText(tf_studentSurname.getText());
                Controller.getInstance().getTeachersFrame().getTf_studentEmail().setText(tf_studentEmail.getText());

                Controller.getInstance().getTeachersFrame().getB_assignStudentToTeacher().setDisable(false);
                Controller.getInstance().getTeachersFrame().getB_removeStudentFromTeacher().setDisable(false);
            } else if (callingFrame.equals("TestResults")) {
                Controller.getInstance().getTestResultsFrame().getTf_studentName().setText(tf_studentName.getText());
                Controller.getInstance().getTestResultsFrame().getTf_studentSurname().setText(tf_studentSurname.getText());
                Controller.getInstance().getTestResultsFrame().getTf_studentEmail().setText(tf_studentEmail.getText());

                Controller.getInstance().getTestResultsFrame().getB_view().setDisable(false);
            } else if (callingFrame.equals("QuestionnaireResults")) {
                Controller.getInstance().getQuestionnaireResultsFrame().getTf_studentName().setText(tf_studentName.getText());
                Controller.getInstance().getQuestionnaireResultsFrame().getTf_studentSurname().setText(tf_studentSurname.getText());
                Controller.getInstance().getQuestionnaireResultsFrame().getTf_studentEmail().setText(tf_studentEmail.getText());

                Controller.getInstance().getQuestionnaireResultsFrame().getB_view().setDisable(false);
            }

            window.close();
        });

        hb_bSelect = new HBox(15);
        hb_bSelect.getChildren().addAll(b_select);
        GridPane.setConstraints(hb_bSelect, 1, 2, 3, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_bSelect, new Insets(10, 0, 12, 0));

        sp_student = new Separator();
        GridPane.setConstraints(sp_student, 0, 3, 6, 1, HPos.CENTER, VPos.TOP);

        cb_search = new ComboBox();
        cb_search.setPromptText("Choose search criteria");
        cb_search.getItems().addAll("Student name", "Student surname", "Student e-mail");
        cb_search.setPrefWidth(200);
        cb_search.setOnAction(e -> {
            searchCriteria = cb_search.getValue().toString().trim();
        });

        tf_search = new TextField();
        tf_search.setPrefWidth(170);
        tf_search.setOnKeyReleased(event -> {
            String criteriaData = tf_search.getText();
            if (callingFrame.equals("QuestionnaireResults")) {
                if (Controller.getInstance().getQuestionnaireResultsFrame().getType().equalsIgnoreCase("subject")) {
                    tw_student.setItems(Controller.getInstance().getSubjectQuestionnaireStudents(Controller.getInstance().getQuestionnaireResultsFrame().getTf_subject().getText(), searchCriteria, criteriaData));
                } else if (Controller.getInstance().getQuestionnaireResultsFrame().getType().equalsIgnoreCase("teacher")) {
                    tw_student.setItems(Controller.getInstance().getTeacherQuestionnaireStudents(Controller.getInstance().getQuestionnaireResultsFrame().getTf_teacherEmail().getText(), searchCriteria, criteriaData));
                }
            } else if (callingFrame.equals("TestResults")) {
                tw_student.setItems(Controller.getInstance().getSubjectTestStudents(Controller.getInstance().getTestResultsFrame().getTf_subject().getText(), searchCriteria, criteriaData));
            } else {
                tw_student.setItems(Controller.getInstance().getStudentsByCriteria(searchCriteria, criteriaData));
            }
        });

        hb_search = new HBox(5);
        hb_search.getChildren().addAll(cb_search, tf_search);
        GridPane.setConstraints(hb_search, 0, 4, 6, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_search, new Insets(30, 0, 0, 0));

        TableColumn<Student, String> col_studentName = new TableColumn<>("Student name");
        col_studentName.setMinWidth(140);
        col_studentName.setCellValueFactory(new PropertyValueFactory<>("studentName"));
        col_studentName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_studentSurname = new TableColumn<>("Student surname");
        col_studentSurname.setMinWidth(180);
        col_studentSurname.setCellValueFactory(new PropertyValueFactory<>("studentSurname"));
        col_studentSurname.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_studentEmail = new TableColumn<>("Student e-mail");
        col_studentEmail.setMinWidth(240);
        col_studentEmail.setCellValueFactory(new PropertyValueFactory<>("studentEmail"));
        col_studentEmail.setCellFactory(TooltippedTableCell.forTableColumn());

        tw_student = new TableView<>();
        tw_student.getColumns().addAll(col_studentName, col_studentSurname, col_studentEmail);
        tw_student.setMinWidth(560);
        tw_student.setMaxWidth(560);
        tw_student.setStyle("-fx-font-size: 10pt;");
        tw_student.setRowFactory( tv -> {
            TableRow<Student> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                String studentName = null;
                String studentSurname = null;
                String studentEmail = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Student rowData = row.getItem();

                    studentName = rowData.getStudentName();
                    studentSurname = rowData.getStudentSurname();
                    studentEmail = rowData.getStudentEmail();
                }

                tf_studentName.setText(studentName);
                tf_studentSurname.setText(studentSurname);
                tf_studentEmail.setText(studentEmail);

            });
            return row;
        });

        if ((callingFrame.equalsIgnoreCase("Subjects")) || (callingFrame.equalsIgnoreCase("Teachers"))) {
            tw_student.setItems(Controller.getInstance().getStudents());
        } else if (callingFrame.equalsIgnoreCase("QuestionnaireResults")) {
            if (Controller.getInstance().getQuestionnaireResultsFrame().getType().equalsIgnoreCase("subject")) {
                tw_student.setItems(Controller.getInstance().getSubjectQuestionnaireStudents(Controller.getInstance().getQuestionnaireResultsFrame().getTf_subject().getText()));
            } else if (Controller.getInstance().getQuestionnaireResultsFrame().getType().equalsIgnoreCase("teacher")) {
                tw_student.setItems(Controller.getInstance().getTeacherQuestionnaireStudents(Controller.getInstance().getQuestionnaireResultsFrame().getTf_teacherEmail().getText()));
            }
        } else if (callingFrame.equalsIgnoreCase("TestResults")) {
            tw_student.setItems(Controller.getInstance().getSubjectTestStudents(Controller.getInstance().getTestResultsFrame().getTf_subject().getText()));
        }

        vb_twStudent = new VBox();
        vb_twStudent.getChildren().add(tw_student);
        vb_twStudent.setMinWidth(560);
        GridPane.setConstraints(vb_twStudent, 0,5, 6, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(vb_twStudent, new Insets(15, 0, 15,0));

        gp_dataDisplay.getChildren().addAll(txt_student, l_studentName, tf_studentName, l_studentSurname, tf_studentSurname, l_studentEmail,
                tf_studentEmail, hb_bSelect, sp_student, hb_search, vb_twStudent);

        scene = new Scene(gp_dataDisplay, 800, 800);
        window.setScene(scene);
        window.showAndWait();
    }
}
