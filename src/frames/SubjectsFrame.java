package frames;

import controller.Controller;
import data.Student;
import data.Subject;
import data.Teacher;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import javax.swing.*;

public class SubjectsFrame extends VBox {

    private Text txt_subject;

    private Label l_subject;
    private TextField tf_subject;
    private Button b_clear;

    private Label l_newSubject;
    private TextField tf_newSubject;

    private Button b_add;
    private Button b_delete;
    private Button b_update;

    private HBox hb_buttons;

    private Separator sp_subject;

    private Text txt_teacher;

    private Button b_findTeacher;

    private Label l_teacherName;
    private TextField tf_teacherName;

    private Label l_teacherSurname;
    private TextField tf_teacherSurname;

    private Button b_assignTeacherToSubject;

    private Label l_teacherEmail;
    private TextField tf_teacherEmail;

    private Label l_teacherTitle;
    private TextField tf_teacherTitle;

    private Button b_removeTeacherFromSubject;

    private Separator sp_teacher;

    private Text txt_student;

    private Button b_findStudent;

    private Label l_studentName;
    private TextField tf_studentName;

    private Label l_studentSurname;
    private TextField tf_studentSurname;

    private Button b_assignStudentToSubject;

    private Label l_studentEmail;
    private TextField tf_studentEmail;

    private Button b_removeStudentFromSubject;

    private Separator sp_student;

    private Label l_subjectName;
    private TextField tf_subjectName;
    private HBox hb_searchSubject;

    private Label l_assigned;
    private HBox hb_assigned;

    private RadioButton rb_teachers;
    private RadioButton rb_students;
    private ToggleGroup tg_radioButtons;
    private HBox hb_radioButtons;

    private HBox hb_searchCriteria;

    private TableView<Subject> tw_subject;
    private TableView<Teacher> tw_teacher;
    private TableView<Student> tw_student;

    private HBox hb_twSubject;

    private GridPane gp_dataDisplay;

    public SubjectsFrame() {
        super(10);
        setStyle("-fx-background-color: #cce6ff;");

        setComponents();
        getChildren().addAll(Controller.getInstance().getTopPageNavigationFrame(), gp_dataDisplay);
    }

    private void setComponents() {

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.TOP_CENTER);
        gp_dataDisplay.setMinWidth(870);
        GridPane.setMargin(gp_dataDisplay, new Insets(0, 0, 15, 0));

        txt_subject = new Text("SUBJECT DATA");
        txt_subject.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_subject, 0, 0, 2, 1);
        GridPane.setMargin(txt_subject, new Insets(0, 0, 0, 15));

        l_subject = new Label();
        l_subject.setText("Subject:");
        GridPane.setConstraints(l_subject, 0, 1,1,1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_subject, new Insets(12, 5, 0, 15));

        tf_subject = new TextField();
        tf_subject.setEditable(false);
        GridPane.setConstraints(tf_subject, 1, 1, 2, 1);
        GridPane.setMargin(tf_subject, new Insets(12, 0, 0, 0));

        b_clear = new Button();
        b_clear.setText("Clear");
        b_clear.setPrefWidth(70);
        b_clear.setPrefHeight(35);
        b_clear.setDisable(true);
        GridPane.setConstraints(b_clear, 3, 1, 1, 1);
        GridPane.setMargin(b_clear, new Insets(12, 0, 0, 15));
        b_clear.setOnAction(e -> {
            tf_subject.setText("");
            if (tf_newSubject.getText() != null && (!tf_newSubject.getText().equals(""))) {
                b_add.setDisable(false);
                b_delete.setDisable(true);
                b_update.setDisable(true);
                b_clear.setDisable(true);
            } else if ((tf_newSubject.getText() == null) || (tf_newSubject.getText().equals(""))) {
                b_add.setDisable(true);
                b_delete.setDisable(true);
                b_update.setDisable(true);
                b_clear.setDisable(true);
            }
        });

        l_newSubject = new Label();
        l_newSubject.setText("New\nSubject:");
        l_newSubject.setTextAlignment(TextAlignment.CENTER);
        GridPane.setConstraints(l_newSubject, 0, 2,1,1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_newSubject, new Insets(6, 5, 0, 15));

        tf_newSubject = new TextField();
        tf_newSubject.setOnKeyReleased(event -> {
            if ((tf_subject.getText() != null && (!tf_subject.getText().equals(""))) && (tf_newSubject.getText() != null && (!tf_newSubject.getText().equals("")))) {
                b_update.setDisable(false);
                b_add.setDisable(true);
                b_delete.setDisable(true);
            } else if ((tf_subject.getText() == null || tf_subject.getText().equals("")) && (tf_newSubject.getText() != null && (!tf_newSubject.getText().equals("")))) {
                b_add.setDisable(false);
                b_update.setDisable(true);
                b_delete.setDisable(true);
            } else if ((tf_subject.getText() != null && (!tf_subject.getText().equals(""))) && (tf_newSubject.getText() == null || tf_newSubject.getText().equals(""))) {
                b_add.setDisable(true);
                b_update.setDisable(true);
                b_delete.setDisable(false);
            } else if ((tf_subject.getText() == null || tf_subject.getText().equals("")) && (tf_newSubject.getText() == null || tf_newSubject.getText().equals(""))) {
                b_add.setDisable(true);
                b_update.setDisable(true);
                b_delete.setDisable(true);
            }
        });
        GridPane.setConstraints(tf_newSubject, 1, 2, 2, 1);
        GridPane.setMargin(tf_newSubject, new Insets(12, 0, 0, 0));

        b_add = new Button();
        b_add.setText("Add");
        b_add.setPrefWidth(70);
        b_add.setPrefHeight(35);
        b_add.setDisable(true);
        b_add.setOnAction(e -> {
            String subjectTitle = tf_newSubject.getText().trim();
            Controller.getInstance().addSubject(subjectTitle);

            tw_subject.setItems(Controller.getInstance().getSubjects());

            tf_subject.setText("");
            tf_newSubject.setText("");

            b_add.setDisable(true);
            b_clear.setDisable(true);
        });

        b_delete = new Button();
        b_delete.setText("Delete");
        b_delete.setPrefWidth(70);
        b_delete.setPrefHeight(35);
        b_delete.setDisable(true);
        b_delete.setOnAction(e -> {
            String subjectTitle = tf_subject.getText().trim();
            boolean test = Controller.getInstance().deleteSubject(subjectTitle);

            if (test) {
                tw_subject.setItems(Controller.getInstance().getSubjects());

                tf_subject.setText("");
                tf_newSubject.setText("");

                b_delete.setDisable(true);
                b_clear.setDisable(true);
            }

        });

        b_update = new Button();
        b_update.setText("Update");
        b_update.setPrefWidth(70);
        b_update.setPrefHeight(35);
        b_update.setDisable(true);
        b_update.setOnAction(e -> {
            String oldSubjectTitle = tf_subject.getText().trim();
            String newSubjectTitle = tf_newSubject.getText().trim();
            boolean test = Controller.getInstance().updateSubject(oldSubjectTitle, newSubjectTitle);

            if (test) {
                tw_subject.setItems(Controller.getInstance().getSubjects());

                tf_subject.setText("");
                tf_newSubject.setText("");

                b_update.setDisable(true);
                b_clear.setDisable(true);
            }

        });

        hb_buttons = new HBox(15);
        hb_buttons.getChildren().addAll(b_add, b_delete, b_update);
        GridPane.setConstraints(hb_buttons, 1, 3, 4, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_buttons, new Insets(10, 0, 12, 0));

        sp_subject = new Separator();
        GridPane.setConstraints(sp_subject, 0, 4, 6, 1, HPos.CENTER, VPos.TOP);

        txt_teacher = new Text("TEACHER DATA");
        txt_teacher.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_teacher, 0, 5, 2, 1);
        GridPane.setMargin(txt_teacher, new Insets(10, 0, 0, 15));

        b_findTeacher = new Button();
        b_findTeacher.setText("Find teacher");
        b_findTeacher.setStyle("-fx-font-size: 12pt;");
        b_findTeacher.setPrefWidth(160);
        b_findTeacher.setPrefHeight(20);
        b_findTeacher.setOnAction(e -> SelectTeacherFrame.display("Subjects"));
        GridPane.setConstraints(b_findTeacher, 5, 6,1,2, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(b_findTeacher, new Insets(0, 0, 0, 0));

        l_teacherName = new Label();
        l_teacherName.setText("Name:");
        GridPane.setConstraints(l_teacherName, 0, 6, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherName, new Insets(12, 5, 0, 15));

        tf_teacherName = new TextField();
        tf_teacherName.setMaxWidth(140);
        GridPane.setConstraints(tf_teacherName, 1, 6);
        GridPane.setMargin(tf_teacherName, new Insets(12, 20, 0, 0));

        l_teacherSurname = new Label();
        l_teacherSurname.setText("Surname:");
        GridPane.setConstraints(l_teacherSurname, 2, 6);
        GridPane.setMargin(l_teacherSurname, new Insets(12, 5, 0, 0));

        tf_teacherSurname = new TextField();
        tf_teacherSurname.setMaxWidth(140);
        GridPane.setConstraints(tf_teacherSurname, 3, 6);
        GridPane.setMargin(tf_teacherSurname, new Insets(12, 40, 0, 0));

        b_assignTeacherToSubject = new Button();
        b_assignTeacherToSubject.setText("Assign to subject");
        b_assignTeacherToSubject.setPrefWidth(160);
        b_assignTeacherToSubject.setDisable(true);
        b_assignTeacherToSubject.setOnAction(e -> {
            if ((tf_subject.getText().equals("") || tf_subject.getText() == null)) {
                JOptionPane.showMessageDialog(null, "You must select subject");
            } else if (tf_teacherEmail.getText().equals("") || tf_teacherEmail.getText() == null) {
                JOptionPane.showMessageDialog(null, "Teacher's e-mail must be entered");
            } else {
                Controller.getInstance().assignTeacherToSubject(tf_subject.getText(), tf_teacherEmail.getText());
                tw_teacher.setItems(Controller.getInstance().getSubjectTeachers(tf_subject.getText()));

                tf_teacherName.setText("");
                tf_teacherSurname.setText("");
                tf_teacherEmail.setText("");
                tf_teacherTitle.setText("");
                b_assignTeacherToSubject.setDisable(true);
                b_removeTeacherFromSubject.setDisable(true);
            }
        });
        GridPane.setConstraints(b_assignTeacherToSubject, 4, 6);
        GridPane.setMargin(b_assignTeacherToSubject, new Insets(12, 40, 0, 0));

        l_teacherEmail = new Label();
        l_teacherEmail.setText("E-mail:");
        GridPane.setConstraints(l_teacherEmail, 0, 7, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherEmail, new Insets(5, 5, 12, 15));

        tf_teacherEmail = new TextField();
        tf_teacherEmail.setMaxWidth(140);
        GridPane.setConstraints(tf_teacherEmail, 1, 7);
        GridPane.setMargin(tf_teacherEmail, new Insets(5, 20, 12, 0));

        l_teacherTitle = new Label();
        l_teacherTitle.setText("Title:");
        GridPane.setConstraints(l_teacherTitle, 2, 7,1,1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherTitle, new Insets(5, 5, 12, 0));

        tf_teacherTitle = new TextField();
        tf_teacherTitle.setMaxWidth(140);
        GridPane.setConstraints(tf_teacherTitle, 3, 7);
        GridPane.setMargin(tf_teacherTitle, new Insets(5, 40, 12, 0));

        b_removeTeacherFromSubject = new Button();
        b_removeTeacherFromSubject.setText("Remove from subject");
        b_removeTeacherFromSubject.setPrefWidth(160);
        b_removeTeacherFromSubject.setDisable(true);
        b_removeTeacherFromSubject.setOnAction(e -> {
            if ((tf_subject.getText().equals("") || tf_subject.getText() == null)) {
                JOptionPane.showMessageDialog(null, "You must select subject");
            } else if (tf_teacherEmail.getText().equals("") || tf_teacherEmail.getText() == null) {
                JOptionPane.showMessageDialog(null, "Teacher's e-mail must be entered");
            } else {
                Controller.getInstance().removeTeacherFromSubject(tf_subject.getText(), tf_teacherEmail.getText());
                tw_teacher.setItems(Controller.getInstance().getSubjectTeachers(tf_subject.getText()));

                tf_teacherName.setText("");
                tf_teacherSurname.setText("");
                tf_teacherEmail.setText("");
                tf_teacherTitle.setText("");

                b_assignTeacherToSubject.setDisable(true);
                b_removeTeacherFromSubject.setDisable(true);
            }
        });
        GridPane.setConstraints(b_removeTeacherFromSubject, 4, 7);
        GridPane.setMargin(b_removeTeacherFromSubject, new Insets(5, 40, 12, 0));

        sp_teacher = new Separator();
        GridPane.setConstraints(sp_teacher, 0, 8, 6, 1, HPos.CENTER, VPos.TOP);

        txt_student = new Text("STUDENT DATA");
        txt_student.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_student, 0, 9, 2, 1);
        GridPane.setMargin(txt_student, new Insets(10, 0, 0, 15));

        b_findStudent = new Button();
        b_findStudent.setText("Find student");
        b_findStudent.setStyle("-fx-font-size: 12pt;");
        b_findStudent.setPrefWidth(160);
        b_findStudent.setPrefHeight(20);
        b_findStudent.setOnAction(e -> SelectStudentFrame.display("Subjects"));
        GridPane.setConstraints(b_findStudent, 5, 10, 1, 2, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(b_findStudent, new Insets(0, 0, 0, 0));

        l_studentName = new Label();
        l_studentName.setText("Name:");
        GridPane.setConstraints(l_studentName, 0, 10, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentName, new Insets(12, 5, 0, 15));

        tf_studentName = new TextField();
        tf_studentName.setMaxWidth(140);
        GridPane.setConstraints(tf_studentName, 1, 10);
        GridPane.setMargin(tf_studentName, new Insets(12, 20, 0, 0));

        l_studentSurname = new Label();
        l_studentSurname.setText("Surname:");
        GridPane.setConstraints(l_studentSurname, 2, 10);
        GridPane.setMargin(l_studentSurname, new Insets(12, 5, 0, 0));

        tf_studentSurname = new TextField();
        tf_studentSurname.setMaxWidth(140);
        GridPane.setConstraints(tf_studentSurname, 3, 10);
        GridPane.setMargin(tf_studentSurname, new Insets(12, 40, 0, 0));

        b_assignStudentToSubject = new Button();
        b_assignStudentToSubject.setText("Assign to subject");
        b_assignStudentToSubject.setPrefWidth(160);
        b_assignStudentToSubject.setDisable(true);
        b_assignStudentToSubject.setOnAction(e -> {
            if ((tf_subject.getText().equals("") || tf_subject.getText() == null)) {
                JOptionPane.showMessageDialog(null, "You must select subject");
            } else if (tf_studentEmail.getText().equals("") || tf_studentEmail.getText() == null) {
                JOptionPane.showMessageDialog(null, "Student's e-mail must be entered");
            } else {
                Controller.getInstance().assignStudentToSubject(tf_subject.getText(), tf_studentEmail.getText());
                tw_student.setItems(Controller.getInstance().getSubjectStudents(tf_subject.getText()));

                tf_studentName.setText("");
                tf_studentSurname.setText("");
                tf_studentEmail.setText("");

                b_assignStudentToSubject.setDisable(true);
                b_removeStudentFromSubject.setDisable(true);
            }
        });
        GridPane.setConstraints(b_assignStudentToSubject, 4, 10);
        GridPane.setMargin(b_assignStudentToSubject, new Insets(12, 40, 0, 0));

        l_studentEmail = new Label();
        l_studentEmail.setText("E-mail:");
        GridPane.setConstraints(l_studentEmail, 0, 11, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentEmail, new Insets(5, 5, 12, 15));

        tf_studentEmail = new TextField();
        tf_studentEmail.setMaxWidth(140);
        GridPane.setConstraints(tf_studentEmail, 1, 11);
        GridPane.setMargin(tf_studentEmail, new Insets(5, 20, 12, 0));

        b_removeStudentFromSubject = new Button();
        b_removeStudentFromSubject.setText("Remove from subject");
        b_removeStudentFromSubject.setPrefWidth(160);
        b_removeStudentFromSubject.setDisable(true);
        b_removeStudentFromSubject.setOnAction(e -> {
            if ((tf_subject.getText().equals("") || tf_subject.getText() == null)) {
                JOptionPane.showMessageDialog(null, "You must select subject");
            } else if (tf_studentEmail.getText().equals("") || tf_studentEmail.getText() == null) {
                JOptionPane.showMessageDialog(null, "Student's e-mail must be entered");
            } else {
                Controller.getInstance().removeStudentFromSubject(tf_subject.getText(), tf_studentEmail.getText());
                tw_student.setItems(Controller.getInstance().getSubjectStudents(tf_subject.getText()));

                tf_studentName.setText("");
                tf_studentSurname.setText("");
                tf_studentEmail.setText("");

                b_assignStudentToSubject.setDisable(true);
                b_removeStudentFromSubject.setDisable(true);
            }
        });
        GridPane.setConstraints(b_removeStudentFromSubject, 4, 11);
        GridPane.setMargin(b_removeStudentFromSubject, new Insets(5, 40, 12, 0));

        sp_student = new Separator();
        GridPane.setConstraints(sp_student, 0, 12, 6, 1, HPos.CENTER, VPos.TOP);

        l_subjectName = new Label();
        l_subjectName.setText("Subject:");
        l_subjectName.setPadding(new Insets(4, 0, 0, 0));

        tf_subjectName = new TextField();
        tf_subjectName.setPrefWidth(190);
        tf_subjectName.setOnKeyReleased(event -> {
            String subjectName = tf_subjectName.getText();
            tw_subject.setItems(Controller.getInstance().getSubjects(subjectName));
        });

        hb_searchSubject = new HBox(5);
        hb_searchSubject.getChildren().addAll(l_subjectName, tf_subjectName);
        hb_searchSubject.setPadding(new Insets(0,40,0,0));

        l_assigned = new Label("Assigned:");
        l_assigned.setPadding(new Insets(4, 0, 0, 0));

        hb_assigned = new HBox();
        hb_assigned.getChildren().add(l_assigned);
        hb_assigned.setPadding(new Insets(0,15,0,0));

        tg_radioButtons = new ToggleGroup();

        rb_teachers = new RadioButton();
        rb_teachers.setText("teachers");
        rb_teachers.setPadding(new Insets(4, 0, 0, 0));
        rb_teachers.setToggleGroup (tg_radioButtons);
        rb_teachers.setSelected(true);
        rb_teachers.setDisable(true);
        rb_teachers.setOnAction(e -> {
            hb_twSubject.getChildren().remove(tw_student);

            if (!hb_twSubject.getChildren().contains(tw_teacher)) {
                hb_twSubject.getChildren().add(tw_teacher);
            }
        });

        rb_students = new RadioButton();
        rb_students.setText("students");
        rb_students.setPadding(new Insets(4, 0, 0, 0));
        rb_students.setToggleGroup (tg_radioButtons);
        rb_students.setDisable(true);
        rb_students.setOnAction(e -> {
            hb_twSubject.getChildren().remove(tw_teacher);
            hb_twSubject.getChildren().add(tw_student);
        });

        hb_radioButtons = new HBox(5);
        hb_radioButtons.getChildren().addAll(rb_teachers, rb_students);

        hb_searchCriteria = new HBox();
        hb_searchCriteria.getChildren().addAll(hb_searchSubject, hb_assigned, hb_radioButtons);
        GridPane.setConstraints(hb_searchCriteria, 0, 13, 6, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_searchCriteria, new Insets(30, 0, 0, 0));

        TableColumn<Subject, String> col_subjectName = new TableColumn<>("Subject");
        col_subjectName.setMinWidth(250);
        col_subjectName.setStyle("-fx-font-size: 10pt;");
        col_subjectName.setCellValueFactory(new PropertyValueFactory<>("subjectTitle"));
        col_subjectName.setCellFactory(tc -> {
            TableCell<Subject, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            text.wrappingWidthProperty().bind(col_subjectName.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            return cell ;
        });

        TableColumn<Teacher, String> col_teacherName = new TableColumn<>("Name");
        col_teacherName.setMinWidth(90);
        col_teacherName.setStyle("-fx-font-size: 9pt;");
        col_teacherName.setCellValueFactory(new PropertyValueFactory<>("teacherName"));
        col_teacherName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherSurname = new TableColumn<>("Surname");
        col_teacherSurname.setMinWidth(115);
        col_teacherSurname.setStyle("-fx-font-size: 9pt;");
        col_teacherSurname.setCellValueFactory(new PropertyValueFactory<>("teacherSurname"));
        col_teacherSurname.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherEmail = new TableColumn<>("E-mail");
        col_teacherEmail.setMinWidth(200);
        col_teacherEmail.setStyle("-fx-font-size: 9pt;");
        col_teacherEmail.setCellValueFactory(new PropertyValueFactory<>("teacherEmail"));
        col_teacherEmail.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherCategoryDescription = new TableColumn<>("Title");
        col_teacherCategoryDescription.setMinWidth(100);
        col_teacherCategoryDescription.setStyle("-fx-font-size: 9pt;");
        col_teacherCategoryDescription.setCellValueFactory(new PropertyValueFactory<>("teacherTitleDescription"));
        col_teacherCategoryDescription.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacher = new TableColumn<>("Teacher");
        col_teacher.setMinWidth(505);
        col_teacher.setStyle("-fx-font-size: 10pt;");
        col_teacher.getColumns().addAll(col_teacherName, col_teacherSurname, col_teacherEmail, col_teacherCategoryDescription);

        TableColumn<Student, String> col_studentName = new TableColumn<>("Name");
        col_studentName.setMinWidth(135);
        col_studentName.setStyle("-fx-font-size: 9pt;");
        col_studentName.setCellValueFactory(new PropertyValueFactory<>("studentName"));
        col_studentName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_studentSurname = new TableColumn<>("Surname");
        col_studentSurname.setMinWidth(170);
        col_studentSurname.setStyle("-fx-font-size: 9pt;");
        col_studentSurname.setCellValueFactory(new PropertyValueFactory<>("studentSurname"));
        col_studentSurname.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_studentEmail = new TableColumn<>("E-mail");
        col_studentEmail.setMinWidth(200);
        col_studentEmail.setStyle("-fx-font-size: 9pt;");
        col_studentEmail.setCellValueFactory(new PropertyValueFactory<>("studentEmail"));
        col_studentEmail.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_student = new TableColumn<>("Student");
        col_student.setMinWidth(505);
        col_student.setStyle("-fx-font-size: 10pt;");
        col_student.getColumns().addAll(col_studentName, col_studentSurname, col_studentEmail);
        col_student.setCellFactory(TooltippedTableCell.forTableColumn());

        tw_subject = new TableView<>();
        tw_subject.getColumns().add(col_subjectName);
        tw_subject.setMaxWidth(250);
        tw_subject.setStyle("-fx-font-size: 9pt;");
        tw_subject.setRowFactory( tv -> {
            TableRow<Subject> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You have to select populated row");
                    return;
                }
                String subjectTitle = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Subject rowData = row.getItem();
                    subjectTitle = rowData.getSubjectTitle();
                }

                if (subjectTitle != null){
                    tf_subject.setText(subjectTitle);
                }

                ObservableList<Teacher> teachers = Controller.getInstance().getSubjectTeachers(subjectTitle);
                Controller.getInstance().setTeachersList(teachers);
                tw_teacher.setItems(Controller.getInstance().getTeachersList());

                ObservableList<Student> subjectStudents = Controller.getInstance().getSubjectStudents(subjectTitle);
                Controller.getInstance().setSubjectStudentsList(subjectStudents);
                tw_student.setItems(Controller.getInstance().getSubjectStudentsList());

                if (rb_teachers.isDisabled()) {
                    rb_teachers.setDisable(false);
                }

                if (rb_students.isDisabled()) {
                    rb_students.setDisable(false);
                }

                tf_subjectName.setText(subjectTitle);

                if (tf_newSubject.getText() == null || tf_newSubject.getText().equals("")) {
                    b_delete.setDisable(false);
                    b_add.setDisable(true);
                    b_update.setDisable(true);
                    b_clear.setDisable(false);
                } else if (tf_newSubject.getText() != null || (!tf_newSubject.getText().equals(""))) {
                    b_update.setDisable(false);
                    b_delete.setDisable(true);
                    b_add.setDisable(true);
                    b_clear.setDisable(false);
                }

            });
            return row;
        });

        tw_subject.setItems(Controller.getInstance().getSubjects());

        tw_teacher = new TableView<>();
        tw_teacher.getColumns().add(col_teacher);
        tw_teacher.setMaxWidth(505);
        tw_teacher.setStyle("-fx-font-size: 9pt;");
        tw_teacher.setRowFactory( tv -> {
            TableRow<Teacher> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You have to select populated row");
                    return;
                }

                String teacherName = null;
                String teacherSurname = null;
                String teacherEmail = null;
                String teacherTitleDescription = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Teacher rowData = row.getItem();

                    teacherName = rowData.getTeacherName();
                    teacherSurname = rowData.getTeacherSurname();
                    teacherEmail = rowData.getTeacherEmail();
                    teacherTitleDescription = rowData.getTeacherTitleDescription();
                }

                tf_teacherName.setText(teacherName);
                tf_teacherSurname.setText(teacherSurname);
                tf_teacherEmail.setText(teacherEmail);
                tf_teacherTitle.setText(teacherTitleDescription);

                b_assignTeacherToSubject.setDisable(false);
                b_removeTeacherFromSubject.setDisable(false);

            });
            return row;
        });

        tw_student = new TableView<>();
        tw_student.getColumns().add(col_student);
        tw_student.setMinWidth(505);
        tw_student.setStyle("-fx-font-size: 9pt;");
        tw_student.setRowFactory( tv -> {
            TableRow<Student> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You have to select populated row");
                    return;
                }

                String studentName = null;
                String studentSurname = null;
                String studentEmail = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Student rowData = row.getItem();

                    studentName = rowData.getStudentName();
                    studentSurname = rowData.getStudentSurname();
                    studentEmail = rowData.getStudentEmail();
                }

                tf_studentName.setText(studentName);
                tf_studentSurname.setText(studentSurname);
                tf_studentEmail.setText(studentEmail);

                b_assignStudentToSubject.setDisable(false);
                b_removeStudentFromSubject.setDisable(false);
            });
            return row;
        });

        tw_student.setItems(Controller.getInstance().getSubjectStudentsList());

        hb_twSubject = new HBox(40);
        hb_twSubject.setMinWidth(860);
        hb_twSubject.getChildren().addAll(tw_subject, tw_teacher);
        GridPane.setConstraints(hb_twSubject, 0,14, 6, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(hb_twSubject, new Insets(15, 0, 15,0));

        gp_dataDisplay.getChildren().addAll(txt_subject, l_subject, tf_subject, b_clear, l_newSubject, tf_newSubject, hb_buttons, sp_subject, txt_teacher, b_findTeacher, l_teacherName, tf_teacherName, l_teacherSurname, tf_teacherSurname,
                                            b_assignTeacherToSubject, l_teacherEmail, tf_teacherEmail, l_teacherTitle, tf_teacherTitle,
                                            b_removeTeacherFromSubject, sp_teacher, txt_student, b_findStudent, l_studentName, tf_studentName, l_studentSurname, tf_studentSurname,
                                            b_assignStudentToSubject, l_studentEmail, tf_studentEmail, b_removeStudentFromSubject, sp_student,
                                            hb_searchCriteria, hb_twSubject);
    }

    public Text getTxt_subject() {
        return txt_subject;
    }

    public TextField getTf_teacherName() {
        return tf_teacherName;
    }

    public TextField getTf_teacherSurname() {
        return tf_teacherSurname;
    }

    public TextField getTf_teacherEmail() {
        return tf_teacherEmail;
    }

    public TextField getTf_teacherTitle() {
        return tf_teacherTitle;
    }

    public TextField getTf_studentName() {
        return tf_studentName;
    }

    public TextField getTf_studentSurname() {
        return tf_studentSurname;
    }

    public TextField getTf_studentEmail() {
        return tf_studentEmail;
    }

    public Button getB_assignTeacherToSubject() {
        return b_assignTeacherToSubject;
    }

    public Button getB_removeTeacherFromSubject() {
        return b_removeTeacherFromSubject;
    }

    public Button getB_assignStudentToSubject() {
        return b_assignStudentToSubject;
    }

    public Button getB_removeStudentFromSubject() {
        return b_removeStudentFromSubject;
    }
}
