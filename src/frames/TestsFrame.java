package frames;

import controller.Controller;
import data.Subject;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import javax.swing.*;

public class TestsFrame extends VBox {

    private Text txt_test;

    private Label l_subject;
    private TextField tf_subject;

    private Button b_findSubject;
    private Button b_clear;
    private HBox hb_button;

    private HBox hb_findSubject;

    private Button b_add;
    private Button b_delete;
    private Button b_view;

    private HBox hb_buttons;

    private Separator sp_test;

    private Text txt_existingTests;

    private Label l_subjectName;
    private TextField tf_subjectName;

    private TableView<Subject> tw_test;

    private VBox vb_twTest;

    private GridPane gp_dataDisplay;

    public TestsFrame() {
        super(10);
        setStyle("-fx-background-color: #cce6ff;");

        setComponents();
        getChildren().addAll(Controller.getInstance().getTopPageNavigationFrame(), gp_dataDisplay);
    }

    private void setComponents() {

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.CENTER);

        txt_test = new Text("TEST DATA");
        txt_test.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_test, 0, 0, 2, 1);
        GridPane.setMargin(txt_test, new Insets(0, 0, 0, 0));

        l_subject = new Label();
        l_subject.setText("Subject:");
//        GridPane.setConstraints(l_subject, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
//        GridPane.setMargin(l_subject, new Insets(12, 5, 0, 0));

        tf_subject = new TextField();
        tf_subject.setMaxWidth(240);
        tf_subject.setEditable(false);
//        GridPane.setConstraints(tf_subject, 1, 1, 1, 1, HPos.LEFT, VPos.CENTER);
//        GridPane.setMargin(tf_subject, new Insets(12, 0, 0, 0));

        b_clear = new Button();
        b_clear.setText("Clear");
        b_clear.setPrefWidth(70);
        b_clear.setPrefHeight(20);
        b_clear.setDisable(true);
        b_clear.setOnAction(e -> {

            tf_subject.setText("");

            b_clear.setDisable(true);
            b_add.setDisable(true);
            b_delete.setDisable(true);
            b_view.setDisable(true);
        });

        b_findSubject = new Button();
        b_findSubject.setText("Find subject");
        b_findSubject.setStyle("-fx-font-size: 12pt;");
        b_findSubject.setPrefWidth(160);
        b_findSubject.setPrefHeight(20);
        b_findSubject.setOnAction(e -> SelectSubjectFrame.display("tests"));

        hb_button = new HBox(40);
        hb_button.getChildren().addAll(b_clear, b_findSubject);
        hb_button.setAlignment(Pos.CENTER_LEFT);
        hb_button.setPadding(new Insets(0,0,0,20));

        hb_findSubject = new HBox(5);
        hb_findSubject.getChildren().addAll(l_subject, tf_subject, hb_button);
        hb_findSubject.setAlignment(Pos.CENTER_LEFT);
        GridPane.setConstraints(hb_findSubject, 0, 1, 2, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_findSubject, new Insets(9, 0, 0, 0));

        b_add = new Button();
        b_add.setText("Add");
        b_add.setPrefWidth(70);
        b_add.setPrefHeight(35);
        b_add.setDisable(true);
        b_add.setOnAction(e -> {
            if ((!tf_subject.getText().equals("")) || tf_subject.getText() != null) {
                Controller.getInstance().addNewTest(tf_subject.getText());
                tw_test.setItems(Controller.getInstance().getSubjectTest(null));

                b_add.setDisable(true);
                b_delete.setDisable(true);
                b_view.setDisable(true);
                b_clear.setDisable(true);

                tf_subject.setText("");
            }
        });

        b_delete = new Button();
        b_delete.setText("Delete");
        b_delete.setPrefWidth(70);
        b_delete.setPrefHeight(35);
        b_delete.setDisable(true);
        b_delete.setOnAction(e -> {
            if ((!tf_subject.getText().equals("")) || tf_subject.getText() != null) {
                Controller.getInstance().deleteTest(tf_subject.getText());
                tw_test.setItems(Controller.getInstance().getSubjectTest(null));

                b_add.setDisable(true);
                b_delete.setDisable(true);
                b_view.setDisable(true);
                b_clear.setDisable(true);

                tf_subject.setText("");
            }
        });

        b_view = new Button();
        b_view.setText("View");
        b_view.setPrefWidth(70);
        b_view.setPrefHeight(35);
        b_view.setDisable(true);
        b_view.setOnAction(e -> TestFrameView.display());

        hb_buttons = new HBox(15);
        hb_buttons.getChildren().addAll(b_add, b_delete, b_view);
        GridPane.setConstraints(hb_buttons, 1, 2, 1, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_buttons, new Insets(10, 0, 12, 0));

        sp_test = new Separator();
        GridPane.setConstraints(sp_test, 0, 3, 2, 1, HPos.CENTER, VPos.TOP);

        txt_existingTests = new Text("EXISTING TESTS");
        txt_existingTests.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_existingTests, 0, 4, 2, 1);
        GridPane.setMargin(txt_existingTests, new Insets(10, 0, 0, 0));

        l_subjectName = new Label();
        l_subjectName.setText("Subject:");
        GridPane.setConstraints(l_subjectName, 0, 5, 1,1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_subjectName, new Insets(30, 5, 0, 0));

        tf_subjectName = new TextField();
        tf_subjectName.setMaxWidth(240);
        tf_subjectName.setOnKeyReleased(event -> {
            String subjectTitleText = tf_subjectName.getText().trim();
            tw_test.setItems(Controller.getInstance().getSubjectTest(subjectTitleText));
        });
        GridPane.setConstraints(tf_subjectName, 1, 5, 2, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(tf_subjectName, new Insets(30, 0, 0, 0));

        TableColumn<Subject, String> col_subjectName = new TableColumn<>("Subject");
        col_subjectName.setMinWidth(240);
        col_subjectName.setStyle("-fx-font-size: 10pt;");
        col_subjectName.setCellValueFactory(new PropertyValueFactory<>("subjectTitle"));
        col_subjectName.setCellFactory(TooltippedTableCell.forTableColumn());

        tw_test = new TableView<>();
        tw_test.getColumns().add(col_subjectName);
        tw_test.setMinWidth(240);
        tw_test.setMaxWidth(240);
        tw_test.setStyle("-fx-font-size: 9pt;");
        tw_test.setRowFactory(tv -> {
            TableRow<Subject> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You have to select populated row");
                    return;
                }

                String subjectTitle = null;

                if (event.getClickCount() == 1 && (!row.isEmpty()) && event.getButton() == MouseButton.PRIMARY) {
                    Subject rowData = row.getItem();

                    subjectTitle = rowData.getSubjectTitle();
                }

                tf_subject.setText(subjectTitle);

                b_add.setDisable(true);
                b_delete.setDisable(false);
                b_view.setDisable(false);
                b_clear.setDisable(false);
            });
            return row;
        });

        tw_test.setItems(Controller.getInstance().getSubjectTest(null));

        vb_twTest = new VBox();
        vb_twTest.getChildren().add(tw_test);
        vb_twTest.setMinWidth(480);
        GridPane.setConstraints(vb_twTest, 0,6, 2, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(vb_twTest, new Insets(15, 0, 15,0));

        gp_dataDisplay.getChildren().addAll(txt_test, hb_findSubject, hb_buttons, sp_test, txt_existingTests, l_subjectName, tf_subjectName, vb_twTest);
    }

    public TextField getTf_subject() {
        return tf_subject;
    }

    public TextField getTf_subjectName() {
        return tf_subjectName;
    }

    public Button getB_add() {
        return b_add;
    }

    public Button getB_delete() {
        return b_delete;
    }

    public Button getB_view() {
        return b_view;
    }

    public Button getB_clear() {
        return b_clear;
    }
}
