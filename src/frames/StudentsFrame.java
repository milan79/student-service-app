package frames;

import controller.Controller;
import data.Student;
import data.Subject;
import data.Teacher;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import javax.swing.*;

public class StudentsFrame extends VBox {

    private Text txt_student;

    private Label l_studentName;
    private TextField tf_studentName;

    private Label l_studentSurname;
    private TextField tf_studentSurname;

    private Label l_studentEmail;
    private TextField tf_studentEmail;
    private Button b_clear;

    private HBox hb_clear;

    private Label l_studentUsernme;
    private TextField tf_studentUsername;

    private Label l_studentPassword;
    private TextField tf_studentPassword;

    private Button b_add;
    private Button b_delete;
    private Button b_update;

    private HBox hb_buttons;

    private Separator sp_student;

    private ComboBox cb_search;
    private String searchCriteria;
    private TextField tf_search;

    private HBox hb_search;

    private TableView<Student> tw_student;

    private VBox vb_twStudent;

    private GridPane gp_dataDisplay;

    public StudentsFrame() {
        super(10);
        setStyle("-fx-background-color: #cce6ff;");

        setComponents();
        getChildren().addAll(Controller.getInstance().getTopPageNavigationFrame(), gp_dataDisplay);
    }

    private void setComponents() {

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.CENTER);

        txt_student = new Text("STUDENT DATA");
        txt_student.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_student, 0, 0, 2, 1);
        GridPane.setMargin(txt_student, new Insets(0, 0, 0, 0));

        l_studentName = new Label();
        l_studentName.setText("Name:");
        GridPane.setConstraints(l_studentName, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentName, new Insets(12, 5, 0, 0));

        tf_studentName = new TextField();
        tf_studentName.setMaxWidth(140);
        tf_studentName.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });
        GridPane.setConstraints(tf_studentName, 1, 1);
        GridPane.setMargin(tf_studentName, new Insets(12, 20, 0, 0));

        l_studentSurname = new Label();
        l_studentSurname.setText("Surname:");
        GridPane.setConstraints(l_studentSurname, 2, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentSurname, new Insets(12, 5, 0, 0));

        tf_studentSurname = new TextField();
        tf_studentSurname.setMaxWidth(140);
        tf_studentSurname.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });
        GridPane.setConstraints(tf_studentSurname, 3, 1);
        GridPane.setMargin(tf_studentSurname, new Insets(12, 20, 0, 0));

        l_studentEmail = new Label();
        l_studentEmail.setText("E-mail:");
        GridPane.setConstraints(l_studentEmail, 4, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentEmail, new Insets(12, 5, 0, 0));

        tf_studentEmail = new TextField();
        tf_studentEmail.setMaxWidth(140);
        tf_studentEmail.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });

        b_clear = new Button();
        b_clear.setText("Clear");
        b_clear.setPrefWidth(70);
        b_clear.setPrefHeight(20);
        b_clear.setDisable(true);
        b_clear.setOnAction(e -> {
            tf_studentName.setText("");
            tf_studentSurname.setText("");
            tf_studentUsername.setText("");
            tf_studentPassword.setText("");
            tf_studentEmail.setText("");

            b_add.setDisable(true);
            b_delete.setDisable(true);
            b_update.setDisable(true);
            b_clear.setDisable(true);

        });

        hb_clear = new HBox(20);
        hb_clear.getChildren().addAll(tf_studentEmail, b_clear);
        GridPane.setConstraints(hb_clear, 5, 1);
        GridPane.setMargin(hb_clear, new Insets(12, 0, 0, 0));

        l_studentUsernme = new Label();
        l_studentUsernme.setText("Username:");
        GridPane.setConstraints(l_studentUsernme, 0, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentUsernme, new Insets(5, 5, 0, 0));

        tf_studentUsername = new TextField();
        tf_studentUsername.setMaxWidth(140);
        tf_studentUsername.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });
        GridPane.setConstraints(tf_studentUsername, 1, 2);
        GridPane.setMargin(tf_studentUsername, new Insets(5, 20, 0, 0));

        l_studentPassword = new Label();
        l_studentPassword.setText("Password:");
        GridPane.setConstraints(l_studentPassword, 2, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_studentPassword, new Insets(5, 5, 0, 0));

        tf_studentPassword = new TextField();
        tf_studentPassword.setMaxWidth(140);
        tf_studentPassword.setOnKeyReleased(event -> {
            boolean answer = clear();
            if (answer) {
                b_clear.setDisable(false);
                if ((!b_delete.isDisabled()) && (!b_update.isDisabled())) {
                    b_add.setDisable(true);
                } else if (b_delete.isDisabled() && b_update.isDisabled()) {
                    b_add.setDisable(false);
                }

            }

            if (!hasText()) {
                b_clear.setDisable(true);
                b_add.setDisable(true);
            }
        });
        GridPane.setConstraints(tf_studentPassword, 3, 2);
        GridPane.setMargin(tf_studentPassword, new Insets(5, 0, 0, 0));

        b_add = new Button();
        b_add.setText("Add");
        b_add.setPrefWidth(70);
        b_add.setPrefHeight(35);
        b_add.setDisable(true);
        b_add.setOnAction(e -> {
            boolean answer = Controller.getInstance().addNewStudent(tf_studentUsername, tf_studentEmail, tf_studentPassword, tf_studentName, tf_studentSurname);
            tw_student.setItems(Controller.getInstance().getStudents());

            if (answer) {
                tf_studentName.setText("");
                tf_studentSurname.setText("");
                tf_studentUsername.setText("");
                tf_studentPassword.setText("");
                tf_studentEmail.setText("");

                b_add.setDisable(true);
                b_clear.setDisable(true);
                b_delete.setDisable(true);
                b_update.setDisable(true);
            }

        });

        b_delete = new Button();
        b_delete.setText("Delete");
        b_delete.setPrefWidth(70);
        b_delete.setPrefHeight(35);
        b_delete.setDisable(true);
        b_delete.setOnAction(e -> {
            boolean test = Controller.getInstance().deleteStudent(tf_studentEmail.getText());

            if (test) {
                tw_student.setItems(Controller.getInstance().getStudents());

                tf_studentName.setText("");
                tf_studentSurname.setText("");
                tf_studentUsername.setText("");
                tf_studentPassword.setText("");
                tf_studentEmail.setText("");

                b_add.setDisable(true);
                b_delete.setDisable(true);
                b_update.setDisable(true);
                b_clear.setDisable(true);
            }

        });

        b_update = new Button();
        b_update.setText("Update");
        b_update.setPrefWidth(70);
        b_update.setPrefHeight(35);
        b_update.setDisable(true);
        b_update.setOnAction(e -> {
            boolean test = Controller.getInstance().updateStudent(tf_studentUsername, tf_studentEmail, tf_studentName, tf_studentSurname, tf_studentPassword);

            if (test) {
                tw_student.setItems(Controller.getInstance().getStudents());

                tf_studentName.setText("");
                tf_studentSurname.setText("");
                tf_studentUsername.setText("");
                tf_studentPassword.setText("");
                tf_studentEmail.setText("");

                b_add.setDisable(true);
                b_delete.setDisable(true);
                b_update.setDisable(true);
                b_clear.setDisable(true);
            }

        });

        hb_buttons = new HBox(15);
        hb_buttons.getChildren().addAll(b_add, b_delete, b_update);
        GridPane.setConstraints(hb_buttons, 1, 3, 5, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_buttons, new Insets(10, 0, 12, 0));

        sp_student = new Separator();
        GridPane.setConstraints(sp_student, 0, 4, 6, 1, HPos.CENTER, VPos.TOP);

        cb_search = new ComboBox();
        cb_search.setPromptText("Choose search criteria");
        cb_search.getItems().addAll("Student name", "Student surname", "Student e-mail");
        cb_search.setPrefWidth(200);
        cb_search.setOnAction(e -> {
            searchCriteria = cb_search.getValue().toString().trim();
        });

        tf_search = new TextField();
        tf_search.setPrefWidth(170);
        tf_search.setOnKeyReleased(event -> {
            String criteriaData = tf_search.getText().trim();
            tw_student.setItems(Controller.getInstance().getStudentsByCriteria(searchCriteria, criteriaData));
        });

        hb_search = new HBox(5);
        hb_search.getChildren().addAll(cb_search, tf_search);
        GridPane.setConstraints(hb_search, 0, 5, 6, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_search, new Insets(30, 0, 0, 0));

        TableColumn<Student, String> col_studentName = new TableColumn<>("Student name");
        col_studentName.setMinWidth(140);
        col_studentName.setStyle("-fx-font-size: 9pt;");
        col_studentName.setCellValueFactory(new PropertyValueFactory<>("studentName"));
        col_studentName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_studentSurname = new TableColumn<>("Student surname");
        col_studentSurname.setMinWidth(180);
        col_studentSurname.setStyle("-fx-font-size: 9pt;");
        col_studentSurname.setCellValueFactory(new PropertyValueFactory<>("studentSurname"));
        col_studentSurname.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_studentEmail = new TableColumn<>("Student e-mail");
        col_studentEmail.setMinWidth(240);
        col_studentEmail.setStyle("-fx-font-size: 9pt;");
        col_studentEmail.setCellValueFactory(new PropertyValueFactory<>("studentEmail"));
        col_studentEmail.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Student, String> col_student = new TableColumn<>("Student");
        col_student.setMinWidth(560);
        col_student.setStyle("-fx-font-size: 10pt;");
        col_student.getColumns().addAll(col_studentName, col_studentSurname, col_studentEmail);
        col_student.setCellFactory(TooltippedTableCell.forTableColumn());

        tw_student = new TableView<>();
        tw_student.getColumns().addAll(col_student);
        tw_student.setMinWidth(560);
        tw_student.setMaxWidth(560);
        tw_student.setStyle("-fx-font-size: 10pt;");
        tw_student.setRowFactory( tv -> {
            TableRow<Student> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                if (row.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "You have to select populated row");
                    return;
                }

                String studentUsername = null;
                String studentName = null;
                String studentSurname = null;
                String studentEmail = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Student rowData = row.getItem();

                    studentUsername = rowData.getStudentUsername();
                    studentName = rowData.getStudentName();
                    studentSurname = rowData.getStudentSurname();
                    studentEmail = rowData.getStudentEmail();
                }

                tf_studentUsername.setText(studentUsername);
                tf_studentName.setText(studentName);
                tf_studentSurname.setText(studentSurname);
                tf_studentEmail.setText(studentEmail);
                tf_studentPassword.setText("");

                b_add.setDisable(true);
                b_delete.setDisable(false);
                b_update.setDisable(false);
                b_clear.setDisable(false);
            });
            return row;
        });

        tw_student.setItems(Controller.getInstance().getStudents());

        vb_twStudent = new VBox();
        vb_twStudent.getChildren().add(tw_student);
        vb_twStudent.setMinWidth(560);
        GridPane.setConstraints(vb_twStudent, 0,6, 6, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(vb_twStudent, new Insets(15, 0, 15,0));

        gp_dataDisplay.getChildren().addAll(txt_student, l_studentName, tf_studentName, l_studentSurname, tf_studentSurname, l_studentEmail,
                                            /*tf_studentEmail*/ hb_clear, l_studentUsernme, tf_studentUsername, l_studentPassword,
                                            tf_studentPassword, hb_buttons, sp_student, hb_search, vb_twStudent);
    }

    private boolean clear() {
        if ((tf_studentUsername.getText() != null || (!tf_studentUsername.getText().equals(""))) || (tf_studentEmail.getText() != null || (!tf_studentEmail.getText().equals("")))
                || (tf_studentName.getText() != null || (!tf_studentName.getText().equals(""))) || (tf_studentSurname.getText() != null || (!tf_studentSurname.getText().equals("")))
                || (tf_studentPassword.getText() != null || (!tf_studentPassword.getText().equals("")))) {
            return true;
        }
        return false;
    }

    private boolean hasText() {
        if ((tf_studentUsername.getText() == null || tf_studentUsername.getText().equals("")) && (tf_studentEmail.getText() == null || tf_studentEmail.getText().equals(""))
                && (tf_studentName.getText() == null || tf_studentName.getText().equals("")) && (tf_studentSurname.getText() == null || tf_studentSurname.getText().equals(""))
                && (tf_studentPassword.getText() == null || tf_studentPassword.getText().equals(""))) {
            return false;
        }
        return true;
    }
}
