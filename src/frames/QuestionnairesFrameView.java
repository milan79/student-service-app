package frames;

import controller.Controller;
import data.Question;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class QuestionnairesFrameView {

    private static Stage window;
    private static Scene scene;

    private static VBox vb_root;

    private static Text txt_subject;

    private static HBox hb_topRow;

    private static Label l_subjectName;
    private static Label l_subjectNameValue;

    private static HBox hb_subject;

    private static VBox vb_subject;

    private static Text txt_teacher;

    private static Label l_teacherName;
    private static Label l_teacherNameValue;

    private static HBox hb_teacherName;

    private static Label l_teacherSurname;
    private static Label l_teacherSurnameValue;

    private static HBox hb_teacherSurname;

    private static HBox hb_teacherData1;

    private static Label l_teacherEmail;
    private static Label l_teacherEmailValue;

    private static HBox hb_teacherEmail;

    private static Label l_teacherTitle;
    private static Label l_teacherTitleValue;

    private static HBox hb_teacherTitle;

    private static HBox hb_teacherData2;

    private static VBox vb_teacher;

    private static VBox vb_teacherAll;

    private static VBox vb_header;

    private static ObservableList<Question> questions;

    private static Button b_close;

    private static HBox hb_close;

    private static Label l_questionNumber;

    private static Label l_question;
    private static TextArea ta_questionText;

    private static HBox hb_question;

    private static Label l_answer;

    private static TextArea ta_answerText;

    private static RadioButton rb_1;
    private static RadioButton rb_2;
    private static RadioButton rb_3;
    private static RadioButton rb_4;
    private static RadioButton rb_5;
    private static ToggleGroup tg_radioButtons;

    private static CheckBox cb_1;
    private static CheckBox cb_2;
    private static CheckBox cb_3;
    private static CheckBox cb_4;
    private static CheckBox cb_5;

    private static VBox vb_offeredAnswers;

    private static HBox hb_answer;

    private static VBox vb_question;

    private static HBox hb_questionAnswer;

    private static VBox vb_questionAnswer;

    private static GridPane gp_questionnaire;

    private static Separator horSeparator1;
    private static Separator horSeparator2;

    private static GridPane gp_display;

    public static void display() {

        window = new Stage();
        window.setTitle("View questionnaire");
        window.initModality(Modality.APPLICATION_MODAL);

        gp_display = new GridPane();
        gp_display.setAlignment(Pos.TOP_LEFT);
        gp_display.setPadding(new Insets(30, 0, 15, 30));

        txt_subject = new Text("SUBJECT DATA");
        txt_subject.setStyle("-fx-font-size: 12pt;");

        b_close = new Button();
        b_close.setText("Close");
        b_close.setPrefWidth(110);
        b_close.setPrefHeight(40);
        b_close.setStyle("-fx-font-size: 12pt;");
        b_close.setOnAction(e -> window.close());

        hb_close = new HBox();
        hb_close.getChildren().add(b_close);

        hb_topRow = new HBox();

        l_subjectName = new Label();
        l_subjectName.setText("TITLE:");
        l_subjectName.setStyle("-fx-font-size: 10pt;");
        l_subjectName.setMaxWidth(80);
        l_subjectName.setPadding(new Insets(2,0,0,0));

        l_subjectNameValue = new Label();
        l_subjectNameValue.setText(Controller.getInstance().getQuestionnairesFrame().getTf_subject().getText());
        l_subjectNameValue.setStyle("-fx-font-size: 11pt;");
        l_subjectNameValue.setMaxWidth(435);
        l_subjectNameValue.setMinWidth(435);
        l_subjectNameValue.setWrapText(true);

        hb_subject = new HBox(5);

        vb_subject = new VBox();

        txt_teacher = new Text("TEACHER DATA");
        txt_teacher.setStyle("-fx-font-size: 12pt;");

        l_teacherName = new Label();
        l_teacherName.setText("NAME:");
        l_teacherName.setStyle("-fx-font-size: 10pt;");
        l_teacherName.setPrefWidth(50);
        l_teacherName.setPadding(new Insets(2,0,0,0));

        l_teacherNameValue = new Label();
        l_teacherNameValue.setText(Controller.getInstance().getQuestionnairesFrame().getTf_teacherName().getText());
        l_teacherNameValue.setStyle("-fx-font-size: 11pt;");
        l_teacherNameValue.setMaxWidth(180);
        l_teacherNameValue.setMinWidth(180);
        l_teacherNameValue.setWrapText(true);

        hb_teacherName = new HBox(5);
        hb_teacherName.getChildren().addAll(l_teacherName, l_teacherNameValue);
        hb_teacherName.setAlignment(Pos.TOP_LEFT);

        l_teacherSurname = new Label();
        l_teacherSurname.setText("SURNAME:");
        l_teacherSurname.setStyle("-fx-font-size: 10pt;");
        l_teacherSurname.setPrefWidth(72);
        l_teacherSurname.setPadding(new Insets(2,0,0,0));

        l_teacherSurnameValue = new Label();
        l_teacherSurnameValue.setText(Controller.getInstance().getQuestionnairesFrame().getTf_teacherSurname().getText());
        l_teacherSurnameValue.setStyle("-fx-font-size: 11pt;");
        l_teacherSurnameValue.setMaxWidth(180);
        l_teacherSurnameValue.setMinWidth(180);
        l_teacherSurnameValue.setWrapText(true);

        hb_teacherSurname = new HBox(5);
        hb_teacherSurname.getChildren().addAll(l_teacherSurname, l_teacherSurnameValue);
        hb_teacherSurname.setAlignment(Pos.TOP_LEFT);

        hb_teacherData1 = new HBox(30);
        hb_teacherData1.getChildren().addAll(hb_teacherName, hb_teacherSurname);

        l_teacherEmail = new Label();
        l_teacherEmail.setText("E-MAIL:");
        l_teacherEmail.setStyle("-fx-font-size: 10pt;");
        l_teacherEmail.setPrefWidth(50);
        l_teacherEmail.setPadding(new Insets(2,0,0,0));

        l_teacherEmailValue = new Label();
        l_teacherEmailValue.setText(Controller.getInstance().getQuestionnairesFrame().getTf_teacherEmail().getText());
        l_teacherEmailValue.setStyle("-fx-font-size: 11pt;");
        l_teacherEmailValue.setMaxWidth(180);
        l_teacherEmailValue.setMinWidth(180);
        l_teacherEmailValue.setWrapText(true);

        hb_teacherEmail = new HBox(5);
        hb_teacherEmail.getChildren().addAll(l_teacherEmail, l_teacherEmailValue);
        hb_teacherEmail.setAlignment(Pos.TOP_LEFT);

        l_teacherTitle = new Label();
        l_teacherTitle.setText("TITLE:");
        l_teacherTitle.setStyle("-fx-font-size: 10pt;");
        l_teacherTitle.setPrefWidth(72);
        l_teacherTitle.setPadding(new Insets(2,0,0,0));

        l_teacherTitleValue = new Label();
        l_teacherTitleValue.setText("Professor");
        l_teacherTitleValue.setText(Controller.getInstance().getQuestionnairesFrame().getTf_placeholderTeacherTitle().getText());
        l_teacherTitleValue.setStyle("-fx-font-size: 11pt;");
        l_teacherTitleValue.setMaxWidth(180);
        l_teacherTitleValue.setMinWidth(180);
        l_teacherTitleValue.setWrapText(true);

        hb_teacherTitle = new HBox(5);
        hb_teacherTitle.getChildren().addAll(l_teacherTitle, l_teacherTitleValue);
        hb_teacherTitle.setAlignment(Pos.TOP_LEFT);

        hb_teacherData2 = new HBox(30);
        hb_teacherData2.getChildren().addAll(hb_teacherEmail, hb_teacherTitle);

        vb_teacher = new VBox(10);

        vb_teacherAll = new VBox();

        vb_header = new VBox();
        GridPane.setConstraints(vb_header, 0, 0);

        if (l_teacherNameValue.getText().equals("") || l_teacherNameValue.getText() == null) {
            hb_close.setAlignment(Pos.CENTER_RIGHT);
            hb_close.setMaxWidth(600);
            hb_close.setMinWidth(600);
            hb_close.setPadding(new Insets(0,0,0,470));

            hb_topRow.getChildren().addAll(txt_subject, hb_close);
            hb_topRow.setAlignment(Pos.TOP_LEFT);

            hb_subject.getChildren().addAll(l_subjectName, l_subjectNameValue);
            hb_subject.setPadding(new Insets(0, 0, 0, 10));
            hb_subject.setAlignment(Pos.TOP_LEFT);

            vb_subject.getChildren().addAll(hb_topRow, hb_subject);

            vb_header.getChildren().addAll(vb_subject);

            questions = Controller.getInstance().getQuestionnaireQuestions(l_subjectNameValue.getText());
        } else if (l_subjectNameValue.getText().equals("") || l_subjectNameValue.getText() == null) {
            hb_close.setAlignment(Pos.CENTER_RIGHT);
            hb_close.setMaxWidth(600);
            hb_close.setMinWidth(600);
            hb_close.setPadding(new Insets(0,0,0,470));

            hb_topRow.getChildren().addAll(txt_teacher, hb_close);
            hb_topRow.setAlignment(Pos.TOP_LEFT);

            vb_teacher.getChildren().addAll(hb_teacherData1, hb_teacherData2);
            vb_teacher.setPadding(new Insets(0, 0, 0, 10));
            vb_teacher.setAlignment(Pos.TOP_LEFT);

            vb_teacherAll.getChildren().addAll(hb_topRow, vb_teacher);

            vb_header.getChildren().addAll(vb_teacherAll);

            questions = Controller.getInstance().getQuestionnaireQuestions(l_teacherEmailValue.getText());
        }

        ColumnConstraints colConstraint_1 = new ColumnConstraints();
        colConstraint_1.setPrefWidth(730);

        gp_questionnaire = new GridPane();
        gp_questionnaire.getColumnConstraints().addAll(colConstraint_1);
        GridPane.setConstraints(gp_questionnaire, 0, 1);
        GridPane.setMargin(gp_questionnaire, new Insets(0, 0, 0, 0));

        for (int i = 0; i < questions.size(); i++) {

            l_questionNumber = new Label((i + 1) + ".");
            l_questionNumber.setStyle("-fx-font-size: 13pt;");
            l_questionNumber.setPrefWidth(50);
            l_questionNumber.setAlignment(Pos.CENTER);

            l_question = new Label("Q:");
            l_question.setStyle("-fx-font-size: 11pt;");
            l_question.setPrefWidth(20);
            l_question.setAlignment(Pos.TOP_RIGHT);
            l_question.setPadding(new Insets(4.3,0,0,0));

            ta_questionText = new TextArea();
            ta_questionText.setStyle("-fx-font-size: 11pt; -fx-overflow-wrap: break-word;");
            ta_questionText.setPrefWidth(500);
            ta_questionText.setPrefHeight(40);
            ta_questionText.setWrapText(true);
            ta_questionText.setText(questions.get(i).getQuestionText());

            hb_question = new HBox(15);
            hb_question.getChildren().addAll(l_question, ta_questionText);
            hb_question.setPadding(new Insets(0, 10, 0, 10));

            l_answer = new Label("A:");
            l_answer.setStyle("-fx-font-size: 11pt;");
            l_answer.setPrefWidth(20);
            l_answer.setAlignment(Pos.TOP_RIGHT);

            if (questions.get(i).getQuestionTypeDescription().equalsIgnoreCase("write answer")) {
                ta_answerText = new TextArea();
                ta_answerText.setStyle("-fx-font-size: 11pt; -fx-overflow-wrap: break-word;");
                ta_answerText.setPrefWidth(500);
                ta_answerText.setPrefHeight(40);
                ta_answerText.setWrapText(true);

                hb_answer = new HBox(15);
                hb_answer.getChildren().addAll(l_answer, ta_answerText);
                hb_answer.setPadding(new Insets(0, 10, 0, 10));

                l_answer.setPadding(new Insets(4.3,0,0,0));
            } else if (questions.get(i).getQuestionTypeDescription().equalsIgnoreCase("radio button")) {
                tg_radioButtons = new ToggleGroup();

                String answers = questions.get(i).getQuestionAnswerRb();
                String[] individualAnswers = answers.split(";");

                rb_1 = new RadioButton();
                rb_1.setText(individualAnswers[0]);
                rb_1.setToggleGroup(tg_radioButtons);
                rb_2 = new RadioButton();
                rb_2.setText(individualAnswers[1]);
                rb_2.setToggleGroup(tg_radioButtons);
                rb_3 = new RadioButton();
                rb_3.setText(individualAnswers[2]);
                rb_3.setToggleGroup(tg_radioButtons);
                rb_4 = new RadioButton();
                rb_4.setText(individualAnswers[3]);
                rb_4.setToggleGroup(tg_radioButtons);
                rb_5 = new RadioButton();
                rb_5.setText(individualAnswers[4]);
                rb_5.setToggleGroup(tg_radioButtons);

                vb_offeredAnswers = new VBox(5);
                vb_offeredAnswers.getChildren().addAll(rb_1, rb_2, rb_3, rb_4, rb_5);

                hb_answer = new HBox(15);
                hb_answer.getChildren().addAll(l_answer, vb_offeredAnswers);
                hb_answer.setPadding(new Insets(0, 10, 0, 10));
            } else if (questions.get(i).getQuestionTypeDescription().equalsIgnoreCase("check box")) {

                String answers = questions.get(i).getQuestionAnswerChkbx();
                String[] individualAnswers = answers.split(";");

                cb_1 = new CheckBox();
                cb_1.setText(individualAnswers[0]);
                cb_2 = new CheckBox();
                cb_2.setText(individualAnswers[1]);
                cb_3 = new CheckBox();
                cb_3.setText(individualAnswers[2]);
                cb_4 = new CheckBox();
                cb_4.setText(individualAnswers[3]);
                cb_5 = new CheckBox();
                cb_5.setText(individualAnswers[4]);

                vb_offeredAnswers = new VBox(5);
                vb_offeredAnswers.getChildren().addAll(cb_1, cb_2, cb_3, cb_4, cb_5);

                hb_answer = new HBox(15);
                hb_answer.getChildren().addAll(l_answer, vb_offeredAnswers);
                hb_answer.setPadding(new Insets(0, 10, 0, 10));
            }

            vb_question = new VBox(10);
            vb_question.getChildren().addAll(hb_question, hb_answer);

            hb_questionAnswer = new HBox();
            hb_questionAnswer.getChildren().addAll(l_questionNumber, vb_question);
            hb_questionAnswer.setAlignment(Pos.CENTER_LEFT);

            horSeparator1 = new Separator();
            horSeparator1.setPrefWidth(700);

            horSeparator2 = new Separator();
            horSeparator2.setPrefWidth(700);
            horSeparator2.setPadding(new Insets(0,0,15,0));

            vb_questionAnswer = new VBox();
            vb_questionAnswer.getChildren().addAll(horSeparator1, horSeparator2, hb_questionAnswer);

            GridPane.setConstraints(vb_questionAnswer, 0, i, 1, 1, HPos.LEFT, VPos.CENTER);
            GridPane.setMargin(vb_questionAnswer, new Insets(10,10,10, 0));

            gp_questionnaire.getChildren().add(vb_questionAnswer);
        }

        gp_display.getChildren().addAll(vb_header, gp_questionnaire);

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(gp_display);

        vb_root = new VBox();
        vb_root.getChildren().addAll(scrollPane);
        vb_root.setAlignment(Pos.CENTER);

        scene = new Scene(vb_root, 800, 700);
        window.setScene(scene);
        window.showAndWait();
    }
}
