package frames;

import controller.Controller;
import data.Subject;
import data.Teacher;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SelectSubjectFrame {
    private static Stage window;
    private static Scene scene;

    private static Text txt_subject;

    private static Label l_subjectTitle;
    private static TextField tf_subjectTitle;

    private static Button b_select;

    private static HBox hb_bSelect;

    private static Separator sp_subject;

    private static Label l_title;
    private static TextField tf_title;

    private static HBox hb_search;

    private static TableView<Subject> tw_subjects;

    private static VBox vb_twSubject;

    private static GridPane gp_dataDisplay;

    public static void display(String callingFrame) {

        window = new Stage();
        window.setTitle("Select subject");
        window.initModality(Modality.APPLICATION_MODAL);

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.TOP_CENTER);
        gp_dataDisplay.setPadding(new Insets(30, 0, 15, 0));

        txt_subject = new Text("SUBJECT DATA");
        txt_subject.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_subject, 0, 0, 2, 1);
        GridPane.setMargin(txt_subject, new Insets(0, 0, 10, 0));

        l_subjectTitle = new Label();
        l_subjectTitle.setText("Title:");
        GridPane.setConstraints(l_subjectTitle, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_subjectTitle, new Insets(12, 5, 0, 0));

        tf_subjectTitle = new TextField();
        tf_subjectTitle.setMaxWidth(140);
        tf_subjectTitle.setEditable(false);
        GridPane.setConstraints(tf_subjectTitle, 1, 1);
        GridPane.setMargin(tf_subjectTitle, new Insets(12, 20, 0, 0));

        b_select = new Button();
        b_select.setText("Confirm");
        b_select.setPrefWidth(130);
        b_select.setPrefHeight(40);
        b_select.setStyle("-fx-font-size: 12pt;");
        b_select.setOnAction(e -> {
            if (callingFrame.equals("questionnaires")) {
                Controller.getInstance().getQuestionnairesFrame().getTf_subject().setText(tf_subjectTitle.getText());

                Controller.getInstance().getQuestionnairesFrame().getTf_teacherName().setText("");
                Controller.getInstance().getQuestionnairesFrame().getTf_teacherSurname().setText("");
                Controller.getInstance().getQuestionnairesFrame().getTf_teacherEmail().setText("");
                Controller.getInstance().getQuestionnairesFrame().getTf_placeholderTeacherTitle().setText("");

                Controller.getInstance().getQuestionnairesFrame().getB_clear().setDisable(false);

                if (Controller.getInstance().verifySubjectQuestionnaire(tf_subjectTitle.getText())) {
                    Controller.getInstance().getQuestionnairesFrame().getB_add().setDisable(true);
                    Controller.getInstance().getQuestionnairesFrame().getB_delete().setDisable(false);
                    Controller.getInstance().getQuestionnairesFrame().getB_view().setDisable(false);
                } else {
                    Controller.getInstance().getQuestionnairesFrame().getB_add().setDisable(false);
                    Controller.getInstance().getQuestionnairesFrame().getB_delete().setDisable(true);
                    Controller.getInstance().getQuestionnairesFrame().getB_view().setDisable(true);
                }
            }

            if (callingFrame.equals("tests")) {
                Controller.getInstance().getTestsFrame().getTf_subject().setText(tf_subjectTitle.getText());
                Controller.getInstance().getTestsFrame().getB_clear().setDisable(false);

                if (Controller.getInstance().verifySubjectTest(tf_subjectTitle.getText())) {
                    Controller.getInstance().getTestsFrame().getB_add().setDisable(true);
                    Controller.getInstance().getTestsFrame().getB_delete().setDisable(false);
                    Controller.getInstance().getTestsFrame().getB_view().setDisable(false);
                } else {
                    Controller.getInstance().getTestsFrame().getB_add().setDisable(false);
                    Controller.getInstance().getTestsFrame().getB_delete().setDisable(true);
                    Controller.getInstance().getTestsFrame().getB_view().setDisable(true);
                }
            }

            window.close();
        });

        hb_bSelect = new HBox(15);
        hb_bSelect.getChildren().addAll(b_select);
        GridPane.setConstraints(hb_bSelect, 1, 2, 2, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_bSelect, new Insets(10, 0, 12, 0));

        sp_subject = new Separator();
        GridPane.setConstraints(sp_subject, 0, 3, 4, 1, HPos.CENTER, VPos.TOP);

        l_title = new Label();
        l_title.setText("Subject:");
        l_title.setPadding(new Insets(4, 0, 0, 0));

        tf_title = new TextField();
        tf_title.setPrefWidth(170);
        tf_title.setOnKeyReleased(event -> {
            String subjectTitleText = tf_title.getText().trim();
            tw_subjects.setItems(Controller.getInstance().getSubjects(subjectTitleText));
        });

        hb_search = new HBox(5);
        hb_search.getChildren().addAll(l_title, tf_title);
        GridPane.setConstraints(hb_search, 0, 5, 4, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_search, new Insets(30, 0, 0, 0));

        TableColumn<Subject, String> col_subjectName = new TableColumn<>("Subject");
        col_subjectName.setMinWidth(220);
        col_subjectName.setStyle("-fx-font-size: 10pt;");
        col_subjectName.setCellValueFactory(new PropertyValueFactory<>("subjectTitle"));
        col_subjectName.setCellFactory(TooltippedTableCell.forTableColumn());

        tw_subjects = new TableView<>();
        tw_subjects.getColumns().add(col_subjectName);
        tw_subjects.setMaxWidth(220);
        tw_subjects.setStyle("-fx-font-size: 9pt;");
        tw_subjects.setRowFactory( tv -> {
            TableRow<Subject> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                String subjectTitle = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Subject rowData = row.getItem();

                    subjectTitle = rowData.getSubjectTitle();
                }

                tf_subjectTitle.setText(subjectTitle);

            });
            return row;
        });

        tw_subjects.setItems(Controller.getInstance().getSubjects());

        vb_twSubject = new VBox();
        vb_twSubject.getChildren().add(tw_subjects);
        vb_twSubject.setMinWidth(220);
        GridPane.setConstraints(vb_twSubject, 0, 6, 4, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(vb_twSubject, new Insets(15, 0, 15, 0));

        gp_dataDisplay.getChildren().addAll(txt_subject, l_subjectTitle, tf_subjectTitle, hb_bSelect, sp_subject, hb_search, vb_twSubject);

        scene = new Scene(gp_dataDisplay, 800, 800);
        window.setScene(scene);
        window.showAndWait();
    }
}
