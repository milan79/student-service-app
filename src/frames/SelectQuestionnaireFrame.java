package frames;

import controller.Controller;
import data.Subject;
import data.Teacher;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SelectQuestionnaireFrame {

    private static Stage window;
    private static Scene scene;

    private static Text txt_questionnaire;

    private static Label l_subject;
    private static TextField tf_subject;

    private static Label l_teacherName;
    private static TextField tf_teacherName;

    private static Label l_teacherSurname;
    private static TextField tf_teacherSurname;

    private static Label l_teacherTitle;
    private static TextField tf_teacherTitle;

    private static Label l_teacherEmail;
    private static TextField tf_teacherEmail;

    private static Button b_select;

    private static HBox hb_bSelect;

    private static Separator sp_teacher;

    private static ComboBox cb_questionnaireType;
    private static TextField tf_subjectTitle;

    private static HBox hb_subjectTitle;

    private static ComboBox cb_teacherTitle;
    private static String teacherTitle;

    private static ComboBox cb_searchCriteria;
    private static TextField tf_searchCriteria;
    private static String searchCriteria;

    private static HBox hb_searchCriteria;

    private static HBox hb_search;

    private static TableView<Subject> tw_subjects;
    private static TableView<Teacher> tw_teachers;

    private static VBox vb_Questionnaire;

    private static String selectedCategory;

    private static GridPane gp_dataDisplay;

    public static void display(String questionnaireOrTest) {

        window = new Stage();
        window.setTitle("Select questionnaire");
        window.initModality(Modality.APPLICATION_MODAL);

        gp_dataDisplay = new GridPane();
        gp_dataDisplay.setAlignment(Pos.TOP_CENTER);
        gp_dataDisplay.setPadding(new Insets(30, 0, 15, 0));

        txt_questionnaire = new Text("QUESTIONNAIRE DATA");
        txt_questionnaire.setStyle("-fx-font-size: 9pt;");
        GridPane.setConstraints(txt_questionnaire, 0, 0, 2, 1);
        GridPane.setMargin(txt_questionnaire, new Insets(0, 0, 10, 0));

        l_subject = new Label();
        l_subject.setText("Title:");
        GridPane.setConstraints(l_subject, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_subject, new Insets(12, 5, 0, 0));

        tf_subject = new TextField();
        tf_subject.setMaxWidth(140);
        tf_subject.setEditable(false);
        GridPane.setConstraints(tf_subject, 1, 1);
        GridPane.setMargin(tf_subject, new Insets(12, 20, 0, 0));

        l_teacherName = new Label();
        l_teacherName.setText("Name:");
        GridPane.setConstraints(l_teacherName, 0, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherName, new Insets(12, 5, 0, 0));

        tf_teacherName = new TextField();
        tf_teacherName.setMaxWidth(140);
        tf_teacherName.setEditable(false);
        GridPane.setConstraints(tf_teacherName, 1, 1);
        GridPane.setMargin(tf_teacherName, new Insets(12, 20, 0, 0));

        l_teacherSurname = new Label();
        l_teacherSurname.setText("Surname:");
        GridPane.setConstraints(l_teacherSurname, 2, 1, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherSurname, new Insets(12, 5, 0, 0));

        tf_teacherSurname = new TextField();
        tf_teacherSurname.setMaxWidth(140);
        tf_teacherSurname.setEditable(false);
        GridPane.setConstraints(tf_teacherSurname, 3, 1);
        GridPane.setMargin(tf_teacherSurname, new Insets(12, 20, 0, 0));

        l_teacherTitle = new Label();
        l_teacherTitle.setText("Title:");
        GridPane.setConstraints(l_teacherTitle, 0, 2,1,1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherTitle, new Insets(12, 5, 12, 0));

        tf_teacherTitle = new TextField();
        tf_teacherTitle.setMaxWidth(140);
        tf_teacherTitle.setEditable(false);
        GridPane.setConstraints(tf_teacherTitle, 1, 2);
        GridPane.setMargin(tf_teacherTitle, new Insets(12, 0, 12, 0));

        l_teacherEmail = new Label();
        l_teacherEmail.setText("E-mail:");
        GridPane.setConstraints(l_teacherEmail, 2, 2, 1, 1, HPos.RIGHT, VPos.CENTER);
        GridPane.setMargin(l_teacherEmail, new Insets(12, 5, 12, 0));

        tf_teacherEmail = new TextField();
        tf_teacherEmail.setMaxWidth(140);
        tf_teacherEmail.setEditable(false);
        GridPane.setConstraints(tf_teacherEmail, 3, 2);
        GridPane.setMargin(tf_teacherEmail, new Insets(12, 0, 12, 0));

        b_select = new Button();
        b_select.setText("Confirm");
        b_select.setPrefWidth(130);
        b_select.setPrefHeight(40);
        b_select.setStyle("-fx-font-size: 12pt;");
        b_select.setOnAction(e -> {
            VBox vb_tempSubject = Controller.getInstance().getQuestionsFrame().getVb_subject();
            VBox vb_tempTeacher = Controller.getInstance().getQuestionsFrame().getVb_teacher();

            HBox hb_tempSubject = Controller.getInstance().getQuestionsFrame().getHb_subject();
            Label l_tempQuestionnaire = Controller.getInstance().getQuestionsFrame().getL_questionnaire();
            Label l_tempTest = Controller.getInstance().getQuestionsFrame().getL_test();

            HBox hb_tempTeacherName = Controller.getInstance().getQuestionsFrame().getHb_teacherName();
            HBox hb_tempTeacherSurname = Controller.getInstance().getQuestionsFrame().getHb_teacherSurname();
            HBox hb_tempTeacherEmail = Controller.getInstance().getQuestionsFrame().getHb_teacherEmail();
            HBox hb_tempTeacherTitle = Controller.getInstance().getQuestionsFrame().getHb_teacherTitle();

            ObservableList<Node> tempList = Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren();

            l_tempTest.setText("");

            if (selectedCategory != null && selectedCategory.equals("subjects") && questionnaireOrTest.equalsIgnoreCase("questionnaire")) {

                l_tempQuestionnaire.setText("QUESTIONNAIRE");

                if (tempList.size() == 1) {
                    Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren().add(1, vb_tempSubject);
                } else if (tempList.size() == 2) {
                    Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren().remove(1);
                    Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren().add(1, vb_tempSubject);
                }

                Controller.getInstance().getQuestionsFrame().getVb_subject().getChildren().addAll(l_tempQuestionnaire, hb_tempSubject);

                Controller.getInstance().getQuestionsFrame().getL_subjectText().setText(tf_subject.getText());

                Controller.getInstance().getQuestionsFrame().getTf_questionRefersTo().setText(tf_subject.getText());

                Controller.getInstance().getQuestionsFrame().getTw_question().setItems(Controller.getInstance().getQuestionnaireQuestions(tf_subject.getText()));
            }

            if (selectedCategory != null && selectedCategory.equals("teachers") && questionnaireOrTest.equalsIgnoreCase("questionnaire")) {

                l_tempQuestionnaire.setText("QUESTIONNAIRE");

                if (tempList.size() == 1) {
                    Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren().add(1, vb_tempTeacher);
                } else if (tempList.size() == 2) {
                    Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren().remove(1);
                    Controller.getInstance().getQuestionsFrame().getHb_twQuestion().getChildren().add(1, vb_tempTeacher);
                }

                Controller.getInstance().getQuestionsFrame().getVb_teacher().getChildren().addAll(l_tempQuestionnaire, hb_tempTeacherName, hb_tempTeacherSurname, hb_tempTeacherEmail, hb_tempTeacherTitle);

                Controller.getInstance().getQuestionsFrame().getL_teacherNameText().setText(tf_teacherName.getText());
                Controller.getInstance().getQuestionsFrame().getL_teacherSurnameText().setText(tf_teacherSurname.getText());
                Controller.getInstance().getQuestionsFrame().getL_teacherEmailText().setText(tf_teacherEmail.getText());
                Controller.getInstance().getQuestionsFrame().getL_teacherTitleText().setText(tf_teacherTitle.getText());

                Controller.getInstance().getQuestionsFrame().getTf_questionRefersTo().setText(tf_teacherEmail.getText());

                Controller.getInstance().getQuestionsFrame().getTw_question().setItems(Controller.getInstance().getQuestionnaireQuestions(tf_teacherEmail.getText()));
            }

            Controller.getInstance().getQuestionsFrame().getTf_questionRefersTo().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTa_questionText().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTa_questionText().setText("");
            Controller.getInstance().getQuestionsFrame().getCb_type().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getCb_type().getSelectionModel().clearSelection();
            Controller.getInstance().getQuestionsFrame().getTf_firstResponse().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTf_firstResponse().setText("");
            Controller.getInstance().getQuestionsFrame().getTf_secondResponse().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTf_secondResponse().setText("");
            Controller.getInstance().getQuestionsFrame().getTf_thirdResponse().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTf_thirdResponse().setText("");
            Controller.getInstance().getQuestionsFrame().getTf_fourthResponse().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTf_fourthResponse().setText("");
            Controller.getInstance().getQuestionsFrame().getTf_fifthResponse().setDisable(false);
            Controller.getInstance().getQuestionsFrame().getTf_fifthResponse().setText("");

            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_1().setSelected(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_1().setDisable(true);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_2().setSelected(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_2().setDisable(true);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_3().setSelected(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_3().setDisable(true);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_4().setSelected(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_4().setDisable(true);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_5().setSelected(false);
            Controller.getInstance().getQuestionsFrame().getCb_correctAnswer_5().setDisable(true);

            Controller.getInstance().getQuestionsFrame().getB_delete().setDisable(true);
            Controller.getInstance().getQuestionsFrame().getB_update().setDisable(true);
            Controller.getInstance().getQuestionsFrame().getB_clear().setDisable(true);

            window.close();
        });

        hb_bSelect = new HBox(15);
        hb_bSelect.getChildren().addAll(b_select);
        GridPane.setConstraints(hb_bSelect, 1, 3, 2, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_bSelect, new Insets(10, 0, 12, 0));

        sp_teacher = new Separator();
        GridPane.setConstraints(sp_teacher, 0, 4, 4, 1, HPos.CENTER, VPos.TOP);

        cb_questionnaireType = new ComboBox<>();
        cb_questionnaireType.setPromptText("Refers to");
        cb_questionnaireType.getItems().addAll("Subject", "Teacher");
        cb_questionnaireType.setPrefWidth(140);
        cb_questionnaireType.setOnAction(e -> {
            String type = cb_questionnaireType.getValue().toString().trim();
            if (type.equals("Subject")) {
                if (hb_search.getChildren().contains(hb_searchCriteria)) {
                    hb_search.getChildren().remove(hb_searchCriteria);
                }
                if (!hb_search.getChildren().contains(hb_subjectTitle)) {
                    hb_search.getChildren().add(hb_subjectTitle);
                }
                if (vb_Questionnaire.getChildren().contains(tw_teachers)) {
                    vb_Questionnaire.getChildren().remove(tw_teachers);
                }
                if (!vb_Questionnaire.getChildren().contains(tw_subjects)) {
                    vb_Questionnaire.getChildren().add(tw_subjects);
                }
                if (gp_dataDisplay.getChildren().contains(l_teacherName)) {
                    gp_dataDisplay.getChildren().remove(l_teacherName);
                }
                if (gp_dataDisplay.getChildren().contains(tf_teacherName)) {
                    gp_dataDisplay.getChildren().remove(tf_teacherName);
                }
                if (gp_dataDisplay.getChildren().contains(l_teacherSurname)) {
                    gp_dataDisplay.getChildren().remove(l_teacherSurname);
                }
                if (gp_dataDisplay.getChildren().contains(tf_teacherSurname)) {
                    gp_dataDisplay.getChildren().remove(tf_teacherSurname);
                }
                if (gp_dataDisplay.getChildren().contains(l_teacherEmail)) {
                    gp_dataDisplay.getChildren().remove(l_teacherEmail);
                }
                if (gp_dataDisplay.getChildren().contains(tf_teacherEmail)) {
                    gp_dataDisplay.getChildren().remove(tf_teacherEmail);
                }
                if (gp_dataDisplay.getChildren().contains(l_teacherTitle)) {
                    gp_dataDisplay.getChildren().remove(l_teacherTitle);
                }
                if (gp_dataDisplay.getChildren().contains(tf_teacherTitle)) {
                    gp_dataDisplay.getChildren().remove(tf_teacherTitle);
                }
                if (!gp_dataDisplay.getChildren().contains(l_subject)) {
                    gp_dataDisplay.getChildren().add(l_subject);
                }
                if (!gp_dataDisplay.getChildren().contains(tf_subject)) {
                    gp_dataDisplay.getChildren().add(tf_subject);
                }

                GridPane.setConstraints(hb_bSelect, 1, 2, 2, 1, HPos.LEFT, VPos.CENTER);
                GridPane.setConstraints(sp_teacher, 0, 3, 4, 1, HPos.CENTER, VPos.TOP);
                GridPane.setConstraints(hb_search, 0, 5, 7, 1, HPos.LEFT, VPos.CENTER);
                GridPane.setConstraints(vb_Questionnaire, 0,6, 7, 1, HPos.CENTER, VPos.CENTER);

                tw_subjects.setItems(Controller.getInstance().getSubjectQuestionnaire(null));
            }

            if (type.equals("Teacher")) {
                if (hb_search.getChildren().contains(hb_subjectTitle)) {
                    hb_search.getChildren().remove(hb_subjectTitle);
                }
                if (!hb_search.getChildren().contains(hb_searchCriteria)) {
                    hb_search.getChildren().add(hb_searchCriteria);
                }
                if (vb_Questionnaire.getChildren().contains(tw_subjects)) {
                    vb_Questionnaire.getChildren().remove(tw_subjects);
                }
                if (!vb_Questionnaire.getChildren().contains(tw_teachers)) {
                    vb_Questionnaire.getChildren().add(tw_teachers);
                }
                if (!gp_dataDisplay.getChildren().contains(l_teacherName)) {
                    gp_dataDisplay.getChildren().add(l_teacherName);
                }
                if (!gp_dataDisplay.getChildren().contains(tf_teacherName)) {
                    gp_dataDisplay.getChildren().add(tf_teacherName);
                }
                if (!gp_dataDisplay.getChildren().contains(l_teacherSurname)) {
                    gp_dataDisplay.getChildren().add(l_teacherSurname);
                }
                if (!gp_dataDisplay.getChildren().contains(tf_teacherSurname)) {
                    gp_dataDisplay.getChildren().add(tf_teacherSurname);
                }
                if (!gp_dataDisplay.getChildren().contains(l_teacherEmail)) {
                    gp_dataDisplay.getChildren().add(l_teacherEmail);
                }
                if (!gp_dataDisplay.getChildren().contains(tf_teacherEmail)) {
                    gp_dataDisplay.getChildren().add(tf_teacherEmail);
                }
                if (!gp_dataDisplay.getChildren().contains(l_teacherTitle)) {
                    gp_dataDisplay.getChildren().add(l_teacherTitle);
                }
                if (!gp_dataDisplay.getChildren().contains(tf_teacherTitle)) {
                    gp_dataDisplay.getChildren().add(tf_teacherTitle);
                }
                if (gp_dataDisplay.getChildren().contains(l_subject)) {
                    gp_dataDisplay.getChildren().remove(l_subject);
                }
                if (gp_dataDisplay.getChildren().contains(tf_subject)) {
                    gp_dataDisplay.getChildren().remove(tf_subject);
                }

                GridPane.setConstraints(hb_bSelect, 1, 3, 2, 1, HPos.LEFT, VPos.CENTER);
                GridPane.setConstraints(sp_teacher, 0, 4, 4, 1, HPos.CENTER, VPos.TOP);
                GridPane.setConstraints(hb_search, 0, 6, 7, 1, HPos.LEFT, VPos.CENTER);
                GridPane.setConstraints(vb_Questionnaire, 0,7, 7, 1, HPos.CENTER, VPos.CENTER);

                tw_teachers.setItems(Controller.getInstance().getTeacherQuestionnaire(null));
            }
        });

        tf_subjectTitle = new TextField();
        tf_subjectTitle.setPrefWidth(170);
        tf_subjectTitle.setOnKeyReleased(event -> {
            String subjectTitleText = tf_subjectTitle.getText().trim();
            tw_subjects.setItems(Controller.getInstance().getSubjectQuestionnaire(subjectTitleText));
        });

        hb_subjectTitle = new HBox();
        hb_subjectTitle.getChildren().add(tf_subjectTitle);
        hb_subjectTitle.setMinWidth(300);
        hb_subjectTitle.setPadding(new Insets(0, 0, 0, 30));

        cb_teacherTitle = new ComboBox();
        cb_teacherTitle.setPromptText("Choose title");
        cb_teacherTitle.getItems().addAll("Professor", "Assistant", "Demonstrator");
        cb_teacherTitle.setPrefWidth(160);
        cb_teacherTitle.setPadding(new Insets(0, 5, 0, 10));
        cb_teacherTitle.setOnAction(e -> {
            teacherTitle = cb_teacherTitle.getValue().toString().trim();
            tw_teachers.setItems(Controller.getInstance().getTeacherQuestionnaire(teacherTitle));
        });

        cb_searchCriteria = new ComboBox();
        cb_searchCriteria.setPromptText("Choose search criteria");
        cb_searchCriteria.getItems().addAll("Teacher name", "Teacher surname", "Teacher e-mail");
        cb_searchCriteria.setPrefWidth(220);
        cb_searchCriteria.setOnAction(e -> {
            searchCriteria = cb_searchCriteria.getValue().toString().trim();
        });

        tf_searchCriteria = new TextField();
        tf_searchCriteria.setPrefWidth(150);
        tf_searchCriteria.setOnKeyReleased(event -> {
            String criteriaData = tf_searchCriteria.getText().trim();
            tw_teachers.setItems(Controller.getInstance().getTeachersQuestionnairesByCriteria(searchCriteria, criteriaData, teacherTitle));
        });

        hb_searchCriteria = new HBox(5);
        hb_searchCriteria.getChildren().addAll(cb_teacherTitle, cb_searchCriteria, tf_searchCriteria);
        hb_searchCriteria.setPadding(new Insets(0, 0, 0, 30));

        hb_search = new HBox();
        hb_search.getChildren().addAll(cb_questionnaireType, hb_subjectTitle);
        GridPane.setConstraints(hb_search, 0, 6, 7, 1, HPos.LEFT, VPos.CENTER);
        GridPane.setMargin(hb_search, new Insets(30, 0, 0, 0));

        TableColumn<Subject, String> col_subjectName = new TableColumn<>("Subject");
        col_subjectName.setMinWidth(220);
        col_subjectName.setStyle("-fx-font-size: 10pt;");
        col_subjectName.setCellValueFactory(new PropertyValueFactory<>("subjectTitle"));
        col_subjectName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherName = new TableColumn<>("Name");
        col_teacherName.setMinWidth(120);
        col_teacherName.setStyle("-fx-font-size: 9pt;");
        col_teacherName.setCellValueFactory(new PropertyValueFactory<>("teacherName"));
        col_teacherName.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherSurname = new TableColumn<>("Surname");
        col_teacherSurname.setMinWidth(160);
        col_teacherSurname.setStyle("-fx-font-size: 9pt;");
        col_teacherSurname.setCellValueFactory(new PropertyValueFactory<>("teacherSurname"));
        col_teacherSurname.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherEmail = new TableColumn<>("E-mail");
        col_teacherEmail.setMinWidth(200);
        col_teacherEmail.setStyle("-fx-font-size: 9pt;");
        col_teacherEmail.setCellValueFactory(new PropertyValueFactory<>("teacherEmail"));
        col_teacherEmail.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacherCategoryDescription = new TableColumn<>("Title");
        col_teacherCategoryDescription.setMinWidth(100);
        col_teacherCategoryDescription.setStyle("-fx-font-size: 9pt;");
        col_teacherCategoryDescription.setCellValueFactory(new PropertyValueFactory<>("teacherTitleDescription"));
        col_teacherCategoryDescription.setCellFactory(TooltippedTableCell.forTableColumn());

        TableColumn<Teacher, String> col_teacher = new TableColumn<>("Teacher");
        col_teacher.setMinWidth(580);
        col_teacher.setStyle("-fx-font-size: 10pt;");
        col_teacher.getColumns().addAll(col_teacherName, col_teacherSurname, col_teacherEmail, col_teacherCategoryDescription);

        tw_subjects = new TableView<>();
        tw_subjects.getColumns().add(col_subjectName);
        tw_subjects.setMaxWidth(220);
        tw_subjects.setStyle("-fx-font-size: 9pt;");
        tw_subjects.setRowFactory( tv -> {
            TableRow<Subject> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                String subjectTitle = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Subject rowData = row.getItem();

                    subjectTitle = rowData.getSubjectTitle();
                }

                tf_subject.setText(subjectTitle);

                tf_teacherSurname.setText("");
                tf_teacherName.setText("");
                tf_teacherEmail.setText("");
                tf_teacherTitle.setText("");

                selectedCategory = "subjects";

            });
            return row;
        });

        tw_teachers = new TableView<>();
        tw_teachers.getColumns().add(col_teacher);
        tw_teachers.setMaxWidth(580);
        tw_teachers.setStyle("-fx-font-size: 9pt;");
        tw_teachers.setRowFactory( tv -> {
            TableRow<Teacher> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                String teacherName = null;
                String teacherSurname = null;
                String teacherEmail = null;
                String teacherTitle = null;

                if (event.getClickCount() == 1 && (! row.isEmpty()) && event.getButton()== MouseButton.PRIMARY) {
                    Teacher rowData = row.getItem();

                    teacherName = rowData.getTeacherName();
                    teacherSurname = rowData.getTeacherSurname();
                    teacherEmail = rowData.getTeacherEmail();
                    teacherTitle = rowData.getTeacherTitleDescription();
                }

                tf_teacherName.setText(teacherName);
                tf_teacherSurname.setText(teacherSurname);
                tf_teacherEmail.setText(teacherEmail);
                tf_teacherTitle.setText(teacherTitle);

                tf_subject.setText("");

                selectedCategory = "teachers";

            });
            return row;
        });

        vb_Questionnaire = new VBox();
        vb_Questionnaire.getChildren().add(tw_subjects);
        GridPane.setConstraints(vb_Questionnaire, 0,7, 7, 1, HPos.CENTER, VPos.CENTER);
        GridPane.setMargin(vb_Questionnaire, new Insets(15, 0, 15,0));

        gp_dataDisplay.getChildren().addAll(txt_questionnaire, l_subject, tf_subject, hb_bSelect, sp_teacher, hb_search, vb_Questionnaire);

        scene = new Scene(gp_dataDisplay, 800, 800);
        window.setScene(scene);
        window.showAndWait();
    }

    public static TextField getTf_subject() {
        return tf_subject;
    }

    public static TextField getTf_teacherName() {
        return tf_teacherName;
    }

    public static TextField getTf_teacherSurname() {
        return tf_teacherSurname;
    }

    public static TextField getTf_teacherTitle() {
        return tf_teacherTitle;
    }

    public static TextField getTf_teacherEmail() {
        return tf_teacherEmail;
    }
}
