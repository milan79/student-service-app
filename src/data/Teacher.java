package data;

public class Teacher {

    private String teacherUsername;
    private String teacherEmail;
    private String teacherName;
    private String teacherSurname;
    private String teacherPassword;
    private int teacherTitleId;

    private String teacherTitleDescription;

    public Teacher(String teacherUsername, String teacherEmail, String teacherName, String teacherSurname, String teacherPassword, int teacherTitleId) {
        this.teacherUsername = teacherUsername;
        this.teacherEmail = teacherEmail;
        this.teacherName = teacherName;
        this.teacherSurname = teacherSurname;
        this.teacherPassword = teacherPassword;
        this.teacherTitleId = teacherTitleId;
        if (this.teacherTitleId == 1) {
            teacherTitleDescription = "Professor";
        } else if (this.teacherTitleId == 2) {
            teacherTitleDescription = "Assistant";
        } else if (this.teacherTitleId == 3) {
            teacherTitleDescription = "Demonstrator";
        }
    }

    public String getTeacherUsername() {
        return teacherUsername;
    }

    public void setTeacherUsername(String teacherUsername) {
        this.teacherUsername = teacherUsername;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherSurname() {
        return teacherSurname;
    }

    public void setTeacherSurname(String teacherSurname) {
        this.teacherSurname = teacherSurname;
    }

    public String getTeacherPassword() {
        return teacherPassword;
    }

    public void setTeacherPassword(String teacherPassword) {
        this.teacherPassword = teacherPassword;
    }

    public int getTeacherTitleId() {
        return teacherTitleId;
    }

    public void setTeacherTitleId(int teacherTitleId) {
        this.teacherTitleId = teacherTitleId;
    }

    public String getTeacherTitleDescription() {
        return teacherTitleDescription;
    }
}
