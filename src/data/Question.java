package data;

public class Question {

    private int questionnaireId;
    private int questionTypeId;
    private String questionAnswerRb;
    private String questionAnswerChkbx;
    private int testId;
    private String questionText;

    private String questionTypeDescription;

    public Question(int questionnaireId, int questionTypeId, String questionAnswerRb, String questionAnswerChkbx, int testId, String questionText) {
        this.questionnaireId = questionnaireId;
        this.questionTypeId = questionTypeId;
        if (questionTypeId == 1) {
            questionTypeDescription = "radio button";
        } else if (questionTypeId == 2) {
            questionTypeDescription = "check box";
        } else if (questionTypeId == 3) {
            questionTypeDescription = "write answer";
        }
        this.questionAnswerRb = questionAnswerRb;
        this.questionAnswerChkbx = questionAnswerChkbx;
        this.testId = testId;
        this.questionText = questionText;
    }

    public int getQuestionnaireId() {
        return questionnaireId;
    }

    public void setQuestionnaireId(int questionnaireId) {
        this.questionnaireId = questionnaireId;
    }

    public int getQuestionTypeId() {
        return questionTypeId;
    }

    public void setQuestionTypeId(int questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public String getQuestionAnswerRb() {
        return questionAnswerRb;
    }

    public void setQuestionAnswerRb(String questionAnswerRb) {
        this.questionAnswerRb = questionAnswerRb;
    }

    public String getQuestionAnswerChkbx() {
        return questionAnswerChkbx;
    }

    public void setQuestionAnswerChkbx(String questionAnswerChkbx) {
        this.questionAnswerChkbx = questionAnswerChkbx;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getQuestionTypeDescription() {
        return questionTypeDescription;
    }

    public void setQuestionTypeDescription(String questionTypeDescription) {
        this.questionTypeDescription = questionTypeDescription;
    }
}