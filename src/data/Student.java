package data;

public class Student {

    private String studentUsername;
    private String studentEmail;
    private String studentName;
    private String studentSurname;
    private String studentPassword;

    public Student(String studentUsername, String studentEmail, String studentName, String studentSurname, String studentPassword) {
        this.studentUsername = studentUsername;
        this.studentEmail = studentEmail;
        this.studentName = studentName;
        this.studentSurname = studentSurname;
        this.studentPassword = studentPassword;
    }

    public String getStudentUsername() {
        return studentUsername;
    }

    public void setStudentUsername(String studentUsername) {
        this.studentUsername = studentUsername;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentSurname() {
        return studentSurname;
    }

    public void setStudentSurname(String studentSurname) {
        this.studentSurname = studentSurname;
    }

    public String getStudentPassword() {
        return studentPassword;
    }

    public void setStudentPassword(String studentPassword) {
        this.studentPassword = studentPassword;
    }
}
