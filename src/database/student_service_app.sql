-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: student_service_app
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrators`
--

DROP TABLE IF EXISTS `administrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `administrators` (
  `administrator_username` varchar(50) NOT NULL,
  `administrator_email` varchar(90) NOT NULL,
  `administrator_password` varchar(128) NOT NULL,
  `administrator_name` varchar(50) NOT NULL,
  `administrator_surname` varchar(50) NOT NULL,
  `users_user_username` varchar(50) NOT NULL,
  PRIMARY KEY (`administrator_username`,`users_user_username`),
  UNIQUE KEY `administrator_username_UNIQUE` (`administrator_username`),
  UNIQUE KEY `administrator_email_UNIQUE` (`administrator_email`),
  KEY `fk_administrators_users1_idx` (`users_user_username`),
  CONSTRAINT `fk_administrators_users1` FOREIGN KEY (`users_user_username`) REFERENCES `users` (`user_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrators`
--

LOCK TABLES `administrators` WRITE;
/*!40000 ALTER TABLE `administrators` DISABLE KEYS */;
/*!40000 ALTER TABLE `administrators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `answer_1` varchar(150) DEFAULT NULL,
  `answer_2` varchar(150) DEFAULT NULL,
  `answer_3` varchar(150) DEFAULT NULL,
  `answer_4` varchar(150) DEFAULT NULL,
  `answer_5` varchar(150) DEFAULT NULL,
  `answers_correct` varchar(5) DEFAULT NULL,
  `questions_question_id` int(11) NOT NULL,
  PRIMARY KEY (`questions_question_id`),
  UNIQUE KEY `answer_1_UNIQUE` (`answer_1`),
  UNIQUE KEY `answer_2_UNIQUE` (`answer_2`),
  UNIQUE KEY `answer_3_UNIQUE` (`answer_3`),
  UNIQUE KEY `answer_4_UNIQUE` (`answer_4`),
  UNIQUE KEY `answer_5_UNIQUE` (`answer_5`),
  CONSTRAINT `fk_answers_questions1` FOREIGN KEY (`questions_question_id`) REFERENCES `questions` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_types`
--

DROP TABLE IF EXISTS `question_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_types` (
  `question_type_id` int(11) NOT NULL,
  `question_type_description` varchar(45) NOT NULL,
  PRIMARY KEY (`question_type_id`),
  UNIQUE KEY `question_type_id_UNIQUE` (`question_type_id`),
  UNIQUE KEY `question_type_description_UNIQUE` (`question_type_description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_types`
--

LOCK TABLES `question_types` WRITE;
/*!40000 ALTER TABLE `question_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `question_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires`
--

DROP TABLE IF EXISTS `questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires` (
  `questionnaire_id` int(11) NOT NULL,
  `questionnaire_title` varchar(150) NOT NULL,
  PRIMARY KEY (`questionnaire_id`),
  UNIQUE KEY `questionnaire_title_UNIQUE` (`questionnaire_title`),
  UNIQUE KEY `questionnaire_id_UNIQUE` (`questionnaire_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires`
--

LOCK TABLES `questionnaires` WRITE;
/*!40000 ALTER TABLE `questionnaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires_questions`
--

DROP TABLE IF EXISTS `questionnaires_questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires_questions` (
  `questionnaires_questionnaire_id` int(11) NOT NULL,
  `questions_question_id` int(11) NOT NULL,
  PRIMARY KEY (`questionnaires_questionnaire_id`,`questions_question_id`),
  KEY `fk_questionnaires_questions_questions1_idx` (`questions_question_id`),
  KEY `fk_questionnaires_questions_questionnaires1_idx` (`questionnaires_questionnaire_id`),
  CONSTRAINT `fk_questionnaires_questions_questionnaires1` FOREIGN KEY (`questionnaires_questionnaire_id`) REFERENCES `questionnaires` (`questionnaire_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_questionnaires_questions_questions1` FOREIGN KEY (`questions_question_id`) REFERENCES `questions` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires_questions`
--

LOCK TABLES `questionnaires_questions` WRITE;
/*!40000 ALTER TABLE `questionnaires_questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questionnaires_questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `question_text` text NOT NULL,
  `question_types_question_type_id` int(11) NOT NULL,
  PRIMARY KEY (`question_id`,`question_types_question_type_id`),
  UNIQUE KEY `question_id_UNIQUE` (`question_id`),
  KEY `fk_questions_question_types1` (`question_types_question_type_id`),
  CONSTRAINT `fk_questions_question_types1` FOREIGN KEY (`question_types_question_type_id`) REFERENCES `question_types` (`question_type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions_tests`
--

DROP TABLE IF EXISTS `questions_tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions_tests` (
  `questions_question_id` int(11) NOT NULL,
  `tests_test_id` int(11) NOT NULL,
  PRIMARY KEY (`questions_question_id`,`tests_test_id`),
  KEY `fk_questions_tests_tests1_idx` (`tests_test_id`),
  KEY `fk_questions_tests_questions1_idx` (`questions_question_id`),
  CONSTRAINT `fk_questions_tests_questions1` FOREIGN KEY (`questions_question_id`) REFERENCES `questions` (`question_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_questions_tests_tests1` FOREIGN KEY (`tests_test_id`) REFERENCES `tests` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions_tests`
--

LOCK TABLES `questions_tests` WRITE;
/*!40000 ALTER TABLE `questions_tests` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions_tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_username` varchar(50) NOT NULL,
  `student_email` varchar(90) NOT NULL,
  `student_password` varchar(128) NOT NULL,
  `student_name` varchar(50) NOT NULL,
  `student_surname` varchar(50) NOT NULL,
  `users_user_username` varchar(50) NOT NULL,
  PRIMARY KEY (`student_id`,`users_user_username`),
  UNIQUE KEY `student_username_UNIQUE` (`student_username`),
  UNIQUE KEY `student_email_UNIQUE` (`student_email`),
  KEY `fk_students_users1_idx` (`users_user_username`),
  CONSTRAINT `fk_students_users1` FOREIGN KEY (`users_user_username`) REFERENCES `users` (`user_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_subjects`
--

DROP TABLE IF EXISTS `students_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_subjects` (
  `students_student_id` int(11) NOT NULL,
  `subjects_subject_id` int(11) NOT NULL,
  PRIMARY KEY (`students_student_id`,`subjects_subject_id`),
  KEY `fk_students_subjects_subjects1_idx` (`subjects_subject_id`),
  KEY `fk_students_subjects_students1_idx` (`students_student_id`),
  CONSTRAINT `fk_students_subjects_students1` FOREIGN KEY (`students_student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_students_subjects_subjects1` FOREIGN KEY (`subjects_subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_subjects`
--

LOCK TABLES `students_subjects` WRITE;
/*!40000 ALTER TABLE `students_subjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `students_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_subjects_questionnaires`
--

DROP TABLE IF EXISTS `students_subjects_questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_subjects_questionnaires` (
  `students_student_id` int(11) NOT NULL,
  `subjects_questionnaires_subjects_subject_id` int(11) NOT NULL,
  `subjects_questionnaires_questionnaires_questionnaire_id` int(11) NOT NULL,
  PRIMARY KEY (`students_student_id`,`subjects_questionnaires_subjects_subject_id`,`subjects_questionnaires_questionnaires_questionnaire_id`),
  KEY `fk_students_subjects_questionnaires_subjects_questionnaires_idx` (`subjects_questionnaires_subjects_subject_id`,`subjects_questionnaires_questionnaires_questionnaire_id`),
  KEY `fk_students_subjects_questionnaires_students1_idx` (`students_student_id`),
  CONSTRAINT `fk_students_subjects_questionnaires_students1` FOREIGN KEY (`students_student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_students_subjects_questionnaires_subjects_questionnaires1` FOREIGN KEY (`subjects_questionnaires_subjects_subject_id`, `subjects_questionnaires_questionnaires_questionnaire_id`) REFERENCES `subjects_questionnaires` (`subjects_subject_id`, `questionnaires_questionnaire_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_subjects_questionnaires`
--

LOCK TABLES `students_subjects_questionnaires` WRITE;
/*!40000 ALTER TABLE `students_subjects_questionnaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `students_subjects_questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_subjects_questionnaires_answers`
--

DROP TABLE IF EXISTS `students_subjects_questionnaires_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_subjects_questionnaires_answers` (
  `students_subjects_questionnaires_answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `students_subjects_questionnaires_answers` varchar(5) DEFAULT NULL,
  `students_subjects_questionnaires_answer_text` text,
  `students_subjects_questionnaires_students_student_id` int(11) NOT NULL,
  `students_subjects_questionnaires_subjects_questionnaires_id` int(11) NOT NULL,
  `students_subjects_questionnaires_subjects_questionnaires_2_id` int(11) NOT NULL,
  PRIMARY KEY (`students_subjects_questionnaires_answer_id`,`students_subjects_questionnaires_students_student_id`,`students_subjects_questionnaires_subjects_questionnaires_id`,`students_subjects_questionnaires_subjects_questionnaires_2_id`),
  UNIQUE KEY `students_teachers_questionnaires_answer_id_UNIQUE` (`students_subjects_questionnaires_answer_id`),
  KEY `fk_students_subjects_questionnaires_answers_students_subjec_idx` (`students_subjects_questionnaires_students_student_id`,`students_subjects_questionnaires_subjects_questionnaires_id`,`students_subjects_questionnaires_subjects_questionnaires_2_id`),
  CONSTRAINT `fk_students_subjects_questionnaires_answers_students_subjects1` FOREIGN KEY (`students_subjects_questionnaires_students_student_id`, `students_subjects_questionnaires_subjects_questionnaires_id`, `students_subjects_questionnaires_subjects_questionnaires_2_id`) REFERENCES `students_subjects_questionnaires` (`students_student_id`, `subjects_questionnaires_subjects_subject_id`, `subjects_questionnaires_questionnaires_questionnaire_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_subjects_questionnaires_answers`
--

LOCK TABLES `students_subjects_questionnaires_answers` WRITE;
/*!40000 ALTER TABLE `students_subjects_questionnaires_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `students_subjects_questionnaires_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_subjects_tests`
--

DROP TABLE IF EXISTS `students_subjects_tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_subjects_tests` (
  `students_student_id` int(11) NOT NULL,
  `subjects_tests_subjects_subject_id` int(11) NOT NULL,
  `subjects_tests_tests_test_id` int(11) NOT NULL,
  PRIMARY KEY (`students_student_id`,`subjects_tests_subjects_subject_id`,`subjects_tests_tests_test_id`),
  KEY `fk_students_subjects_tests_subjects_tests1_idx` (`subjects_tests_subjects_subject_id`,`subjects_tests_tests_test_id`),
  KEY `fk_students_subjects_tests_students1_idx` (`students_student_id`),
  CONSTRAINT `fk_students_subjects_tests_students1` FOREIGN KEY (`students_student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_students_subjects_tests_subjects_tests1` FOREIGN KEY (`subjects_tests_subjects_subject_id`, `subjects_tests_tests_test_id`) REFERENCES `subjects_tests` (`subjects_subject_id`, `tests_test_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_subjects_tests`
--

LOCK TABLES `students_subjects_tests` WRITE;
/*!40000 ALTER TABLE `students_subjects_tests` DISABLE KEYS */;
/*!40000 ALTER TABLE `students_subjects_tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_subjects_tests_answers`
--

DROP TABLE IF EXISTS `students_subjects_tests_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_subjects_tests_answers` (
  `students_subjects_tests_answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `students_subjects_tests_answers` varchar(5) DEFAULT NULL,
  `students_subjects_tests_answer_text` text,
  `students_subjects_tests_students_student_id` int(11) NOT NULL,
  `students_subjects_tests_subjects_tests_subjects_subject_id` int(11) NOT NULL,
  `students_subjects_tests_subjects_tests_tests_test_id` int(11) NOT NULL,
  PRIMARY KEY (`students_subjects_tests_answer_id`,`students_subjects_tests_students_student_id`,`students_subjects_tests_subjects_tests_subjects_subject_id`,`students_subjects_tests_subjects_tests_tests_test_id`),
  UNIQUE KEY `students_teachers_questionnaires_answer_id_UNIQUE` (`students_subjects_tests_answer_id`),
  KEY `fk_students_subjects_tests_answers_students_subjects_tests1_idx` (`students_subjects_tests_students_student_id`,`students_subjects_tests_subjects_tests_subjects_subject_id`,`students_subjects_tests_subjects_tests_tests_test_id`),
  CONSTRAINT `fk_students_subjects_tests_answers_students_subjects_tests1` FOREIGN KEY (`students_subjects_tests_students_student_id`, `students_subjects_tests_subjects_tests_subjects_subject_id`, `students_subjects_tests_subjects_tests_tests_test_id`) REFERENCES `students_subjects_tests` (`students_student_id`, `subjects_tests_subjects_subject_id`, `subjects_tests_tests_test_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_subjects_tests_answers`
--

LOCK TABLES `students_subjects_tests_answers` WRITE;
/*!40000 ALTER TABLE `students_subjects_tests_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `students_subjects_tests_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_teachers_questionnaires`
--

DROP TABLE IF EXISTS `students_teachers_questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_teachers_questionnaires` (
  `students_student_id` int(11) NOT NULL,
  `teachers_questionnaires_teachers_teacher_id` int(11) NOT NULL,
  `teachers_questionnaires_questionnaires_questionnaire_id` int(11) NOT NULL,
  PRIMARY KEY (`students_student_id`,`teachers_questionnaires_teachers_teacher_id`,`teachers_questionnaires_questionnaires_questionnaire_id`),
  KEY `fk_students_teachers_questionnaires_teachers_questionnaires_idx` (`teachers_questionnaires_teachers_teacher_id`,`teachers_questionnaires_questionnaires_questionnaire_id`),
  KEY `fk_students_teachers_questionnaires_students1_idx` (`students_student_id`),
  CONSTRAINT `fk_students_teachers_questionnaires_students1` FOREIGN KEY (`students_student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_students_teachers_questionnaires_teachers_questionnaires1` FOREIGN KEY (`teachers_questionnaires_teachers_teacher_id`, `teachers_questionnaires_questionnaires_questionnaire_id`) REFERENCES `teachers_questionnaires` (`teachers_teacher_id`, `questionnaires_questionnaire_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_teachers_questionnaires`
--

LOCK TABLES `students_teachers_questionnaires` WRITE;
/*!40000 ALTER TABLE `students_teachers_questionnaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `students_teachers_questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students_teachers_questionnaires_answers`
--

DROP TABLE IF EXISTS `students_teachers_questionnaires_answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students_teachers_questionnaires_answers` (
  `students_teachers_questionnaires_answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `students_teachers_questionnaires_answers` varchar(5) DEFAULT NULL,
  `students_teachers_questionnaires_answer_text` text,
  `students_teachers_questionnaires_students_student_id` int(11) NOT NULL,
  `students_teachers_questionnaires_teachers_questionnaires_id` int(11) NOT NULL,
  `students_teachers_questionnaires_teachers_questionnaires_2_id` int(11) NOT NULL,
  PRIMARY KEY (`students_teachers_questionnaires_answer_id`,`students_teachers_questionnaires_students_student_id`,`students_teachers_questionnaires_teachers_questionnaires_id`,`students_teachers_questionnaires_teachers_questionnaires_2_id`),
  UNIQUE KEY `students_teachers_questionnaires_answer_id_UNIQUE` (`students_teachers_questionnaires_answer_id`),
  KEY `fk_students_teachers_questionnaires_answers_1` (`students_teachers_questionnaires_students_student_id`,`students_teachers_questionnaires_teachers_questionnaires_id`,`students_teachers_questionnaires_teachers_questionnaires_2_id`),
  CONSTRAINT `fk_students_teachers_questionnaires_answers_1` FOREIGN KEY (`students_teachers_questionnaires_students_student_id`, `students_teachers_questionnaires_teachers_questionnaires_id`, `students_teachers_questionnaires_teachers_questionnaires_2_id`) REFERENCES `students_teachers_questionnaires` (`students_student_id`, `teachers_questionnaires_teachers_teacher_id`, `teachers_questionnaires_questionnaires_questionnaire_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students_teachers_questionnaires_answers`
--

LOCK TABLES `students_teachers_questionnaires_answers` WRITE;
/*!40000 ALTER TABLE `students_teachers_questionnaires_answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `students_teachers_questionnaires_answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects` (
  `subject_id` int(11) NOT NULL,
  `subject_title` varchar(150) NOT NULL,
  PRIMARY KEY (`subject_id`),
  UNIQUE KEY `subject_title_UNIQUE` (`subject_title`),
  UNIQUE KEY `subject_id_UNIQUE` (`subject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects`
--

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects_questionnaires`
--

DROP TABLE IF EXISTS `subjects_questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects_questionnaires` (
  `subjects_subject_id` int(11) NOT NULL,
  `questionnaires_questionnaire_id` int(11) NOT NULL,
  PRIMARY KEY (`subjects_subject_id`,`questionnaires_questionnaire_id`),
  KEY `fk_subjects_questionnaires_questionnaires1_idx` (`questionnaires_questionnaire_id`),
  KEY `fk_subjects_questionnaires_subjects1_idx` (`subjects_subject_id`),
  CONSTRAINT `fk_subjects_questionnaires_questionnaires1` FOREIGN KEY (`questionnaires_questionnaire_id`) REFERENCES `questionnaires` (`questionnaire_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_subjects_questionnaires_subjects1` FOREIGN KEY (`subjects_subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects_questionnaires`
--

LOCK TABLES `subjects_questionnaires` WRITE;
/*!40000 ALTER TABLE `subjects_questionnaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `subjects_questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subjects_tests`
--

DROP TABLE IF EXISTS `subjects_tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subjects_tests` (
  `subjects_subject_id` int(11) NOT NULL,
  `tests_test_id` int(11) NOT NULL,
  PRIMARY KEY (`subjects_subject_id`,`tests_test_id`),
  KEY `fk_subjects_tests_tests1_idx` (`tests_test_id`),
  KEY `fk_subjects_tests_subjects1_idx` (`subjects_subject_id`),
  CONSTRAINT `fk_subjects_tests_subjects1` FOREIGN KEY (`subjects_subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_subjects_tests_tests1` FOREIGN KEY (`tests_test_id`) REFERENCES `tests` (`test_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subjects_tests`
--

LOCK TABLES `subjects_tests` WRITE;
/*!40000 ALTER TABLE `subjects_tests` DISABLE KEYS */;
/*!40000 ALTER TABLE `subjects_tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `superadministrators`
--

DROP TABLE IF EXISTS `superadministrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `superadministrators` (
  `superadministrator_username` varchar(50) NOT NULL,
  `superadministrator_email` varchar(90) NOT NULL,
  `superadministrator_password` varchar(128) NOT NULL,
  `superadministrator_name` varchar(50) NOT NULL,
  `superadministrator_surname` varchar(50) NOT NULL,
  `users_user_username` varchar(50) NOT NULL,
  PRIMARY KEY (`superadministrator_username`,`users_user_username`),
  UNIQUE KEY `superadministrators_username_UNIQUE` (`superadministrator_username`),
  UNIQUE KEY `superadministrator_email_UNIQUE` (`superadministrator_email`),
  KEY `fk_superadministrators_users1_idx` (`users_user_username`),
  CONSTRAINT `fk_superadministrators_users1` FOREIGN KEY (`users_user_username`) REFERENCES `users` (`user_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `superadministrators`
--

LOCK TABLES `superadministrators` WRITE;
/*!40000 ALTER TABLE `superadministrators` DISABLE KEYS */;
INSERT INTO `superadministrators` VALUES ('superadmin','milanmvujanic@gmail.com','superadmin','Milan','Vujanic','superadmin');
/*!40000 ALTER TABLE `superadministrators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher_titles`
--

DROP TABLE IF EXISTS `teacher_titles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_titles` (
  `teacher_title_id` int(11) NOT NULL,
  `teacher_title_description` varchar(45) NOT NULL,
  PRIMARY KEY (`teacher_title_id`),
  UNIQUE KEY `teacher_title_id_UNIQUE` (`teacher_title_id`),
  UNIQUE KEY `teacher_title_description_UNIQUE` (`teacher_title_description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher_titles`
--

LOCK TABLES `teacher_titles` WRITE;
/*!40000 ALTER TABLE `teacher_titles` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher_titles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers`
--

DROP TABLE IF EXISTS `teachers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers` (
  `teacher_id` int(11) NOT NULL,
  `teacher_username` varchar(50) NOT NULL,
  `teacher_email` varchar(90) NOT NULL,
  `teacher_password` varchar(128) NOT NULL,
  `teacher_name` varchar(50) NOT NULL,
  `teacher_surname` varchar(50) NOT NULL,
  `teacher_titles_teacher_title_id` int(11) NOT NULL,
  `users_user_username` varchar(50) NOT NULL,
  PRIMARY KEY (`teacher_id`,`teacher_titles_teacher_title_id`,`users_user_username`),
  UNIQUE KEY `teacher_username_UNIQUE` (`teacher_username`),
  UNIQUE KEY `teacher_name_UNIQUE` (`teacher_email`),
  UNIQUE KEY `teacher_id_UNIQUE` (`teacher_id`),
  KEY `fk_teachers_teacher_titles1_idx` (`teacher_titles_teacher_title_id`),
  KEY `fk_teachers_users1_idx` (`users_user_username`),
  CONSTRAINT `fk_teachers_teacher_titles1` FOREIGN KEY (`teacher_titles_teacher_title_id`) REFERENCES `teacher_titles` (`teacher_title_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_teachers_users1` FOREIGN KEY (`users_user_username`) REFERENCES `users` (`user_username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers`
--

LOCK TABLES `teachers` WRITE;
/*!40000 ALTER TABLE `teachers` DISABLE KEYS */;
/*!40000 ALTER TABLE `teachers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers_questionnaires`
--

DROP TABLE IF EXISTS `teachers_questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers_questionnaires` (
  `teachers_teacher_id` int(11) NOT NULL,
  `questionnaires_questionnaire_id` int(11) NOT NULL,
  PRIMARY KEY (`teachers_teacher_id`,`questionnaires_questionnaire_id`),
  KEY `fk_teachers_questionnaires_questionnaires1_idx` (`questionnaires_questionnaire_id`),
  KEY `fk_teachers_questionnaires_teachers1_idx` (`teachers_teacher_id`),
  CONSTRAINT `fk_teachers_questionnaires_questionnaires1` FOREIGN KEY (`questionnaires_questionnaire_id`) REFERENCES `questionnaires` (`questionnaire_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_teachers_questionnaires_teachers1` FOREIGN KEY (`teachers_teacher_id`) REFERENCES `teachers` (`teacher_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers_questionnaires`
--

LOCK TABLES `teachers_questionnaires` WRITE;
/*!40000 ALTER TABLE `teachers_questionnaires` DISABLE KEYS */;
/*!40000 ALTER TABLE `teachers_questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers_students`
--

DROP TABLE IF EXISTS `teachers_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers_students` (
  `teachers_teacher_id` int(11) NOT NULL,
  `students_student_id` int(11) NOT NULL,
  PRIMARY KEY (`teachers_teacher_id`,`students_student_id`),
  KEY `fk_teachers_students_students1_idx` (`students_student_id`),
  KEY `fk_teachers_students_teachers1_idx` (`teachers_teacher_id`),
  CONSTRAINT `fk_teachers_students_students1` FOREIGN KEY (`students_student_id`) REFERENCES `students` (`student_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_teachers_students_teachers1` FOREIGN KEY (`teachers_teacher_id`) REFERENCES `teachers` (`teacher_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers_students`
--

LOCK TABLES `teachers_students` WRITE;
/*!40000 ALTER TABLE `teachers_students` DISABLE KEYS */;
/*!40000 ALTER TABLE `teachers_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teachers_subjects`
--

DROP TABLE IF EXISTS `teachers_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teachers_subjects` (
  `teachers_teacher_id` int(11) NOT NULL,
  `subjects_subject_id` int(11) NOT NULL,
  PRIMARY KEY (`teachers_teacher_id`,`subjects_subject_id`),
  KEY `fk_teachers_subjects_subjects1_idx` (`subjects_subject_id`),
  KEY `fk_teachers_subjects_teachers1_idx` (`teachers_teacher_id`),
  CONSTRAINT `fk_teachers_subjects_subjects1` FOREIGN KEY (`subjects_subject_id`) REFERENCES `subjects` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_teachers_subjects_teachers1` FOREIGN KEY (`teachers_teacher_id`) REFERENCES `teachers` (`teacher_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teachers_subjects`
--

LOCK TABLES `teachers_subjects` WRITE;
/*!40000 ALTER TABLE `teachers_subjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `teachers_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tests`
--

DROP TABLE IF EXISTS `tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tests` (
  `test_id` int(11) NOT NULL,
  `test_title` varchar(150) NOT NULL,
  PRIMARY KEY (`test_id`),
  UNIQUE KEY `test_title_UNIQUE` (`test_title`),
  UNIQUE KEY `test_id_UNIQUE` (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tests`
--

LOCK TABLES `tests` WRITE;
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_privileges`
--

DROP TABLE IF EXISTS `user_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_privileges` (
  `user_privilege_id` int(11) NOT NULL,
  `user_privilege_description` varchar(45) NOT NULL,
  PRIMARY KEY (`user_privilege_id`),
  UNIQUE KEY `user_privilege_description_UNIQUE` (`user_privilege_description`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_privileges`
--

LOCK TABLES `user_privileges` WRITE;
/*!40000 ALTER TABLE `user_privileges` DISABLE KEYS */;
INSERT INTO `user_privileges` VALUES (2,'administrator'),(4,'student'),(1,'superadministrator'),(3,'teacher');
/*!40000 ALTER TABLE `user_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_username` varchar(50) NOT NULL,
  `user_email` varchar(90) NOT NULL,
  `user_password` varchar(128) NOT NULL,
  `user_privileges_user_privilege_id` int(11) NOT NULL,
  PRIMARY KEY (`user_username`,`user_privileges_user_privilege_id`),
  UNIQUE KEY `user_username_UNIQUE` (`user_username`),
  UNIQUE KEY `user_email_UNIQUE` (`user_email`),
  UNIQUE KEY `user_password_UNIQUE` (`user_password`),
  KEY `fk_users_user_privileges_idx` (`user_privileges_user_privilege_id`),
  CONSTRAINT `fk_users_user_privileges` FOREIGN KEY (`user_privileges_user_privilege_id`) REFERENCES `user_privileges` (`user_privilege_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('superadmin','milanmvujanic@gmail.com','superadmin',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-17 12:00:17
