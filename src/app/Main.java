package app;

import controller.Controller;
import frames.LoginFrame;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {

        Controller.getInstance().setPrimaryStage(primaryStage);

        LoginFrame loginFrame = new LoginFrame();
        Controller.getInstance().setLoginFrame(loginFrame);

        Scene scene = new Scene(loginFrame, 900, 815);
        Controller.getInstance().setSceneLogin(scene);

        primaryStage.setScene(Controller.getInstance().getSceneLogin());
        primaryStage.setTitle("Login");
        primaryStage.show();
    }

    public static void main(String[] args) {
	    launch(args);
    }
}
